You can use "Template":'<Query>' as predefined:<DisplayName> or predefined:<ID> in GEODI queries.

DisplayName provides dual language support.

GEODI standard query rules are supported.

A meta predefined can use another meta defined in the same folder. For example Template:'Layer:date and predefined:<ID>'