﻿Birden fazla tanımı yapılabilen ILoginProvider gerçeklemeleri için tanım klasörüdür.
Örnek olarak Oauth2/OpenID için birkaç ayar yazılarak Facebook ile, google ile giriş yapma sağlanabilir.

ayar bir jSettings dosyasına atılmalıdır.
/api adresinden yararlanarak tüm içerik kriptolanabilir. 

{
	__type: "Factory.LoginProviderFatory:OAuth2LoginProvider",
	ProviderName:"Google",
	AuthorizationURL:"https://accounts.google.com/o/oauth2/auth",
	AccessTokenURL:"https://accounts.google.com/o/oauth2/token",
	UserinfoURL:"https://openidconnect.googleapis.com/v1/userinfo?access_token={TOKEN}",
	ClientID : ".....",
	ClientSecret : "......",
	RedirectURL : "https://www.dece.com.tr/redirservice",
	Scope:"openid profile email",
	IconName:"share/google-32",
	DisplayName:"Google ile oturum aç"
}

intranet sample

{
	__type: "Factory.LoginProviderFatory:OAuth2LoginProvider",
	ProviderName:"MyIntranetAppName",
	AuthorizationURL:"...",
	AccessTokenURL:"...",
	UserinfoURL:"...",
	ClientID : "....",
	ClientSecret : "....",
	Scope:"openid profile email",
	DisplayName:"... ile oturum aç"
}