﻿Bu konumdaki tanımlar değiştirilmemeli, sadece yeni tanım eklenmelidir.
Değişen tanımlar yeniden okunamayacak indekslere neden olacaktır.
Büyük küçük harf ayrımı yapılır.
ID değerleri 65536'dan küçük ve benzersiz olmalıdır.