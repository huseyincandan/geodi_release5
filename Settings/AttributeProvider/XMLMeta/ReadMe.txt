﻿eklenecek .jSettings uzantılı dosyalar dikkate alınır. Dosyalar xml olarak okunup ilgili dosyanın özniteliği olarak kabul edilir.
Değişiklik yapıldığında veya yeni tanım dosyası eklendiğinde uygulamanın yeniden başlatılması gerekmektedir.

Örnek Tanım dosyası içeriği : 

{
      TargetExtension:".tif",
      MainFileFormat:"[FILE][EXT].meta.xml",
	  IgnoreMetaFile:".meta.xml",
      GroupElement:"metadata",
      KeyElement:"META_NAME",
      ValueElement:"META_VALUE",
	  AreaElement:"META_AREA"
}



Bu içerikde *.meta.xml uzantılı dosyalar *.tif dosyaları ile ilişkilendiriliyor. xml dosya içeriklerinin aşağıdaki gibi olması bekleniyor.
AreaElement değeri Header veya Content olabilir. Content summary içeirğinde de görülür. Varsayılan değeri Header'dır.

<...>
	<metadata>
		<META_NAME>Kolon</META_NAME>
		<META_VALUE>Değer</META_VALUE>
	</metadata>
	<metadata>
		<META_NAME>Kolon</META_NAME>
		<META_VALUE>Değer</META_VALUE>
	</metadata>
	<metadata>
		<META_NAME>Kolon</META_NAME>
		<META_VALUE>Değer</META_VALUE>
		<META_AREA>Content</META_AREA>
	</metadata>
	....
</...>

KeyElement ile alınan Projection ifadesi her zaman projeksiyon olarak kabul edilir. EPSG:.. veya WKT olabilir.

Tanımlar :
	TargetExtension: Hede dosya uzantısı. Opsiyonel. Tanımlanmazsa tüm dosyalar ile isim eşleşmesi bağlantı kontrol edilir.
	MainFileFormat: Dosyalarla isim eşleşmesinn nasıl kontrol edilecğeini belirler. [FILE] dosya adını [EXT] uzantıyı gösterir.
	IgnoreMetaFile:Meta dosyalarının ihmal edilmesini sağlar. Opsiyonel. Boş bırakılırsa meta dosyası'da GEODI tarafından ayrıca indexlenir.
	GroupElement:XML üzerindeki grup node adı.
	KeyElement: Grup node altındaki Key node. Opsiyoneldir.
	ValueElement:Grup node altındaki Value node. 
	AreaElement:Grup node altındaki Alan tipi değeri. Indexteki konumunu belirler. Content değeri kullanılırsa özetlerde görüntülenir.
	

META_NAME alanı için ön tanımlı değerler
projection : META_VALUE alanına WKT veya EPSG cinsinden projeksiyon yazılmalıdır.
geom : META_VALUE alanına WKT geometri yazılmalıdır.
previewbase64:base64 imaj atılarak önizleme resmi zorlanabilir.
forcedsummary:tüm aramalarda görüntülenecek summary önizlemesi zorlanabilir.
forcedviewurl:viewer(içeriği açmak) için kullanılacak adres ayarlanabilir.
permit:Yetkili kullanıcı listesidir. kullanıcı id, group id, sağlayıcı grup id listesi içerebilir. Aktif kullanıcıya ait bilgiler /api adresinden alınabilir. Örnek : geodi:deneme,ldap\...,UserGroup_a1224,S-111-22155
deny:Yasaklı kullanıcı listesidir. kullanıcı id, group id, sağlayıcı grup id listesi içerebilir. Aktif kullanıcıya ait bilgiler /api adresinden alınabilir. Örnek : geodi:deneme,ldap\...,UserGroup_a1224,S-111-22155