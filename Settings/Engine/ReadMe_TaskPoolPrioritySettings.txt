﻿Level_0 en yavaş Level_4 en hızlı seçenek için ayarları ifade eder.
TaskPoolPrioritySettings.json.sample dosyası varsayılan ayarları içerir. .sample uzantısı kaldırlıp ayarlar değiştirilebilir. 
GEODI'nin yeniden başlatılması gerekir.


Level Ayarları
        Level_n: {
            ThreadPriority : 0, //Thread hızı 0 -4 arası, 0 Lowest, 4 Highest
            MaxTaskCount : 2, // Eşzamanlı kaç dosya taranabilir, 0 İşlemci durumuna göre otomaik değer alır.
            AutoGCWaitCount : 4, //Kaç dosyada bir bellek boşaltma çalışması yapılmalı
            AutoGCTimeout : 4000, //Bellek boşaltma için timeout süresi. Garbage Collector bellek boşaltımı için en fazla ne kadar zaman ayrılmalı.
            AutoSleepAllRunTime : 500 //Her dosya için fazladan yavaşlatma süresi. Bu ayarda her dosya için 500 ms işlemci boşta bırakılıyor.
        }
