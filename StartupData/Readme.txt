﻿GeodiAppData isimli klasörler aynı iç klasör düzeni ile AppData altına uygulama klasörüne atılır.
GeodiAppData ile başlaması şartı ile GeodiAppData, GeodiAppData2, GeodiAppData_v2 gibi istenilen isim kullanılabilir.
Alt klasör içerisinde bir DataVersion.txt bulunmalıdır. DataVersion.txt içeriği değişmediği sürece AppData altına dosyalar yeniden atılmaz.


AppData isimli klasörlerde aynı şekilde davranır. bilgileri %appdata%dece adresine kopyalar.

DataVersion.txt içeriğine Dece.IO.CopyToolsSimpleConditions JSON içeriği yazılarak kopyalama koşulları eklenebilir.
Örnek :
{
  Version:"1.0",
  DefaultOverwrite:true,
  DefaultPattern:"*.*",
  IgnoreOverwrite:["*.storage"]
}
