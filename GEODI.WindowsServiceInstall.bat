@ECHO It should be run as a manager. Installed message not received is not installed.
@ECHO AppDataFolder should be specified in systemsettings.json.
@ECHO Geodi.exe firewall permissions must be defined.
@cd /d "%~dp0"
@PAUSE
start /wait GEODI.exe InstallWindowsService
sc config "GEODI.WindowsService" start= auto