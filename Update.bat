@echo off
set br="%~dp0"
set upd=%br%\Updater-temp.bat
del /q %upd%

rem Checks for updates

>> %upd% echo set br="%~dp0"
>> %upd% echo %%br%%updater.exe /justcheck
>> %upd% echo if %%errorlevel%% EQU 0 (
>> %upd% echo set /A geodiupdate=1 
>> %upd% echo ) ELSE ( 
>> %upd% echo    set /A geodiupdate=0 
>> %upd% echo )
>> %upd% echo %%br%%geodi.console.exe HasUpdateModules noui ^> %%TEMP%%\gl.txt
>> %upd% echo SET /p moduleupdate= ^< %%TEMP%%\gl.txt
>> %upd% echo del %%TEMP%%\gl.txt

>> %upd% echo set /A updaterequired=%%moduleupdate%%+%%geodiupdate%%
>> %upd% echo if %%updaterequired%% EQU 0 ( exit )

rem go-on

>> %upd% echo sc stop GEODI.WindowsService

rem WaitForServiceStop.bat waits for stop the GEODI Windows Service

>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo REM Run your command line here - probably: NET STOP Service
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo REM Wait for Service Update service to stop
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo :LOOP
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo sc query %%%%1 ^^^| find "STOPPED" ^^^>nul 2^^^>^^^&1
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo IF ERRORLEVEL 1 (
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo Timeout /T 5 /Nobreak
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo GOTO LOOP
>> %upd% echo ^>^> %%br%%WaitForServiceStop.bat echo )

>> %upd% echo call %%br%%WaitForServiceStop.bat GEODI.WindowsService

rem WaitFor.bat waits finish the updater.exe in Task Manager

>> %upd% echo ^>^> %%br%%WaitFor.bat echo :WAIT
>> %upd% echo ^>^> %%br%%WaitFor.bat echo timeout /T 1
>> %upd% echo ^>^> %%br%%WaitFor.bat echo tasklist ^^^| find %%%%1
>> %upd% echo ^>^> %%br%%WaitFor.bat echo if %%%%errorlevel%%%% EQU 0 goto :WAIT

>> %upd% echo if %%geodiupdate%% EQU 1 (
>> %upd% echo %%br%%updater.exe /silentall
>> %upd% echo call %%br%%WaitFor.bat "updater.exe"
>> %upd% echo )

rem GEODI modules are update

>> %upd% echo if %%moduleupdate%% EQU 1 ( %%br%%geodi.console.exe UpdateModules noui )

>> %upd% echo sc start GEODI.WindowsService

%upd%
