﻿window.FavStatus = {
    None: 0,
    Favorite: 1,
    Watch: 2,
    Has: function (item, opt) {
        return (item & opt) == opt;
    }
};
function GetCurrentQueryFromGUI() {
    var newName = $("#txtDisplayName").val().trim();
    if (newName == '') {
        showMessage('[$notification:query_name_required/js]', '[$default:error/js]', 'danger');
        return;
    }

    var favStatusfv = FavStatus.None;
    if ($("#chkWatch").prop('checked'))
        favStatusfv |= FavStatus.Watch;

    if ($("#chkFavorite").prop('checked'))
        favStatusfv |= FavStatus.Favorite;




    var item = {
        Query: $("#txtQuery").val(),
        Facet: $("#txtFacet").val(),
        WsName: $("#txtWsName").val(),
        Description: $("#txtDescriptionForWatch").val(),
        DisplayName: newName,
        Fav_Status: favStatusfv,
        ID: $("#txtId").val(),
        Mode: $("#txtWatchGUIMode").val()
    };

    //IsPublic: $("#chkPublic").prop('checked') ? true : false,

    if (window.___prmWatch) {
        var prmlist = window.___prmWatch.SetPermissions();
        if (prmlist == null)
            item.IsPublic = 1;//herkese açık
        else if (prmlist.Permit.length == 1)
            item.IsPublic = 0;
        else {
            item.IsPublic = 2;
            item.Permit = prmlist.Permit;
        }
    }
    return item;
}

function SaveCurrentQuery() {
    var item = GetCurrentQueryFromGUI();
    if (item)
        $.post(
            HttpUtility.ResolveUrl('~/GeodiQueryRepostroryHandler?op=CreateOrUpdate', false, true),
            {
                'item': JSON.stringify(item)
            },
            function (data) {

                if (!data.Saved) {
                    $("#queryNotSavedInfo").fadeIn();
                    return;
                }
                $('#editQueryModal').modal('hide');
                refreshMyQuerylist();
            }, 'json'
        );
}


function btnSetFavStatus(obj, id, value) {
    obj = $(obj);
    var item = window.activeQueryList[id];
    var watching = obj.hasClass("isOk") ? true : false;
    watching = !watching;
    if (watching)
        value = item.Fav_Status | value;
    else
        value = item.Fav_Status & ~value;
    $.post(
        HttpUtility.ResolveUrl('~/GeodiQueryRepostroryHandler?op=SetFavStatus', false, true),
        {
            'query_id': id,
            'newStatus': value,
        },
        function (data) {
            if (watching)
                obj.addClass('isOk').removeClass('isnotOk');
            else
                obj.removeClass('isOk').addClass('isnotOk');

            item.Fav_Status = value;
            obj.attr("status", watching ? 1 : 0);
            //gui
        }, 'json'
    );
}
function btnDeleteQuery(id, obj) {
    var item = window.activeQueryList[id];
    showMessage("[$notification:delete_message/js]".replace("{0}", HttpUtility.HtmlEncode(item.DisplayName)), "[$default:warning/js]", null, true, true, false,
        function (result) {
            if (result == "yes") {
                var imgObj = $("span", $(obj));
                imgObj.attr("class", "glyphicon glyphicon-refresh glyphicon-refresh-animate");
                $.post(HttpUtility.ResolveUrl('~/GeodiQueryRepostroryHandler?op=Delete', false, true),
                    {
                        'query_id': id,
                    },
                    function (data) {
                        refreshMyQuerylist();
                    }, 'json'
                ).fail(function () {
                    imgObj.attr("class", "img-delete16 img-default16")
                });
            }
        });
}
function btnEditQuery(id) {
    var item = window.activeQueryList[id];
    $("#txtWsName").val(item.WsName);
    $("#txtWsName").attr("disabled", "disabled");
    $("#txtQuery").val(item.Query);
    $("#txtFacet").val(item.Facet);
    if (!item.Mode && window.___WatchGUIModeGet)
        item.Mode = window.___WatchGUIModeGet();
    $("#txtWatchGUIMode").val(item.Mode);

    __updateWatchReadonlyQuery();


    $("#txtDisplayName").val(item.DisplayName__NonLocalized ? item.DisplayName__NonLocalized : item.DisplayName);
    $("#txtDescriptionForWatch").val(item.Description__NonLocalized ? item.Description__NonLocalized : item.Description);
    if (!item.IsPublic && (!item.Permit || item.Permit.length == 0) && window.Auth && window.Auth.CurrentUser)
        item.Permit = [window.Auth.CurrentUser.ID.toLowerCase()];

    var lblobj = {
        Permissions: { Permit: item.Permit }
    };

    if (window.Auth)
        window.___prmWatch = new LabelPermissions(item.WsName, null, lblobj, true, "#editQueryModal");

    $("#chkWatch").prop('checked', FavStatus.Has(item.Fav_Status, FavStatus.Watch));
    $("#chkFavorite").prop('checked', FavStatus.Has(item.Fav_Status, FavStatus.Favorite));
    $("#txtId").val(item.ID);
    $("#queryNotSavedInfo").hide();
    $('#editQueryModal').modal({ backdrop: 'static', keyboard: false });
    $("#editHeader").html("[$default:edit/js]")
    $('#editQueryModal').modal('show');
    window.setTimeout(function () { $("#txtDisplayName").focus(); }, 500);
}
function updateWatchGUIFromCurrentQuery() {
    $("#txtWatchGUIMode").val(window.___WatchGUIModeGet ? window.___WatchGUIModeGet() : "");
    var query = GetEditingQuery();
    if (query) {
        $("#txtQuery").val(query.SearchString ? query.SearchString : "");
        $("#txtFacet").val(query.ToFacetStr());
    }
    __updateWatchReadonlyQuery();
}

function __updateWatchReadonlyQuery() {
    $("#txtQueryReadony").val(
        ($("#txtQuery").val() ? $("#txtQuery").val() + "; " : "") +
        ($("#txtFacet").val() ? $("#txtFacet").val() + "; " : "") +
        ($("#txtWatchGUIMode").val() ? $("#txtWatchGUIMode").val() + "; " : "")
    );
}

function btnCreateQuery() {
    $("#editHeader").html("[$default:save/js]")
    $("#queryNotSavedInfo").hide();
    $("#txtDisplayName").val('');
    $("#txtDescriptionForWatch").val('');

    $("#txtWatchGUIMode").val(window.___WatchGUIModeGet ? window.___WatchGUIModeGet() : "");

    var lblobj = {
        Permissions: { Permit: [] }
    };


    if (!window.GetEditingQuery) {
        window.___prmWatch = null;
        $("#txtId").val('');
        $('#editQueryModal').modal('show');
        $("#txtWsName").attr("disabled", null);
        $('#editQueryModal').modal('show');
        window.setTimeout(function () { $("#txtDisplayName").focus(); }, 500);
    }
    else {
        var query = GetEditingQuery();
        if (query) {
            $("#txtWsName").val(query.wsName);

            if (window.Auth)
                window.___prmWatch = new LabelPermissions(query.wsName, null, lblobj, true, "#editQueryModal");
            $("#txtQuery").val(query.SearchString ? query.SearchString : "");
            $("#txtFacet").val(query.ToFacetStr());

            $("#txtId").val('');
            $('#editQueryModal').modal('show');
            window.setTimeout(function () { $("#txtDisplayName").focus(); }, 500);

        }
    }
    __updateWatchReadonlyQuery();
}

function btnRunQuery(id) {
    if (window.__LastLoadedWatchList) {
        var qItem = null;
        $.each(window.__LastLoadedWatchList, function (k, v) { if (v.ID == id) qItem = v; })
        if (qItem && window.CurrentQueryContainer && window.CurrentQueryContainer.CurrentQuery) {
            window.SavedQueryLoading = true;
            var HandlerName = null;
            var TabParams = "";
            if (qItem.Mode) {
                HandlerName = qItem.Mode.split("\\");
                TabParams = HandlerName[1];
                HandlerName = HandlerName[0];
                if (!HandlerName) HandlerName = null;
            }
            var currentGMode = null;
            if (window.___WatchGUIModeGet) currentGMode = window.___WatchGUIModeGet().split('\\')[0];
            if (!currentGMode) currentGMode = null;
            if (!HandlerName) HandlerName = null;
            if (HandlerName == currentGMode) {
                $("#txtSearchDefault").val(qItem.Query != undefined ? qItem.Query : "");
                window.CurrentQueryContainer.CurrentQuery.SetFacetStr(qItem.Facet != undefined ? qItem.Facet : "");
                window.__LastCalledQuery = id;
                window.__LastCalledQueryObj = qItem;
                window.__LastCalledQueryTime = new Date();
                if (window.ChangeView && TabParams) {
                    TabParams = TabParams.split('&');
                    var tb, ts, bbox;
                    $.each(TabParams, function (v, k) {
                        if (k) {
                            if (k.indexOf("Tab=") == 0) tb = k;
                            if (k.indexOf("TS=") == 0) ts = k.replace("TS=", "");
                            if (k.indexOf("BBOX=") == 0) bbox = k.replace("BBOX=", "");
                        }
                    })
                    window.ContainerInitalizing = true;
                    if (window.SetTabBBOX) SetTabBBOX(bbox);
                    if (window.SetTabState) SetTabState(ts);
                    window.IsLoadingQuery = true;
                    if (tb)
                        ChangeView(tb.replace("Tab=", ""), true);
                    window.IsLoadingQuery = false;
                    if (window.FireTabStatusChanged) FireTabStatusChanged();

                    window.ContainerInitalizing = false;
                }
                else if (window.ChangeView)
                    ChangeView(window.CurrentQueryContainer.GetActiveViewName(), true);
                window.doQueryText();

                $(document.body).trigger("GUI_SavedQueryRuned");

                if (!window.isGuest || !window.isGuest())
                    $.get(HttpUtility.ResolveUrl('~/GeodiQueryRepostroryHandler?op=Query&redirect=false&query_id=' + id, false, true))
                return;
            }

            window.SavedQueryLoading = false;
        }
    }
    document.location.href = HttpUtility.ResolveUrl('~/GeodiQueryRepostroryHandler?op=Query&query_id=' + id, false, true);
    return false;
}