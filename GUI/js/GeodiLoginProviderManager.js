﻿function GeodiLoginProviderManager() {
    this.CreateOrUpdateGeodiLoginProviderUser = function (user, callBackFunction) {
        $.post('GeodiLoginProviderService?op=CreateOrUpdateGeodiLoginProviderUser', { 'user': JSON.stringify(user) }, callBackFunction, 'json');
    }
    this.GetGeodiLoginProviderUserFromUsername = function (user_id, callBackFunction) {
        $.post('GeodiLoginProviderService?op=GetGeodiLoginProviderUserFromUsername', { 'user_id': user_id }, callBackFunction, 'json');
    }
}
window.geodiLoginProviderManager = new GeodiLoginProviderManager();

function GeodiLoginProviderUser() {
    this.UserID = '';
    this.Name = '';
    this.Surname = '';
    this.Password = '';
    this.EMail = '';
}