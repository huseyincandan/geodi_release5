﻿function StylesServiceManager(id) {
    this.ID = id;
    this.GetSystemRules = function (callBackFunction) {
        $.post('StylesService?op=GetSystemRules', null, callBackFunction, 'json');
    }
}

window.StylesServiceManager = new StylesServiceManager('');
