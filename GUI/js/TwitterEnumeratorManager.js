﻿function TwitterEnumeratorManager() {
    this.getValidateUrl = function (obj, clientRequestId, callBackFunction) {
        $.post('TwitterEnumeratorManagerWebHandler?op=GetValidateUrl', { 'obj': JSON.stringify(obj), "clientRequestId": clientRequestId }, callBackFunction, 'json');
    };
    this.clearToken = function (obj, clientRequestId, callBackFunction) {
        $.post('TwitterEnumeratorManagerWebHandler?op=ClearToken', { 'obj': JSON.stringify(obj), 'clientRequestId': clientRequestId }, callBackFunction, 'json');
    };
    this.isOk = function (clientRequestId, callBackFunction) {
        $.post('TwitterEnumeratorManagerWebHandler?op=isOk', { 'clientRequestId': clientRequestId }, callBackFunction, 'json');
    };
    this.setAccessToken = function (clientRequestId, callBackFunction) {
        $.post('TwitterEnumeratorManagerWebHandler?op=setAccessToken', { "clientRequestId": clientRequestId }, callBackFunction, 'json');
    };
    this.GetAccountSettings = function (obj, clientRequestId, callBackFunction) {
        $.post('TwitterEnumeratorManagerWebHandler?op=GetAccountSettings', { 'obj': JSON.stringify(obj), "clientRequestId": clientRequestId }, function (obj) { callBackFunction(JSON.parse(obj)) }, 'json');
    };
}

window.TwitterEnumeratorManager = new TwitterEnumeratorManager();