﻿window.StateForm = {
    DoPost: function (arg, url) {
        if (!arg) arg = '';
        $('#_CmdArg').val(arg);
        if (url) {
            url = HttpUtility.ResolveUrl(url);
            $('#MainForm').attr('action', url);
        }
        $('#MainForm').submit();
    },
    GetState: function (name) { return $('#st_' + name).val(); },
    SetState: function (name, val, insertForce) {
        if ($('#st_' + name).length == 0 && insertForce)
            $("#MainForm").append("<input type=hidden name='st_" + name + "' id='st_" + name + "' />");
        $('#st_' + name).val(val);
    }
}