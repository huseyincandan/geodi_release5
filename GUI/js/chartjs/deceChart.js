if (!window.Dece) window.Dece = {};

window.Dece.ToChartDataSet = function (dcJson, dcChartOptions) {
    var labels = [];
    var _labelsFull = {};
    var _dataSet = {};
    var _treeLabels = {
        Items: []
    };

    var getCount = function (item, defaultValue) {
        var cnt = item.Count;
        if (dcChartOptions.valueDataField)
            cnt = item.Fields[dcChartOptions.valueDataField];
        if (cnt == null || cnt == undefined)
            return defaultValue;
        return cnt;
    };

    var getCountOrValue = function (item, fieldKey) {
        var cnt = item.Count;
        if (item.Fields[fieldKey] != null) {
            cnt = item.Fields[fieldKey];
        }
        return cnt;
    };


    var updateCountsForSort = function () { /*calculateCount*/
        var bnCounts = {};
        $(dcJson).each(function (i, item) {
            var labelValue = [];
            if (dcChartOptions.chartLabels && dcChartOptions.chartLabels.length > 0) {
                $.each(dcChartOptions.chartLabels, function (i2, fieldKey) {
                    var lbl = (item.Fields ? item.Fields[fieldKey] : "");
                    if (lbl == null || lbl == undefined) lbl = "";
                    if (lbl.trim) lbl = lbl.trim();
                    labelValue.push(lbl);
                });

            }
            var label = labelValue.join(" / ");
            if (!bnCounts[label])
                bnCounts[label] = { itemRefs: [item], count: getCount(item, 0) };
            else {
                bnCounts[label].count += getCount(item, 0);
                bnCounts[label].itemRefs.push(item);
            }
        });
        $.each(bnCounts, function (i) {
            var item = bnCounts[i];
            if (item && item.itemRefs)
                $(item.itemRefs).each(function (j, itemRef) {
                    itemRef.__Count = item.count;
                });
        });
    };
    var padFnd = function (val, length) {
        return $.isNumeric(val) ? pad(val, length) : val;
    }
    var fndFieldValue = function (fieldOnj, key) {
        return fieldOnj.hasOwnProperty(key + ".fieldValue") ? fieldOnj[key + ".fieldValue"] : fieldOnj[key];
    }
    var sortFunction = null;
        switch (dcChartOptions.sortType)
        {
            case "shortForPerCount":
                sortFunction = function (a, b) { return getCount(a, 0) - getCount(b, 0); }
                break;
            case "shortForPerCountDesc":
                sortFunction = function (a, b) { return getCount(b, 0) - getCount(a, 0); }
                break;
            case "shortForCountDesc":
                updateCountsForSort();
                sortFunction = function (a, b) {
                    return b.__Count - a.__Count;
                };
                break;
            case "shortForLabel":
                sortFunction = function (a, b) {
                    if (dcChartOptions.chartLabels) {
                        var key1Arr = [], key2Arr = [];
                        for (var i = 0; i < dcChartOptions.chartLabels.length; i++) {
                            key1Arr.push(a && a.Fields ? padFnd(fndFieldValue(a.Fields, dcChartOptions.chartLabels[i]), 50) : "");
                            key2Arr.push(b && b.Fields ? padFnd(fndFieldValue(b.Fields, dcChartOptions.chartLabels[i]), 50) : "");
                        }
                        key2Arr = key2Arr.join("_");
                        key1Arr = key1Arr.join("_");
                        if (window.UpdateStrForSort) {
                            key2Arr = UpdateStrForSort(key2Arr);
                            key1Arr = UpdateStrForSort(key1Arr);
                        }

                        return key1Arr.localeCompare(key2Arr);
                    }
                    return false;
                };
                break;
            case "shortForLabelDesc":
                sortFunction = function (a, b) {
                    if (dcChartOptions.chartLabels) {
                        var key1Arr = [], key2Arr = [];
                        if (!a) a = { Fields: [] }; if (!b) b = { Fields: [] };
                        for (var i = 0; i < dcChartOptions.chartLabels.length; i++) {
                            key1Arr.push(a && a.Fields ? padFnd(fndFieldValue(a.Fields, dcChartOptions.chartLabels[i]), 50) : "");
                            key2Arr.push(b && b.Fields ? padFnd(fndFieldValue(b.Fields, dcChartOptions.chartLabels[i]), 50) : "");
                        }
                        key2Arr = key2Arr.join("_");
                        key1Arr = key1Arr.join("_");
                        if (window.UpdateStrForSort) {
                            key2Arr = UpdateStrForSort(key2Arr);
                            key1Arr = UpdateStrForSort(key1Arr);
                        }
                        return key2Arr.localeCompare(key1Arr);
                    }
                    return false;
                };
                break;
            case "none": break;
            default:
                updateCountsForSort();
                sortFunction = function (a, b) { return a.__Count - b.__Count; }
                break;
    }

    if (sortFunction)
        dcJson.sort(sortFunction);


    $(dcJson).each(function (i, item) {

        var labelValue = [];
        var node = _treeLabels;
        if (dcChartOptions.chartLabels && dcChartOptions.chartLabels.length > 0) {
            $.each(dcChartOptions.chartLabels, function (i2, fieldKey) {
                var lbl = (item.Fields ? item.Fields[fieldKey] : "");
                if (lbl == null || lbl == undefined) lbl = "";
                if (lbl.trim) lbl = lbl.trim();
                labelValue.push(lbl);
                if (!node[lbl]) {
                    node[lbl] = { Items: [], Label: lbl };
                    node.Items.push(node[lbl]);
                }
                node = node[lbl];
            });
            node.IsValue = true;
        }
        else {
            if (_treeLabels.Items.length == 0) {
                node = { Label: "", LabelIndex: 0, IsValue: true, Items: [] };
                _treeLabels.Items.push(node);
            }
        }

        var label = labelValue.join(" / ");
        if (!_labelsFull[label]) {
            _labelsFull[label] = (labels.length + 1);
            labels.push(label);
        }

        node.LabelIndex = (_labelsFull[label] - 1);

        
        var dataValue = [];
        if (dcChartOptions.chartDataFields) {
            $.each(dcChartOptions.chartDataFields, function (i2, fieldKey) {
                dataValue.push(item.Fields[fieldKey]);
            });
        } 
        
        //var dataKey = dcChartOptions.chartDataFields ? dataValue.join(" / ") : " "/*NONE*/;

        //if (!_dataSet[dataKey])
         //   _dataSet[dataKey] = {};
        //var cnt = getCount(item)
        //_dataSet[dataKey][label] = _dataSet[dataKey][label] ? _dataSet[dataKey][label] + (typeof cnt == 'string' ? ',' + cnt : cnt) : cnt;

        /* eklenebilir
        var dataValue = [];
        var dataKey = [];
        var chartDataFieldsValue;
        if (dcChartOptions.chartDataFields) {
            $.each(dcChartOptions.chartDataFields, function (i2, fieldKey) {
                dataValue.push(item.Fields[fieldKey]);
                if (dcChartOptions.splitDataFields) {
                    chartDataFieldsValue = getCount(item);
                }
            });
        } 
        */
        var dataKey = [];
        if (dcChartOptions.splitDataFields) {
            //$.each(dataValue, function (i3, key) {
              //  dataKey.push(key);
            //});
            dataKey = dataValue;

        }
        else {
            var tempString = dcChartOptions.chartDataFields ? dataValue.join(" / ") : " "/*NONE*/;
            dataKey.push(tempString); 
        }
            
        /*
        if (!_dataSet[dataKey])
            _dataSet[dataKey] = {};
        */
        $.each(dataKey, function (i3, key) {
            if (!_dataSet[key])
                _dataSet[key] = {};
            var ky1 = key;
            if (dcChartOptions.valueDataField && dcChartOptions.useValueDataFieldCounter)
                ky1 = dcChartOptions.valueDataField;
            var cnt = getCountOrValue(item, ky1);
            _dataSet[key][label] = _dataSet[key][label] ? _dataSet[key][label] + (typeof cnt == 'string' ? ',' + cnt : cnt) : cnt;
        });

        /*
        var cnt = getCount(item)
        _dataSet[dataKey][label] = _dataSet[dataKey][label] ? _dataSet[dataKey][label] + (typeof cnt =='string'?','+cnt:cnt) : cnt;
        */

    });

    var dataSets = [];
    $.each(_dataSet, function (dataKey) {

        var currentDataSet = {
            label: dataKey,
            borderWidth: 1,
            data: []
        };
        //_dataSet[dataKey] = _dataSet[dataKey].toLocaleString(window.DeceClientState.GetLanguage());
        currentDataSet.backgroundColor = GetColorFor(dataKey, false, (dcChartOptions.chartOptions && dcChartOptions.chartOptions.colorPalette ? window.ColorPalettes[dcChartOptions.chartOptions.colorPalette] : window.ColorPalettes["Palet1"])) + "99";
        currentDataSet.borderColor = GetColorFor(dataKey + "_Border", false, (dcChartOptions.chartOptions && dcChartOptions.chartOptions.colorPalette ? window.ColorPalettes[dcChartOptions.chartOptions.colorPalette] : window.ColorPalettes["Palet1"]));
        if (dcChartOptions.DisableBackground) currentDataSet.fill = false;
        $.each(labels, function (i2, labelKey) {
            var data1 = _dataSet[dataKey];
            var val = null;
            if (data1[labelKey])
                val = data1[labelKey];
            currentDataSet.data.push(val);
        });

        dataSets.push(currentDataSet);

    });

    var dcChartData = {
        labels: labels,
        datasets: dataSets,
        treelabels: _treeLabels
    };
    var dataSets2 = [];
    var labels2 = [];
    if (dataSets.length > 0) {
        var arr0 = dataSets[0];
        for (var i = 0; i < arr0.data.length; i++) {
            var dataSetInverse = {
                label: labels[i],
                backgroundColor: [],
                borderColor: [],
                borderWidth: 1,
                data: []
            };
            if (dcChartOptions.DisableBackground) dataSetInverse.fill = false;
            labels2 = [];
            for (var j = 0; j < dataSets.length; j++) {
                var arr = dataSets[j];
                labels2.push(arr.label);
                dataSetInverse.data.push(arr.data[i]);
                dataSetInverse.backgroundColor.push(arr.backgroundColor);
                dataSetInverse.borderColor.push(arr.borderColor);
            }

            dataSets2.push(dataSetInverse);


        }

    }

    dcChartData.Inverse_datasets = dataSets2;
    dcChartData.Inverse_labels = labels2;


    return dcChartData;
}

window.Dece.PivotTableAndChart = function (dcJson, dcChartOptions, chartTargetDom, pivotTargetDom, pivotTemplateDom) {
    return {
        Chart: window.Dece.PivotChart(dcJson, dcChartOptions, chartTargetDom),
        Pivot: window.Dece.PivotTable(dcJson, dcChartOptions, pivotTargetDom, pivotTemplateDom),
        Options: dcChartOptions,
        Update: function (newData, newOptions) {
            if (newOptions) this.Options = newOptions;
            this.Chart.Update(newData, newOptions);
            this.Pivot.Update(newData, newOptions);
        }
    }
}

window.Dece.PivotChart = function (dcJson, dcChartOptions, chartTargetDom) {
    var dcChartData = window.Dece.ToChartDataSet(dcJson, dcChartOptions);

    if (dcChartOptions.chartOptions.maximumNumberData && dcChartOptions.chartOptions.maximumNumberData > 0) {
        dcChartData.Inverse_datasets = dcChartData.Inverse_datasets.slice(0, dcChartOptions.chartOptions.maximumNumberData);
        dcChartData.Inverse_labels = dcChartData.Inverse_labels.slice(0, dcChartOptions.chartOptions.maximumNumberData);
        dcChartData.datasets = dcChartData.datasets.slice(0, dcChartOptions.chartOptions.maximumNumberData);
        dcChartData.labels = dcChartData.labels.slice(0, dcChartOptions.chartOptions.maximumNumberData);
    }

    var target = chartTargetDom;
    var domObj = $(target)[0];

    if (domObj && domObj.chart) {
        try { domObj.chart.destroy(); }
        catch (ex) { }
    }

    var ctx = domObj.getContext('2d');

    var chartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }


    if (dcChartOptions.chartOptions)
        $.extend(true, chartOptions, dcChartOptions.chartOptions);

    if (dcChartOptions.InverseDataset || (dcChartOptions.chartType && (dcChartOptions.chartType.toLowerCase() == "pie" || dcChartOptions.chartType.toLowerCase() == "polararea" || dcChartOptions.chartType.toLowerCase() == "doughnut"))) {
        dcChartData.datasets = dcChartData.Inverse_datasets;
        dcChartData.labels = dcChartData.Inverse_labels;
    }

    

    if (dcChartOptions.chartType && dcChartOptions.chartType.toLowerCase() == "bubble") {
        var dts1 = [];
        var maxV = null, minV;
        $.each(dcChartData.datasets, function (ij, dts) {
            $.each(dts.data, function (ij2, dataItm) {
                if (dataItm) {
                    var yValue = parseFloat(dcChartData.labels[ij2]);
                    var vl = parseFloat(dataItm);
                    dts1.push({
                        x: parseFloat(dts.label),
                        y: yValue,
                        v: vl,
                        backgroundColor: dts.backgroundColor,
                        borderColor: dts.borderColor,
                    });
                    maxV = maxV == null ? vl : Math.max(vl, maxV);
                    minV = minV == null ? vl : Math.min(vl, minV);
                }
            })
        })
        var datanew = {
            datasets: [{ data: dts1 }]
        };
        dcChartData = datanew;
        var totalRange = 0;
        if (maxV != null && minV != null) totalRange = Math.abs(maxV - minV);
        var options = {
            aspectRatio: 1,
            legend: false,
            tooltips: false,

            elements: {
                point: {
                    backgroundColor: function (context) {
                        var value = context.dataset.data[context.dataIndex];
                        return value.backgroundColor;
                    },
                    borderColor: function (context) {
                        var value = context.dataset.data[context.dataIndex];
                        return value.borderColor;
                    },
                    borderWidth: function (context) {
                        return 1;
                    },
                    hoverBackgroundColor: 'transparent',
                    hoverBorderWidth: function (context) {
                        var value = context.dataset.data[context.dataIndex];
                        if (totalRange > 0) {
                            var v = value.v + minV;
                            v = v * 4.0 / totalRange;
                            if (v < 1) v = 1;
                            return v + 3;
                        }
                        return 6;
                    },
                    radius: function (context) {
                        var value = context.dataset.data[context.dataIndex];
                        if (totalRange > 0) {
                            var v = value.v + minV;
                            v = v * 4.0 / totalRange;
                            if (v < 1) v = 1;
                            return v;
                        }
                        return 3;
                    }
                }
            }
        }
        $.extend(true, chartOptions, options);
    }
    else {
        if (!dcChartOptions.chartOptions || !dcChartOptions.chartOptions.legend) {
            if (!chartOptions.legend)
                chartOptions.legend = {};
            chartOptions.legend.display = dcChartOptions.chartLegend && dcChartOptions.chartLegend != "none";
            if (chartOptions.legend.display) {
                chartOptions.legend.position = dcChartOptions.chartLegend;
                if (!chartOptions.scales) chartOptions.scales = {};
                if (!chartOptions.scales.xAxes || chartOptions.scales.xAxes.length == 0) chartOptions.scales.xAxes = [{}];

               
                if (!dcChartOptions.show_xAxess)
                    chartOptions.scales.xAxes[0].display = false;

                if (dcChartOptions.chartLegendFontColor) {
                    chartOptions.legend.labels = { fontColor: dcChartOptions.chartLegendFontColor }
                    chartOptions.scales.xAxes[0].ticks = { fontColor: dcChartOptions.chartLegendFontColor };
                    chartOptions.scales.yAxes[0].ticks = { fontColor: dcChartOptions.chartLegendFontColor };
                }
            }
        }


    }

    var chartJson = {
        lbl: dcChartOptions.chartLabels,
        type: dcChartOptions.chartType,
        data: dcChartData,
        options: chartOptions
    };


    if (chartJson.type && chartJson.type.indexOf && chartJson.type.indexOf("line.") == 0) {
        $.each(dcChartData.datasets, function (ij, dts) {
            dts.fill = false;
            if (chartJson.type == ("line.point"))
                dts.showLine = false;
        });
        chartJson.type = "line";
    }

    

    domObj.chart = new Chart(ctx, chartJson);

    return {
        Chart: domObj.chart,
        Options: dcChartOptions,
        ChartOptions: chartJson,
        _dcJson: dcJson,
        Update: function (newData, newOptions) {
            if (newOptions) this.Options = newOptions;
            if (newData) this._dcJson = newData;
            var dcChartData = window.Dece.ToChartDataSet(this._dcJson, this.Options);
            if (this.Options && this.Options.chartType)
                this.ChartOptions.type = this.Options.chartType;
            if ((this.Options && this.Options.InverseDataset) || this.ChartOptions.type.toLowerCase() == "pie" || this.ChartOptions.type.toLowerCase() == "polararea" || this.ChartOptions.type.toLowerCase() == "doughnut") {
                dcChartData.datasets = dcChartData.Inverse_datasets;
                dcChartData.labels = dcChartData.Inverse_labels;
            }
            this.ChartOptions.data = dcChartData;

            if (newOptions && newOptions.chartOptions)
                $.extend(true, this.ChartOptions.options, newOptions.chartOptions);
            this.Chart.update();
        }
    }
}

window.Dece.PivotTable = function (dcJson, dcChartOptions, pivotTargetDom, pivotTemplateDom) {

    var rtn = {
        Options: dcChartOptions,
        _dcJson: dcJson,

        Update: function (newData, newOptions) {
            if (newOptions) this.Options = newOptions;
            if (newData) this._dcJson = newData;
            var data = window.Dece.ToChartDataSet(this._dcJson, this.Options);
            var target = pivotTargetDom;
            // Pivot Table Most Left Header
            data.pivotTitleLabel = "-"
            if (this.Options.pivotTitleLabel)
                data.pivotTitleLabel = this.Options.pivotTitleLabel;
            // Target Element
            target = $(target);
            var template = pivotTemplateDom ? pivotTemplateDom : $("#PivotTemplate");
            target.html(TemplateToHtml(template, data, { root: data }));
        }
    }

    rtn.Update();
    return rtn;
}