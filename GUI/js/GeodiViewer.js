﻿
window.__CloseMapContext = function () {
    $('#_mapcontextMenu').remove();
    window.___opencontextMenuTime = null;
}

function initContextMenuForMap(view, map) {
    view.on('change:center', function () { window.__CloseMapContext(); });
    initContextMenuFor($(map.getViewport()));
}

function initContextMenuFor(tagret) {
    tagret = $(tagret);
    $(document.body).on('mousedown',
        function (e) {
            if ($(e.target).parents("#_mapcontextMenu").length == 0)
                window.__CloseMapContext();
        }
    );
    if (!(/iphone|ipad|ipod/i).test(navigator.userAgent) && !window.BrowserType.IsMobile()) {
        tagret.on('contextmenu', function (e) {
            e.preventDefault();
            openContextMenu(e.pageX, e.pageY);
        });
    }
    else {
        tagret
            .on("touchstart", function (e) {
                window.__CloseMapContext();
                if (e.originalEvent.touches.length > 1) {
                    window.__touchEdd = new Date();
                    clearTimeout(window.___timerLongTouch);
                }
                else {
                    var x = e.originalEvent.touches[0].pageX;
                    var y = e.originalEvent.touches[0].pageY;
                    window.__touchStt = new Date();
                    window.___timerLongTouch = setTimeout(function () {
                        if (window.__touchEdd && Math.abs(window.__touchEdd - window.__touchStt) < 500)
                            return;
                        openContextMenu(x, y);
                    }, 1000);
                }
            })
            .on("touchmove", function (e) {
                window.__touchEdd = new Date();
                clearTimeout(window.___timerLongTouch);
            })
            .on("touchend", function () {
                window.__touchEdd = new Date();
                clearTimeout(window.___timerLongTouch);
            })
            .on("ontouchforcechange", function () {
                window.__touchEdd = new Date();
                clearTimeout(window.___timerLongTouch);
            })
            .on("ontouchcancel", function () {
                window.__touchEdd = new Date();
                clearTimeout(window.___timerLongTouch);
            });
    }
}



window.ContextMenuItems = [];

window.ContextMenuItems.push({
    ID: 'addNote',
    Icon: 'GUIJsonService?op=getIco&IconName=content/note',
    DisplayName: '[$docViewer:add_label/js]',
    onclick: function (x, y) {
        window.CreateLabelForPoint(x, y);
    },
    condition: function () { return !window.isDesktopOpenWith && !window._hideAddLabel && window.CreateLabelForPoint ? true : false; }
});


function openContextMenu(x, y) {
    window.__CloseMapContext();
    var w = $(window);
    var mnItems = [];
    for (var i = 0; i < ContextMenuItems.length; i++) {
        var mn = ContextMenuItems[i];
        if (mn.condition && !mn.condition()) continue;
        var ico = (mn.Icon ? mn.Icon : 'GUIJsonService?op=getIco&IconName=empty');
        ico += (ico.indexOf('?') == -1 ? '?' : '&') + '&v=' + HttpUtility.GetVersionCode();
        var mn = '<li role="presentation"><a type="button" href="javascript:handleContexMenuEvent(\'' + mn.ID + '\', \'' + x + '\', \'' + y + '\');"><img src=' + ico + '/> ' + mn.DisplayName + ' </a></li>';
        mnItems.push(mn)
    }
    if (mnItems.length == 0)
        return;
    if (w.width() - x < 160) x = w.width() - 160;
    if (w.height() - y < (mnItems.length * 40)) y = w.height() - (mnItems.length * 40);
    window.___opencontextMenuTime = new Date();

    var target = $(document.body);// window.UpdateContextMenuTarget();
    target.append('<ul id="_mapcontextMenu" class="dropdown-menu" style="position:absolute;display:block;top: ' + y + 'px; left:' + x + 'px;">' +
        mnItems.join('') +
        '</ul>');
}

window.handleContexMenuEvent = function (option, x, y) {
    window.__CloseMapContext();

    for (var i = 0; i < ContextMenuItems.length; i++) {
        var mn = ContextMenuItems[i];
        if (mn.ID == option) {
            if (mn.onclick)
                mn.onclick(x, y);
            return;
        }
    }
}