﻿if (!window.HttpUtility) {
    window.HttpUtility = {
        HtmlEncode: function (val) { if (!val || typeof (val) != "string") return val; return val.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;'); },
        HtmlDecode: function (val) { if (!val || typeof (val) != "string") return val; return window.HttpUtility.HtmlJsonDecode(val.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&#39;/g, '\'').replace(/&lt;/g, '<').replace(/&gt;/g, '>')); },
        HtmlJsonDecode: function (val) { if (!val || typeof (val) != "string") return val; return val.replace(/\\u003c/g, '<').replace(/\\u003e/g, '>').replace(/\\u0027/g, '\''); }, 
        UrlEncode: function (val) { return window.encodeURIComponent ? window.encodeURIComponent(val) : val.replace(/&/g, '%26'); },
        UrlDecode: function (val) { return window.decodeURIComponent ? window.decodeURIComponent(val) : val.replace(/%26/g, '&'); },
        ToBase64: function (val, IgnoreBase64prefix) {
            var bs64Test = btoa(this.UrlEncode(val).replace(/%([0-9A-F]{2})/g, function (match, p1) { return String.fromCharCode('0x' + p1); }));
            if (!IgnoreBase64prefix)
                bs64Test = ".b64." + bs64Test;
            return bs64Test;
        },
        ScriptEncode: function (val, encodeEnter) { if (!val || typeof (val) != "string") return val; val = val.replace(/\\/g, '\\\\').replace(/'/g, '\\\'').replace(/"/g, '\\"'); if (encodeEnter) val = val.replace(/\r/g, '\\r').replace(/\n/g, '\\n'); return val; }
    }
}

function GeodiEnvelope() {
    this.MaxX = 0;
    this.MaxY = 0;
    this.MinX = 0;
    this.MinY = 0;

    this.Width = function () {
        return this.MaxX - this.MinX;
    }
    this.Height = function () {
        return this.MaxY - this.MinY;
    }
}

function _GetCurrentName(defaultWsName) {
    if (window.__currentWsName) return window.__currentWsName;
    if (window.StateForm)
        return StateForm.GetState("wsName");
    return defaultWsName;
}

var GUISearchOptions = {
    SimpleResutView: 1, HideDLV: 2, HideKLV: 4, HideMap: 8, HideKML: 16, HideExport: 32, HideAdvanceQuery: 64, HideNoteIcon: 128,
    Empty1: 256, HideChangeProject: 512, Empty2: 1024, Empty3: 2048, HideChangeLanguage: 4096, HideLogout: 8192,
    HideFacet: 16384, HideDGRAPH: 32768, HideQueryBar: 65536, HideLogoBar: 131072, DisableSimpleSearchStart: 262144, HideWatchQueries: 524288,
    HideActions: 1048576, HideRightDocInfoPanel: 2097152, HideSimilarty: 4194304, HideCalendar: 8388608,
    HideChangeTheme: 16777216, HideDashboards: 33554432, HideDocParentsInfo: 67108864, ClosedFacet: 134217728, HideExtensionSetupMenu: 268435456, ShowExplorerView: 536870912,
    GetSafeBits: function () {
        if (this.Is(GUISearchOptions.HideNoteIcon))
            return GUISearchOptions.HideNoteIcon;
        return 0;
    },
    Is: function (opt) {
        return window.DGUIOptions ? (window.DGUIOptions & opt) == opt : 0;
    },
    Create: function (options) {
        var r = JSON.parse(JSON.stringify(this));
        r.Is = function (opt) {
            return (options & opt) == opt;
        }
        return r;
    }
};

function ShowQueryModal(query, wsName, modalTitle, tab, guiOptions) {
    if (!modalTitle)
        modalTitle = '';
    var queryStr = query;
    if (typeof (query) == "object") {
        if (!wsName && query.wsName)
            wsName = query.wsName;
        queryStr = query.ToString();
    }

    if (!tab)
        tab = "DLV";
    if (!wsName && window.StateForm)
        wsName = _GetCurrentName(wsName);
    if (!(guiOptions >= 0)) guiOptions = 3276801;
    var url = '~/?ismaprequest=1&GUIOptions=' + guiOptions + '&TAB=' + tab + '&q=' + HttpUtility.UrlEncode(queryStr);
    if (wsName)
        url += '&wsName=' + HttpUtility.UrlEncode(wsName);
    ShowModalUrl(url, modalTitle, null, null, null, null, { height: '65vh' });
}

function ExtendObjectWithDecimalAdd(source, appendData, __stack) {
    if (!__stack) __stack = 0;
    if (__stack > 20)
        return;
    var BBOX1 = source.BBOX;
    var BBOX2 = appendData.BBOX;

    if (BBOX1 && BBOX2 && typeof (BBOX1) == "object") {
        BBOX1 = {
            MinX: Math.min(BBOX1.MinX, BBOX2.MinX),
            MinY: Math.min(BBOX1.MinY, BBOX2.MinY),
            MaxX: Math.max(BBOX1.MaxX, BBOX2.MaxX),
            MaxY: Math.max(BBOX1.MaxY, BBOX2.MaxY)
        }
        delete source.BBOX;
    }

    

    $.each(appendData, function (key, value) {
        if (!source[key])
            source[key] = value;
        else {
            if (typeof source[key] =="number" &&  typeof value == "number")
                source[key] += value;
            else {
                if (typeof value == "object")
                    if (value.constructor === Array) {
                        if (!source[key]) source[key] = [];
                        if (source[key].constructor === Array) {
                            var srcTarget = source[key];
                            for (var i = 0; i < value.length; i++) {
                                var itm = value[i];
                                if (itm.ID && srcTarget.find(function (f) { return f.ID === itm.ID }))
                                        continue;
                                srcTarget.push(itm);
                            }
                        }
                    }
                    else
                        ExtendObjectWithDecimalAdd(source[key], value, __stack + 1);
                }

        }
    })

    source.BBOX = BBOX1;
}
//cache
function GeodiQuery(pObject) {
    this.Clear = function (clearSearchString, resetSettings) {
        this.IncludeKeywords = [];
        this.ExcludeKeywords = [];
        this.NearKeywords = [];
        this.DocParents = [];
        this.DocParentsTree = [];
        this.SimilarTo = null;
        this.FullTextSearchKeywords = [];
        this.ContentId = null;
        this.ContentIdentifiers = [];
        this.Enumarators = null;
        this.Envelope = null;
        this.RecognizerIDs = '';
        this.DocDateStart = '';
        this.DocDateEnd = '';
        this.ContentReaders = [];
        this.ContentTypes = [];
        this.GeometryStatus = 0;
        this.LabelStatus = 0;
        this.SearchStrings = [];
        this.SearchStringsHiden = [];
        if (clearSearchString)
            this.SearchString = '';
        if (resetSettings) {
            this.NearDistance2 = 10;
            this.StartIndex = 0;
            this.EndIndex = 0;
            this.OrderKeywordsByRecognizerID = false;
            this.IgnoreKWRank = false;
            this.GetDocumentInfo = false;
            this.IgnoreDefaultQuery = false;
        }
    }
    if (pObject) {
        if (typeof (pObject) != "object")
            pObject = JSON.parse(pObject);
        $.extend(true,this, pObject);
        if (pObject.SubQuery) {
            this.SubQuery = new GeodiQuery();
            $.extend(true,this.SubQuery, pObject.SubQuery);
        }

    } else {
        this.Clear(false,true);
    }

    this.IsComplex = function () {
        return (this.NearKeywords != null && this.NearKeywords.length > 0);
    };

    this.ResetForSave = function () {
        delete this.SuggestFilter_DOC;
        delete this.SuggestFilter_KW;
        delete this.OptimizeForSuggest;
        delete this.GetKeywordItem_ID;
        delete this.IgnoreDefaultQuery;
        this.StartIndex = 0;
        this.EndIndex = 0;
        if (this.SubQuery != null) {
            delete this.SubQuery.SuggestFilter_DOC;
            delete this.SubQuery.SuggestFilter_KW;
            delete this.SubQuery.OptimizeForSuggest;
            delete this.SubQuery.GetKeywordItem_ID;
            delete this.SubQuery.IgnoreDefaultQuery;
            this.SubQuery.StartIndex = 0;
            this.SubQuery.EndIndex = 0;
        }
    };

    if (window.StateForm)
        this.wsName = _GetCurrentName(this.wsName);
    var that = this;

    this.Clone = function () {
        return new GeodiQuery(this);
    }

    this.GetSource= function() {
        if (that.SourceList && that.SourceList.length > 0) {
            if (that.CurrentSourceIndex == undefined) that.CurrentSourceIndex = 0;
            var src = that.SourceList[that.CurrentSourceIndex];
            if (src) return src;
        }
        return null;
    }
    this.UpdateSource = function (src) {
        if (!src)
            src = that.GetSource();
        if (!src)
            return;
        if (src.wsName) that.wsName = src.wsName;
            else if (window.StateForm) this.wsName = _GetCurrentName(this.wsName);
        that.ServerUrl = src.ServerUrl;
        that.Token = src.Token;
    }
    this.HasNextSource = function () {
        if (that.SourceList && that.SourceList.length > 0) {
            if (that.CurrentSourceIndex == undefined) that.CurrentSourceIndex = 0;
            return that.CurrentSourceIndex < (that.SourceList.length - 1);
        }
        return false;
    }
    this.HasMultipleSource = function () {
        return (that.SourceList && that.SourceList.length > 1);
    }

    this.YYYYToDate = function (value) {
        return new Date(value.substr(0, 4) + "-" + value.substr(4, 2) + "-" + value.substr(6, 2));
    }
    this.formatDateYYYY = function (value) {
        value = new Date(value);
        if (value === "Invalid Date")
            return null;
        var date = "0" + value.getDate();
        var month = "0" + (parseInt(value.getMonth()) + 1);
        var rtn = (value.getFullYear()) + "" +
            (month.length === 3 ? month.substr(1, 2) : month.substr(0, 2)) + "" +
            (date.length === 3 ? date.substr(1, 2) : date.substr(0, 2));
        return rtn;
    }

    this.ToJson = function () { return JSON.stringify(that); }
    this.ToFacetStr = function () {
        var rtn = [];
        var addTo = function (key, value) {
            rtn.push(key + "." + value);
        }
        var sp = "(";
        if (that.DocDateStart && that.DocDateStart.UTC &&
            that.DocDateEnd && that.DocDateEnd.UTC)
            addTo("d", that.formatDateYYYY(that.DocDateStart.UTC) + sp + that.formatDateYYYY(that.DocDateEnd.UTC));
        else if (that.DocDateStart && that.DocDateStart.UTC)
            addTo("d", that.formatDateYYYY(that.DocDateStart.UTC) + sp);
        else if (that.DocDateEnd && that.DocDateEnd.UTC)
            addTo("d", sp + that.formatDateYYYY(that.DocDateEnd.UTC));

        if (that.ContentId != null && that.ContentId != undefined)
            addTo("c0", that.ContentId);
        if (that.ContentIdentifiers != null && that.ContentIdentifiers.length > 0)
            addTo("c1", that.ContentIdentifiers.join(sp));
        if (!that.IgnoreDefaultQuery && that.Enumarators != null && that.Enumarators.length > 0)
            addTo("s", that.Enumarators.join(sp));

        if (that.RecognizerIDs && that.RecognizerIDs.length > 0)
            addTo("l", that.RecognizerIDs);

        if (that.ContentDisplayNames != null && that.ContentDisplayNames.length > 0)
            addTo("cd", that.ContentDisplayNames.join(sp));
        if (that.ContentReaders != null && that.ContentReaders.length > 0)
            addTo("cr", that.ContentReaders.join(sp).replace(/Factory.ContentReaderFactory:/g, "f\)").replace(/ContentReader/g, "c\)").replace(/Reader/g, "r\)"));
        if (that.ContentTypes != null && that.ContentTypes.length > 0)
            addTo("ct", that.ContentTypes.join(sp).replace(/filecontent:/g, "f\)"));

        if (that.DocParents != null && that.DocParents.length > 0)
            addTo("p1", that.DocParents.join(sp));
        if (that.DocParentsTree != null && that.DocParentsTree.length > 0)
            addTo("p2", that.DocParentsTree.join(sp));

        if (that.DocParentsSingleTxt) {
            var t1 = that.DocParentsSingleTxt;
            if (t1.length > 30) t1 = t1.substr(0, 30);
            addTo("p2t", t1.replace(/\(/g,""));
        }
        if (that.DocParentsSingleContentType)
            addTo("p2c", that.DocParentsSingleContentType);


        if (that.SimilarTo && that.SimilarTo.length && that.SimilarTo.length > 0)
            addTo("sm", that.SimilarTo);

        if (that.SimilarDisplayName) {
            var t1 = that.SimilarDisplayName;
            if (t1.length > 30) t1 = t1.substr(0, 30);
            addTo("smt", t1.replace(/\(/g, ""));
        }


        if (that.NearKeywords != null && that.NearKeywords.length > 0)
            addTo("kn", that.NearKeywords.join(sp));
        if (that.FullTextSearchKeywords != null && that.FullTextSearchKeywords.length > 0)
            addTo("kf", that.FullTextSearchKeywords.join(sp));
        if (that.IncludeKeywords != null && that.IncludeKeywords.length > 0)
            addTo("ki", that.IncludeKeywords.join(sp));
        if (that.ExcludeKeywords != null && that.ExcludeKeywords.length > 0)
            addTo("ke", that.ExcludeKeywords.join(sp));


        if (that.ItemIdentifier && that.ItemIdentifier.length > 0)
            addTo("ii", that.ItemIdentifier);
        if (that.ItemIdentifiersMust != null)
            addTo("im", that.ItemIdentifiersMust.join(sp));
        if (that.ItemIdentifiers != null && that.ItemIdentifiers.length > 0)
            addTo("ii2", that.ItemIdentifiers.join(sp));


        

        var m = "";
        if (that.GeometryStatus == 2)
            m += "pg";
        else if (that.GeometryStatus == 1)
            m += "ng";
        if (this.LabelStatus == 1)
            m += "pn";
        else if (this.LabelStatus == 2)
            m += "nn";
        if (m)
            addTo("m", m);
        if (that.SearchStrings != null && that.SearchStrings.length > 0)
            addTo("ss", that.SearchStrings.join("|"));

        if (that.SearchStringsHiden != null && that.SearchStringsHiden.length > 0)
            addTo("hs", that.SearchStringsHiden.join("|"));
        
        return rtn.join("!");
    }
    this.SetFacetStr = function (str) {
        this.Clear(false);
        if (!str)
            return;

        var sp = "(";
        str = str.split('!');
        for (var i = 0; i < str.length; i++) {
            var t = str[i];
            var ti = t.indexOf(".");
            if (ti != -1) {
                var key = t.substr(0, ti);
                var val = t.substr(ti + 1);
                switch (key) {
                    case "d":
                        val = val.split(sp);
                        if (val.length == 2) {
                            if (val[0] && val[0].length == 8)
                                that.DocDateStart = toNetDateString(that.YYYYToDate(val[0]));
                            if (val[1] && val[1].length == 8)
                                that.DocDateEnd = toNetDateString(that.YYYYToDate(val[1]));
                        }
                        break;
                    case "c0": that.ContentId = parseInt(val); break;
                    case "c1": that.ContentIdentifiers = []; $.each(val.split(sp), function (k, v) { that.ContentIdentifiers.push(parseInt(v)) }); break;
                    case "ss": that.SearchStrings = val.split("|"); break;
                    case "hs": that.SearchStringsHiden = val.split("|"); break;
                    case "s": that.Enumarators = []; $.each(val.split(sp), function (k, v) { that.Enumarators.push(parseInt(v)) }); break;
                    case "l": that.RecognizerIDs = val; break;
                    case "cd": that.ContentDisplayNames = val.split(sp); break;
                    case "cr": that.ContentReaders = val.replace(/f\)/g, "Factory.ContentReaderFactory:").replace(/c\)/g, "ContentReader").replace(/r\)/g, "Reader").split(sp); break;
                    case "ct": that.ContentTypes = val.replace(/f\)/g, "filecontent:").split(sp); break;
                    case "p1": that.DocParents = []; $.each(val.split(sp), function (k, v) { that.DocParents.push(parseInt(v)) }); break;
                    case "p2": that.DocParentsTree = []; $.each(val.split(sp), function (k, v) { that.DocParentsTree.push(parseInt(v)) }); break;
                    case "p2t": that.DocParentsSingleTxt = val; break;
                    case "smt": that.SimilarDisplayName = val; break;
                    case "p2c": that.DocParentsSingleContentType = val; break;
                    case "sm": that.SimilarTo = val; break;
                    case "kn": that.NearKeywords = val.split(sp); break;
                    case "kf": that.FullTextSearchKeywords = val.split(sp); break;
                    case "ki": that.IncludeKeywords = val.split(sp); break;
                    case "ke": that.ExcludeKeywords = val.split(sp); break;
                    case "ii": that.ItemIdentifier = val; break;
                    case "im": that.ItemIdentifiersMust = []; $.each(val.split(sp), function (k, v) { that.ItemIdentifiersMust.push(parseInt(v)) }); break;
                    case "ii2": that.ItemIdentifiers = []; $.each(val.split(sp), function (k, v) { that.ItemIdentifiers.push(parseInt(v)) }); break;

                    case "m":
                        if (val.indexOf("pg") != -1)
                            that.GeometryStatus = 2;
                        else if (val.indexOf("ng") != -1)
                            that.GeometryStatus = 1;
                        if (val.indexOf("pn") != -1)
                            this.LabelStatus = 1;
                        else if (val.indexOf("nn") != -1)
                            this.LabelStatus = 2;
                        break;
                }
            }
        }
    }

    this.ToString = function (ExQ, isSubQuery, lvl,ignoreSearchString) {
        if (!lvl) lvl = 0;
        if (lvl > 1) return null;
        var rtn = [];
        var NotIgnoreCurrentOldStyle = (isSubQuery || !that.IgnoreDefaultQuery);
        var idList = [];
        var EnumarateStringWidthFormat = function (arr, format) {
            var p = [];
            $.each(arr, function (key, val) {
                var val2 = format.replace("{0}", val);
                p.push(val2);
            })
            return p;
        }
        if (NotIgnoreCurrentOldStyle) //eski stil
        {
            if (that.DocDateStart && that.DocDateStart.UTC &&
                that.DocDateEnd && that.DocDateEnd.UTC)
                rtn.push(that.formatDateYYYY(that.DocDateStart.UTC) + "-" + that.formatDateYYYY(that.DocDateEnd.UTC));
            else if (that.DocDateStart && that.DocDateStart.UTC)
                rtn.push(that.formatDateYYYY(that.DocDateStart.UTC) + "-20991231");
            else if (that.DocDateEnd && that.DocDateEnd.UTC)
                rtn.push("19010101-" + that.formatDateYYYY(that.DocDateEnd.UTC));

            if (that.ContentId != null && that.ContentId != undefined)
                idList.push(that.ContentId);
            if (that.ContentIdentifiers != null)
                idList = idList.concat(that.ContentIdentifiers);
            if (idList.length > 0)
                rtn.push("contentid:\"" + idList.join(",") + "\" ");
            idList = [];
            if ((isSubQuery || !that.IgnoreDefaultQuery) && that.Enumarators != null && that.Enumarators.length > 0)
                rtn.push("source:\"" + that.Enumarators.join(",") + "\" ");
            if (that.ContentDisplayNames != null && that.ContentDisplayNames.length > 0)
                rtn.push("content:\"" + that.ContentDisplayNames.join(",") + "\" ");
            if (that.ContentReaders != null && that.ContentReaders.length > 0)
                rtn.push("content:\"" + that.ContentReaders.join(",") + "\" ");
            if (that.ContentTypes != null && that.ContentTypes.length > 0)
                rtn.push("content:\"" + that.ContentTypes.join(",") + "\" ");
            if (that.DocParents != null && that.DocParents.length > 0)
                rtn.push("parentid:\"" + that.DocParents.join(",") + "\" ");
            if (that.DocParentsTree != null && that.DocParentsTree.length > 0)
                rtn.push("parentidtree:\"" + that.DocParentsTree.join(",") + "\" ");
            if (that.SimilarTo && that.SimilarTo.length && that.SimilarTo.length > 0)
                rtn.push("similarto:\"" + that.SimilarTo + "\" ");
            if (that.NearKeywords != null && that.NearKeywords.length > 0)
                rtn.push(EnumarateStringWidthFormat(that.NearKeywords, "\"{0}\"(" + that.NearDistance2 + ")").join(" INGNORESPLITTXT "));
            if (that.FullTextSearchKeywords != null && that.FullTextSearchKeywords.length > 0)
                rtn.push(that.FullTextSearchKeywords.join(" INGNORESPLITTXT "));
            if (that.IncludeKeywords != null && that.IncludeKeywords.length > 0)
                rtn.push(that.IncludeKeywords.join(" INGNORESPLITTXT "));
            if (that.ExcludeKeywords != null && that.ExcludeKeywords.length > 0)
                rtn.push(EnumarateStringWidthFormat(that.ExcludeKeywords, "-\"{0}\"").join(" INGNORESPLITTXT "));
            if (that.ItemIdentifier && that.ItemIdentifier.length > 0 && that.ItemIdentifier != "FS")
                idList.push(parseInt(that.ItemIdentifier));
            if (that.ItemIdentifiersMust != null)
                idList = idList.concat(that.ItemIdentifiersMust);
            if (idList.length > 0)
                rtn.push("keywordidmust:\"" + idList.join(",") + "\" "); //Must kalkabilir ??
            if (that.ItemIdentifiers != null && that.ItemIdentifiers.length > 0)
                rtn.push("keywordid:\"" + that.ItemIdentifiers.join(",") + "\" ");
            if (that.RecognizerIDs && that.RecognizerIDs.length > 0)
                rtn.push("layer:\"" + that.RecognizerIDs + "\" ");
            if (that.GeometryStatus == 2)
                rtn.push("geom");
            else if (that.GeometryStatus == 1)
                rtn.push("-geom");
            if (this.LabelStatus == 1)
                rtn.push("note");
            else if (this.LabelStatus == 2)
                rtn.push("-note");
        }
        if (ExQ && ExQ.length > 0)
            rtn.push(ExQ);
        if (that.SubQuery != null) {
            var lAdd = that.SubQuery.ToString(null, true, lvl + 1, ignoreSearchString);
            if (lAdd && lAdd.length > 0)
                rtn.push(lAdd);
        }
        var rtnstr;
        if (!ignoreSearchString && NotIgnoreCurrentOldStyle && that.SearchString && that.SearchString.length > 0) {
            if (rtn.length > 0) {
                rtn.unshift(that.SearchString + " and (");
                rtn.push(")");
                rtnstr = rtn.join(" ").trim();
            }
            else
                rtnstr = that.SearchString;
        }
        else
            rtnstr = rtn.join(" ").trim();
        if (that.SearchStrings && that.SearchStrings.length > 0) {
            if (rtnstr != undefined && rtnstr.length > 0)
                rtnstr = "(" + rtnstr + ") and ";
            rtnstr += "(" + that.SearchStrings.join(") and (") + ")";
        }
        if (that.SearchStringsHiden && that.SearchStringsHiden.length > 0) {
            if (rtnstr != undefined && rtnstr.length > 0)
                rtnstr = "(" + rtnstr + ") and ";
            rtnstr += "(" + that.SearchStringsHiden.join(") and (") + ")";
        }
        
        return rtnstr;
    }
    this.ResolveUrl = function (Url, addVersionCode, notAddValidator, options) {
        if (that.ServerUrl) {
            var rtn = null;
            if (options && options.FullUrl && window.URL && Url && Url.indexOf("~/") != -1 ) {
                rtn = Url.replace("~/", that.ServerUrl + "/");
                rtn = new URL(rtn, document.location.href).toString();
            }
            else
                rtn = Url.replace("~/", that.ServerUrl + "/");

            if ((!options || !options.IgnoreToken) && that.Token) {
                rtn += (rtn.indexOf("?") == -1 ? "?" : "&") + "UserSession=" + HttpUtility.UrlEncode(that.Token)
            }
            return rtn;
        }
        //if (!HttpUtility)
        return HttpUtility.ResolveUrl(Url, addVersionCode, notAddValidator);
    }

    this.GetKeywordGeom = function (KwId, callBackFunction, noasync, GetWKT) {

        return $.ajax({
            type: 'POST',
            url: that.ResolveUrl('~/GeodiJSONService?op=GetKeywordGeom', false, true),
            data: { 'KwId': KwId, 'wsName': that.wsName, 'GetWKT': GetWKT ? true : false },
            success: callBackFunction,
            dataType: "json",
            async: noasync ? false : true
        });

    }
    this.GetDocumentSummaries = function (callBackFunction, fillSummaryEntries) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getDocumentSummaries', false, true), { 'query': JSON.stringify(that), 'wsName': that.wsName, 'fillSummaryEntries': (fillSummaryEntries ? true : false) }, callBackFunction, 'json');
    }

    this.GetFirstMapLayerKeywords = function (bbox, epsg, callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getFirstMapLayerKeywords', false, true), { 'query': JSON.stringify(that), bbox: bbox, epsg: epsg, 'wsName': that.wsName }, callBackFunction, 'json');
    }





    this.Recognize = function (text, callBackFunction) {
        return $.post(that.ResolveUrl('~/DataExtractionHandler?op=Recognize', false, true), { 'Text': text, 'wsName': that.wsName, SortTextLength: true, AddFilterInfo: false }, callBackFunction, 'json');
    }
    this.SetDefaultQuery = function (callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=SetDefaultQuery', false, true), { 'query': JSON.stringify(that), 'wsName': that.wsName },
          callBackFunction
          , 'json');

    }
    this.GetDocumentsForKeywords = function (keywordIds, listIsDoc, callBackFunction, urlParams) {
        return that.GetDocumentsForKeywords2(keywordIds, listIsDoc, { urlParams: urlParams }, callBackFunction);
    }

    this.GetDocumentsForKeywords2 = function (keywordIds, listIsDoc, parameters, callBackFunction) {
        if (!parameters) parameters = {}; //parameters.Result="onlyname"//simple
        parameters.query = JSON.stringify(that);
        parameters.wsName = that.wsName;
        if (keywordIds)
            parameters.keywordIds = JSON.stringify(keywordIds);
        parameters.listIsDoc = listIsDoc ? true : false;
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getDocumentsForKeywords' + (parameters.urlParams ? '&' + parameters.urlParams : ''), false, true), parameters,
            callBackFunction
            , 'json');
    }

    this.GetDocuments = function (callBackFunction, UpdateDefaultQuery, UsageLog, parameters) {
        if (!parameters) parameters = {};
        if (UpdateDefaultQuery) parameters.UpdateDefaultQuery = true;
        if (UsageLog) parameters.UsageLog = true;
        return this.GetDocuments2(callBackFunction, parameters);
    }

    this.GetDocuments2 = function (callBackFunction, parameters) {
        if (!parameters) parameters = {};
        parameters.query = JSON.stringify(that);
        parameters.wsName = that.wsName;

        return $.ajax({
            type: "POST",
            url: that.ResolveUrl('~/GeodiJSONService?op=getDocuments', false, true),
            data: parameters,
            success: callBackFunction,
            dataType: 'json',
            OnAjaxError: function (a, b, c, d) {
                var rp = false, rp2=false;
                if (that._OnAjaxErrorInternal)
                    rp = that._OnAjaxErrorInternal(a, b, c, d, callBackFunction);
                if (that.OnAjaxError) {
                    rp2 = that.OnAjaxError(a, b, c, d, callBackFunction);
                    if (c.RunCallBack) {
                        callBackFunction([], {}, {})
                        rp2 = true;
                    }
                }
                return rp | rp2;
            }
        });
    }

    this.GetKeywordsForDocs = function (docs, listIsKW, callBackFunction, urlParams) {
        return that.GetKeywordsForDocs2(docs,listIsKW, {urlParams:urlParams}, callBackFunction);
    }

    this.GetKeywordsForDocs2 = function (docs, listIsKW, parameters, callBackFunction) {
        if (!parameters) parameters = {}; //parameters.Result="onlyname"//simple
        parameters.query = JSON.stringify(that);
        //query.FillOptions.Geometry/GeometryCenter supported
        parameters.wsName = that.wsName;
        if(docs)
            parameters.docs=JSON.stringify(docs);
        parameters.listIsKW = listIsKW ? true : false;
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getKeywordsForDocs'+ (parameters.urlParams ? '&' + parameters.urlParams : ''), false, true), parameters,
            callBackFunction
            , 'json');
    }
    this.GetDocumentCounts = function (callBackFunction, qureies,useThisQuery,useUserLastQuery) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getDocumentCounts', false, true), {
            'queries': JSON.stringify(qureies),
            'baseQuery': (useThisQuery ? JSON.stringify(that) : null),
            'UseUserLastQuery': (useUserLastQuery ? "true":"false"),
            'wsName': that.wsName
        },
            callBackFunction
            , 'json');
    }
    
    this.GetKeywords = function (callBackFunction, fillActions, UpdateDefaultQuery, UsageLog) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getKeywords', false, true), { 'query': JSON.stringify(that), 'wsName': that.wsName, 'fillActions': (fillActions ? true : false), 'UpdateDefaultQuery': (UpdateDefaultQuery ? true : false), 'UsageLog': (UsageLog ? true : false) },
            callBackFunction
            , 'json');
    }
    this.GetFacet = function (callBackFunction, UpdateDefaultQuery, LastWSScanKey, optimizeTarget) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getFacet', false, true), {
            'query': JSON.stringify(that),
            'OptimizeTarget': optimizeTarget ? optimizeTarget : "0",
            'wsName': that.wsName, 'UpdateDefaultQuery': (UpdateDefaultQuery ? true : false), 'LastWSScanKey': (LastWSScanKey ? LastWSScanKey : null)
        },
            function (a, b, c) { if (callBackFunction) callBackFunction(a,b,c,that)}
            , 'json');
    }
    this.GetWSRecognizers = function (callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getWSRecognizers', false, true), { 'wsName': that.wsName }, callBackFunction, 'json');
    }
    this.GetWSEnumarators = function (callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getWSEnumarators', false, true), { 'wsName': that.wsName }, callBackFunction, 'json');
    }

    this.GetWSInfo = function (IncludeInfo, callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=GetWSInfo', false, true), { 'wsName': that.wsName, "IncludeInfo": IncludeInfo ? IncludeInfo : 0 }, callBackFunction, 'json');
    }

    this.GetContentInfo = function (ContentId, RunUpdateClientData, callBackFunction, ParentInfoMaxLevel) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=GetContentInfo', false, true), {
            'wsName': that.wsName, "ContentId": ContentId,
            RunUpdateClientData: RunUpdateClientData ? true : false,
            ParentInfoMaxLevel: ParentInfoMaxLevel ? ParentInfoMaxLevel:null
        }, callBackFunction, 'json');
    }

    

    this.UpdateSearchStringField = function (fieldkey, value, hidden, disableAddSplitter,disableRemove) {
        var lstName = hidden ? "SearchStringsHiden" : "SearchStrings";
        var q = that[lstName];
        if (!q) q = [];
        var hasfield = false;
        if (typeof (fieldkey) == "string" && fieldkey.length>0) {
            hasfield = true;
            if (!disableAddSplitter && fieldkey.indexOf(":") == -1) fieldkey += ":";
            if(!disableRemove)
                for (var i = 0; i < q.length; i++) {
                    if (q[i].indexOf(fieldkey) != -1) {
                        q.splice(i, 1);
                        break;
                    }
                }
        }
        q.push((hasfield ? fieldkey : "") + value);
        that[lstName] = q;//.join(',');
    }


    this.SetIncludeKeywords = function (keys, getTextFunction) {
        var pLits = [];
        $.each(keys, function (idx, val) {
            if (getTextFunction)
                pLits.push(getTextFunction(idx, val));
            else
                pLits.push(val);
        })
        that.IncludeKeywords = pLits;//.join(',');
    }
    this.SetExcludeKeywords = function (keys, getTextFunction) {
        var pLits = [];
        $.each(keys, function (idx, val) {
            if (getTextFunction)
                pLits.push(getTextFunction(idx, val));
            else
                pLits.push(val);
        })
        that.ExcludeKeywords = pLits;//.join(',');
    }
    this.SetNearKeywords = function (keys, getTextFunction) {
        var pLits = [];
        $.each(keys, function (idx, val) {
            if (getTextFunction)
                pLits.push(getTextFunction(idx, val));
            else
                pLits.push(val);
        })
        that.NearKeywords = pLits;//.join(',');
    }
    this.GetMaxMinDocDate = function (callBackFunction) {
        return $.post(that.ResolveUrl('~/GeodiJSONService?op=getMaxMinDocDate', false, true), { 'query': JSON.stringify(that), 'wsName': that.wsName }, callBackFunction);
    }
    this.IsEmpty = function (IgnoreFulltext) {
        if (!IgnoreFulltext && that.SearchString && that.SearchString.length > 0)
            return false;
        if ((that.IncludeKeywords && that.IncludeKeywords.length > 0) || (that.ExcludeKeywords && that.ExcludeKeywords.length > 0) ||
            (that.NearKeywords && that.NearKeywords.length > 0) || (that.FullTextSearchKeywords && that.FullTextSearchKeywords.length > 0))
            return false;

        if (that.SearchStrings && that.SearchStrings.length > 0)
            return false;
        if (that.SearchStringsHiden && that.SearchStringsHiden.length > 0)
            return false;
        

        if (that.Enumarators && that.Enumarators.length > 0)
            return false;
        

        if (that.DocParents && that.DocParents.length > 0)
            return false;
        if (that.DocParentsTree && that.DocParentsTree.length > 0)
            return false;

        if (that.SimilarTo && that.SimilarTo.length > 0)
            return false;

        if (that.ItemIdentifier && that.ItemIdentifier.length > 0)
            return false;

        if (that.ItemIdentifiersMust != null && that.ItemIdentifiersMust.length > 0)
            return false;
        if (that.ItemIdentifiers != null && that.ItemIdentifiers.length > 0)
            return false;

        if (that.ContentDisplayNames && that.ContentDisplayNames.length > 0)
            return false;
        if (that.ContentId != null && that.ContentId != undefined)
            return false;
        if (that.ContentIdentifiers && that.ContentIdentifiers.length > 0)
            return false;
        if (that.RecognizerIDs)
            return false;
        if (that.RecognizerId)
            return false;
        if ((that.DocDateStart && that.DocDateStart.UTC) || (that.DocDateEnd && that.DocDateEnd.UTC))
            return false;
        if (that.GeometryStatus && that.GeometryStatus != 3)
            return false;
        if (that.LabelStatus)
            return false;
        //if (that.GetNumberOfOccurrences) return false;//artık gerekli değil.
        if (that.ContentReaders && that.ContentReaders.length > 0)
            return false;
        if (that.ContentTypes && that.ContentTypes.length > 0)
            return false;
        if (that.SubQuery)
            return that.SubQuery.IsEmpty();
        return true;
    }
}

/*INFO*/
function GeodiWSInfo(wsName, pWsData, createState) {
    this.createState = createState;
    this.wsName = wsName;
    this.queryObj = new GeodiQuery();
    this.queryObj.wsName = this.wsName;
    this.Recognizers = null;
    this.Enumarators = null;
    this.ContentReaders = null;
    this.EnumaratorPermissions = null;
    this.GroupContentReaders = null;
    this.EnuratorIdRefrence = {};
    this.ContentTypeRefrence = {};
    var that = this;
    this.GetEnumratorTitleFromId = function (id) {
        if (that.EnuratorIdRefrence && that.EnuratorIdRefrence[id])
            return that.EnuratorIdRefrence[id].DisplayName;
        return "";
    }
    this.GetContentTypeTitleFromId = function (contenttype) {
        if (that.ContentTypeRefrence && that.ContentTypeRefrence[contenttype])
            return that.ContentTypeRefrence[contenttype].Category.Text;
        return "";
    }
    this.GetWS = function (wsName) {
        if (that.Workspaces)
            for (var i = 0; i < that.Workspaces.length; i++)
                if (that.Workspaces[i].Name == wsName)
                    return that.Workspaces[i];
        return null;
    }
    this.GetRecognizerIconFromId = function (id) {
        if (id == "-1" || id == "0")
            return "FullTextSeacrhIco";
        if (that.RecognizerIdReference && that.RecognizerIdReference[id])
            return that.RecognizerIdReference[id].IconName;
        return "";
    }
    this.GetEnumeratorGenericSetting = function (id,key) {
        if (that.EnuratorIdRefrence[id] && that.EnuratorIdRefrence[id].ClientGenericSettings)
            return that.EnuratorIdRefrence[id].ClientGenericSettings[key];
        return null;
    }
    this.GetRecognizerGenericSetting = function (id, key) {
        if (that.RecognizerIdReference[id] && that.RecognizerIdReference[id].ClientGenericSettings)
            return that.RecognizerIdReference[id].ClientGenericSettings[key];
        return null;
    }
    this.GetWsGenericSetting = function (wsName, key) {
        var ws = that.GetWS(wsName);
        if (ws && ws.ClientGenericSettings)
            return ws.ClientGenericSettings[key];
        return null;
    }
    this._Load = function (data) {
        that.Workspaces = data.Workspaces;
        that.Languages = data.Languages;
        that.Dashboards = data.Dashboards;
        that.DynamicDashboards = data.DynamicDashboards;
        that.Themes = data.Themes;
        that.Recognizers = data.Recognizers;
        that.RecognizerIdReference = {};
        if (that.Recognizers)
            for (var j = 0; j < that.Recognizers.length; j++)
                that.RecognizerIdReference[that.Recognizers[j].ItemHashCode] = that.Recognizers[j];


        that.Enumarators = data.Enumarators;
        that.EnumaratorPermissions = data.EnumaratorPermissions;
        that.EnuratorIdRefrence = {};
        if (that.Enumarators)
            for (var j = 0; j < that.Enumarators.length; j++)
                that.EnuratorIdRefrence[that.Enumarators[j].ItemHashCode] = that.Enumarators[j];


        that.ContentReaders = data.ContentReaders;
        that.GroupContentReaders = {
            NamesOrder: [],
            Names: {},
            OtherCategory: null
        };
        that.ContentTypeRefrence = {};
        for (var j = 0; j < that.ContentReaders.length; j++) {
            var item = that.ContentReaders[j];
            var inItem = null;
            var txt = "";
            if (item.Category) {
                var txt = item.Category.Text ? item.Category.Text : ""; //&& item.Category.Text
                if (!that.GroupContentReaders.Names[txt]) {
                    that.GroupContentReaders.Names[txt] = {
                        SubList: [],
                        AllTyes: [],
                        Text: txt,
                        Category: item.Category
                    };
                    that.GroupContentReaders.NamesOrder.push(txt);
                }

            }

            inItem = that.GroupContentReaders.Names[txt];
            if (!inItem) continue;
            inItem.SubList.push(item);
            var itemTypes = item.ContentTypeKeys;
            if (itemTypes && itemTypes.length > 0)
                for (var k = 0; k < itemTypes.length; k++) {
                    var ItemId = itemTypes[k];
                    if (ItemId == ":.?") {
                        item.Category.Text = item.DisplayName;
                        that.GroupContentReaders.OtherCategory = item;
                        item.SubList = [item];
                    }
                    that.ContentTypeRefrence[ItemId] = item;
                    if (inItem.AllTyes.indexOf(ItemId) < 0)
                        inItem.AllTyes.push(ItemId);
                }

        }
        if (window.OnloadWSInfoObj)
            window.OnloadWSInfoObj(this);
    }

    this.Reload = function (wsData) {
        //callbakc?
        that.FullFacet = wsData.FullFacet;//null allow!!!
        that.Enumarators = null;
        that.EnumaratorPermissions = null;
        that.Recognizers = null;
        that.ContentReaders = null;
        that.GroupContentReaders = null;
        that.EnuratorIdRefrence = {};
        that.RecognizerIdReference = {};
        that.ContentTypeRefrence = {};
        that.Workspaces = null;
        that.Languages = null;
        that.Dashboards = null;
        that.DynamicDashboards = null;
        that.Themes = null;
        if (wsData)
            that._Load(wsData);
        else
            that.queryObj.GetWSInfo(0, function (data, textStatus) {//??
                that._Load(data);
            });

    }
    this.Reload(pWsData);
    $(window).on("PCCommand",
         function (e, obj) {
             if ((obj.Command == "WSScanEnd" && obj.Arg == that.queryObj.wsName) ||
                 (obj.Command == "WSUpdated" && obj.wsName == that.queryObj.wsName)) {
                 that.Reload();
                 //changed
             }
         });
}

/*GUI Help*/

function GetIsDocElement(elem, cssClassName) {
    if (!cssClassName) cssClassName = "IsDoc";
    var el = $(elem);
    var p = el.parents("." + cssClassName);
    if ((!p || p.length == 0) && el.hasClass(cssClassName))
        return el;
    return p;
}

window.___DomStartDate = new Date();
function __CallOpenDoc(obj, e) {
    var itm = $(".openLink", GetIsDocElement(obj));
    if (itm.length > 0)
        itm[0].click(itm[0], e);
    else {
        itm = $(".docOpenLink", $(obj).parents(".docOpenLinkRoot"));
        if (itm.length > 0)
            itm[0].click(itm[0], e);
    }
}

function SummaryGUIHelper() {
    /*helper*/ //GE ???
    this.prepareActionUrl = function (url, info, callback) {
        //info : Keyword, ItemIdentifier, wsName, SummaryId,
        var session = (window.Auth && window.Auth.Session) ? window.Auth.Session : "";
        var tojs = function (txt) {
            if (txt)
                return txt.replace(/\\/, '\\\\').replace(/\r/, '\\\r').replace(/\n/, '\\\n').replace(/'/, '\\\'').replace(/"/, '\\\"')
            return "";
        }

        url = url.replace(/\[Session\]/g, session);

        if (info.Keyword) {
            url = url.replace(/\[Keyword\]/g, HttpUtility.UrlEncode(info.Keyword));
            url = url.replace(/\[Keyword_js\]/g, tojs(info.Keyword));
        }
        if (info.Keyword) {
            url = url.replace(/\[Keyword_Extra\]/g, HttpUtility.UrlEncode(info.Keyword_Extra));
            url = url.replace(/\[Keyword_Extra_js\]/g, tojs(info.Keyword_Extra));
        }

        if (info.ItemIdentifier!=null) {
            url = url.replace(/\[KeywordId\]/g, HttpUtility.UrlEncode(info.ItemIdentifier));
            url = url.replace(/\[KeywordId_js\]/g, tojs(info.ItemIdentifier));
        }

        if (info.wsName) {
            url = url.replace(/\[wsName\]/g, HttpUtility.UrlEncode(info.wsName));
            url = url.replace(/\[wsName_js\]/g, tojs(info.wsName));
        }

        if (info.SummaryId) {
            url = url.replace(/\[SummaryId\]/g, HttpUtility.UrlEncode(info.SummaryId));
            url = url.replace(/\[SummaryId_js\]/g, tojs(info.SummaryId));
        }
        if (info.ContentId!=null) {
            url = url.replace(/\[ContentId\]/g, HttpUtility.UrlEncode(info.ContentId));
            url = url.replace(/\[ContentId_js\]/g, tojs(info.ContentId));
        }
        if (info.ParentID!=null) {
            url = url.replace(/\[ParentId\]/g, HttpUtility.UrlEncode(info.ParentID));
            url = url.replace(/\[ParentId_js\]/g, tojs(info.ParentID));
        }
        if (info.EnumID != null) {
            url = url.replace(/\[EnumId\]/g, HttpUtility.UrlEncode(info.EnumID));
            url = url.replace(/\[EnumId_js\]/g, tojs(info.EnumID));
        }
        if (info.ContentName) {
            url = url.replace(/\[ContentName\]/g, info.ContentName);
            url = url.replace(/\[ContentName_js\]/g, tojs(info.ContentName));
        }

        url = url.replace(/\[UrlRoot\]/g, $('#UrlRoot').attr("value"));

        if (url.indexOf("[BBOX") != -1 || url.indexOf("[Center") != -1 ||
            url.indexOf("[WKT]") != -1 || url.indexOf("[WKT_js]") != -1 ||
            url.indexOf("[BBOXInverse]") != -1 || url.indexOf("[CenterInverse]") != -1) {

            var lquery = new GeodiQuery();
            if (info.wsName)
                lquery.wsName = info.wsName;

            var dataCheck = function (data) {
                if (data && data.BBOX) {
                    url = url.replace(/\[BBOX\]/g, data.BBOX[0] + "," + data.BBOX[1] + "," + data.BBOX[2] + "," + data.BBOX[3]);
                    url = url.replace(/\[BBOX.0\]/g, data.BBOX[0]);
                    url = url.replace(/\[BBOX.1\]/g, data.BBOX[1]);
                    url = url.replace(/\[BBOX.2\]/g, data.BBOX[2]);
                    url = url.replace(/\[BBOX.3\]/g, data.BBOX[3]);
                }
                if (data && data.CENTER) {
                    url = url.replace(/\[Center\]/g, data.CENTER[0] + "," + data.CENTER[1]);
                    url = url.replace(/\[Center.X\]/g, data.CENTER[0]);
                    url = url.replace(/\[Center.Y\]/g, data.CENTER[1]);
                }

                if (data && data.BBOX)
                    url = url.replace(/\[BBOXInverse\]/g, data.BBOX[1] + "," + data.BBOX[0] + "," + data.BBOX[3] + "," + data.BBOX[2]);
                if (data && data.CENTER)
                    url = url.replace(/\[CenterInverse\]/g, data.CENTER[1] + "," + data.CENTER[0]);

                if (data && data.WKT) {
                    url = url.replace(/\[WKT\]/g, data.WKT);
                    url = url.replace(/\[WKT_js\]/g, tojs(data.WKT));
                }

                callback(url);
            }
            if (url.indexOf("popupCoord") != -1 && window.StateForm.GetState('ismaprequest') == '1') {
                try {
                    var coord = window.parent.lastPopupCoord;
                    if (coord) {

                        window.parent.UpdateProjectionXY(coord, window.parent.view.getProjection().getCode(), "EPSG:4326",
                                              function (pos0) {
                                                  var wkt = window.parent.toWKTPoint(pos0);
                                                  var data = {
                                                      BBOX: [pos0[0], pos0[1], pos0[0], pos0[1]],
                                                      CENTER: pos0,
                                                      WKT: wkt
                                                  }
                                                  dataCheck(data);
                                              }
                                              );
                        return;

                    }
                }
                catch (err) {
                    err = err;
                }
            }

            lquery.GetKeywordGeom(info.ItemIdentifier, dataCheck, (!window.BrowserType || !window.BrowserType.IsWinApp()), url.indexOf("[WKT") != -1);//] !
        }
        else
            callback(url);
    }
    this.prepareActionRun = function (url, info, options) {
        if (window.CancelEvent) window.CancelEvent();
        if (!options) options = {};
        return this.prepareActionUrl(url, info, function (url) {
            window.SummaryGUI.RunURL(url, options);
        });
        if (options.target != "javascript" && options.target != "hide")
            return false;
    }
    this.RunURL = function (url, options) {
        //GE
        if (!options) options = {};
        if (options.target == "javascript") {
            eval(url);

        }
        else if (options.target == "hide") {
            if ($("#__linker").length == 0) {
                $(document.body).append($("<iframe id=__linker style='position:absolute;width:10px;height:10px;left:-1000px;top:-1000px;'></iframe>"));
            }
            $("#__linker").attr("src", HttpUtility.ResolveUrl(url));
        }
        else {
            var targetName = options.title;
            _PSCC.Command({ Command: 'OpenWindow', Url: url, Name: targetName, MinWidth: 400, MinHeight: 400, Width: 800, Height: 600, ExecuteAsync: false, isInApp: options.target != "InApp" }, null, event);
        }
        return false;
    }

    this.UpdateSummaryDOM = function (summaryDOM, activeData, wsName, httpUtilityOwner) {
        /*DateTimeFormat*/
        var fncCheck = function (dtDomLst, formatcallback) {
            $.each(dtDomLst, function (index, dom) {
                dom = $(dom);
                var val = dom.attr("data-value");
                if (val) {
                    var dt = null;
                    try { dt = new Date(val); } catch (e) { }
                    if (!dt || !dt.getTime || isNaN(dt.getTime()))
                        try { dt = new Date(val.replace(/[.]/g,"/")); } catch (e) { }
                    if (dt)
                        formatcallback(dom, dt);
                }
            });
        }
        if (typeof (moment) != "undefined") {
            fncCheck($(".datetime", summaryDOM), function (dom, dt) { dom.html(dom.html() + "<div class='sub'>" + moment(dt).fromNow() + "</div>"); });
            fncCheck($(".simpledatetime", summaryDOM), function (dom, dt) { dom.html(moment(dt).format('L') + " - <span class='sub'>" + moment(dt).fromNow() + "</span>"); });
        }
        else {
            fncCheck($(".datetime", summaryDOM), function (dom, dt) { if (dt) dom.html(dt.toLocaleDateString()); });
            fncCheck($(".simpledatetime", summaryDOM), function (dom, dt) { if (dt) dom.html(dt.toLocaleString()); });
        }


        dtDomLst = $(".ThumbContentRef", summaryDOM);
        $.each(dtDomLst, function (index, dom) {
            dom = $(dom);
            var targetDoc = dom.attr("data-docid");
            if (!(targetDoc >= 0))
                targetDoc = activeData.ContentIdentifier;
            var imgDom =
                //activeData && activeData.ContentType == "foldercontent:.folder" ?
                //$('<img onerror="this.style.display=\'none\'" src="' + (httpUtilityOwner ? httpUtilityOwner : HttpUtility).ResolveUrl('~/GeodiJSONService?op=getContentThumbnailWithQuery&query=parentid:' + targetDoc + '&noPreviewThrowError=true&MaxCount=4&wsName=' + HttpUtility.UrlEncode(wsName) + '&rnd=' + window.___DomStartDate, false) + '" onclick=__CallOpenDoc(this,event) style="cursor:pointer">') :
                $('<img src="' + (httpUtilityOwner ? httpUtilityOwner : HttpUtility).ResolveUrl('~/GeodiJSONService?op=getContentThumbnail&ContentIdentifier=' + targetDoc + '&wsName=' + HttpUtility.UrlEncode(wsName) + '&rnd=' + window.___DomStartDate, false) + '" onclick=__CallOpenDoc(this,event) style="cursor:pointer">');

            dom.html('');
            var val = dom.attr("data-value");
            if (val) {
                var splPos = val.split(',');
                if (splPos.length == 2) {
                    var px = splPos[0];
                    var py = splPos[1];
                    if (px != "-1" && py != "-1") { //line?

                        imgDom.load(function () {
                            var imgInLoad = $(this);
                            px = imgInLoad.width() * px;
                            px -= 15;
                            py = imgInLoad.height() * py;
                            py += 10;
                            dom.append('<div style="position:absolute;margin-left:' + px + 'px;margin-top:-' + py + 'px;width: 20px;height: 20px;background-color: #FFF;border:4px solid red;border-radius: 10px 10px 10px 10px;opacity: 0.8;">&nbsp;</div>');
                        })

                    }
                }

            }

            dom.append(imgDom);

        });
    }

    this.escapeRegExp = function (string) {
        return string ? string.replace(/([*+?^=!:{}()|\[\]\/\\])/g, "\\$1").replace(/ /g, " ").replace("$", "[$]").replace(".", "[.]") : string;
    }
    this.ReplaceAllKeywords = function (src, key, rplc) {
        if (!key)
            return src;
        key = key.trim();
        if (key.length==0)
            return src;
        var allIsWord = key == null || key.replace(/\b.*\b/, "").trim().length == 0;
        return src.replace(/&nbsp;/g, " ").replace(new RegExp(("(^|[\\" + (allIsWord ? "W" : "s") + "]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+)") + "(" + this.escapeRegExp(key) + ")" + "($|[\\" + (allIsWord ? "W" : "s")+"]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+)", 'gi'), rplc);
    }

    this.Formatter = function (element, wsName) {
        if (!wsName) wsName = _GetCurrentName();
        var query = new GeodiQuery();
        element = $(element);
        query.Recognize(element.text(), function (dataIn) {
            window.SummaryGUI.ApplyFormatter({}, element, dataIn.Entries);
        });
    }
    this.ApplyFormatter = function (activeData, summaryDOM, entries) {
        if (summaryDOM.hasClass("noformatter")) return;
        var childs = summaryDOM.children();
        var allIsBrOrEmpty = true;
        if (childs.length != 0)
            for (i = 0; i < childs.length; i++)
                if (childs[i].tagName != "BR" && childs[i].tagName != "br") {
                    allIsBrOrEmpty = false;
                    break;
                    }

        if (allIsBrOrEmpty && (!summaryDOM.attr("class") || summaryDOM.attr("class").indexOf("ighlight") == -1)) {
            var txtSummary = summaryDOM.html();
            if (txtSummary) {
                txtSummary = " " + txtSummary + " ";
                if (entries) {
                    var rplist = {};
                    for (var ix = 0; ix < entries.length; ix++)
                        if ((activeData.Keyword == entries[ix].Text || activeData.Keyword == entries[ix].OriginalText) && !rplist[entries[ix].OriginalText]) {
                            rplist[entries[ix].OriginalText] = true;
                            txtSummary = this.ReplaceAllKeywords(txtSummary, entries[ix].OriginalText, "$1<span class='highlight'>$2</span>$3"); //+ entries[ix].OriginalText +
                        }
                    for (var ix = 0; ix < entries.length; ix++)
                        if (activeData.Keyword != entries[ix].Text && !rplist[entries[ix].OriginalText]) {
                            rplist[entries[ix].OriginalText] = true;
                            txtSummary = this.ReplaceAllKeywords(txtSummary, entries[ix].OriginalText, "$1<span class='lightHighlight'>$2</span>$3"); // + entries[ix].OriginalText +
                        }
                    summaryDOM.html(txtSummary);
                }

                txtSummary = this.ReplaceAllKeywords(txtSummary, activeData.Keyword, "$1<span class='highlight'>$2</span>$3"); // + activeData.Keyword +
                summaryDOM.html(txtSummary);

            }
        }

        for (var ch = 0; ch < childs.length; ch++) {
            var jobj = $(childs[ch]);
            this.ApplyFormatter(activeData, jobj, entries);
        }
    }
}

window.SummaryGUI = new SummaryGUIHelper();

/*Render-Template?*/

window.__DLVsummaryActionClick = function (sender) {

    var info = new Object();
    var prnt = $(sender).parents("[isAttContainer]");
    if (prnt.length > 0) prnt = $(prnt[0]);
    info.Keyword = prnt.attr('keyword');
    info.ItemIdentifier = prnt.attr('ItemIdentifier');
    info.wsName = _GetCurrentName(); //options!
    info.SummaryId = prnt.attr("summaryid");
    info.ContentId = $(sender).attr("contentid"); //GetIsDocElement(sender).attr("contentid");

    var titleData = prnt.data("title_data");
    if (!titleData)
        titleData = $(sender).parents('.pnl-query-item').data("title_data");
    if (!titleData)
        titleData = $(sender).parents('.docDetail').find('.divCurrentDocumentInfo').data("title_data");

    if (titleData)
        info.ContentName = titleData.txt;
    else
        info.ContentName = $(".openLink", prnt).text();

     
    info.ParentID = (titleData && titleData.all && titleData.all.AdditionalValues) ? titleData.all.AdditionalValues.PId : null;
    info.EnumID = (titleData && titleData.all && titleData.all.EnumaratorId) ? titleData.all.EnumaratorId+"" : null;
    
    var url = $(sender).attr('href');

    return window.SummaryGUI.prepareActionRun(url, info, {
        target: $(sender).attr('target'),
        title: $(sender).attr('title'),
        targetContent: $(sender).attr("contentid")
    })
}
window.__KLVVsummaryActionClick = function (sender) {
    var info = new Object();
    info.Keyword = $(sender).parents('.pnl-query-item').find('.divPanelTitleText').text();
    info.wsName = _GetCurrentName();/*options!*/
    var itemData = $(sender).parents('.pnl-query-item').data('item-data');
    if (itemData) { //itemdataextend
        info.ItemIdentifier = itemData.ItemIdentifier;
        if (itemData.Keyword) info.Keyword = itemData.Keyword;
        if (itemData.Keyword_Extra) info.Keyword_Extra = itemData.Keyword_Extra;
    }

    info.SummaryId = $(sender).attr("summaryid");
    info.ContentId = GetIsDocElement(sender).attr("contentid");
    info.ContentName = " ";
    var dInfo = $(sender).parents('.docDetail').find('.divCurrentDocumentInfo')
    if (!info.ContentId) {
        
        info.ContentId = dInfo.attr('docid')
        info.ContentName = dInfo.text();
    }
    var titleData = dInfo.data("title_data");
    info.ParentID = (titleData && titleData.all && titleData.all.AdditionalValues) ? titleData.all.AdditionalValues.PId : null;
    info.EnumID = (titleData && titleData.all && titleData.all.EnumaratorId) ? titleData.all.EnumaratorId+"" : null;
    var url = $(sender).attr('href');
    return window.SummaryGUI.prepareActionRun(url, info,
        {
            target: $(sender).attr('target'),
            title: $(sender).attr('title')
        });
}



function ShowOrDownloadDoc(elem, e, advanceSupportPanel) {
    e.stopPropagation();
    var url = $(elem).attr("curl");
    var forDownload = url ? (url.indexOf('op=downloadDoc') != -1 || url.indexOf(':downloadDoc') != -1) : false;
    if (!(e && e.ctrlKey && url && !forDownload)) {
        if (advanceSupportPanel) {
            $(".ItemSelectedForAdvRight").removeClass("ItemSelectedForAdvRight");
            $(advanceSupportPanel).css("opacity", 0);
        }
        if (advanceSupportPanel && $(advanceSupportPanel).is(":visible")) {
            $(advanceSupportPanel).removeClass("quickContentViewerParentDiv");
            var hArgs = {
                elem: elem,
                e: e,
                panel: advanceSupportPanel,
                handled: false
            };
            $(document.body).trigger("ShowOrDownloadClick", hArgs);
            if (hArgs.handled)
                return;
        }
    }
    
    if (url) {
        if (forDownload) {
            var contentId = $(elem).attr("contentid");
            downloadDoc(contentId);
        }
        else {
            showDoc(elem, e);
        }
    }
}
function showContent(content_id, targetwindowName) {
    var url = HttpUtility.ResolveUrl('~/DefaultContentViewer?content_id=' + content_id + '&wsName=' + HttpUtility.UrlEncode(_GetCurrentName())/*options!*/ + _GetDfBBOX(), false, true);
    var targetName = targetwindowName ? targetwindowName : 'parentContentViewer';
    if (targetName == "?") targetName = null;
    return _PSCC.Command({ Command: 'OpenWindow', Url: url, Name: targetName, MinWidth: 400, MinHeight: 400, Width: 800, Height: 600, ExecuteAsync: false }, null, GetEvent());

}
function showDoc(elem, e) {

    var p = GetIsDocElement(elem); //parent

    e.stopPropagation();
    var url = $(elem).attr("curl");
    var targetName = $(elem).attr("target");
    var targetContent = $(elem).attr("contentid");
    var disableNewWindow = $(elem).attr("DisableNewWindow");
    


    if (window.BrowserType && window.BrowserType.IsWinApp())
        targetContent = "docView";

    var Text, Ico1, Ico2;
    var titleData = $(elem).parents('.pnl-query-item').data("title_data");
    if (!titleData)
        titleData = $(elem).parents('.docDetail').find('.divCurrentDocumentInfo').data("title_data");

    if (!titleData)
        titleData = $("#inRightPanelForDoc").data("title_data");


    if (titleData) {
        Text = titleData.txt;
        if (titleData.ctyp && titleData.ctyp !="file_db:geodi_row")
            Ico1 = HttpUtility.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + titleData.ctyp, true, true);
        if (titleData.enm)
            Ico2 = HttpUtility.ResolveUrl('~/GeodiJSONService?op=getEnumaratorIco&ID=' + titleData.enm + '&wsName=' + HttpUtility.UrlEncode(_GetCurrentName()/*options!*/, true, true));
    }


    var cq = p.attr("cq");
    var cp = p.attr("cp");
    if (cq || cp) {
        if (url.indexOf("#") == -1) url += "#";
        if (cq) url += "&cq=" + HttpUtility.UrlEncode(cq);
        if (cp) url += "&cp=" + HttpUtility.UrlEncode(cp);
        if (p.attr("cr"))
            url += "&cr=1";
    }


    if (e.ctrlKey && !window.DisableQuickContentViewer && $("#inRightPanelForDoc").is(":visible")) {

        if ("filecontent:.html" != p.attr("ct") && "filecontent:.html" != p.attr("pct")) {

            if ($("#inRightPanelForDoc").css("opacity") == "0")
                $("#inRightPanelForDoc").css("opacity", 1);

            if ($(".quickContentViewerFrame").is(":visible")) {
                if ($(".quickContentViewerFrame").attr("srcOrj") == url) {
                    CancelEvent(e);
                    return;
                }
            }


            if (!$("#inRightPanelForDoc").hasClass("quickContentViewerParentDiv")) {
                var $contentDiv = $("<div/>", { style: "display: none" });
                $contentDiv.append($("#inRightPanelForDoc").children());

                $("#inRightPanelForDoc").empty().append($contentDiv);
                $("#inRightPanelForDoc").addClass("quickContentViewerParentDiv");
                $("#inRightPanelForDoc").append('<button type="button" class="qContentViewerCloseButton inCloseBtn rb" onclick="closeQuickContentViewer();CancelEvent(event);" ontouchend="window.setTimeout(function() {closeQuickContentViewer()},0);CancelEvent(event);" ><span aria-hidden="true">×</span></button>');
                var $iFrame = $("<iframe/>", { srcOrj: url, src: url, class: "quickContentViewerFrame" });
                $("#inRightPanelForDoc").append($iFrame);



            }
            else {
                $(".quickContentViewerFrame").attr("srcOrj", url);
                $(".quickContentViewerFrame").attr("src", url);
            }

            if ($(".quickContentViewerFrame")[0]) {
                var sInterval = setInterval(function () {
                    var fR = $(".quickContentViewerFrame");
                    if (!fR || fR.length == 0) return;
                    var frameDoc =  $(".quickContentViewerFrame")[0].contentDocument || $(".quickContentViewerFrame")[0].contentWindow.document;
                    if ($(frameDoc).find("#liHelp").length > 0) {
                        $(frameDoc).find("#liHelp").remove();
                        clearInterval(sInterval);
                    }

                }, 200);


            }
            CancelEvent(e);
            return;
        }
    }

    if ((disableNewWindow== "" || disableNewWindow == undefined) && window.DisableNewWindowGlobal)
        DisableNewWindow = true;

    return _PSCC.Command({ Command: 'OpenWindow', DisableNewWindow:(disableNewWindow && !e.ctrlKey ? true:false) , Url: url, Name: e.ctrlKey ? "" : targetName, NameIdGroup: e.ctrlKey ? "" : targetContent, MinWidth: 400, MinHeight: 400, Width: 800, Height: 600, ExecuteAsync: false, Title: Text, Ico: Ico1, Ico2: Ico2 }, null, e);
}
function closeQuickContentViewer() {
    if (!$("#inRightPanelForDoc").hasClass("quickContentViewerParentDiv"))
        return;
    $("#inRightPanelForDoc > button.qContentViewerCloseButton").remove();
    $("#inRightPanelForDoc > iframe").remove();
    $("#inRightPanelForDoc").removeClass("quickContentViewerParentDiv");
    var v = $("#inRightPanelForDoc > div").eq(0).children();
    if (v.length == 0)
        $("#inRightPanelForDoc").css("opacity", 0);
    $("#inRightPanelForDoc").append(v);
}
function downloadDoc(contentId,e) {

    var wsname = _GetCurrentName()/*options!*/;
    if (window.BrowserType && window.BrowserType.IsGoogleEarth()) {
        var bs64Test = btoa(encodeURIComponent(wsname).replace(/%([0-9A-F]{2})/g, function (match, p1) { return String.fromCharCode('0x' + p1); }));
        wsname = ".b64." + bs64Test;
    }
    var url = "~/GeodiJSONService?op=downloadDoc&";
    if (window.GetEvent)
        e = GetEvent(e);

    if (!e || !e.shiftKey)//shift tıklanmamışsa yedekteki orjinali indir.
        url += "&ForceFromBackup=1&";

    return _PSCCDownload(

        window.HttpUtility.ResolveUrl(url+"DisableChangeContentForViewer=1&"), {
            __mode: "OpenContent",
            content_id: contentId,
            wsName: wsname
        }
    );
}

function openDocOnBrowser(contentId,url,e) {//url eklenecek  

    var wsname = _GetCurrentName()/*options!*/;
    if (window.BrowserType && window.BrowserType.IsGoogleEarth()) {
        var bs64Test = btoa(encodeURIComponent(wsname).replace(/%([0-9A-F]{2})/g, function (match, p1) { return String.fromCharCode('0x' + p1); }));
        wsname = ".b64." + bs64Test;
    }
    url = url.replace("%wsName%", HttpUtility.UrlEncode(wsname));
    url = url.replace("%content_id%", HttpUtility.UrlEncode(contentId));
    return _PSCC.Command({ Command: 'OpenWindow', Url: url , Name: new Date() }, null, e);

}

function OpenDocFolder(contentId, e) {
    var wsname = _GetCurrentName()/*options!*/;
    if (window.GetEvent)
        e = GetEvent(e);
    if (window.BrowserType && window.BrowserType.IsGoogleEarth())
        wsname = window.HttpUtility.ToBase64(wsname);
    return _PSCC.Command({ Command: 'OpenContentFolder', shiftKey: e && e.shiftKey ? "1" : "0", ContentID: contentId, WsName: wsname });
}


function DestroyChart(canvasId) {
    try {
        var cnv = document.getElementById(canvasId);
        if (cnv && cnv.chart)
            cnv.chart.destroy();
    }
    catch (e) { }
}
function CreateChart(canvasId, typ, keysort, data, labelgetter, clickFnc, ForTimeLine, extStyle, extOptions, runclickFncWithEvent) {
    var cnv = document.getElementById(canvasId);
    var dt = [];
    $.each(data, function (key, value) {
        if (key != "min" && key != "max") {
            var key2 = labelgetter ? labelgetter(key) : key;
            dt.push({
                label: key2,
                rkey: key,
                bg: ForTimeLine ? null : GetColorFor(key, true),
                val: value
            })
        }
    });
    if (!ForTimeLine) {
        dt.sort(function (a, b) { return a.val - b.val });

        if (dt.length > 12) {
            dt[0].label = [dt[0].label];
            for (var i = 1; i < dt.length - 12; i++) {
                dt[0].label.push(dt[i].label);
                dt[0].val += dt[i].val;
            }
            dt[0].label.sort();
            if (dt[0].label.length > 10) {
                dt[0].label.splice(10, dt[0].label.length - 10);
                dt[0].label.push("...");
            }

            dt.splice(1, dt.length - 11);
        }
    }

    if (keysort)
        dt.sort(function (a, b) {
            if (a.rkey < b.rkey) return -1;
            if (a.rkey > b.rkey) return 1;
            return 0;
        });
    var labels = [];
    var values = [];
    var bg = [];
    var brdW = 2;
    var brd = "rgba(75, 192, 192,0.5)";
    $.each(dt, function (key, value) {
        labels.push(value.label);
        values.push(value.val);
        if (!ForTimeLine)
            bg.push(value.bg);
    });

    if (ForTimeLine) {
        brdW = 4;
        brd = "rgba(54, 162, 235,0.9)"
    }
    delete dt;

    var ctx = cnv.getContext('2d');
    var chartData = {
        type: typ ? typ : 'pie',//doughnut
        data: {
            labels: labels,
            datasets: [
                 {
                     label: "[$dictionary_wizard:content]",
                     data: values,
                     backgroundColor: bg,
                     borderColor: brd,
                     borderWidth: brdW,
                     pointRadius: 0,
                     fill: ForTimeLine ? false : true
                 }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            tooltip: {
                enabled: false,
            },
            elements: {
                tension: 0.001
            },
            legend: {
                display: false
            },
            animation: {
                animateRotate: true,
                animateScale: true
            },
            onClick: function (e, d) {
                if (clickFnc) {
                    if (runclickFncWithEvent)
                        clickFnc(e);
                    else if (d && d.length > 0) {
                        d = d[0];
                        var lbl = d._chart.data.labels[d._index];
                        clickFnc(lbl);
                    }
                }
            }

        }
    };


    if (extStyle)
        $.extend(chartData.data.datasets[0], extStyle);
    if (extOptions)
        $.extend(chartData.options, extOptions);
    if (ForTimeLine) {
        chartData.data.datasets[0].type = "line";
        if (window.DefaultChartTimelineStyle)
            $.extend(chartData.data.datasets[0], window.DefaultChartTimelineStyle);
        if (window.DefaultChartTimelineOptions)
            $.extend(chartData.options, window.DefaultChartTimelineOptions);
    }
    else {
        if (window.DefaultChartStyle)
            $.extend(chartData.data.datasets[0], window.DefaultChartStyle);
        if (window.DefaultChartOptions)
            $.extend(chartData.options, window.DefaultChartOptions);
    }

    cnv.chart = new Chart(ctx, chartData);
}

function QueryTimeToDate(str) {
    var y = str.substr(0, 4),
       m = str.substr(4, 2) - 1,
       d = str.substr(6, 2);
    return new Date(y, m, d);
}
function AnalyzQueryForTime(query, options, callback) {
    if (!options) options = {};
    var result = {};
    result._yearsDataSource = {};
    result._monthsDataSource = {};
    result._daysDataSource = {};
    result._monthsAllDataSource = {};
    if (options.collectAllDate)
        result.dayAllDataSource = {};

    result.datasource = [];
    var cntGet=query.GetNumberOfOccurrences;
    var _fli_cnt = options.GetFieldName ? 4 : 3;
    var lastSh = query.GetKeywordsForDocs2([null], false, { GetFieldName: options.GetFieldName ? true : false, LogActionName: options.LogActionName}, function (data, textStatus, request) {
        if (options.Aborter)
            options.Aborter.Remove(lastSh);
        if (data && data["NODATA"]) {
            var arr = data["NODATA"];

            var min = -1, max = -1;
            var minForRange = -1, maxForRange = -1;
            for (var i = 0; i < arr.length; i++) {
                var cnt = cntGet?arr[i][_fli_cnt]:1;
                var dt = options.EnableRangeScope ? arr[i][1].split('-') : [];
                if (dt.length > 1) {
                    minForRange = minForRange == -1 ? cnt : Math.min(cnt, minForRange);
                    maxForRange = maxForRange == -1 ? cnt : Math.max(cnt, maxForRange);
                }
                else {
                    min = min == -1 ? cnt : Math.min(cnt, min);
                    max = max == -1 ? cnt : Math.max(cnt, max);
                }
            }
            max -= min;
            maxForRange -= minForRange;
            if (max == 0) max = 1;
            if (maxForRange == 0) maxForRange = 1;
            for (var i = 0; i < arr.length; i++) {
                var itm = arr[i];
                var dt = itm[1].split('-');
                var start = QueryTimeToDate(dt[0].replace(" ", ""));
                var end = dt.length > 1 ? QueryTimeToDate(dt[1].replace(" ", "")) : start;
                var alpha =
                    options.EnableRangeScope && dt.length>1 ?
                        ((((((cntGet?itm[_fli_cnt]:1) - minForRange) + 1) * 0.95) / (maxForRange + 1)) + 0.05) :
                        ((((((cntGet?itm[_fli_cnt]:1) - min) + 1) * 0.95) / (max + 1)) + 0.05);

                var dayDiff = 0;
                if (end != start)
                    dayDiff = (end - start) / 60000 / 60 / 24;
                var current = new Date(start);
                for (var dayCntr = 0; dayCntr <= dayDiff; dayCntr++) {
                    var yr = pad(current.getFullYear(), 4);
                    var mnt = pad(current.getMonth() + 1, 2);
                    var dyOfweek = current.getDay();

                    if (!result._yearsDataSource[yr]) result._yearsDataSource[yr] = 0;
                    result._yearsDataSource[yr] += cnt;

                    if (!result._monthsDataSource[mnt]) result._monthsDataSource[mnt] = 0;
                    result._monthsDataSource[mnt] += cnt;

                    if (!result._monthsAllDataSource[yr + "/" + mnt]) result._monthsAllDataSource[yr + "/" + mnt] = 0;
                    result._monthsAllDataSource[yr + "/" + mnt] += cnt;

                    if (!result._daysDataSource[dyOfweek]) result._daysDataSource[dyOfweek] = 0;
                    result._daysDataSource[dyOfweek] += cnt;

                    if (options.collectAllDate) {
                        var dy = pad(current.getDate(), 2);
                        if (!result.dayAllDataSource[yr + "/" + mnt + "/" + dy]) result.dayAllDataSource[yr + "/" + mnt + "/" + dy] = 0;
                        result.dayAllDataSource[yr + "/" + mnt + "/" + dy] += cnt;
                    }

                    current.setDate(current.getDate() + 1);
                }


                result.datasource.push({
                    count: cnt,
                    //color: 'rgba(255, 74, 50,' + alpha + ')',
                    alpha: alpha,
                    startDate: start,
                    endDate: end,
                    fieldName:options.GetFieldName?itm[2]:null
                });
            }

            var max = -1;
            var min = -1;

            for (var p in result._yearsDataSource)
                if (result._yearsDataSource[p]) {
                    var cnt = result._yearsDataSource[p];
                    min = min == -1 ? cnt : Math.min(cnt, min);
                    max = max == -1 ? cnt : Math.max(cnt, max);
                }
            result._yearsDataSource.max = max;
            result._yearsDataSource.min = min;
        }

        callback(result);
    });

    if (options.Aborter)
        options.Aborter.Add(lastSh);


}

function _GetDfBBOX(renderOptions) {
    //if (renderOptions && renderOptions.Query && renderOptions.Query.Envelope)
    
    urlex1 = "";
    if (renderOptions && renderOptions.GetViewerExtraUrlParam) urlex1 += renderOptions.GetViewerExtraUrlParam();
    urlex1 += window.GetViewerExtraUrlParam ? GetViewerExtraUrlParam(urlex1) : "";
    if (window.StateForm && StateForm.GetState("DFBBOX") && urlex1.indexOf("dfBBOX") == -1)
        urlex1 += "&dfBBOX=" + StateForm.GetState("DFBBOX").replace("[", "").replace("]", "");

    if (urlex1.indexOf("dfOpener") == -1) {
        var dfOpenerParam = window.___WatchGUIModeGet ? HttpUtility.UrlEncode(window.___WatchGUIModeGet()) : (window.GetUrlParameter?GetUrlParameter("dfOpener"):"");
        urlex1 += "&dfOpener=" + dfOpenerParam;
    }
    return urlex1;
}

function _hideTDrop(obj) {
    $(obj).parents(".open").removeClass("open");
}
function _MoreLinkRow(obj) {
    obj = $(obj);
    var tbl = obj.parents(".root");
    var intMaxRow = typeof window.IntMaxRow != "undefined" ? window.IntMaxRow : 5;
    if (obj.attr("formore") == 1) {
        if (intMaxRow == 0)
            $(".propGrid", tbl).fadeIn("fast", function () { obj.text("[$default:readLess/js]") });
        else
            $(".pgRow", tbl).fadeIn("fast", function () { obj.text("[$default:readLess/js]") });
        obj.attr("formore", 0);
    }
    else {
        if (intMaxRow == 0)
            $(".propGrid", tbl).fadeOut("fast", function () { obj.text("[$default:readMore/js]") });
        else
            $(".pgRow:gt(" + (intMaxRow - 1) + ")", tbl).fadeOut("fast", function () { obj.text("[$default:readMore/js]") });
        obj.attr("formore", 1);
    }
}

function RenderDLV_Hor(o) {
    o.IsHor = true;
    RenderDLV(o,true);
}
function RenderDLV(o,isHor) {
    if (!o.wsName && o.Query) o.wsName = o.Query.wsName;
    if (!o.GUISearchOptions)
        o.GUISearchOptions = GUISearchOptions.Create(GUISearchOptions.SimpleResutView | GUISearchOptions.GetSafeBits());
    //o.TargetDiv
    //??
    if (!o.DLVGetterAjax)
        o.DLVGetterAjax = function (opt, q, callback, UpdateDefaultQuery, UsageLog,LogActionName) {
            var prms={
                    UpdateDefaultQuery: UpdateDefaultQuery,
                    UsageLog: UsageLog,
                    SummaryCount: o.DisableSummary ? 0 : 1
            };
            if (LogActionName)
                prms.LogActionName = LogActionName;
            if(opt.QueryOptions)
                $.extend(prms,opt.QueryOptions);
            return q.GetDocuments2(callback, prms);
        }

    var dfBBox = _GetDfBBOX(o);
    o.targetDom = $(o.TargetDiv ? o.TargetDiv : '#divDocAccordion');
    var args = {
        MultipleTarget: o.MultipleTarget,
        Template: o.Template,
        TemplateSelector: o.TemplateSelector,
        targetDom: o.targetDom,
        Query: o.Query,
        Options: o,
        FastLinkTagName: window.BrowserType && window.BrowserType.IsGoogleEarth() ? "a" : "span",
        FastButtonTagName : window.BrowserType && window.BrowserType.IsGoogleEarth() ? "a" : "button",
        ResolveUrl : o.Query.ResolveUrl    ,
        HasAdvancedPanel : function () {
            return o.AdvancedTargetPanel && (!window.BrowserType || (!window.BrowserType.IsMobile() && !window.BrowserType.IsGoogleEarth()));
        }
    };

    if (args.Query && args.Query.UpdateSource) {
        args.Query.UpdateSource();
        if (args.Query.SourceList && args.Query.SourceList.length > 1) {
            args.Query._OnAjaxErrorInternal = function (event, data, settings, argd, callBackFunction) {
                args.Data = []; args.totalCount = 0; args.Contents = {};
                if (o.OnEndRender) o.OnEndRender(args);
                $(document.body).trigger("GUI_DLVOnEndRender", args);
                return true; 
            }
        }
    }

    if (!args.Template)
        args.Template = o.Template = window.HtmlTemplates.GetContainerTemplate(args, isHor);
    if (!args.TemplateSelector)
        args.TemplateSelector = o.TemplateSelector = (isHor ? window.TemplateSelectors.DefaultDLV_Hor : window.TemplateSelectors.DefaultDLV);


    if (o.OnStarting) o.OnStarting(args);
    if (args.Cancel) return;
    $(document.body).trigger("GUI_DLVOnStarting", args);
    if (args.Cancel) return;

    var InMtdPost = o.DLVGetterAjax(o, args.Query, function (alldata, textStatus, request) {
        if (o.Aborter)
            o.Aborter.Remove(InMtdPost);
        args.Data= alldata;
        args.textStatus= textStatus;
        args.request= request;
        args.Contents = {};
        args.totalCount = alldata.length;
        if (request && request.getResponseHeader) {
            var c = request.getResponseHeader("Count");
            if (c) args.totalCount = parseInt(c);
            var jump = request.getResponseHeader("EndIndex");
            if (jump) args.responseEndIndex = parseInt(jump);
        }
        var current = args.Query.StartIndex + 1;
        var queryStrEncoded = args.totalCount > 1 ? HttpUtility.HtmlEncode(HttpUtility.ScriptEncode(
                o.ExForceQuery ? o.ExForceQuery : args.Query.ToString(o.ExQuery), true)
                .replace(/\\"/g, '"')) : "";
        var dfBBox = _GetDfBBOX(o);
        var domObjects = [];
        for (var i = 0; i < alldata.length; i++) {
            var data = alldata[i];
            data.Context = args;
            data.RedirId =  (data.DocRedirect != null && data.DocRedirect != undefined) ? data.DocRedirect : data.ContentIdentifier
            var sm=null;
            if (data.Summary) {
                sm = $("<div>" + data.Summary + "</div>");
                //if (data.ContentType == "foldercontent:.folder")
                    //$(".root", sm).prepend("<dic class=ThumbContentRef data-docid=" + data.ContentId + "></div>");
            }
            data.JSONData = {}

            if (sm)
                window.SummaryGUI.UpdateSummaryDOM(sm, data, args.Query.wsName, args.Query);
            if (data.AdditionalValues && data.AdditionalValues["FCD_JsonData"])
                data.JSONData = JSON.parse(data.AdditionalValues["FCD_JsonData"]);
            else if (sm != null) {
                $.each($(".pgRow", sm), function () {
                    var d1Dom = $(".pgVal", $(this));
                    var t1 = d1Dom.text().trim();
                    var maxMore = window.RowSummaryMaxLength != undefined && window.RowSummaryMaxLength != null ? window.RowSummaryMaxLength : 256;
                    if (t1.length > maxMore) {
                        t1 = "<span>"+t1.substr(0, maxMore) +"</span>"+
                            "<a href='javascript:void(0)' onclick='$(this).next().show();$(this).hide()' ><b>...</b></a>" +
                            "<span style='display:none'>" +
                            t1.substr(maxMore) +
                            "</span>";
                        d1Dom.html(t1);
                    }
                    data.JSONData[$(".pgCol", $(this)).text().trim()] = t1;
                })
                
                if (data.ContentType == "file_db:geodi_row" || data.ContentType == "mobidi:.mobidi") {
                    var propGrid = (data.ContentType == "mobidi:.mobidi" ? $(".attributeGrid", sm) : $(".propGrid", sm));
                    var intMaxRow = typeof window.IntMaxRow != "undefined" ? window.IntMaxRow : 5;
                    var htmlList = [];
                    var i1 = 0;
                    $.each(data.JSONData, function (k, v) {
                        if (v != null && v != undefined && v.length > 0) {
                            htmlList.push('<div ' + (intMaxRow > 0 && i1 >= intMaxRow ? 'style="display:none"' : '') + ' class="pgRow"><div class="pgCol noformatter">' + k + '</div><div class="pgVal">' + v + '</div></div>');
                            i1++;
                        }
                    });
                    if (htmlList.length > 0) {
                        if (intMaxRow == 0)
                            propGrid.hide();

                       
                        propGrid.html(htmlList.join(""));
                      
                        if (i1 > intMaxRow)
                            propGrid.after($('<div align=right class=moreSide><a class=moreSideLink formore=1  href="javascript:void(0)" onclick="_MoreLinkRow(this)">[$default:readMore/js]</a></div>'));
                    }
                }
                
            }
            if (sm) {
                data.Summary = sm.html();
                var args0 = {
                    options: o,
                    args: args,
                    img: $(".ThumbContentRef img", sm),
                    data: data,
                    sm:sm
                };
                s = args0.sm;
                $(document.body).trigger("GUI_OnSummaryPreparing", args0);
                if (args0.img && args0.img.length) {
                    data.PreviewHTML = args0.img[0].outerHTML;
                    args0.img.remove();
                }
                data.SummaryHTML = sm.html();
            }
            data.QueryAttr = ' cp="' + (args.totalCount > 1 ? ((current + i) + "." + args.totalCount) : '') + '" cq="' + queryStrEncoded + '" ' + (args.Query.UseRank ? "cr=1" : "") + ' ';

            data.HasEnumPerm = function (bitIndex, defaultVal) {
                if (o.CurrentWSInfo) {
                    var enmID = this.EnumaratorId;
                    var ep = o.CurrentWSInfo.EnumaratorPermissions;
                    if (ep && ep.hasOwnProperty && ep.hasOwnProperty(enmID)) {
                        var per = ep[enmID];
                        return (per & bitIndex) == bitIndex;
                    }
                }
                return defaultVal;
            }
            data.SupportDownload = function () {
                if (this.DisableDownload) return false;
                if (!window.BrowserType || (!window.BrowserType.IsGoogleEarth()))
                    return this.HasEnumPerm(4, true);
                return false;
            }
            data.SupportNote = function () {
                if ((!window.BrowserType || !window.BrowserType.IsGoogleEarth()) && !o.GUISearchOptions.Is(o.GUISearchOptions.HideNoteIcon)) {
                    if (this.LabelId) return true;
                    return this.HasEnumPerm(1, true);
                }
                return false;
            }
            data.GetDocItemAttr = function () {
                return TemplateToHtml(window.HtmlTemplates.DefaultDocItemAtttributes, this, this.Context);
            }
            data.GetHrefFull = function () {
               
                return TemplateToHtml(window.HtmlTemplates.DefaultOpenAttributes, this, this.Context);
            }
            data.GetHref = function (forclick) {
                if (this.HasViewer) {
                    var wsname = forclick ? HttpUtility.UrlEncode(args.Query.wsName) : HttpUtility.HtmlEncode(args.Query.wsName);
                    var hrfLink = this.RelationIdentifier ? "relation_id=" + this.RelationIdentifier : "content_id=" + this.ContentIdentifier;
                    return args.ResolveUrl('~/DefaultContentViewer?' + hrfLink + '&wsName=' + wsname + dfBBox, true, true, { IgnoreToken: true })
                }
                if (this.SupportDownload())
                    return "javascript:downloadDoc('" + this.ContentIdentifier + "');";
                return forclick ? "" : "javascript:void(0)";
            }
            if (!data.HasEnumPerm(4, true))
                data.HasViewer = false;

            data.GetIcon = function () {
                if (this.AdditionalValues && this.AdditionalValues["FCD_Icon"]) {
                    var v = this.AdditionalValues["FCD_Icon"];
                    if (v.indexOf(";base64") != -1)
                        return v;
                    else if (v.indexOf("~") == 0)
                        return args.ResolveUrl(v, true, true);
                    else if (v.indexOf("http") == 0)
                        return v;
                    return args.ResolveUrl('~/GUIJsonService?op=getIco&Size=16x16&IconName=' + v, true, true);
                }
                if (this.ContentType == "file_db:geodi_row" || this.ContentType == "file_db: geodi_table") {
                    if (this.AdditionalValues && this.AdditionalValues["EnumaratorType"]) {
                        var et = this.AdditionalValues["EnumaratorType"];
                        if (et && this.EnumaratorId && et.indexOf("DatabaseContentReaderEnumarator")!=-1)
                            return args.ResolveUrl("~/GeodiJSONService?op=getEnumaratorIco&ID=" + this.EnumaratorId + "&wsName=" + HttpUtility.UrlEncode(args.Query.wsName), true, true);
                    }
                    if (this.AdditionalValues && this.AdditionalValues["OwnerContentType"])
                        return args.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + this.AdditionalValues["OwnerContentType"], true, true)
                    
                    
                }
                return args.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + this.ContentType, true, true)
            }

            data.GetTitle = function () {
                var titleDoc = HttpUtility.HtmlEncode(this.DisplayName);
                var titleDocEnumarator = HttpUtility.HtmlEncode(!o.CurrentWSInfo ? '' : o.CurrentWSInfo.GetEnumratorTitleFromId(this.EnumaratorId));
                if (titleDocEnumarator && titleDoc != titleDocEnumarator &&
                    (!(this.AdditionalValues && this.AdditionalValues.Parent) || !this.AdditionalValues.Parent || titleDocEnumarator != this.AdditionalValues.Parent.DisplayName)
                    ) titleDoc += "\r\n" + titleDocEnumarator;

                if (window.moment && this.AdditionalValues && this.AdditionalValues.Date) {
                    var docDt = moment(new Date(this.AdditionalValues.Date));
                    titleDoc += "\r\n" + docDt.format('L') + ' (' + docDt.fromNow() + ')';
                }
                return titleDoc;
            }

            data.GetInfoHtml = function () {
                if (!this.AdditionalValues || o.GUISearchOptions.Is(o.GUISearchOptions.HideDocParentsInfo))
                    return "";
                var extrahtml = [];
                var rank = this.AdditionalValues["Rank"];
                if (window.moment && this.AdditionalValues.Date) {
                    var docDt = moment(new Date(this.AdditionalValues.Date));
                    var docDateStr = ' <span title="' + docDt.format('L') + '">' + docDt.fromNow() + "</span> ";
                    extrahtml.push(' <small style=color:gray>' + docDateStr + '</small> ');
                }
                if (rank)
                    extrahtml.push(' <small style=color:lightgray>%' + Math.round(parseFloat(rank) * 100) + '</small> ');
                if (this.AdditionalValues["SP_Upload:User"])
                    extrahtml.push(' <small style=color:gray title="[$Recognizer:att_Upload/js/html]">' + this.AdditionalValues["SP_Upload:User"] + '</small> ');
                else if (this.AdditionalValues["SP_Owner:User"])
                    extrahtml.push(' <small style=color:gray title="[$Recognizer:att_Owner/js/html]">' + this.AdditionalValues["SP_Owner:User"] + '</small> ');

                if (this.AdditionalValues["GroupCount"] && this.AdditionalValues["GroupCount"] > 1)
                    extrahtml.push(' <small style=color:gray class=inLink onclick="FieldLink(\'domain:' + (HttpUtility.ScriptEncode(this.AdditionalValues["GroupName"])) + '\',event)">' + ('[$default:group_link_text]'.replace('{0}', this.AdditionalValues["GroupName"]).replace('{1}', FormatNumber(this.AdditionalValues["GroupCount"], 0))) + '</small> ');
                else if (this.AdditionalValues["UNC"] && this.AdditionalValues["UNC"].indexOf && this.AdditionalValues["UNC"].indexOf("://") == 0) {
                    var unc = this.AdditionalValues["UNC"].substr(3);
                    var nx = unc.indexOf("/");
                    if (nx != -1) unc = unc.substr(0, nx);
                    if (unc)
                        extrahtml.push(' <small style=color:gray class=inLink onclick="FieldLink(\'domain:' + (HttpUtility.ScriptEncode(unc)) + '\',event)">' + (unc) + '</small> ');
                }
                var fctParent = null;
                if (this.AdditionalValues.Parent && this.AdditionalValues.Parent.DisplayName)
                    fctParent = this.AdditionalValues.Parent;
                if (this.AdditionalValues.NoteParent && this.AdditionalValues.NoteParent.DisplayName)
                    fctParent = this.AdditionalValues.NoteParent;
                if (fctParent != null) {
                    var lunc = fctParent.DisplayName;
                    var cType = fctParent.ContentTypeKey;
                    if (!cType) cType = "";
                    var lnkOnclick = 'onclick=FacetLink(' + fctParent.ID + ',"goparent",null,null,$(this).text(),"' + cType + '")';
                    var imgHtml = '';//'<img class="imgSmallParent" src="args.Query.ResolveUrl('GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + cType ,true,true) + '" title="' + HttpUtility.HtmlEncode(o.CurrentWSInfo.GetContentTypeTitleFromId(cType)) + '"/> ';
                    if (!window.FacetLink || o.IsMapRequest || o.GUISearchOptions.Is(o.GUISearchOptions.HideFacet))
                        lnkOnclick = '';
                    var luncTitle = lunc;
                    if (lunc.length > 50) {
                        if (cType == "file_db:geodi_row" || cType == "mobidi:.mobidi")
                            lunc = lunc.substr(0, 50);
                        else
                            lunc = lunc.substr(lunc.length - 50, 50);
                    }
                    extrahtml.unshift(imgHtml + '<span class=docparent><span title="' + HttpUtility.HtmlEncode(luncTitle) + '" class=inLink ' + lnkOnclick + '>' + lunc + '</span></span> ');
                }
                if (!fctParent && this.AdditionalValues && this.AdditionalValues.EnumaratorName) {

                    var lnkOnclick = 'onclick=FacetLink(' + this.AdditionalValues.EnumaratorID + ',"enumarator")';
                    extrahtml.unshift('<span class=enmparent><span class=inLink ' + lnkOnclick + '>' + this.AdditionalValues.EnumaratorName + '</span></span> ');

                }
                var spChar = (window.SummaryInfoHtmlSplitChar == undefined || window.SummaryInfoHtmlSplitChar == null) ? "|" : window.SummaryInfoHtmlSplitChar;
                return extrahtml.join(spChar);
            }

            data.CustomRenderList = [];// { target:summary/buttons, html:...(function/data) }
            data.CustomRender = function (currentArea, data, context) {
                var html1 = [];
                $.each(data.CustomRenderList, function (i1, d1) {
                    if (d1 && (d1.target == "*" || d1.target == currentArea || (d1.targets && d1.targets.indexOf(currentArea)!=-1)) && d1.html) {
                        if (typeof d1.html === "function")
                            html1.push(d1.html(currentArea, data, context));
                        else
                            html1.push(d1.html);
                    }
                })
                return html1.join(' ');
            }
            args.Contents[data.ContentIdentifier] = data;
        }


        if (o.OnStartRender) o.OnStartRender(args);
        if (args.Cancel) return;
        $(document.body).trigger("GUI_DLVOnStartRender", args);
        if (args.Cancel) return;


        if (alldata.length == 0 && args.Query.StartIndex == 0 && (!args.Query.HasNextSource || (!args.Query.HasNextSource() && $(".IsDoc", args.targetDom).length == 0) ))     //clear ?
            args.targetDom.append($('<div class=norecord>[$default:no_record_fts/js]</div>'));

        var appendTo = function appendTo(parentdom, template, data, p, domObjects) {
            p.targetDom = parentdom;
            var html1 = TemplateToHtml(template, data, p);
            html1 = $(html1);
            var setData = function (domItem) {
                domItem = $(domItem);
                var ci = domItem.attr("contentid");
                if (p.Contents[ci]) {
                    var data = p.Contents[ci];
                    domItem.data("query_data", args.Query);
                    domItem.data("title_data",
                       {
                           enm: data.EnumaratorId,
                           ctyp: data.ContentType,
                           txt: data.DisplayName,
                           all: data
                       }
                    );

                }
            }
            $(html1).each(function (i, d) { setData(d); })
            $(".IsDoc", html1).each(function (i, d) { setData(d); })
            if (!window.Favorites && window.LabelManager)
                window.Favorites = new LabelManager(1);
            if (window.Favorites)
                Favorites.setFavorite(html1); //???
            parentdom.append(html1);
            if (domObjects)
                domObjects.push(html1)
            return html1;
        }
        //template!
        appendTo(args.targetDom, args.Template, args.Data, args, domObjects);
        if (args.MultipleTarget) {
            var _TemplateSelectorStart = args.TemplateSelector;
            for (var i = 0; i < args.MultipleTarget.length; i++) {
                var mt = args.MultipleTarget[i];
                if (mt.TemplateSelector) args.TemplateSelector = mt.TemplateSelector;
                if (!mt.Template) mt.Template = args.Template;
                appendTo($(mt.TargetDiv), mt.Template, args.Data, args, domObjects);
            }
            args.TemplateSelector = _TemplateSelectorStart;
        }
        if (!o.DisableRecognize) {
            var str = []
            for (var i = 0; i < domObjects.length; i++) {
                var smt = $(".geodiFormatable", domObjects[i]).text()
                if (smt) str.push(smt);
            }
            if (str.length > 0) {
                str = str.join('\r\n --------- \r\n');
                var rSh = args.Query.Recognize(str, function (dataIn) {
                    if (o.Aborter)
                        o.Aborter.Remove(rSh);
                    for (var i = 0; i < domObjects.length; i++) {
                        var dom = domObjects[i];
                        $(".geodiFormatable", dom).each(function () {
                            var ci = $(this).attr("contentid")
                            window.SummaryGUI.ApplyFormatter(args.Contents[ci] ? args.Contents[ci] : {}, $(this), dataIn.Entries);
                        })
                    }
                });
                if (o.Aborter)
                    o.Aborter.Add(rSh);
            }
        }

        if (o.OnEndRender) o.OnEndRender(args);
        $(document.body).trigger("GUI_DLVOnEndRender", args);

    },
                    o.Clear ? o.UpdateDefaultQuery : false,
                        (o.Clear && !o.IsMapRequest && !o.DisableLogging) ? true : false ,//UsageLog
                        o.LogActionName
                    );

    if (o.Aborter)
        o.Aborter.Add(InMtdPost);

}

function Render_ContentInfo(att, wsName, topHtml, util, forViewer, fastWsObject) { //, content
    
    if (!util) util = HttpUtility;
    var dtC = att.ContentDate ? moment(parseNetDate(att.ContentDate)) : null;
    var waAdded = false;
    var wa = function (t, v) {
        if (!v || typeof v === 'object')
            return "";
        var rtn1 = '<div class=attD>' + t + ' : ' + '<span class=attV>' + v + '</span></div>';;
        if (!waAdded) {
            waAdded = true;
            rtn1 = "<div class='iDetail ipnl' >" + rtn1;
        }
        return rtn1;
    };

    var spl = '<span class=isplit> </span>'
    var dataContent = topHtml ? "<div class='itop ipnl' " + (forViewer?"style='display:inline'":"")+">" + topHtml + "</div>" : "";
    dataContent += "<div class='ipath ipnl'>";

    var allowQuery = window.FacetLink && window.StateForm && StateForm.GetState('ismaprequest') != '1';
    if (att.EnumaratorName && att.EnumaratorName != att.DisplayName &&
        (!att.Parent || att.EnumaratorName != att.Parent.DisplayName)
        ) {
        dataContent +=
            '<' + (allowQuery ? 'a style="cursor:pointer" onclick=FacetLink(' + att.EnumaratorID + ',"enumarator") href=# ' : 'span') + ' class="ienum iblock">' +
            '<img src="'+util.ResolveUrl('~/GeodiJSONService?op=getEnumaratorIco&ID=' + att.EnumaratorID + '&wsName=' + HttpUtility.UrlEncode(wsName),false,true) + '" /' +
            '<span class=itext>' + att.EnumaratorName + '</span></' + (allowQuery ? 'a' : 'span') + '>';
        waAdded = true;
    }

    if (att.Parent) {
        
        var clk = "";
        if (allowQuery)
            clk = 'FacetLink(' + att.Parent.ID + ',\'goparent\',null,null,$(this).text(),\'' + att.Parent.ContentTypeKey + '\')';
        if (att.Parent.HasViewer) {
            if (clk)
                clk = 'event.ctrlKey?showContent(' + att.Parent.ID + '):' + clk;
            else
                clk = 'showContent(' + att.Parent.ID + ')';
        }
        var iimgsrc = "";
        
        if (att.ContentType == "file_db:geodi_row" && att.EnumaratorId)
            iimgsrc = util.ResolveUrl("~/GeodiJSONService?op=getEnumaratorIco&ID=" + att.EnumaratorId + "&wsName=" + HttpUtility.UrlEncode(wsName), true, true);
        else
            iimgsrc = util.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + att.Parent.ContentTypeKey, true, true)

        dataContent +=
            (waAdded ? spl : '') +
        ' <' + (clk ? 'a style="cursor:pointer" onclick="' + clk + '" href=# ' : 'span') + ' class="iparent iblock" title="' + att.Parent.UNC + '"> <img src="' + iimgsrc + '" target="_blank"><span class="itext">' + att.Parent.DisplayName + '</span></' + (att.Parent.HasViewer ? 'a' : (allowQuery ? 'a' : 'span')) + '>';
        
        

        //FormatBytes(att.Parent.ContentSize, 2)
        //var dtCP = att.Parent.ContentDate ? moment(parseNetDate(att.Parent.ContentDate)) : null;
    }
    waAdded = false;
    dataContent += "</div>";

    dataContent += wa('[$Recognizer:att_Upload/js]', att["SP_Upload:User"]);
    if (att["DocLayersNames"]) {
        var layerNames = att["DocLayersNames"];
        dataContent += wa("[$default:layers/js]", layerNames.join(", "));


    } else if (att["DocLayers"] && fastWsObject && fastWsObject.RecognizerIdReference) {
        var layerNames = [];
        $.each(att["DocLayers"], function (i, v) {
            if (fastWsObject.RecognizerIdReference[v])
                layerNames.push(fastWsObject.RecognizerIdReference[v].DisplayName);
            else
                layerNames.push("?");
        })
        dataContent += wa("[$default:layers/js]", layerNames.join(", "));
    }
    
    if (att["ContentPerms"]) {
        var cPerm = att["ContentPerms"];
        var cPermText = "";
        if (cPerm.Permit && cPerm.Permit.length > 0)
            cPermText = cPerm.Permit.join(", ");
        if (cPerm.Deny && cPerm.Deny.length > 0)
            cPermText += (cPermText ? ", " : "") + " Deny: ", cPerm.Deny.join(", ");
        if (cPermText)
            dataContent += wa("[$Authorization:permissions/js]", cPermText);
    }
    
    var sign = null;
    $.each(att, function (key, val) {
        if (key == "SP_Signature")
            sign = val;
        else if (key && val && key != "SP_Upload:User" & key != "SP_Owner:User" && key.indexOf && key.indexOf("SP_") == 0) {
            var sp1 = key.indexOf(":");
            var kEx = "";
            var key0 = key;
            if (sp1 != -1) {
                kEx = key0.substr(sp1 + 1);
                key0 = key0.substr(0, sp1);
            }
            key0 = key0.substr(3);
            var key0txt = key0;
            if (key0 == "Upload" || key0 == "Owner")
                return;
            dataContent += wa(key0 + (kEx ? ' (' + kEx + ') ' : ''), val);
        }
    });
    
   
    if (sign) {
        var signText = [];
        try {
            
            var SignObject = JSON.parse(sign);
            try {
                $.each(SignObject, function getNodeById(i, e) {
                    var sObj = {};

                    var keys = e.SignerCertificate.split(',');
                    $.each(keys, function (j, key) {
                        var vals = key.split('=');
                        sObj[vals[0].trim()] = vals[1];
                    })
                    if (e.SignerCertificate.includes(':')) {
                        var pos = e.SignerCertificate.indexOf(':') + 1;
                        var emr = "<b>" + [e.SignerCertificate.slice(0, pos), "</b>", e.SignerCertificate.slice(pos)].join('');
                        e.SignerCertificate = (sObj["CN_I"] ? (sObj["CN_I"] + (sObj["EMail"] ? ("&lt;" + sObj["EMail"] + "&gt;") : "")) : emr);

                    }
                    if (e.SerialSignatures!=null && e.SerialSignatures.length>0)
                    $.each(e.SerialSignatures, function (c, b) { getNodeById(c, b) });


                });
                var templateHtml = TemplateToHtml(window.HtmlTemplates.SignTemplate, SignObject, {});
                signText.push(templateHtml);

                if (signText.length > 0) {
                    window.setTimeout(function () { $('[data-toggle="popover"]').popover(); }, 1000);
                    
                    dataContent += wa("[$Recognizer:att_Signature/js]", "<span class=iSignatures style='display: inline-block;vertical-align: top; text-transform:lowercase'>" + signText.join("") + "</span>");

                }
            } catch (ex) {
                console.log("TemplateToHtml Method Error");
            }

        }
        catch (ex){
            var ssign = sign.replace(/\r/g, '').replace(/\n\t/g, '= &nbsp').replace(/\n/g, '<br/>');
            var sign2 = ssign.split('<br/>');
            
            $.each(sign2, function (i, t) {
                var sObj = {};

                var keys = t.split(',');
                $.each(keys, function (j, key) {
                    var vals = key.split('=');
                    sObj[vals[0].trim()] = vals[1];
                })
                if (t.includes(':')) {
                    var pos = t.indexOf(':') + 1;
                    t = "<b>" + [t.slice(0, pos), "</b>", t.slice(pos)].join('');

                }
                var title = HttpUtility.ScriptEncode(HttpUtility.HtmlEncode(t.replace(/,/g, "\r\n")));
                signText.push("<div class=iSignature title=\"" + title + "\">" + (sObj["CN"] ? (sObj["CN"] + (sObj["EMail"] ? ("&lt;" + sObj["EMail"] + "&gt;") : "")) : t) + "</div>");
                //+ "[ " + sObj["Time"] + "]"
                //C,L,Email,SERIALNUMBER,T,Time,CN
            });
            if (signText.length > 0)
                dataContent += wa("[$Recognizer:att_Signature/js]", "<div class=iSignatures style='padding-left:6px; max-height: 400px; overflow-y: scroll; display: inline-block;vertical-align: top;'>" + signText.join("") + "</div>");
        }
        
    }

    if (waAdded)
        dataContent += "</div>";
    dataContent += "<div class='isummary ipnl'>";
    var spw = false;
    if (att.ContentSize && att.ContentSize > 1) {
        dataContent += "<span class='isummaryText isize' title='[$docViewer:size/js]'>" + FormatBytes(att.ContentSize, 2) + "</span>";
        spw = true;
    }
    if (dtC) {
        if (spw) dataContent += spl;
        dataContent += "<span class='isummaryText idate' title='[$docViewer:date/js]'>" + dtC.format('L') + ' (' + dtC.fromNow() + ')' + "</span>";
        spw = true;
    }
    if (att["SP_Owner:User"]) {
        if (spw) dataContent += spl;
        dataContent += "<span class='isummaryText iowner' title='[$Recognizer:att_Owner/js]'>" + att["SP_Owner:User"] + "</span>";
        spw = true;
    }
    dataContent += "</div>";


    return dataContent;
}

function LoadChildEditors(contentid, args, noReset, httpUtilityOwner,refQuery) {
    var query = new GeodiQuery();
    query.wsName = StateForm.GetState("wsName");
    if (refQuery) {
        query.ServerUrl = refQuery.ServerUrl;
        query.Token = refQuery.Token;
        if (refQuery.wsName)
            query.wsName = refQuery.wsName;
    }
    
    

    query.StartIndex = 0;
    query.EndIndex = 10;
    query.SearchString = "IsChildAttr parentid:" + contentid;

    if (!noReset && window.__CEQ)
        window.__CEQ.abort();
    if (window.OnLoadChildEditorsQueryUpdate)
        window.OnLoadChildEditorsQueryUpdate(query, contentid, args, noReset);

    if (!noReset && window.__loadedCE) {
        for (var i in window.__loadedCE)
            if (window.__loadedCE[i] && window.__loadedCE[i].Waits)
                window.__loadedCE[i].Waits = [];
    }

    window.__CEQ = query.GetDocuments(function (data) {
        if (data && (Object.prototype.toString.call(data) === '[object Array]')) {
            var groups = {};
            for (var i = 0; i < data.length; i++) {
                var d = data[i];
                if (d.AdditionalValues) {
                    var cef = d.AdditionalValues.CEFnc;
                    if (cef) {
                        if (!groups[cef]) groups[cef] = [];
                        groups[cef].push(d);
                    }
                }
            }
            $.each(groups, function (key, items) {
                var d = items[0];
                var ce = d.AdditionalValues[args && args.type ? args.type : "ES"];
                if (ce) {

                    LoadChildEditor(ce, function (edt,firstLoad) {
                        if (edt.Loading)
                            edt.WaitList = items;
                        else if (window[key])
                            window[key](firstLoad ? edt.WaitList : items, args);
                    },
                        items, httpUtilityOwner
                    )
                }
            });
        }
    }, null, null, { RunUpdateClientData: true, ForLoadEditor: args && args.type ? args.type : "?" });
}

function LoadChildEditor(ce, callback, items,httpUtilityOwner) {
    if (!window.__loadedCE)
        window.__loadedCE = {};
    if (!window.__loadedCE.hasOwnProperty(ce)) {
        window.__loadedCE[ce] = {
            Loading: true,
            WaitList: items ? items:[]
        };
        $.get((httpUtilityOwner ? httpUtilityOwner : HttpUtility).ResolveUrl(ce), function (html) {
            $(document.body).append(html);
            var edt = window.__loadedCE[ce];
            edt.Loading = false;
            callback(edt,true);
            delete edt.WaitList;
        });
    }
    else
        callback(window.__loadedCE[ce],false)
}

/*TemplateSupport*/
window.HtmlTemplates = {
    Cache: {},
    __DomOrValue: function (key) {
        if (!this.Cache[key]) {
            var dom = $("#" + key);
            //if (dom.length == 0) dom = $("." + key);
            this.Cache[key] = (dom && dom.length > 0) ? $(dom[0]) : "--NOTSET--";
        }
        var rtn = this.Cache[key];
        return rtn == "--NOTSET--" ? null : rtn;
    },
    GetTemplate: function (key, defaultValue, args, data, isHor) {
        if (isHor) key += "-hor";
        else if (args.Options.targetDom.hasClass("inDefaultPanel")) {
            var keyDefault = key + "-default";
            var val = this.__DomOrValue(keyDefault);
            if (val) return val;
        }
        var rtn = this.__DomOrValue(key);//+ajax
        return rtn ? rtn : defaultValue;
    },
    GetContainerTemplate: function (args, isHor) {
        return this.GetTemplate("geodi-container-template", this.DefaultDLVContainer, args, null,isHor);
    },
    DefaultDLVContainer: '<d:r data="data">{{TemplateToHtml(context.TemplateSelector(data,context),data,context)}}</d:r>',
    DefaultOpenAttributes: '   href="{{data.GetHref()}}"' +
                    '                                                             curl="{{data.GetHref(true)}}"' +
                    '                                                             parentId="{{(data.AdditionalValues  ? data.AdditionalValues.PId : \'-1\')}}"' +
                    '                                                             unc="{{(data.AdditionalValues && data.AdditionalValues.UNC ? HttpUtility.HtmlEncode(data.AdditionalValues.UNC) : \'\')}}"' +
                    '                                                             data-item="{{data.RedirId}}" contentid="{{data.ContentIdentifier}}" id="cntLink_{{data.ContentIdentifier}}"' +
                    '                                                             {{ data.HasViewer ?\' target="docViewer" \':""  }}' +
                    '                                                             onclick="ShowOrDownloadDoc(this,event, {{context.HasAdvancedPanel()?"\'"+context.Options.AdvancedTargetPanel+"\'":false}})"',
    DefaultDocItemAtttributes: ' isAttContainer keyword="{{data.Keyword}}" itemidentifier="{{data.ItemIdentifier}}" summaryid="{{data.RelationIdentifier}}" '+
                    '   disabledownload="{{(data.SupportDownload() ? 0 : 1)}}"' +
                    '                 hasviewer="{{data.HasViewer}}" data-item="{{data.RedirId}}" contentid="{{data.ContentIdentifier}}"' +
                    '                 ct="{{data.ContentType}}" pct="{{(data.AdditionalValues && data.AdditionalValues.Parent && data.AdditionalValues.Parent.ContentTypeKey ? data.AdditionalValues.Parent.ContentTypeKey : \'\')}}"' +
                '                 unc="{{(data.AdditionalValues && data.AdditionalValues.UNC ? HttpUtility.HtmlEncode(data.AdditionalValues.UNC) : \'\')}}"' +
                '                 parentId="{{(data.AdditionalValues  ? data.AdditionalValues.PId : \'-1\')}}"' +
        '                 {{ data.QueryAttr }} ',
    _DefaultSummaryBottomActionsBottom: '                <d:r visible="!context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideActions)">' +
        '                    <div class="_SML SummaryLinks" {{  (window.GeodiButtonsStyle=="1.0" || context.Options.SummaryRenderType=="bottom")?"":"style=\'display:none\'" }}>' +
        '                        <d:r visible="{{data.HasViewer}}">' +
        '                            <{{context.FastButtonTagName}} type="button" class="btn btn-default img-document16 img-default16 docOpenLink" title="[$default:open/js]" onclick="showDoc(this,event);" href="{{data.GetHref()}}" curl="{{data.GetHref(true)}}" target="docViewer" contentid="{{data.ContentIdentifier}}"></{{context.FastButtonTagName}}>' +
        '                        </d:r>' +
        '                        <d:r visible="{{!data.HasViewer && window.FacetLink && data.AdditionalValues && data.AdditionalValues.IsRoot && data.DisplayName && !context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.SimpleResutView) && !context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideFacet)}}">' +
        '                            <{{context.FastButtonTagName}} type="button" class="btn btn-default img-query16 img-default16 docOpenLink" title="[$query:do_query/js/html]" onclick="if(window.FacetLink)FacetLink({{data.ContentIdentifier}},\'goparent\',null,null,\'{{ HttpUtility.ScriptEncode(data.DisplayName) }}\',\'{{data.ContentType ? data.ContentType : \'\'}}\')"></{{context.FastButtonTagName}}>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.SupportDownload() }}">' +
        '                            <{{context.FastButtonTagName}} type="button" class="btn btn-default img-download16 img-default16" style="margin-top:0px;" title="[$default:download/js]" onclick="downloadDoc(\'{{data.ContentIdentifier}}\',event);return false;" href="javascript:void(0)"></{{context.FastButtonTagName}}>' +
        '                        </d:r>' +
        '                        <d:r visible="{{data.AdditionalValues && data.AdditionalValues.FCD_AlternateUrl }}">' +
        '                            <{{context.FastButtonTagName}} type="button" class="btn btn-default img-external-link16 img-default16 docOpenLink" title="[$default:openInBrowser/js]"  onclick="openDocOnBrowser(\'{{data.ContentIdentifier}}\',\'{{HttpUtility.ScriptEncode(data.AdditionalValues.FCD_AlternateUrl )}}\');" href="{{data.GetHref()}}" curl="{{data.GetHref(true)}}" contentid="{{data.ContentIdentifier}}"></{{context.FastButtonTagName}}>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.AdditionalValues && (data.AdditionalValues.FolderCanBeShown==1 || (data.AdditionalValues.Summary && data.AdditionalValues.Summary.FolderCanBeShown == 1)) && (!window.BrowserType || window.BrowserType.IsWinApp()) }}">' +
        '                            <{{context.FastButtonTagName}} type="button" class="btn btn-default img-folder16 img-default16" style="margin-top:0px;" title="[$DocViewer:open_folder/js/html]" onclick="OpenDocFolder(\'{{data.ContentIdentifier}}\',event);return false;" href="javascript:void(0)"></{{context.FastButtonTagName}}>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.SupportNote() }}">' +
        '                            <span id="fav_{{data.ContentIdentifier}}" data-label="{{data.LabelId}}" data-item="{{(data.RedirId)}}" onclick="GetAndShowLabelList(this, event,\'#inRightPanelForDoc\');" style="cursor:pointer;margin-right: .5em; margin-top: .2em" class="favLabel"><i class="fa fa-sticky-note-o"></i> </span>' +
        '                        </d:r>' +
        '                        <d:r visible="data.AdditionalValues && (data.AdditionalValues.Actions || data.AdditionalValues.Summary)" data="data.AdditionalValues.Actions?data.AdditionalValues.Actions:data.AdditionalValues.Summary.Actions">' +
        '                            <d:r visible=\'!context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideSimilarty) || (data.ID != "Get_DuplicateDocs" && data.ID != "Get_SimilarDocs")\'>' +
        '                                <{{context.FastButtonTagName}} type="button" id="btnIn_{{data.ID}}" class="btn btn-default img-default16 actionLink" style="background:url({{ context.ResolveUrl(\'~/GUIJsonService?op=getIco&Size=16x16&IconName=\' + data.IconName, true, true) }}) no-repeat center center" title="{{ data.DisplayName }}" href="{{data.Url}}" onclick="__DLVsummaryActionClick(this);" target="{{data.Target }}" contentid="{{data.parentdata.ContentIdentifier}}"></{{context.FastButtonTagName}}>' +
        '                            </d:r>' +
        '                        </d:r>' +
        '                        {{ data.CustomRender("buttons",data,context) }}' +
        '                    </div>' +
        '                </d:r>',

    _DefaultSummaryBottomActions: '                <d:r visible="!context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideActions)">' +
        '                    <div class="dropdown action-menu-container SummaryLinksTop">' +
        '<button class="action-menu-button btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="true">' +
        '<div class="action-menu-button"></div></button>' +
        '<ul class="dropdown-menu dropdown-menu-right dropdown-menu-balloon" role="menu">' +

        '                        <d:r visible="{{data.HasViewer}}">' +
        '                            <li role="presentation"><a role="menuitem" tabindex="-1" onclick="_hideTDrop(this);showDoc(this,event);" href="{{data.GetHref()}}" curl="{{data.GetHref(true)}}" target="docViewer" contentid="{{data.ContentIdentifier}}"><span class="img-document16 img-default16 docOpenLink" ></span> [$default:open/js] </a></li>' +
        '                        </d:r>' +
        '                        <d:r visible="{{!data.HasViewer && window.FacetLink && data.AdditionalValues && data.AdditionalValues.IsRoot && data.DisplayName && !context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.SimpleResutView) && !context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideFacet)}}">' +
        '                            <li role="presentation"><a role="menuitem" tabindex="-1" onclick="_hideTDrop(this);if(window.FacetLink)FacetLink({{data.ContentIdentifier}},\'goparent\',null,null,\'{{ HttpUtility.ScriptEncode(data.DisplayName) }}\',\'{{data.ContentType ? data.ContentType : \'\'}}\')"><span class="img-query16 img-default16 docOpenLink" ></span> [$query:do_query/js/html]</a></li>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.SupportDownload() }}">' +
        '                            <li role="presentation"><a role="menuitem" tabindex="-1" onclick="_hideTDrop(this);downloadDoc(\'{{data.ContentIdentifier}}\');return false;" href="javascript:void(0)"><span class="img-download16 img-default16" ></span> [$default:download/js]</a></li>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.AdditionalValues && (data.AdditionalValues.FolderCanBeShown==1 || (data.AdditionalValues.Summary && data.AdditionalValues.Summary.FolderCanBeShown == 1)) && (!window.BrowserType || window.BrowserType.IsWinApp()) }}">' +
        '                            <li role="presentation"><a role="menuitem" tabindex="-1" onclick="_hideTDrop(this);OpenDocFolder(\'{{data.ContentIdentifier}}\',event);return false;" href="javascript:void(0)"><span class="img-folder16 img-default16"></span> [$DocViewer:open_folder/js/html]</a></li>' +
        '                        </d:r>' +
        '                        <d:r visible="{{ data.SupportNote() }}">' +
        '                            <li role="presentation"><a role="menuitem" tabindex="-1" id="fav_{{data.ContentIdentifier}}" data-label="{{data.LabelId}}" data-item="{{(data.RedirId)}}" onclick="_hideTDrop(this);GetAndShowLabelList(this, event,\'#inRightPanelForDoc\');" style="cursor:pointer;" class="favLabel" notitle=1><span class="img-default16 fa fa-sticky-note-o"></span> [$default:add_note/js]</a></li>' +
        '                        </d:r>' +
        '                        <d:r visible="data.AdditionalValues && (data.AdditionalValues.Actions || data.AdditionalValues.Summary)" data="data.AdditionalValues.Actions?data.AdditionalValues.Actions:data.AdditionalValues.Summary.Actions">' +
        '                            <d:r visible=\'!context.Options.GUISearchOptions.Is(context.Options.GUISearchOptions.HideSimilarty) || (data.ID != "Get_DuplicateDocs" && data.ID != "Get_SimilarDocs")\'>' +
        '                                <li role="presentation"><a role="menuitem" tabindex="-1" id="btnIn_{{data.ID}}"  href="{{data.Url}}" onclick="_hideTDrop(this);__DLVsummaryActionClick(this);" target="{{data.Target }}" contentid="{{data.parentdata.ContentIdentifier}}"><span class="img-default16 actionLink" style="background:url({{ context.ResolveUrl(\'~/GUIJsonService?op=getIco&Size=16x16&IconName=\' + data.IconName, true, true) }}) no-repeat center center"></span> {{ data.DisplayName }}</a></li>' +
        '                            </d:r>' +
        '                        </d:r>' +
        '                        {{ data.CustomRender("buttons",data,context) }}' +

        '                    </ul></div>' +
        '                </d:r>',
    DefaultSummary: '        <div class="docDetail">' +
                    '            <div class="SummaryAll">' +
                    '                <div class="SummaryText __textclass geodiFormatable" contentid="{{data.ContentIdentifier}}">' +
                    '                    {{ data.Summary }}' +
                    '                    {{ data.CustomRender("summary",data,context) }}' +
                    '                </div>' +
                    '                       {{  TemplateToHtml(window.HtmlTemplates._DefaultSummaryBottomActionsBottom,data,context) }}' +
                    '            </div>' +
                    '        </div>',
    DefaultDLV: '            <div class="list-item pnl-query-item IsDoc DocALL docOpenLinkRoot DocArea dt_{{data.ContentType?data.ContentType.split(\':\')[1].replace(\'.\', \'\'):\'\' }}" ' +
                    '                 {{ data.GetDocItemAttr() }} >' +
                    '                <div id="heading_{{data.ContentIdentifier}}">' +
        '                    <div class="headerDocRow">' +
        '                <d:r visible="window.GeodiButtonsStyle!=\'1.0\' && (!context.Options.SummaryRenderType || context.Options.SummaryRenderType==\'top\') ">' +
        ' {{ TemplateToHtml(window.HtmlTemplates._DefaultSummaryBottomActions, data, context) }} ' +
        '                </d:r>' +

                    '                        <div>' +
                    '                            <div style="height:auto; position:relative;" class="imgContainer">' +
                    '                                {{ (context.Options.IconBefore ? context.Options.IconBefore(data.ContentIdentifier, data,context.Options,context) : \'\') }}' +
                    '                                <img class="imgPanelTitleIcon" src="{{data.GetIcon()}}"' +
                    '                                     title="{{HttpUtility.HtmlEncode(!context.Options.CurrentWSInfo ? \'\' : context.Options.CurrentWSInfo.GetContentTypeTitleFromId(data.ContentType))}}">' +
                    '                            </div>' +
                    '                            <d:r visible=context.Options.Query.GetNumberOfOccurrences>' +
                    '                                <span class="badge" style="display: inline-table; padding-left: 2%; padding-right: 2%;">{{data.NumberOfOccurrences?data.NumberOfOccurrences:0}}</span>' +
                    '                            </d:r>' +
                    '                            <div class="divPanelTitleText divPanelTitleTextEx fortooltip" title="{{data.GetTitle()}}">' +
                    '                                <{{context.FastLinkTagName}} class="inLink openLink geodiFormatable" {{ data.GetHrefFull() }} >' +
                    '                                        {{(data.DisplayName ? data.DisplayName : "-")}}</{{context.FastLinkTagName}}>' +
                    '                            </div><div class="docParentArea">' +
                    '                                {{ data.GetInfoHtml() }}' +
                    '                            </div>' +
                    '                        </div>' +
                    '                    </div>' +
                    '                </div>' +
                    '                <d:r visible="!context.Options.DisableSummary">' +
                    '                    <div id="collapse_{{data.ContentIdentifier}}" aria-expanded="true" class="SummaryAllCollapse collapse in">' +
                    '                        {{ TemplateToHtml(window.HtmlTemplates.DefaultSummary,data,context) }}' +
                    '                    </div>' +
                    '                </d:r>' +
                    '            </div>',
    DefaultDLV_Hor: '            <div class="documentViewFrame IsDoc docOpenLinkRoot DocArea defaultDlvHor"' +
                    '                 {{ data.GetDocItemAttr() }}>' +
                    '                <div class="documentViewFrameTitle">' +
                    '                    {{ (context.Options.IconBefore ? context.Options.IconBefore(data.ContentIdentifier, data,context.Options,context) : \'\') }}' +
                    '                    <img class="imgPanelTitleIcon" src="{{data.GetIcon()}}"' +
                    '                         title="{{HttpUtility.HtmlEncode(!context.Options.CurrentWSInfo ? \'\' : context.Options.CurrentWSInfo.GetContentTypeTitleFromId(data.ContentType))}}">' +
                    '                    <div class="divPanelTitleText divPanelTitleTextEx fortooltip" title="{{data.GetTitle()}}">' +
                    '                        <{{context.FastLinkTagName}} class="inLink openLink geodiFormatable" {{ data.GetHrefFull() }} > ' +
                    '                                   {{(data.DisplayName ? data.DisplayName : "-")}}</{{context.FastLinkTagName}}>' +
                    '                    </div>' +
                    '                </div>' +
                    '                <d:r visible="!context.Options.DisableSummary">' +
                    '                    {{ TemplateToHtml(window.HtmlTemplates.DefaultSummary,data,context) }}' +
                    '                </d:r>' +
                    '            </div>',
    DefaultDLV_List:'<ul class="list-group" >'+
	                '	<d:r data=data>'+
                    '		<li  class="list-group-item IsDoc DocALL docOpenLinkRoot DocArea dt_{{data.ContentType?data.ContentType.split(\':\')[1].replace(\'.\', \'\'):\'\'}} " {{ data.GetDocItemAttr() }}>' +
                    '			<a {{ data.GetHrefFull() }} class="data-kw ">' +
                    '               {{ (context.Options.IconBefore ? context.Options.IconBefore(data.ContentIdentifier, data,context.Options,context) : \'\') }}' +
                    '				<img class="imgPanelTitleIcon" src="{{data.GetIcon()}}" title="{{HttpUtility.HtmlEncode(!context.Options.CurrentWSInfo ? \'\' : context.Options.CurrentWSInfo.GetContentTypeTitleFromId(data.ContentType))}}"> ' +
                    '				<span title="{{(data.DisplayName ?HttpUtility.HtmlEncode(data.DisplayName) : "")}}"> {{(data.DisplayName ? data.DisplayName : "-")}} </span>' +
                    '			</a>' +
                    '		</li>' +
                    '	</d:r>' +
                    '</ul>',
    SignTemplate:  
        '<ul  style=\"list-style: none; padding-left: {{context.sub?20:0 }}px;\">' +
        '{{context.sub=true; null;}}'+
                    '   <d:r data=\"data\">' +
                    '           <li  class="{{data.SignatureStatus!==0? \'list-group-item-danger\':\'\'}}"><a href="#" title="[$GeodiDigitalSigner:SignatureValidation/js]" data-toggle="popover" data-trigger="focus" data-placement="bottom" data-content="{{data.ValidationResults.join("")}}">{{data.SignerCertificate}}</a>' +
                    '               {{TemplateToHtml(window.HtmlTemplates.SignTemplate, data.SerialSignatures, context)}}' +
                    '           </li>' +
                    '   </d:r>' +
                    '</ul>'
       
}

window.TemplateSelectors = {
    DefaultDLV: function (data, context) {
        return window.HtmlTemplates.GetTemplate("geodi-template", window.HtmlTemplates.DefaultDLV, context, data,false)
    },
    DefaultDLV_Hor: function (data, context) {
        return window.HtmlTemplates.GetTemplate("geodi-template", window.HtmlTemplates.DefaultDLV_Hor, context, data,true)
    }
}


function FieldValueGet(txt, queryObj, targetArrayName) {
    if (txt == null || txt == undefined) return null;
    var fld = txt.indexOf(":");
    if (fld != -1) txt = txt.substr(0, fld);
    if (txt.length == 0) return nul;
    txt += ":";
    var query = queryObj ? queryObj : (window.CurrentQueryContainer ? window.CurrentQueryContainer.CurrentQuery : null);
    if (!query) return null;
    if (!targetArrayName) targetArrayName = "SearchStrings";
    if (!query[targetArrayName])
        query[targetArrayName] = [];
    for (var i = 0; i < query[targetArrayName].length; i++) {
        if (query[targetArrayName][i].indexOf(txt) != -1) {
            var val = query[targetArrayName][i];
            var spIndex = val.indexOf(":") + 1;
            return val.substr(spIndex, val.length - spIndex);
        }
    }
    return null;
}
function FieldValueSet(txt, disableDoQuery, queryObj, targetArrayName) {
    FieldLink(txt, null, disableDoQuery, queryObj, targetArrayName);
}
function FieldLink(txt, e, disableDoQuery, queryObj, targetArrayName) {
    if (e)
        CancelEvent(e);
    if (!targetArrayName) targetArrayName = "SearchStrings";
    var query = queryObj ? queryObj : (window.CurrentQueryContainer ? window.CurrentQueryContainer.CurrentQuery : null);
    if (!query) throw new Error('query object required');
    if (!query[targetArrayName])
        query[targetArrayName] = [];
    if (query[targetArrayName].indexOf(txt) != -1) return false;
    var fld = txt.indexOf(":");
    if (fld < 0) {
        if (query[targetArrayName].indexOf(txt) == -1)
            query[targetArrayName].push(txt);
        return true;
    }
    var fldtext = txt.substr(0, fld + 1);
    for (var i = 0; i < query[targetArrayName].length; i++) {
        if (query[targetArrayName][i].indexOf(fldtext) != -1) {
            query[targetArrayName].splice(i, 1);
            break;
        }
    }
    var vl = txt.substr(fld + 1).trim();
    if (vl.length > 0)
        query[targetArrayName].push(txt);
    if (!disableDoQuery && window.doQueryText) doQueryText(true);
    return true;
}

//<dcc:ResourceControl src = "GUI/js/DeceTemplate.js" addline=true description="IncludeScript" />