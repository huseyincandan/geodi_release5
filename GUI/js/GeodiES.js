﻿window._ViewItems = [];
function AddViewItem(itm) {
    if (itm.priority == undefined)
        itm.priority = ((window._ViewItems.length + 1) * 100);
    window._ViewItems.push(itm);
}
function InitViewItem() {
    window._ViewItems.sort(function (a, b) {
        if (a.priority < b.priority) return -1;
        if (a.priority > b.priority) return 1;
        return 0;
    });
    var d = $("#helpButtonsArea");
    d.html('');
    for (var i = 0; i < window._ViewItems.length; i++) {
        var v = window._ViewItems[i];
        if (v.HelpDOM) {
            v.Help = $(v.HelpDOM).html();
        }
        if (v.Help) {
            var html = '<span id="help_' + v.Name + '" class="forHelp" data-help="none" data-placement="left" data-ns="none" style="display:none;vertical-align:top"><span class="directValue" style="display:none">' + v.Help + '</span></span>';
            d.append($(html));
        }
    }
    d.embedHelp();
}

window._MenuItems = [];
function AddStartMenuItem(itm) {
    if (itm.priority == undefined)
        itm.priority = ((window._MenuItems.length + 1) * 100);
    window._MenuItems.push(itm);
}

function _addOptions(arr, selObjs, wGetter, defautOptionText, disableAddDefault, hideDefault,disableClear) {
    $.each(selObjs, function (i1, selObj) {
        if (!disableClear)
            selObj.empty();
        var op;
        if (!disableAddDefault && $(".isDefaultOption", selObj).length == 0) {
            op = $("<option class='isDefaultOption' " + (hideDefault ? ' style="display:none"' : '') + "></option");
            op.html(defautOptionText ? defautOptionText : "[$default:auto/js]");
            op.attr("value", "");
            selObj.append(op);
        }
        for (var i = 0; i < arr.length; i++) {
            var lng = wGetter(arr[i]);
            if (lng == null)
                continue;
            op = $("<option></option>");
            op.html(lng.Name);
            op.attr("value", lng.Value);
            if (lng.Selected)
                op.attr("selected", true);
            selObj.append(op);
        }
    });
}

function RenderMenuNow() {

    window._MenuItems.sort(function (a, b) {
        if (a.priority < b.priority) return -1;
        if (a.priority > b.priority) return 1;
        return 0;
    });

    _RenderMenuIn(window._MenuItems, ".menuMobile", "mobile");
    _RenderMenuIn(window._MenuItems, ".menuRoot", "desktop");
    _RenderMenuIn(window._MenuItems, "#_startButtons", "startmenu");
    _RenderMenuIn(window._MenuItems, ".SearchTextBoxArea", "search");


}
function IsActiveView(viewName) {
    if (window.CurrentQueryContainer && window.CurrentQueryContainer.ActiveViews) {
        for (var i = 0; i < window.CurrentQueryContainer.ActiveViews.length; i++)
            if (window.CurrentQueryContainer.ActiveViews[i].Name.toLowerCase() == viewName.toLowerCase())
                return true;
    }
    return false;
}

function _RenderMenuIn(menu, targetDom, mode) {

    targetDom = $(targetDom);
    targetDom.html('');

    $.each(menu, function (key, itm) {
        var html = "";

        var exStyleIn = "";
        var mblClass = "";

        switch (mode) {
            case 'mobile':
                if (!itm.ShowMobile) return;
                if (itm.useExPadding)
                    exStyleIn = 'padding-left:5px;margin:5px;';
                mblClass = 'visible-xs-';
                break;
            case 'desktop':
                if (!itm.ShowDesktop) return;
                if (itm.useExPadding)
                    exStyleIn = 'padding-left:15px;margin:5px;';
                break;
            case "startmenu":
                if (!itm.ShowStart) return;
                break;
            case "search":
                if (!itm.ShowSearch) return;
                break;
        }
        var exClass2 = itm.exClass2Fnc ? itm.exClass2Fnc() : "";
        if (mode == "startmenu") {

            html = '<button type="button" class="btn btn-default ' + itm.exClass + ' ' + exClass2 + '" style="margin:4px 4px" onclick="' + itm.click + '" title="' + (itm.title?itm.title:itm.text) + '">' +
                       '<span class="' + (itm.icoClass ? itm.icoClass : '') + ' img-default" aria-hidden="true" ' + (itm.icoStyle ? ('style="' + itm.icoStyle + '"') : "") + '>&nbsp;</span> '
                       + itm.text +
                    '</button>';
            targetDom.append($(html));
            return;
        }

        if (mode == "search") {
            html = '<button type="button" class="btn btn-default modeBtn searchBorder ' + itm.exClass + ' ' + exClass2 + ' ' + (itm.MobileItem ? 'VisibleMinMobile' : 'VisibleMaxMobile') + '" style="padding:4px 4px" onclick="' + itm.click + '" title="' + (itm.title?itm.title:itm.text) + '">' +
                                       '<span class="' + (itm.icoClass ? itm.icoClass : '') + ' img-default" aria-hidden="true" ' + (itm.icoStyle ? ('style="' + itm.icoStyle + '"') : "") + '>&nbsp;</span>' +
                                       (itm.MobileItem ? '' : '<span class="modeBtnText">' + itm.text + '</span>') +
                                   '</button>';
            targetDom.append($(html));
            return;
        }

        if (itm.Data && itm.Template && itm.Template.length>0) {
            html = TemplateToHtml(itm.Template, itm, { menu: menu, targetDom: targetDom, mode: mode, item:itm })
            targetDom.append($(html));
            return 
        }



        var pStyle = (itm.hide ? "display:none;" : "") + (exStyleIn);
        if (pStyle) pStyle = 'style="' + pStyle + '" ';

        var eCss = itm.exClass;
        if (exClass2) {
            eCss = eCss ? (eCss + " " + exClass2) : exClass2;
        }

        eCss = eCss ? ("class=\"" + eCss + "\"") : "";
        if (itm.HTML_renderer)
            html = itm.HTML_renderer(itm);
        else
            html = '<li id="' + itm.id + '" role="presentation" ' + eCss + ' ' + pStyle + ' ' + (itm.cancelProp ? 'onclick="event.stopPropagation();$(\'.DPStyleDropDownContainer\').not($(\'.DPStyleDropDownContainer\',this)).removeClass(\'open\');"' : '') + ' >' +
                (itm.renderer || itm.noHrefContainer ? '' : '<a class="inLink" title="' + (itm.title?itm.title:itm.text) + '" ' + (itm.click ? ('onclick="' + itm.click + '"') : '') + ' ' + (itm.dataHtml ? itm.dataHtml : '') + '>') +
                            (itm.icoHtml ?
                                itm.icoHtml :
                                '<span class="' + (itm.icoClass ? itm.icoClass : '') + ' img-default inSpanIcon" ' + (itm.icoStyle ? ('style="' + itm.icoStyle + '"') : "") + '></span>') +

                            (itm.icoExHtml ? itm.icoExHtml : '') +
                            (itm.renderer ? '<span class=inrender></span>' : '<span class="' + (itm.exClassSpan?itm.exClassSpan:'') + ' ' + mblClass + 'inSpanText"> ' + itm.text + '</span>') +
                            (itm.afterHtml ? itm.afterHtml : '') +
                        (itm.renderer || itm.noHrefContainer ? '' : '</a>') +
                '</li>';
        if (html) {
            var dom = $(html);
            if (itm.renderer) {
                itm.renderer(dom, $(".inrender", dom), itm);
            }

            targetDom.append(dom);
            if (itm.onDomAdded)
                itm.onDomAdded(targetDom, dom, itm);
        }
    });

}

