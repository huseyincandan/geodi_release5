﻿function inputSearch(pThis, target,callback,textChildClass,showingCallback,lowerCaseFunction) {
    var tl = function (vl) {
        if (lowerCaseFunction)
            return lowerCaseFunction(vl);
        return vl.turkishToLower ? vl.turkishToLower() : vl.toLowerCase();
    }
    var value = $(pThis).val();
    if (value)
        value = tl(value);
    else {
        $(target).show();
        if (callback)
            callback(value,true,true);
        return;
    };
    var showAny = false;
    var equalAny = false;
    $(target).filter(function () {
        var tagetDom = $(this);
        var textDom = textChildClass ? $(textChildClass, tagetDom) : tagetDom;
        var isHide = tl(textDom.text()).indexOf(value) == -1;
        if (showingCallback) {
            var state = {
                root: target,
                item: tagetDom,
                text: textDom.text(),
                search: value,
                show: isHide ? false : true
            }
            showingCallback(state);
            isHide = state.show ? false : true;
        }

        if (isHide) {
            tagetDom.hide();
        }
        else {
            tagetDom.show();
            showAny = true;
            if (callback && !equalAny)
                equalAny = tl(textDom.text()).trim() == value.trim();
           
        }
    });
    if (callback)
        callback(value, showAny, equalAny);
}