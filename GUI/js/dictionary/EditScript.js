﻿function ___GetDictionaryEditHandler() {
    return window.DictionaryEditHandler ? window.DictionaryEditHandler : "GeodiDictionaryService";
}
function GeodiDictionary(id) {
    this.ID = id;
    this.InsertGeodiDictionaryItemFromCSVStream = function (dictName, filePath, callBackFunction) {
        $.post(___GetDictionaryEditHandler()+'?op=InsertGeodiDictionaryItemFromCSVStream', { 'dictName': dictName, 'filePath': filePath }, callBackFunction, 'json');
    }
    this.GetGeodiDictionaryItems = function (dictName, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=GetGeodiDictionaryItems', { 'dictName': dictName }, callBackFunction, 'json');
    }
    this.GetGeodiDictionaryItemGroups = function (dictName, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=GetGeodiDictionaryItemGroups', { 'dictName': dictName }, callBackFunction, 'json');
    }
    this.InsertGeodiDictionaryItemGroup = function (dictName, groupName, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=InsertGeodiDictionaryItemGroup', { 'dictName': dictName, 'groupName': groupName }, callBackFunction, 'json');
    }
    this.InsertGeodiDictionaryItem = function (dictName, item, callBackFunction) {
        $.post(___GetDictionaryEditHandler()+'?op=InsertGeodiDictionaryItem', { 'dictName': dictName, 'item': JSON.stringify(item) }, callBackFunction, 'json');
    }
    this.UpdateGeodiDictionaryItem = function (dictName, item, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=UpdateGeodiDictionaryItem', { 'dictName': dictName, 'item': JSON.stringify(item) }, callBackFunction, 'json');
    }
    this.GetGeodiDictionaryItem = function (dictName, objectid, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=GetGeodiDictionaryItem', { 'dictName': dictName, 'objectid': objectid }, callBackFunction, 'json');
    }
    this.DeleteGeodiDictionaryItems = function (dictName, objectIDs, callBackFunction) {
        $.post(___GetDictionaryEditHandler() + '?op=DeleteGeodiDictionaryItems', { 'dictName': dictName, 'objectIDs': objectIDs }, callBackFunction, 'json');
    }
    
    this.UploadDictionaryDts2 = function (dts, ClearList, callBackFunction) {
       
        $.post(___GetDictionaryEditHandler() + '?op=UploadDictionaryDts2', {
            "DictionaryName": this.ID,
            "DTSoutput": JSON.stringify(dts),
            "ClearList": ClearList?true:false
        }, callBackFunction, 'json');
    }
}

window.currentDictionary = new GeodiDictionary('');

function GeodiSourceItem() {
    this.GroupID = 0;
    this.Keyword = '';
    this.Alternatives = '';
    //this.GeometryWKT = '';
}