﻿function ___GetDictionaryBaseHandler() {
    return window.DictionaryBaseHandler ? window.DictionaryBaseHandler : "GeodiDictionaryRecognizerBaseService";
}

function GeodiDictionaryRecognizerBase(id) {
    this.ID = id;

    //TODO : Aşağıdaki fonksiyonlarda dictName parametresi gereksiz. this.Name kullanalım.
    this.CreateOrUpdateGeodiDictionary = function (dict, callBackFunction) {
        return $.post(___GetDictionaryBaseHandler()+'?op=CreateOrUpdateGeodiDictionary', { 'dict': JSON.stringify(dict) }, callBackFunction, 'json');
    }
    this.GetNewGeodiDictionaryObject = function (factoryTypeName, callBackFunction) {
        return $.post(___GetDictionaryBaseHandler() + '?op=GetNewGeodiDictionaryObject', { 'factoryTypeName': factoryTypeName }, callBackFunction, 'json');
    }
    this.GetGeodiDictionary = function (dictName, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetGeodiDictionary', { 'dictName': dictName }, callBackFunction, 'json');
    }
    this.GetDictionaryList = function (callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetDictionaryList', {},
            function (data, b, c, d) {
                if (data && data.sort && "".localeCompare)
                    data.sort(function (a, b) { return a.DisplayName.localeCompare(b.DisplayName) });

                callBackFunction(data, b, c, d);

        }, 'json');
    }
    this.GetTemplateDictionaryList = function (callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetTemplateDictionaryList', {}, callBackFunction, 'json');
    }
    this.GetAllDictionaryList = function (callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetAllDictionaryList', {}, callBackFunction, 'json');
    }
    this.SelectDictionaries = function (tableSchemaID, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=SelectDictionaries', { 'tableSchemaID': tableSchemaID }, callBackFunction, 'json');
    }
    this.SelectDictionariesByLabel = function (label, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=SelectDictionariesByLabel', { 'Label': label }, callBackFunction, 'json');
    }
    this.GetDictionaryGroupNames = function (callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetDictionaryGroupNames', {}, callBackFunction, 'json');
    }
    this.GetDictionaryClasses = function (callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetDictionaryClasses', {}, callBackFunction, 'json');
    }
    this.DeleteDictionary = function (dictName, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=DeleteDictionary', { 'dictName': dictName }, callBackFunction, 'json');
    }
    this.CreateDictionaryObjectFromFile = function (fileName, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=CreateDictionaryObjectFromFile', { 'fileName': fileName }, callBackFunction, 'json');
    }
    this.CreateGeodiDictionaryObjectFromFileDictionary = function (fileDict, displayName, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=CreateGeodiDictionaryObjectFromFileDictionary', { 'fileDict': JSON.stringify(fileDict), 'displayName': displayName }, callBackFunction, 'json');
    }
    this.GetCreateGeodiDictionaryObjectFromFileDictionaryStatus = function ( displayName, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=GetCreateGeodiDictionaryObjectFromFileDictionaryStatus', { 'displayName': displayName }, callBackFunction, 'json');
    }
    this.IsDictionaryPresent = function (id, callBackFunction) {
        $.post(___GetDictionaryBaseHandler() + '?op=IsDictionaryPresent', { 'id': id }, callBackFunction, 'json');
    }
}



window.currentDictionaryRecognizerBase = new GeodiDictionaryRecognizerBase('');
