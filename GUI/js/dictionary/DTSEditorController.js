﻿function $extendObject(oldObj,newObj) {
    var asgObj = Object.assign(oldObj, newObj);
    oldObj = asgObj;
}

DTSEditorController = function (settings)
{
    
    var elementTypes = { Dictionary: 1 };
    var options = {
        element: elementTypes.Dictionary,
        current: {
            Data: (typeof window.currentDictionary.Data === 'undefined' ? {} : window.currentDictionary.Data) ,
            TableSchema: (typeof window.currentDictionary.TableSchema === 'undefined' ? {} : window.currentDictionary.TableSchema),
            GUIHideFileSelect: false
        },
        header: '',
        submitButtonText: '',
        resources: {},
        dtsProperties: {
            IsInit: false,
            tableNameVisible: true,
            DTSElementsVisible: false,
            DTSElementsShowPanel: false
        },
        dtsAdvancedScreenStyle: false
    };
   

    $extendObject(options, settings);
   
    $extendObject(window.DefaultDTS, options.dtsProperties);
   
    this.Schema = {};
    var DefaultDTS = window.DefaultDTS;
    CreateDtsShema = function (obj) {
        var TableSchema = obj;
        this.Schema = {
            "ID": TableSchema.ID,
            "DisplayName": (typeof TableSchema.DisplayName === 'undefined' ? '' : TableSchema.DisplayName),
            "Columns": []
        }
        if (options.element === elementTypes.Dictionary) {
           
            this.Schema.Columns.push({ "Attributes": { "Required": true, "DefaultColumnNames": ["Keywords", "Anahtar Kelime", "Kelime"] }, "ID": "___Keyword", "DisplayName": "[$dictionary_wizard:keyword/js]" });
            if (!obj.GUIHideGeomColumn && TableSchema.IsSpatial) {
                this.Schema.Columns.push({ "Attributes": { "Required": false, "DefaultColumnNames": ["Geometry", "Geometri"] }, "ID": "Geom", "DisplayName": "[$dictionary_wizard:geometry/js]" });
                this.Schema.Columns.push({ "Attributes": { "Required": false, "EPSGField": true, "DefaultColumnNames": ["SRID", "SRID"] }, "ID": "SRID", "DisplayName": "SRID" });
            }
            
            this.Schema.Columns.push({ "Attributes": { "Required": false, "DefaultColumnNames": ["Group", "Grup"] }, "ID": "___Group", "DisplayName": "[$default:group/js]" });
            this.Schema.Columns.push({ "Attributes": { "Required": false, "DefaultColumnNames": ["Description", "Açıklama"] }, "ID": "___Detail", "DisplayName": "[$default:description/js]" });

            this.Schema.Columns.push({ "Attributes": { "Required": false, "MultiSelect": true, "DefaultColumnNames": ["Alternatives", "Eş Anlamlılar", "Eşanlamlılar", "Synonyms"] }, "ID": "___Alternatives", "DisplayName": "[$dictionary_wizard:alternatives/js]" })

            if (TableSchema.Columns)
                for (var i = 0; i < TableSchema.Columns.length; i++) {
                    var column = TableSchema.Columns[i];
                    this.Schema.Columns.push({ "Attributes": { "Required": false, "DefaultColumnNames": [column.ID, column.DisplayName] }, "ID": column.ID, "DisplayName": column.DisplayName });
                }
        }
        return this.Schema;
    };
    this.Destroy = function () {
        DefaultDTS.Destroy();
    };
    Init = function () {
        try {
            if (window.parent && window.parent.SupportsFromFileFilter)
                window.SupportsFromFileFilter = window.parent.SupportsFromFileFilter;
        }
        catch (e) { }

        
        if (options.current.GUIHideFileSelect)
            DefaultDTS.DTSShowSelectFile = false;

        
       
        DefaultDTS.UpdateGUI(options.current.Data);
        
        DefaultDTS.Init(CreateDtsShema(options.current.TableSchema));

        if (options.dtsAdvancedScreenStyle) {
            var defaultRowMultiplier = 45;
            var numberFormGroup = $("#__DTSElementsPanel div.panel-default div.form-group").length;
            var realHeight = (defaultRowMultiplier * numberFormGroup) + "px";
            $("#__DTSElementsPanel div.panel").css("height", realHeight).css("padding-top", "10px");
            $("#__DTSElementsPanel div.panel div.noPadding").addClass("customDtsStyle");
        }
    }
    Init();
   
    return this;
}
