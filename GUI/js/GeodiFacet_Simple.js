﻿

function GeodiFacetLink(params, pThis) {
    var wsInfoItem = params.wsInfoItem;
    this.icon = (params.wsInfoItem ? { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=" + wsInfoItem.IconName, true, true) } : "");
    this.title = (params.wsInfoItem ? (HttpUtility.HtmlEncode(wsInfoItem.DisplayName)) : "");
    this.html = (params.wsInfoItem ? wsInfoItem.DisplayName : "");
    this.onclick = pThis.Helpers.LinkParser({ id: params.Id, type: params.Name });
    this.class = "facetLink FacetEnum";
    return this;
}
function GeodiFacet(options) {
    this.FacetArea = "#facetArea";
    this.FacetItems = [];
    this.Helpers = {
        pThis: this,
        CurrentQuery: function () { return window.CurrentQueryContainer.CurrentQuery; },
        CreateLink: function (properties) {
            var $innerHtmlSpan = $("<span/>", {});
            var $removeIcon = $("<span/>", { class: "img-delete16 img-button16", style: "vertical-align: middle;" });
            var $element = $("<div/>", { class: properties.class, onclick: properties.onclick });

            $element.attr("itemid", properties.itemid);

            if (typeof properties.title !== "undefined")
                $element.attr("title", properties.title);

            if (properties.remove)
                $element.append($removeIcon);

            if (typeof properties.icon !== 'undefined') {
                var $iconImg = $("<img/>", { src: properties.icon.src, class: "img-default" });
                $element.append($iconImg);
            }

            if (typeof properties.html !== 'undefined') {
                $innerHtmlSpan.html(properties.html);
                $element.append($innerHtmlSpan);
            }

            if (typeof properties.countBar !== 'undefined') {
                var $countBar = $("<span/>", properties.countBar);
                $countBar.html(properties.countBar.html);
                $element.append($countBar);
            }

            return $element;
        },
        CreatePanel: function (properties) {
            var pThis = this.pThis;
            if (!pThis.FacetPanels)
                pThis.FacetPanels = [];

            var $panel = $("<div/>", { class: "panel panel-default", id: properties.id.replace("#", "") });
            if (properties.style)
                $panel.attr("style", properties.style);

            $panel.header = $("<div/>", { class: "panel-heading", id: properties.id.replace("#", "") + "_header" });
            $panel.header.title = $("<span/>");
            $panel.body = $("<div/>", { class: "panel-body", id: properties.id.replace("#", "") + "_body" });
            $panel.body.addItem = function (link) {
                $panel.body.append(link);
                $(document.body).trigger("facetPanelChange", [{ target: $panel }]);
            };
            if (properties.body) {
                if (properties.body.class)
                    $panel.body.addClass(properties.body.class);
            }
            if (properties.priorty)
                $panel.priorty = properties.priorty

            if (typeof properties.header !== 'undefined') {
                if (properties.header.title) {
                    $panel.header.html(properties.header.title);
                    $panel.header.append($panel.header.title);
                }
                if (properties.header.html)
                    $panel.header.append(properties.header.html);
                if (properties.chart) {
                    var $chartSpan = $("<span/>", { class: "facetCountR", style: "cursor: pointer", onclick: "GeodiFacet.Helpers.ShowPieChart('" + properties.id.replace("#", "") + "_chartArea" + "')" });
                    $chartSpan.html("<i class='fa fa-pie-chart'></i>");
                    $panel.header.append($chartSpan);
                }
                $panel.append($panel.header);
            }
            $panel.append($panel.body);
            if (properties.chart) {
                $panel.chart = $("<div/>", { id: properties.id.replace("#", "") + "_chartArea", class: "panel-body chartArea", style: "display: none;" });
                $panel.append($panel.chart);
            }
            if (properties.clear) {
                $panel.body.clear = function () {
                    $panel.body.empty();
                    $(document.body).trigger("facetPanelChange", [{ target: $panel }]);
                }
                if (properties.chart)
                    $panel.chart.clear = function () {
                        $panel.chart.empty();
                    }
            }

            var facetArea = this.pThis.FacetArea;

            $panel.priorty = (properties.priorty ? properties.priorty : 0);
            var fIndex = pThis.FacetPanels.findIndex(function (panel) { return panel.prop("id") == $panel.prop("id") });
            if (fIndex < 0)
                pThis.FacetPanels.push($panel);
            else
                pThis.FacetPanels[fIndex] = $panel;

            pThis.FacetPanels.AddedPriorties = [];
            $(facetArea).empty();
            pThis.FacetPanels.sort(function (a, b) { return b.priorty - a.priorty }).forEach(function (panel) {
                pThis.FacetPanels.AddedPriorties.push(panel.priorty);
                if (pThis.FacetPanels.AddedPriorties.max() == panel.priorty)
                    $(facetArea).prepend(panel);
                else
                    $(facetArea).append(panel);

            });

            return $panel;
        },
        ConvertObjToArray: function (rObj) {
            var arr = [];
            for (var key in rObj) {
                var obj = {};
                obj.id = key;
                obj.value = rObj[key];
                arr.push(obj);
            }
            return arr;
        },
        CreateCountBarStyle: function (params) {
            var pStyle = '', pValue = '';
            if (params.foundDocument)
                pValue = (params.count / params.foundDocument) * 100;
            if (!params.direction) params.direction = "to right";
            var val = "linear-gradient(" + params.direction + ", " + (window.FacetPercentColor ? window.FacetPercentColor : "orange") + " " + (pValue) + "%, #DEDEDE " + (pValue) + "%)" //rgb(48, 228, 48)
            if (!params.onlyVal)
                val = "background: " + val + ";\"";
            return val;
        },
        CreateChart: function (params) {
            if (!params.data) return;
            var pieArea = $(params.chart);
            var pieDom = pieArea[0];
            if (!params.chartType) params.chartType = 'pie';
            if (pieDom.PieChart)
                pieDom.PieChart.destroy();

            var cdata = [];
            params.data.forEach(function (f) {
                cdata.push(f.ChartItem);
            });

            pieDom.LinkData = params.data;

            pieDom.PieChart = c3.generate({
                bindto: "#" + params.chart.prop("id"),
                size: {
                    height: 270,
                    width: 280
                },
                data: {
                    columns: cdata,
                    type: params.chartType,
                    order: 'desc',
                    onclick: function (d, el) {

                        var dm = $(el).parents(".chartArea");
                        var data = dm[0].LinkData.find(function (f) { return f.ChartItem[0] == d.id });

                        if (data) {
                            var linkData = data.LinkData;
                            FacetLink(linkData.Id, linkData.Category, false, true);
                        }

                    }
                },
                legend: {
                    show: params.hideLegend ? false : true
                }
            });
        },
        ContentTypeData: function () {
            var returnObj = [];
            var data = window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders;
            if (data && data.NamesOrder.length > 0) {
                data.NamesOrder.forEach(function (namesOrder) {
                    var names = data.Names[namesOrder];
                    if (names.AllTyes && names.AllTyes.length > 0)
                        returnObj.push({ namesOrder: namesOrder, names: data.Names[namesOrder] });

                });
            }
            return returnObj;
        },
        CurrentQueryAddorRemove: function (params, pThis) {
            var currentQuery = this.CurrentQuery();
            if (!currentQuery[params.CurrentQueryItemKey]) currentQuery[params.CurrentQueryItemKey] = (params.CurrentQueryItemKey == "RecognizerIDs" ? "" : []);
            var items = (!Array.isArray(currentQuery[params.CurrentQueryItemKey]) ? (currentQuery[params.CurrentQueryItemKey] == "" ? [] : currentQuery[params.CurrentQueryItemKey].split(',')) : currentQuery[params.CurrentQueryItemKey]);
            var index = items.findIndex(function (f) { return f == params.Id });
            if (params.remove) {
                if (index > -1) {
                    items.splice(index, 1);
                    if (!Array.isArray(currentQuery[params.CurrentQueryItemKey]))
                        currentQuery[params.CurrentQueryItemKey] = items.join(',');
                    else
                        currentQuery[params.CurrentQueryItemKey] = items;
                    return true;
                }
            }
            else {
                if (index < 0) {
                    items.push(params.Id);
                    if (!Array.isArray(currentQuery[params.CurrentQueryItemKey]))
                        currentQuery[params.CurrentQueryItemKey] = items.join(',');
                    else
                        currentQuery[params.CurrentQueryItemKey] = items;
                    return true;
                }
            }

        },
        FacetModeSwap: function (panel, obj) {

            var allArea = $("#" + panel);
            var listArea = $("#" + panel + "_body");
            var pieArea = $("#" + panel + "_chartArea");

            if (allArea.attr("fmode") == 0) {
                listArea.hide();
                pieArea.fadeIn("fast");
                $(obj).css("color", "black");
                /*if (pieArea[0].PieChart)
                    pieArea[0].PieChart.flush();*/
                allArea.attr("fmode", 1);
            }
            else {
                pieArea.hide();
                $(obj).css("color", "gray");
                listArea.fadeIn("fast");
                allArea.attr("fmode", 0);
            }
        },
        LinkParser: function (params) {
            if (!params.remove)
                params.remove = false;
            return "GeodiFacet.AddorRemove(" + JSON.stringify(params) + ",this)";
        },
        ShowPieChart: function (chartElement) {
            var element = $("#" + chartElement);
            if (element.css("display") == "none") {
                element.parent().find("div.panel-body").eq(0).hide();
                element.show();
            } else {
                element.hide();
                element.parent().find("div.panel-body").eq(0).show();
            }
        }

    };
    this.Init = {};
    this.Clear = function () {
        this.FacetPanels.forEach(function (panel) {
            if (panel.body.clear) {
                panel.body.clear();
            }
            if (panel.chart) {
                panel.chart.clear();
            }
        });
        this.FacetItems.forEach(function (facetItem) {
            if (facetItem.ChartData)
                facetItem.ChartData = [];
        });
        $(document.body).trigger("GUI_FacetReady");
    };
    this.AddFacetItem = function (newFacetItem) {
        //newFacetItem=$.extend(new baseFacetItem(), newFacetItem);
        this.FacetItems.push(newFacetItem);
        return newFacetItem;
    }
    this.Render = function (params) {
        this.__LastParams = params;
        if (!params) params = this.__LastParams;
        var pThis = this;
        pThis.Clear();
        this.FacetItems.forEach(function (facetItem) {
            facetItem.Render($.extend(params, { serverInfo: params.data, clientTime: params.cTime, serverTime: params.sTime }), pThis);
        });
        $(document.body).trigger("GUI_FacetReady");
    };
    this.AddorRemove = function (params, target) {
        var pThis = this;
        var sParams = { Id: params.id, Type: params.type, Op1: params.op1, Op2: params.op2 };
        var facetItem = this.FacetItems.find(function (f) { return f.Name == params.type });
        if (facetItem) {
            var changed = false;
            if (remove)
                changed = facetItem.Remove(sParams, pThis);
            else
                changed = facetItem.Add(sParams, pThis);
            if (changed)
                getResult(true, UpdateQueryIsEnable());
        }
    };

    $(document.body).on("facetPanelChange", function (e, arg) {
        if ($(arg.target.body).find("div").length < 1) $(arg.target).hide();
        else $(arg.target).show();
    });
}
$(document).ready(function () {
    window.GeodiFacet = new GeodiFacet();
    var detailPanel = window.GeodiFacet.Helpers.CreatePanel({ id: "fc_detail_p", priorty: 2, clear: true, header: { title: "[$query:content_properties/js]" } });
    var contentTypePanel = window.GeodiFacet.Helpers.CreatePanel({ id: "fc_contentType_p", priorty: 1, clear: true, chart: true, header: { title: "[$query:type/js]" } });
    window.GeodiFacet.QueryPanel = window.GeodiFacet.Helpers.CreatePanel({ id: "fc_query_p", clear: true, priorty: 5, style: "display: none", header: { title: "[$default:query_page/js]" } });
    window.GeodiFacet.AddFacetItem({
        Name: "status",
        Target: window.GeodiFacet.Helpers.CreatePanel({ id: "fc_status_p", priorty: 6, body: { class: "fc-status" } }),
        Render: function (params, pThis) {
            var body = this.Target.body;
            $(body).empty();
            if (!window.IsTestMode) {
                var clientTimeTitle = "";
                if (params.clientTime >= 0)
                    clientTimeTitle = (params.clientTime / 1000).toFixed(2) + " s network";
                if (params.serverTime >= 0)
                    $(body).append("<div>" + (params.serverTime / 1000).toFixed(2) + " s <span style='color:#AAA;font-size:12px;'>( " + clientTimeTitle + " )</span></div>");
                else if (clientTimeTitle)
                    $(body).append("<div>" + clientTimeTitle + " </div>");
            }

            var formatTxt = "[$query:format_total_info/js]";
            if (params.serverInfo.FoundDocument > 1)
                formatTxt = "[$query:format_total_info_multiple/js]";
            $(body).append("<div>" + formatTxt.replace("{0}", FormatNumber(params.serverInfo.FoundDocument)).replace("{1}", FormatNumber(params.serverInfo.TotalDocument)) + "  </div>");


            var pStyle = pThis.Helpers.CreateCountBarStyle({ count: params.serverInfo.FoundDocument, foundDocument: params.serverInfo.TotalDocument, onlyVal: true });
            $(body).css("background", pStyle);
        }

    });
    window.GeodiFacet.AddFacetItem({
        Name: "enumarator",
        Target: window.GeodiFacet.Helpers.CreatePanel({ id: "fc_enumarator_p", priorty: 4, header: { title: "[$query:enumarators/js]" }, clear: true, chart: true }),
        ChartData: [],
        Helpers: {
            GetWsInfoItem: function (id) {
                return window.CurrentQueryContainer.CurrentWSInfo.Enumarators.find(function (f) { return f.ItemHashCode == id });
            },
            AddChartData: function (enumarator, fThis) {
                var wsInfoItem = this.GetWsInfoItem(enumarator.id);
                var displayName = (wsInfoItem ? wsInfoItem.DisplayName : "");
                var obj = { LinkData: { Id: enumarator.id, Category: fThis.Name }, ChartItem: [displayName, enumarator.value] };
                fThis.ChartData.push(obj);
            }
        },
        CurrentQueryItemKey: "Enumarators",
        Render: function (params, pThis) {
            var fThis = this, linkProperties = null, serverInfo = params.serverInfo, currentQuery = pThis.Helpers.CurrentQuery();

            this.GetLinkProperties = function (enumaratorId) {
                params.Id = enumaratorId;
                params.wsInfoItem = this.Helpers.GetWsInfoItem(enumaratorId);
                if (!params.wsInfoItem)
                    params.wsInfoItem = {
                        IconName: "layer/geodi",
                        DisplayName: ' ? '
                    }
                params.Name = fThis.Name;
                linkProperties = new GeodiFacetLink(params, pThis);
                linkProperties.chart = true;

                linkProperties.itemid = params.Id;

                return linkProperties;
            }

            var data = pThis.Helpers.ConvertObjToArray(serverInfo.DocForEnumarator);
            data.forEach(function (enumarator) {
                var linkProperties = fThis.GetLinkProperties(enumarator.id);
                if (linkProperties) {
                    linkProperties.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: enumarator.value, foundDocument: serverInfo.FoundDocument }), html: FormatNumber(enumarator.value) };
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    fThis.Target.body.addItem($facetLink);
                    fThis.Helpers.AddChartData(enumarator, fThis);
                }
                if (data[data.length - 1].id == enumarator.id)
                    pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
            });

            if (currentQuery[fThis.CurrentQueryItemKey]) {
                currentQuery[fThis.CurrentQueryItemKey].forEach(function (enumaratorId) {
                    var linkProperties = fThis.GetLinkProperties(enumaratorId);
                    if (linkProperties) {
                        linkProperties.remove = true;
                        linkProperties.onclick = pThis.Helpers.LinkParser({ id: enumaratorId, type: fThis.Name, remove: true });
                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                        window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                    }
                });
            }
        },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "recognizer",
        Target: window.GeodiFacet.Helpers.CreatePanel({ id: "fc_recognizer_p", priorty: 3, header: { title: "[$query:layers/js]" }, clear: true, chart: true }),
        ChartData: [],
        Helpers: {
            GetWsInfoItem: function (id) {
                return window.CurrentQueryContainer.CurrentWSInfo.Recognizers.find(function (f) { return f.ItemHashCode == id });
            },
            AddChartData: function (recognizer, fThis) {
                var wsInfoItem = this.GetWsInfoItem(recognizer.id);
                var displayName = (wsInfoItem ? wsInfoItem.DisplayName : "");
                var obj = { LinkData: { Id: recognizer.id, Category: fThis.Name }, ChartItem: [displayName, recognizer.value] };
                fThis.ChartData.push(obj);
            }
        },
        CurrentQueryItemKey: "RecognizerIDs",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this, linkProperties = null, serverInfo = params.serverInfo;

            this.GetLinkProperties = function (recognizerId) {
                params.Id = recognizerId;
                params.wsInfoItem = this.Helpers.GetWsInfoItem(recognizerId);
                params.Name = fThis.Name;
                if (!params.wsInfoItem)
                    params.wsInfoItem = {
                        IconName: "layer/geodi",
                        DisplayName: ' ? '
                    }
                linkProperties = new GeodiFacetLink(params, pThis);
                linkProperties.chart = true;
                return linkProperties;
            }

            var data = pThis.Helpers.ConvertObjToArray(serverInfo.DocForRecongizer);
            data.forEach(function (recognizer) {
                var linkProperties = fThis.GetLinkProperties(recognizer.id);
                if (linkProperties) {
                    linkProperties.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: recognizer.value, foundDocument: serverInfo.FoundDocument }), html: FormatNumber(recognizer.value) };
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    fThis.Target.body.addItem($facetLink);
                    fThis.Helpers.AddChartData(recognizer, fThis);
                }
                if (data[data.length - 1].id == recognizer.id)
                    pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
            });

            if (currentQuery[fThis.CurrentQueryItemKey]) {
                var data = currentQuery[fThis.CurrentQueryItemKey].split(',');
                data.forEach(function (recognizerId) {
                    var linkProperties = fThis.GetLinkProperties(recognizerId);
                    if (linkProperties) {
                        linkProperties.remove = true;
                        linkProperties.onclick = pThis.Helpers.LinkParser({ id: recognizerId, type: fThis.Name, remove: true });
                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                        window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                    }
                });
            }

        },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "geom",
        Target: detailPanel,
        CurrentQueryItemKey: "GeometryStatus",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = {
                icon: { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=Layer/geography", true, true) },
                class: "facetLink FacetEnum"
            };

            this.RenderLinks = function () {
                if (currentQuery[fThis.CurrentQueryItemKey] != 1) {
                    linkProperties.html = "[$query:geometry_status_null/js]";
                    linkProperties.onclick = pThis.Helpers.LinkParser({ id: 1, type: fThis.Name });
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    fThis.Target.body.addItem($facetLink);
                }

                if (currentQuery[fThis.CurrentQueryItemKey] != 2) {
                    linkProperties.html = "<span class='img-not12'>&nbsp;</span>  [$query:geometry_status_not_null/js]";
                    linkProperties.onclick = pThis.Helpers.LinkParser({ id: 2, type: fThis.Name });
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    fThis.Target.body.addItem($facetLink);
                }
            };

            this.RenderFilterLinks = function () {
                if (currentQuery[fThis.CurrentQueryItemKey] == 1) {
                    linkProperties.html = "[$query:geometry_status_null/js]";
                    linkProperties.onclick = pThis.Helpers.LinkParser({ id: 2, type: fThis.Name, remove: true });
                    linkProperties.remove = true;
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                }

                if (currentQuery[fThis.CurrentQueryItemKey] == 2) {
                    linkProperties.html = "<span class='img-not12'>&nbsp;</span>  [$query:geometry_status_not_null/js]";
                    linkProperties.onclick = pThis.Helpers.LinkParser({ id: 1, type: fThis.Name, remove: true });
                    linkProperties.remove = true;
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                }
            };

            this.RenderLinks();
            this.RenderFilterLinks();
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = params.Id;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = 3;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "label",
        Target: detailPanel,
        CurrentQueryItemKey: "LabelStatus",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = function () {
                this.icon = { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=geodi:.geodinote", true, true) },
                this.class = "facetLink FacetEnum"
            };


            var linkProp = new linkProperties();
            linkProp.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: null, html: FormatNumber(params.serverInfo.TotalHasNote) }

            if (params.serverInfo.TotalHasNote > 0 && currentQuery[fThis.CurrentQueryItemKey] != 1 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "[$query:fav_status_not_null/js]";
                linkProp.onclick = pThis.Helpers.LinkParser({ id: 1, type: fThis.Name });
                linkProp.countBar.style = pThis.Helpers.CreateCountBarStyle({ count: params.serverInfo.TotalHasNote, foundDocument: params.serverInfo.FoundDocument });
                var $facetLink = pThis.Helpers.CreateLink(linkProp);
                fThis.Target.body.addItem($facetLink);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] != 2 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                var pNotNoteCount = (params.serverInfo.FoundDocument - params.serverInfo.TotalHasNote);
                if (params.serverInfo.DocForContentType && params.serverInfo.DocForContentType["geodi:.geodinote"])
                    pNotNoteCount -= params.serverInfo.DocForContentType["geodi:.geodinote"];
                if (pNotNoteCount) {
                    linkProp.html = "<span class='img-not12'>&nbsp;</span>  [$query:fav_status_null/js]";
                    linkProp.onclick = pThis.Helpers.LinkParser({ id: 2, type: fThis.Name });
                    linkProp.countBar.style = pThis.Helpers.CreateCountBarStyle({ count: pNotNoteCount, foundDocument: params.serverInfo.FoundDocument });
                    linkProp.countBar.html = FormatNumber(pNotNoteCount);
                    var $facetLink = pThis.Helpers.CreateLink(linkProp);
                    fThis.Target.body.addItem($facetLink);
                }

            }


            linkProp = new linkProperties();
            if (params.serverInfo.TotalHasNote > 0 && currentQuery[fThis.CurrentQueryItemKey] == 1 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "[$query:fav_status_not_null]";
                linkProp.onclick = pThis.Helpers.LinkParser({ id: 1, type: fThis.Name, remove: true });
                linkProp.remove = true;
                var $facetLink = pThis.Helpers.CreateLink(linkProp);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] == 2 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "<span class='img-not12'>&nbsp;</span>  [$query:fav_status_null/js]";
                linkProp.onclick = pThis.Helpers.LinkParser({ id: 2, type: fThis.Name, remove: true });
                linkProp.remove = true;
                var $facetLink = pThis.Helpers.CreateLink(linkProp);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink);
            }


        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = params.Id;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = 3;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "nums",
        Target: detailPanel,
        CurrentQueryItemKey: "GetNumberOfOccurrences",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = function () {
                this.icon = { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=Action/shownumbers", true, true) },
                this.class = "facetLink FacetEnum",
                    this.html = "[$query:show_number_of_occurrences/js]",
                this.onclick = pThis.Helpers.LinkParser({ id: true, type: fThis.Name })
            };
            if (!currentQuery[fThis.CurrentQueryItemKey]) {
                var linkProp = new linkProperties();
                var $facetLink = pThis.Helpers.CreateLink(linkProp);
                fThis.Target.body.addItem($facetLink);
            }
            if (currentQuery[fThis.CurrentQueryItemKey]) {
                var linkProp = new linkProperties();
                linkProp.remove = true;
                linkProp.onclick = pThis.Helpers.LinkParser({ id: true, type: fThis.Name, remove: true });
                var $facetLink = pThis.Helpers.CreateLink(linkProp);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink);
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = true;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = false;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "cgroup",
        Target: contentTypePanel,
        CurrentQueryItemKey: "ContentReaders",
        ChartData: [],
        Helpers: {
            GetData: function () { return window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders; },
            GetGroupItem: function (params, pThis) {
                var currentQuery = pThis.Helpers.CurrentQuery(), wsInfo = this.GetData();
                if (!window.CurrentQueryContainer.CurrentWSInfo || !wsInfo)
                    return;

                if (!currentQuery.ContentReaders)
                    currentQuery.ContentReaders = [];

                var item = null;
                if (wsInfo.OtherCategory && wsInfo.OtherCategory.Category.Text == params.Id)
                    item = wsInfo.OtherCategory;
                else {
                    var namesOrder = wsInfo.NamesOrder.find(function (f) { return f == params.Id });
                    if (namesOrder)
                        item = wsInfo.Names[namesOrder];
                }
                return item;
            }
        },
        Render: function (params, pThis) {
            var fThis = this, currentQuery = pThis.Helpers.CurrentQuery(), data = pThis.Helpers.ContentTypeData(), wsInfo = this.Helpers.GetData();
            var linkProperties = function () { this.class = "facetLink facetLinkPLine" };

            if (wsInfo && wsInfo.NamesOrder.length > 0 && params.serverInfo.DocForContentType) {
                wsInfo.NamesOrder.forEach(function (namesOrder) {
                    var names = wsInfo.Names[namesOrder];
                    var titleElmProperties = { $titleElement: null, count: 0 };
                    if (names.AllTyes && names.AllTyes.length > 0)
                        names.AllTyes.forEach(function (type) {
                            if (params.serverInfo.DocForContentType[type]) {
                                if (!titleElmProperties.$titleElement) {

                                    var linkProp = new linkProperties();
                                    linkProp.countBar = { class: "facetCountR facetCountRPBar facetCountRPBarLarge", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", };
                                    linkProp.title = (HttpUtility.HtmlEncode(namesOrder));
                                    linkProp.onclick = pThis.Helpers.LinkParser({ id: namesOrder, type: fThis.Name });
                                    linkProp.html = "<span class=\"img-default\">&nbsp;</span><b>" + namesOrder + "</b>";
                                    titleElmProperties.$titleElement = pThis.Helpers.CreateLink(linkProp);
                                    fThis.Target.body.addItem(titleElmProperties.$titleElement);
                                }
                                var count = params.serverInfo.DocForContentType[type];
                                titleElmProperties.count += count;
                            }
                        });
                    var pStyle = pThis.Helpers.CreateCountBarStyle({ count: titleElmProperties.count, foundDocument: params.serverInfo.FoundDocument });
                    $(titleElmProperties.$titleElement).find("span.facetCountRPBarLarge").attr("style", pStyle).html("<b>" + FormatNumber(titleElmProperties.count) + "</b>");
                });
            }

            if (currentQuery[fThis.CurrentQueryItemKey] && currentQuery[fThis.CurrentQueryItemKey].length > 0 && wsInfo && wsInfo.NamesOrder.length > 0) {
                var AddedGroups = [];
                wsInfo.NamesOrder.forEach(function (namesOrder) {
                    var names = wsInfo.Names[namesOrder];
                    if (names && names.SubList) {
                        names.SubList.forEach(function (subList) {
                            var index = currentQuery[fThis.CurrentQueryItemKey].findIndex(function (f) { return f == subList.__type; });
                            if (index > -1 && AddedGroups.findIndex(function (f) { return f == namesOrder }) == -1) {
                                var linkProp = new linkProperties();
                                linkProp.title = (HttpUtility.HtmlEncode(namesOrder));
                                linkProp.onclick = pThis.Helpers.LinkParser({ id: namesOrder, type: fThis.Name, remove: true });
                                linkProp.html = "<span class=\"img-default\">&nbsp;</span><b>" + namesOrder + "</b>";
                                linkProp.remove = true;
                                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
                                AddedGroups.push(namesOrder);
                            }
                        });
                    }
                });

            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), item = this.Helpers.GetGroupItem(params, pThis);

            if (item && item.SubList)
                item.SubList.forEach(function (subList) {
                    var typeName = subList.__type;
                    var index = currentQuery.ContentReaders.findIndex(function (f) { return f == typeName });
                    if (index < 0)
                        currentQuery.ContentReaders.push(typeName);
                });

            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), item = this.Helpers.GetGroupItem(params, pThis);

            if (item && item.SubList)
                item.SubList.forEach(function (subList) {
                    var typeName = subList.__type;
                    var index = currentQuery.ContentReaders.findIndex(function (f) { return f == typeName });
                    if (index > -1)
                        currentQuery.ContentReaders.splice(index, 1);
                });

            return true;
        }

    });
    window.GeodiFacet.AddFacetItem({
        Name: "ctype",
        Target: contentTypePanel,
        GetData: function () { return window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders },
        CurrentQueryItemKey: "ContentTypes",
        ChartData: [],
        Render: function (params, pThis) {
            var fThis = this, currentQuery = pThis.Helpers.CurrentQuery(), data = pThis.Helpers.ContentTypeData();
            var linkProperties = function () { this.class = "facetLink" };

            if (data) {
                data.forEach(function (item) {
                    item.names.AllTyes.forEach(function (type) {
                        var txt = type.split(':')[1];
                        if (params.serverInfo.DocForContentType && params.serverInfo.DocForContentType[type]) {
                            var linkProp = new linkProperties();
                            linkProp.title = (HttpUtility.HtmlEncode(txt));
                            linkProp.html = txt;
                            linkProp.onclick = pThis.Helpers.LinkParser({ id: type, type: fThis.Name });
                            linkProp.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: params.serverInfo.DocForContentType[type], foundDocument: params.serverInfo.FoundDocument }), html: FormatNumber(params.serverInfo.DocForContentType[type]) }
                            linkProp.icon = { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=" + type, true, true) };
                            linkProp.chart = true;
                            pThis.Helpers.CreateLink(linkProp).insertAfter($(fThis.Target.body).find("div[title=\"" + item.namesOrder + "\"]"));

                            var obj = { LinkData: { Id: type, Category: fThis.Name }, ChartItem: [txt, params.serverInfo.DocForContentType[type]] };
                            fThis.ChartData.push(obj);

                        }
                        if (currentQuery.ContentTypes && currentQuery.ContentTypes.length > 0) {
                            var contentType = currentQuery.ContentTypes.find(function (f) { return f == type });
                            if (contentType) {
                                var linkQueryProp = new linkProperties();
                                linkQueryProp.title = (HttpUtility.HtmlEncode(txt));
                                linkQueryProp.html = txt;
                                linkQueryProp.onclick = pThis.Helpers.LinkParser({ id: type, type: fThis.Name, remove: true });
                                linkQueryProp.icon = { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=" + type, true, true) };
                                linkQueryProp.remove = true;
                                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkQueryProp));
                            }
                        }
                    });
                    if (data[data.length - 1] == item)
                        pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                })

            }


        },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "goparent",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            if (currentQuery.DocParentsTree && currentQuery.DocParentsTree.length > 0) {

                var linkProp = { class: "facetLink", remove: true };
                var txtInExtTitle = "[$query:has_parent_filter/js]";
                var txtIn = txtInExtTitle;
                var txtImgText = "<span class=\"img-default\">&nbsp;</span> ";

                if (currentQuery.DocParentsSingleTxt) {
                    txtIn = currentQuery.DocParentsSingleTxt;
                    txtInExtTitle = '\r\n' + txtInExtTitle;
                }
                if (currentQuery.DocParentsSingleContentType)
                    linkProp.icon = { src: '"' + HttpUtility.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + currentQuery.DocParentsSingleContentType, true, true) + '"', class: "img-default" };
                linkProp.html = txtIn;
                linkProp.title = txtIn + txtInExtTitle;
                linkProp.onclick = pThis.Helpers.LinkParser({ id: null, type: fThis.Name, remove: true });
                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.DocParentsTree = [params.Id];
            currentQuery.DocParentsSingleTxt = params.Op1;
            currentQuery.DocParentsSingleContentType = params.Op2;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.DocParentsTree = null;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "gosimilarto",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            if (currentQuery.SimilarTo) {
                var linkProp = { class: "facetLink", remove: true };
                linkProp.icon = { src: "" + HttpUtility.ResolveUrl('~/GeodiJSONService?op=GetSimilarToIcon&tmpFn=' + currentQuery.SimilarTo) + "" };
                linkProp.html = "&nbsp;" + "Benzer: " + currentQuery.SimilarDisplayName;
                linkProp.onclick = pThis.Helpers.LinkParser({ id: null, type: fThis.Name, remove: true });
                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.SimilarTo = params.Id;
            currentQuery.SimilarDisplayName = params.Op1;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.SimilarTo = null;
            return true;
        }
    });
});