﻿window.preventGenerateLabelList = true;
function Timeout(fn, interval) {
    var id = setTimeout(fn, interval);
    this.cleared = false;
    this.clear = function () {
        this.cleared = true;
        clearTimeout(id);
    };
}

// GeodiLabelManager.js ile çalıştırılmalıdır.
function OpenLayerLabelManager() {
    // Farenin her hareketinde event tetiklendiği için tooltip üzerinde fare varken kapanabiliyor. O yüzden farenin tooltip üzerinde olup olmadığını anlama ihtiyacı oluştu.
    // Alternatif olarak farenin pozisyonundan tooltip üzerinde olup olmadığı hesaplanabilir.
    window.isMouseOnToolTip = false;
    var __marks = [];
    var __editLabels = function (geodiLabel, feature) {
        editLabel({
            geodiLabel: geodiLabel
        });
    };

    var __mapClick = function (e) {
        var element = $("#btnToggleMark");
        var feature = map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
            return feature;
        });

        if (feature && typeof(feature.attributes) !== 'undefined') {
            __editLabels(feature.attributes.geodiLabel, feature);
        }
    };

    var __getPinStyle = function (opacity, ReferenceIsOtherVersion)
    {
        return new ol.style.Style({
            image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
                anchor: [0.5, 24],
                anchorXUnits: 'fraction',
                anchorYUnits: 'pixels',
                opacity: opacity,
                src: HttpUtility.ResolveUrl('~/GUI/img/pin24' + (ReferenceIsOtherVersion ? 'b' : '') + '.png')
            }))
        });
    }

    this.ActivateLabel = function () {
        window.__mapLabelsActivated = true;
        if (!window.__mapLabelsActivatedOK && window.__mapLabelsActivated && window.GenerateLabelsList) {
            window.__mapLabelsActivatedOK = true;
            GenerateLabelsList();
        }
    };

    this.AppendMapEvents = function (map) {
        var info = $('#info');
        //info.tooltip({
        //    animation: false,
        //    trigger: 'manual',
        //    html: true,
        //    template:
        //        '<div class="tooltip" role="tooltip">\
        //            <div class="tooltip-arrow"></div>\
        //            <div class="tooltip-inner FixedToolTip" style="display:block !important;position:relative !important;"onmouseover="onMouseOverTooltip();" onmouseleave="onMouseLeaveTooltip();"></div>\
        //        </div>'
        //});
        //GEODI-3793
        info.tooltip({
            animation: false,
            trigger: 'manual',
            html: true,
            template:
                '<div class="tooltip FixedToolTip" role="tooltip" style="background: #80808061; display:block; position:absolute; border:none;">\
                    <div class="tooltip-arrow"></div>\
                    <div class="tooltip-inner" onmouseover="onMouseOverTooltip();" onmouseleave="onMouseLeaveTooltip();"></div>\
                </div>'
        });
        

        map.on('click', function (e) {
            __mapClick(e);
        });
        window.CreateLabelForPoint = function (x1, y1) {
            y1 -= $(window.map.getTargetElement()).offset().top;

            var cords = map.getCoordinateFromPixel([x1, y1]);//2px
            var x = cords[0];
            var y = cords[1];

                var geodiLabel = startAddLabel({
                    Attributes: {
                        MinX: x,
                        MinY: y,
                        MaxX: x,
                        MaxY: y
                    },
                    Section: window.contentSection ? window.contentSection : 0,
                });
            
        }
        var displayFeatureInfo = function (pixel) {
            info.css({
                left: pixel[0] + 'px',
                top: (pixel[1] - 15) + 'px'
            });
            var feature = map.forEachFeatureAtPixel(pixel, function (feature, layer) {
                return feature;
            });
            feature && feature.attributes ? showTooltip(feature.attributes) : setHideTooltipTimeout();
        };

        map.on('pointermove', function (evt) {
            if (evt.dragging) {
                setHideTooltipTimeout();
            }
            displayFeatureInfo(map.getEventPixel(evt.originalEvent));
        });
        if (!window.__mapLabelsActivatedOK && window.__mapLabelsActivated && window.GenerateLabelsList) {
            window.__mapLabelsActivatedOK = true;
            GenerateLabelsList();
        }
    }

    // Fare imleci pinin üzerine gelindiğinde tetiklenir.
    window.showTooltip = function(featureAttributes) {
        if (window.lastout && !window.lastout.cleared())
        {
            window.lastout.clear();
        }

        if (featureAttributes)
        {
            $("#info")
                .attr('title', featureAttributes.geodiText)
                .tooltip('fixTitle')
                .tooltip('show');
        }
    }

    // Fare imleci pinin üzerinden ayrıldığında tetiklenir.
    window.setHideTooltipTimeout = function ()
    {
        // Tooltip varsa kapatmak için timeout koy.
        if ((!window.lastout || !window.lastout.cleared()) && $(".FixedToolTip").length != 0 && !window.isMouseOnToolTip)
        {
            window.lastout = Timeout(function ()
            {
                if (!window.isMouseOnToolTip) {
                    $("#info").tooltip('hide');
                }
            }, 500);
        }
    }
    
    window.onMouseOverTooltip = function ()
    {
        window.isMouseOnToolTip = true;
        showTooltip();
    }

    window.onMouseLeaveTooltip = function ()
    {
        window.isMouseOnToolTip = false;
    }

    window.StartLabelEdit = function (label) {
        var LabelID = label.LabelId;
        var editFeature;
        window.vSourceMarks.forEachFeature(function (feature) {
            var featureLabelId = feature.attributes.geodiLabel.LabelId;
            if (LabelID == featureLabelId) {
                editFeature = feature;
            }
        });
        if (typeof (editFeature) != 'undefined' || editFeature != null) {
            var geodiLabel = editFeature.attributes.geodiLabel;
            __editLabels(geodiLabel, editFeature);
        }
        else
            __editLabels(label);
    };

    window.OnLabelAddMark = function (geodiLabel) {
        if (window.contentSection && window.contentSection.toString() != geodiLabel.LabelSection.Section )
            return;

        var x, y;
        if (typeof (geodiLabel.LabelSection.Attributes.minX) != "undefined") {
            x = geodiLabel.LabelSection.Attributes.minX;
            y = geodiLabel.LabelSection.Attributes.minY;
        }
        else if (typeof (geodiLabel.LabelSection.Attributes.MinX) != "undefined") {
            x = geodiLabel.LabelSection.Attributes.MinX;
            y = geodiLabel.LabelSection.Attributes.MinY;
        }
        else
            return;


        var iconFeature = new ol.Feature({
            geometry: new ol.geom.Point([x, y]),
        });

        iconFeature.setStyle(__getPinStyle(1, geodiLabel.ReferenceIsOtherVersion));

        iconFeature.attributes = { "geodiLabel": geodiLabel, "geodiText": formatLabelMark(geodiLabel) };
        window.__iconFeature = iconFeature;

        if (window.vSourceMarks) {
            window.vSourceMarks.addFeature(iconFeature);
            __marks.push(iconFeature);
        }
        return iconFeature;
    };

    window.OnLabelsProcess = function (data) {
        return data;
    };

    window.OnLabelListUpdate = function () {
        while(__marks.length > 0)
            window.vSourceMarks.removeFeature(__marks.pop());
    };

    window.ShowMarks = function (isVisible)
    {
        window.vSourceMarks.forEachFeature(function (feature, layer) {
            feature.setStyle(__getPinStyle(isVisible ? 1 : 0));
        });
    }
}

window.OpenLayerLabelManager = new OpenLayerLabelManager();


