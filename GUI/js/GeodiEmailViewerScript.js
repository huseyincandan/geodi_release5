﻿function GeodiEmail(id) {

    this.listMailMessages = function(content_id, wsName, callBackFunction){
        $.post('AsposeEMailViewer?op=listMailMessages', { 'content_id': content_id, 'wsName': wsName,'UpdateCID':1 }, callBackFunction, 'json');    
    }
    this.getMailMessage = function (uniqueID, content_id, wsName, callBackFunction) {
        $.post('AsposeEMailViewer?op=getMailMessage', { 'uniqueID': uniqueID, 'content_id': content_id, 'wsName': wsName, 'UpdateCID': 1 }, callBackFunction, 'json');
    }
    this.validateAccountInfo = function (accountInfo, callBackFunction) {

        $.ajax({
            type: "POST",
            url: 'AsposeEMailViewer?op=validateAccountInfo',
            data: { 'accountInfo': HttpUtility.ToBase64(JSON.stringify(accountInfo)) },
            success: function (objR) {
                if (callBackFunction)
                    callBackFunction(objR);
            },
            dataType: 'json',
            OnAjaxError: function (a, b, c, d) {
                if (callBackFunction) {
                    callBackFunction(false);
                    return true;
                }
                else
                    return false;
            }
        });

    }
}

window.currentEmail = new GeodiEmail('');
