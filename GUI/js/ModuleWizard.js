﻿function handleCheckAll(sender, items) {
    if (items) {
        var isChecked0 = sender.prop('checked');
        items = items.split(":");
        for (var i = 0; i < items.length; i++) {
            var className = '.chkModule' + items[i];
            var el = $(className);
            var isChecked = el.prop('checked');
            if (isChecked0 != isChecked) {
                el.prop('checked', isChecked0);
            }
        }
    }
}
function clearHtml(txt) {
    return HttpUtility.HtmlEncode($('<div>' + txt + '</div>').text());
}


function showModulesWizardIfPossible() {
    if (!window._checkedAutoUpdates)
        _checkAutoUpdates();
}
function showModulesWizard() {
    _showWizardDlg(false,
        function (ok) {
            if (!ok)
                showMessage("[$ModuleExtract:modules_are_uptodate/js]", '[$default:info/js]');
        });
}
function _checkAutoUpdates() {
    if (!window.BrowserType.IsWinApp()) {
        $("#dlgModuleWizard").html('');
        window._checkedAutoUpdates = true;
        return;
    }
    CheckModuleAutoUpdates(
        function (res, modules) {
            if (modules)
                window._modules = modules;
            if (res) {
                if (res.DirtyExists || res.InstallOrUninstallRequestedExists || res.MandatoryExists)
                    _showWizardDlg(false);
                else if (res.OptionalExists) {
                    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=IsModuleWizardCompletedOnce'),
                        function (completed) {
                            if (!completed)
                                _showWizardDlg(false);
                            else
                                window._checkedAutoUpdates = true;
                        });
                }
                else
                    window._checkedAutoUpdates = true;
            }
        });
}
function _showWizardDlg(reset, callback) {
    if (!window.BrowserType.IsWinApp()) {
        $("#dlgModuleWizard").html('');
        window._checkedAutoUpdates = true;
    }
    else {
        show_moduleWizardMain(
            function () {
                checkAndUpdateDlg(function (ok) {
                    if (ok) {
                        var el = $("#dlgModuleWizard");
                        el.on('shown.bs.modal', function (e) {
                            $('.dlgModuleWizardPanel').show();
                            initWizard();
                            show_moduleWizardMain();
                            updateModulesStatus();
                        });
                        $.when($('#moduleWizardMain').hide()).done(
                            function () {
                                el.modal({ backdrop: 'static', keyboard: false });
                            });
                    }
                    if (callback)
                        callback(ok);
                }, reset);
            });
    }
}


function checkAndUpdateDlg(callback, reset) {
    if (!reset)
        _checkAndUpdateDlg(callback);
    else
        //ResetModules(function (modules) {
        LoadModules(function (modules) {
            if (modules)
                window._modules = modules;
            _checkAndUpdateDlg(callback);
        });
}
function _checkAndUpdateDlg(callback) {
    var pageCount = 0;
    var modules = window._modules;
    if (modules && modules.length) {

        modules.sort(function (a, b) {
            var res = b.AutoInstall - a.AutoInstall;
            return res ? res : _getPageVal(a) - _getPageVal(b);
        });

        var titleWasSet;
        var elWizard = $('#moduleWizardMain');
        //mod AutoInstall -> No = 0, Optional = 1, Yes(mandatory) = 2
        {//mandatory
            var lstMand = [];
            for (var i = 0; i < modules.length; i++) {
                var mod = modules[i];
                if (!mod.Installed)
                    if (!(mod.UpdateVersionNotCompatible || mod.LicenseIsNotOK)) {
                        if ((!mod.FileSize) && mod.UpdateLinkFileSize)
                            mod.FileSize = HumanFileSize(mod.UpdateLinkFileSize);
                        if (mod.UpdateLink && (!mod.Hidden)) {
                            if (mod.AutoInstall == 2) {//mandatory
                                lstMand.push(mod);
                            }
                        }
                    }
            }
            if (lstMand.length) {
                var html1 = TemplateToHtml($("#template_mand"), lstMand, { All: modules, GetModuleIconName: GetModuleIconName, GetModuleKey: GetModuleKey, Page: ++pageCount });
                elWizard.append($(html1));
                setWizardTitle("[$ModuleExtract:autoInstallDlgTitle/js]");
                titleWasSet = true;
            }
            $('#secMand').show();
        }
        {//optional
            var pageVal = null;
            var lstOpt = [];
            for (var i = 0; i < modules.length; i++) {
                var mod = modules[i];
                if (!mod.Installed)
                    if (!(mod.UpdateVersionNotCompatible || mod.LicenseIsNotOK)) {
                        if (mod.UpdateLink && (!mod.Hidden)) {
                            if (mod.AutoInstall == 1) {//optional
                                var val = _getPageVal(mod);
                                if (val !== pageVal) {
                                    if (lstOpt.length) {
                                        var pageTitle = _addOpt(elWizard, lstOpt, modules, ++pageCount);
                                        if (!titleWasSet) {
                                            titleWasSet = true;
                                            setWizardTitle(pageTitle);
                                        }
                                        lstOpt = [];
                                    }
                                    pageVal = val;
                                }
                                lstOpt.push(mod);
                            }
                        }
                    }
            }
            if (lstOpt.length) {
                var pageTitle = _addOpt(elWizard, lstOpt, modules, ++pageCount);
                if (!titleWasSet) {
                    titleWasSet = true;
                    setWizardTitle(pageTitle);
                }
            }
        }
    }
    if (pageCount == 0) {
        closeAutoInstallDlg();
        callback(false);
    }
    else {
        if (pageCount == 1)
            $('.submitBtnSubstitude').fadeIn();
        else
            $('.submitBtnSubstitude').hide();
        callback(true);
    }
}
function _addOpt(elWizard, lstOpt, modules, page) {
    var html1 = "";
    var items = [];
    for (var i = 0; i < lstOpt.length; i++)
        items.push(GetModuleKey(lstOpt[i]));
    var itemsCount = items.length;
    items = items.join(":");
    html1 += TemplateToHtml($("#template_opt"), lstOpt, { All: modules, GetModuleIconName: GetModuleIconName, GetModuleKey: GetModuleKey, Items: items, ItemsCount: itemsCount, Page: page });
    var el = $(html1);
    var retval;
    if (lstOpt.length > 1)
        retval = "[$ModuleExtract:select_anything_you_desire/js]";
    else if (lstOpt.length == 1)
        retval = lstOpt[0].Wizard_Text;
    else
        retval = " ";
    if (!retval)
        retval = " ";
    el.data("pageTitle", retval);
    el.show();
    elWizard.append(el);
    return retval
}
function _getPageVal(mod) {
    return Math.floor(mod.Wizard_Group ? mod.Wizard_Group : 999999);
}

function initWizard() {
    if (!window._moduleWizardMainInited) {
        window._moduleWizardMainInited = true;
        $('#moduleWizardMain').easyWizard({
            buttonsClass: 'btn btn-default btn-sm prev-next-button',
            submitButtonClass: 'btn btn-default btn-sm wizard-submit',
            nextButton: '[$default:next/js] »',
            prevButton: '« [$default:previous/js]',
            submitButtonText: '[$ModuleExtract:install/js]',
            showSteps: false,
            after: function (wizardObj, prevStepObj, currentStepObj) {
                var handled;
                if (currentStepObj && currentStepObj.length) {
                    var id = currentStepObj[0].id;
                    if (id == "secMand") {
                        setWizardTitle("[$ModuleExtract:autoInstallDlgTitle/js]");
                        handled = true;
                    }
                    else if (id == "secOpt") {
                        setWizardTitle($(currentStepObj[0]).data("pageTitle"));
                        handled = true;
                    }
                }
                if (!handled)
                    setWizardTitle(" ");
            }
        });
        if ($('#sectionStart').length > 0)
            $('.prev-next-button').hide();
        $('.wizard-submit').on('click', function () {
            installAutoUpdates();
            return true;
        });
        show_moduleWizardMain();
        $('.easyWizardWrapper').css('float', 'left');//GEODI-5123, GEODI-5125
    }
}
$(window).ready(function () {
    showModulesWizardIfPossible();

    $(window).resize(function () {
        if (window._moduleWizardMainInited)
            $('#moduleWizardMain').easyWizard('resize', $('#moduleWizardMain .active'));
    });
    
    $(window).on("PCCommand",
        function (e, obj) {
            if (obj.Command == "modulesLoaded") {
                window._modules = obj.Modules;
                updateModulesStatus();
            }
            else if (obj.Command == "modulesStatusChanged") {
                updateModulesStatus();
            }
            else if (obj.Command == "moduleInstallStarted") {
                updateModulesStatus();
            }
        });
});


function onRetryDownloadClick() {
    $('#fin_retryDownload').fadeOut();
    $('#fin_downloadErrors').fadeOut();
    clearDownloadErrors(
        function () {
            delete window._finishedHandled;
            installAutoUpdates();
        });
}
function closeAutoInstallDlg() {
    show_moduleWizardMain();
    $('#dlgModuleWizard').modal('hide');

    var elMain = $('#moduleWizardMain');
    elMain.removeClass("easyWizardElement");
    elMain.removeClass("easyPager");
    elMain.empty();
    elMain.hide();

    delete window._modules;
    delete window._finishedHandled;
    delete window._checkedAutoUpdates;
    delete window._moduleWizardMainInited;

    clearDownloadErrors();
}
function clearDownloadErrors(callback) {
    $('#fin_downloadErrors').fadeOut();
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=ClearDownloadErrors'), {},
        function (ok) {
            LoadModules(
                function (modules) {
                    if (modules)
                        window._modules = modules;
                    $('#fin_downloadErrors').fadeOut();
                    if (callback)
                        callback();
                });
        });
}


function _getHideArr(showId) {
    var arr = [];
    if (showId != '#moduleWizardMain')
        arr.push($('#moduleWizardMain'));
    if (showId != '#downloadingMain')
        arr.push($('#downloadingMain'));
    if (showId != '#finishedMain')
        arr.push($('#finishedMain'));
    if (showId != '#restartingMain')
        arr.push($('#restartingMain'));
    return arr;
}
function show_moduleWizardMain(callback) {
    delete window._finishedHandled;
    if (!window._restartingMain_shown)
        _show($('#moduleWizardMain'), _getHideArr('#moduleWizardMain'), callback);
}
function show_downloadingMain(updateWizardTitle, callback) {
    if (!window._restartingMain_shown) {
        if (updateWizardTitle)
            setWizardTitle("[$ModuleExtract:downloading_module/js]");
        _show($('#downloadingMain'), _getHideArr('#downloadingMain'), callback);
    }
}
function show_finishedMain(callback) {
    if (!window._restartingMain_shown)
        _show($('#finishedMain'), _getHideArr('#finishedMain'), callback);
}
function show_restartingMain(callback) {
    setWizardTitle(" ");
    delete window._finishedHandled;
    window._restartingMain_shown = true;
    _show($('#restartingMain'), _getHideArr('#restartingMain'), callback);
}

function isShown_moduleWizardMain(callback) {
    window._last_shownID == '#moduleWizardMain';
}
function isShown_downloadingMain(callback) {
    window._last_shownID == '#downloadingMain';
}
function isShown_finishedMain(callback) {
    window._last_shownID == '#finishedMain';
}
function isShown_restartingMain(callback) {
    window._last_shownID == '#restartingMain';
}
function _show(elShowID, elHideArr, callback) {
    if (window._last_shownID != elShowID) {
        var elShow = $(elShowID);
        window._last_shownID = elShowID;
        if (elHideArr)
            for (var i = 0; i < elHideArr.length; i++) {
                var elHide = elHideArr[i];
                if (elHide.is(":visible")) {
                    $.when(elHide.fadeOut()).done(
                        function () {
                            _show(elShow, elHideArr, callback);
                        });
                    return;
                }
            }
        if (!elShow.is(":visible")) {
            $.when(elShow.fadeIn()).done(
                function () {
                    if (callback) callback();
                });
        }
        else if (callback) callback();
    }
}



function onRestartAppClick() {
    show_restartingMain(function () {
        window._freezeUpdateModulesStatus = true;
        SetPauseHandleAjaxError(true);
        $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=RestartApp'),
            function (res) {
                if (res && (res != "OK")) {
                    SetPauseHandleAjaxError(false);
                    showMessage(res, '[$default:warning/js]');
                    show_downloadingMain();
                }
                else
                    checkAppStart();
            })
            .fail(function (response) {
                checkAppStart();
            });
    });
}
function checkAppStart() {
    window.setTimeout(
        function () {
            try {
                LoadModules(
                    function (modules) {
                        if (modules)
                            window._modules = modules;
                        SetPauseHandleAjaxError(false);
                        //closeAutoInstallDlg();
                        delete window._freezeUpdateModulesStatus;
                        location.reload();
                    },
                    function (jqXHR, textStatus, errorThrown) {//fail
                        if (jqXHR && jqXHR.status == 401)
                            location.reload();
                    });               
            }
            catch (ex) { }
            if (window.PauseHandleAjaxError)
                checkAppStart();
        }, 1000);
}



function GetModuleKey(mod) {
    return mod.ID
        .replace(new RegExp('{', 'g'), '')
        .replace(new RegExp('}', 'g'), '')
        .replace(new RegExp('-', 'g'), '')
}







function updateModulesStatus(immediate) {
    if ($('#dlgModuleWizard').is(":visible")) {
        if (window.__modulestattimer) {
            window.clearTimeout(window.__modulestattimer);
            delete window.__modulestattimer;
        }
        LoadModules(
            function (modules) {
                window._modules = modules;
                if (window._freezeUpdateModulesStatus)
                    return;
                if (modules && modules.length) {
                    var modules_DL_Downloading = [];
                    var modules_DL_Finished = [];
                    var modules_DL_Cancelled = [];
                    var modules_DL_Error = [];
                    var resetRequired = false;

                    for (var i = 0; i < window._modules.length; i++) {
                        var mod = window._modules[i];
                        if (mod.DL_Downloading) {
                            modules_DL_Downloading.push(mod);
                        }
                        else if (mod.DL_Finished) {
                            modules_DL_Finished.push(mod);
                            if (mod.Reset)
                                resetRequired = true;
                        }
                        else if (mod.DL_Cancelled) {
                            modules_DL_Cancelled.push(mod);
                        }
                        else if (mod.DL_Error) {
                            modules_DL_Error.push(mod);
                        }

                        window._modules[i] = mod;
                    }

                    if (window._freezeUpdateModulesStatus)
                        return;

                    if (modules_DL_Downloading.length) {
                        delete window._finishedHandled;
                        show_downloadingMain();
                        var pgTotal = 0;
                        var pgValue = 0;
                        var arr = modules_DL_Downloading.concat(modules_DL_Finished);

                        for (var i = 0; i < arr.length; i++) {
                            var mod = arr[i];
                            if (!mod.UpdateLinkFileSize)
                                mod.UpdateLinkFileSize = 1;
                            if (mod.DL_Downloaded)
                                pgValue += mod.DL_Downloaded;
                            pgTotal += mod.UpdateLinkFileSize;
                        }
                        if (pgValue > pgTotal)
                            pgTotal = pgValue;
                        if (!window._freezePb && pgTotal) {
                            $('#progressTotal').css("width", 100.0 * pgValue / pgTotal + "%");
                            var downloaded = pgValue;
                            var toBeDownloaded = pgTotal - pgValue;
                            $('#progressTotalMsg').html("[$ModuleExtract:downloaded/js]: " + HumanFileSize(downloaded) + " - " + "[$ModuleExtract:remaining/js]: " + HumanFileSize(toBeDownloaded));
                        }

                        if (window.__modulestattimer)
                            window.clearTimeout(window.__modulestattimer);
                        window.__modulestattimer = window.setTimeout(function () { updateModulesStatus(true); }, 500);
                    }
                    else if (modules_DL_Finished.length || modules_DL_Cancelled.length || modules_DL_Error.length) {
                        show_finishedMain();

                        var val = modules_DL_Finished.length * 100 + modules_DL_Cancelled.length * 10 + modules_DL_Error.length;
                        if (window._finishedHandled != val) {
                            window._finishedHandled = val;

                            var cancelledExists = (modules_DL_Cancelled.length > 0);
                            var finishedExists = (modules_DL_Finished.length > 0);
                            var errorMessage;

                            for (var i = 0; i < modules_DL_Error.length; i++) {
                                var dmod = modules_DL_Error[i];
                                if (dmod.DL_Error) {
                                    if (!errorMessage)
                                        errorMessage = "";
                                    errorMessage += "<p><stong>" + dmod.ModuleName + "</strong>: " + dmod.DL_Error + "</p>";
                                }
                            }
                            finishedDownloading(resetRequired, cancelledExists, finishedExists, errorMessage);
                        }
                    }
                    else {
                        if (isShown_downloadingMain()) {
                            LoadModules(
                                function (modules) {
                                    if (modules)
                                        window._modules = modules;
                                    updateModulesStatus();
                                });
                            return;
                        }
                    }
                }

                //if ($('#dlgModuleWizard').is(":visible")) {
                //    window._checkedAutoUpdates = true;
                //    if (window.__modulestattimer)
                //        window.clearTimeout(window.__modulestattimer);
                //    window.__modulestattimer = window.setTimeout(function () { updateModulesStatus(true); }, immediate ? 500 : 2000);
                //}
            },
            callbackError = function (data) {
                return _onGetModulesStatusErr(data);
            });


        //if (window.__modulestattimer)
        //    window.clearTimeout(window.__modulestattimer);
        //window.__modulestattimer = window.setTimeout(function () { updateModulesStatus(true); }, immediate ? 500 : 2000);
    }
}



function finishedDownloading(resetRequired, cancelledExists, finishedExists, errorMessage) {
    if (finishedExists)
        $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=SetModuleWizardAsCompleted'));

    if (errorMessage) {
        $('#fin_downloadErrors').fadeIn();
        $('#downloadErrorsDetails').html(errorMessage);

        $('#fin_retryDownload').fadeIn();
        setWizardTitle("[$ModuleExtract:error_occured/js]");
    }
    else {
        $('#fin_downloadErrors').fadeOut();
        $('#downloadErrorsDetails').html('');

        $('#fin_retryDownload').fadeOut();

        if (cancelledExists) {
            setWizardTitle("[$ModuleExtract:download_cancelled/js]");
            if (!resetRequired) {
                closeAutoInstallDlg();
                return;
            }
        }
        else if (finishedExists)
            setWizardTitle("[$ModuleExtract:download_completed/js]");
        else
            setWizardTitle(" ");
    }


    if (resetRequired) {
        CanRestartApp(
            function (ok) {
                if (ok) {
                    $('#fin_restartApp').fadeIn();
                    $('#fin_restartAppNoBtn').fadeOut();
                    if (cancelledExists || errorMessage)
                        $('#someDlAreSucc').fadeIn();
                    else
                        $('#someDlAreSucc').fadeOut();
                }
                else {
                    $('#fin_restartAppNoBtn').fadeIn();
                    $('#fin_restartApp').fadeOut();
                }
            });
        $('#fin_closeSuccessful').fadeOut();
    }
    else {
        $('#fin_restartApp').fadeOut();
        $('#fin_restartAppNoBtn').fadeOut();

        if (finishedExists) {
            if ((!cancelledExists) && (!errorMessage))
                $('#fin_closeSuccessful').fadeIn();
            else
                $('#fin_closeSuccessful').fadeOut();
        }
        else {
            $('#fin_closeSuccessful').fadeOut();
        }
    }

}

function _onGetModulesStatusErr(data) {
    if (window.__modulestattimer)
        window.clearTimeout(window.__modulestattimer);
    window.__modulestattimer = window.setTimeout(function () { updateModulesStatus(true); }, 5000);

    if (data.statusText != "abort" && !data.abortDC) {
        if (data.status == 0)
            $(".messages").html("[$ModuleExtract:app_not_responding/js]");
        else
            $(".messages").html("[$ModuleExtract:app_not_responding2/js]");
        return true;
    }
    return false;
}


function cancelModuleDownload() {
    $('#downloadingMain').fadeOut();
    setWizardTitle("[$ModuleExtract:download_stopping/js]");
    window._freezeUpdateModulesStatus = true;
    window._freezePb = true;
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=CancelAllDownloads'),
        function () {
            _cancelModuleDownload();
        })
        .fail(function (response) {
            _cancelModuleDownload();
        });
}
function _cancelModuleDownload() {
    LoadModules(
        callback = function (modules) {
            if (modules)
                window._modules = modules;
            __cancelModuleDownload();
        },
        failCallback = function () {
            __cancelModuleDownload();
        });
}
function __cancelModuleDownload() {
    delete window._freezeUpdateModulesStatus;
    delete window._finishedHandled;
    updateModulesStatus();
    window.setTimeout(function () { delete window._freezePb; }, 3000);
}


function _getCheckedModulesLinks() {
    var arrLink = [];
    var arrMod = [];
    var modules = window._modules;
    if (modules && modules.length) {
        for (var i = 0; i < modules.length; i++) {
            var mod = modules[i];
            if (!mod.Installed)
                if (!(mod.UpdateVersionNotCompatible || mod.LicenseIsNotOK)) {
                    if (mod.UpdateLink && (!mod.Hidden) && (mod.AutoInstall == 2))//isMandatory
                    {
                        arrMod.push(mod);
                        arrLink.push(mod.UpdateLink);
                    }
                    else {
                        var key = GetModuleKey(mod);
                        if ($('.chkModule' + key).is(":checked")) {
                            arrMod.push(mod);
                            arrLink.push(mod.UpdateLink);
                        }
                    }
                }
        }
    }
    if (arrLink.length)
        return { Mods: arrMod, Links: arrLink };
}

function installAutoUpdates() {
    delete window._finishedHandled;
    var checkedMods = _getCheckedModulesLinks();
    if (checkedMods) {
        show_downloadingMain(true);
        autoInstallModules(checkedMods);
    }
    else {
        //GEODI-5108 showMessage("[$ModuleExtract:no_module_selected/js]", '[$default:warning/js]');        
        var buttons = [];
        buttons.push(createModalButton('ok',
            function () {
                show_moduleWizardMain();
            }));
        buttons.push(createModalButton('close',
            function () {
                closeAutoInstallDlg();
                $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=SetModuleWizardAsCompleted'));
            }));
        messageBoxEx(null, '[$default:warning/js]', '[$ModuleExtract:no_module_selected/js]', buttons);
    }
}

function autoInstallModules(checkedMods) {
    if (checkedMods) {
        window._autoInstallModulesStarted = true;
        show_downloadingMain(true);
        OnModulesStatusChanged();
        updateModulesStatus();
        $('#progressTotalMsg').html("");
        $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=InstallOrUpdateModulesAsync'), { 'modules': JSON.stringify(checkedMods.Mods) },
            function (ok) {
                if (ok) {
                    CheckModuleAutoUpdates(
                        function (res, modules) {
                            window._modules = modules;
                            OnModuleInstallStarted();
                            updateModulesStatus(true);
                        });
                }
            });
    }
    else
        show_moduleWizardMain();
}

function setWizardTitle(title) {
    $('#altKwDialogLabel').html(title);
}