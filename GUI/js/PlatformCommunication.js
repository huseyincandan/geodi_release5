﻿/*DECE*///Platform ile haberleşme

//alttakiler kalkacak. Komutların incelenmesi gerekli.
//_PSCC.Command({ Message: new Date(), ExecuteAsync: false, Command: 'Message' })
//_PSCC.Command({ Command: 'Sleep', ExecuteAsync: true, ms: Math.floor((Math.random() * 5000) + 10) }, function (clb) { alert(clb.RequestId); })
//_PSCC.Command({ Url: '~/GUI/WorkspaceWizard.html', Command: 'OpenWindow', Width:700, Height:500 })
delete window.PointerEvent;
window.navigator.msPointerEnabled = false;

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

window._WappPSSC = readCookie("_Wapp");


$(window).ready(function () {

    RunMetodOnParentWindow("__ReadyIframe");
    //__ParentExist true ise element boşuna ekleniyor.
    if ($("#__PSCE").length == 0)
        $("body").append("<input type='hidden' id='__PSCE' />");
    _PSCC.__SafeElement = $("#__PSCE");

    if (window._PSCCInitalizer)
        window._PSCCInitalizer(_PSCC);
    else
        _PSCC.InitalizeDefault();
    window.BrowserType._Update();
    //Generic post and run method listen
    ListenWindowPostMessage(function (data, e) {
        if (!window.$) return;
        var d1 = null;
        try {
             d1 = JSON.parse(data);
        } catch (e1) { }

        if (d1 && d1.RunMethod) {
            if (d1.RunAll || d1.RunOnWindow)
                RunMetodOnChildWindows(d1.RunMethod, d1.Param1);
            if (d1.RunAll || d1.RunOnRoot)
                RunMetodOnRootWindow(d1.RunMethod, d1.Param1);
            if (d1.RunAll || d1.RunOnParent)
                RunMetodOnParentWindow(d1.RunMethod, d1.Param1);
            if ((d1.RunAll || d1.RunCurrent) && window[d1.RunMethod])
                window[d1.RunMethod](d1.Param1);
        }
        
        
    })
});


function IsAccessableParentObject(obj, mtdOrObject) { try { return IsAccessableObject(obj.parent, mtdOrObject); } catch (e) { } return false; }
function IsAccessableObject(obj, mtdOrObject) { try { if (obj && obj[mtdOrObject]) return true; } catch (e) { } return false; }
function HasAccessableParentWindow(window) { if (window.IsRootForParentAccess) return false; try { return window.parent != window } catch (e) { return false; } }

function _PSCCDownload(url, args, commandArgs) {
    if (!args)
        args = {};
    if (!args["language"] && window.DeceClientState)
        args["language"] = window.DeceClientState.GetLanguage();


    if (url && url.indexOf("&UserSession") == -1 && (window.BrowserType.IsWinApp() || window.BrowserType.IsGoogleEarth()) &&
        !args["UserSession"] && window.Auth && window.Auth.CurrentUser && window.Auth.Session)  //?
        args["UserSession"] = window.Auth.Session;

    var cmdObject = {
        Command: 'Download', Url: url, Args: args
    };
    if (commandArgs)
        for (var key in commandArgs)
            cmdObject[key] = commandArgs[key];

    _PSCC.Command(cmdObject,
        function (rtn, cmd) {
            if (rtn.Response == false) {
                window._PSCC.DefaultCommander.Command(cmdObject);
            }
        }
    );

    return false;
}


window._PSCC = {
    requestIdCounter: 0,
    requestList: {},
    __SafeCommand: function (cmd) {
        this.__SafeElement.val(JSON.stringify(cmd));
        this.__SafeElement.click();
    },
    __DirectCommand: function (cmd) {
        window.external.ExecuteCommand(JSON.stringify(cmd));//Dönen obje nesne olarak alınamıyor! COM register için başka metod ihtiyaç halinde eklenebilir.
    },
    __ParentExist: function () {
        return HasAccessableParentWindow(window) && IsAccessableParentObject(window, "_PSCC");
    },
    __GetParentOrThis: function () {
        if (this.__ParentExist()) return window.parent._PSCC.__GetParentOrThis(); else return this;
    },
    InitalizeDefault: function () {
        if (this.__ParentExist())
            this.__ActiveCommander = this.__GetParentOrThis();
    },
    DefaultCommander: null,
    Command: function (cmd, callback, event) {
        if (cmd && cmd.Url) cmd.Url = HttpUtility.ResolveUrl(cmd.Url, false, true);
        //if (this.StartCommander && this.StartCommander.Command(cmd, callback, event)) return;//Yönetilmesi zor.
        if (cmd.Command == 'EditConnection' && window.StartConnectionEditorModal) {
            if (!window.BrowserType.IsWinApp()) {
                StartConnectionEditorModal(cmd.ActiveConnection, function (connection) {
                    callback({ Response: connection });
                });
                return;
            }
        }
        if (this.__ActiveCommander) {
            this.__ActiveCommander.Command(cmd, callback, event); return false;/*Handled*/
        }
        cmd.RequestId = this.requestIdCounter++;
        if (callback) this.requestList[cmd.RequestId] = { command: cmd, cmdcallback: callback };//Async
        if (window.external && window.external.IsObjectForScriptingClass) { //select command type
            this.__DirectCommand(cmd, callback); //DefaultCommander içerisindeki herşeyi handle etmiş olmalı.
            return false;/*Handled*/
        }
        else if (this.__SafeElement && this.__SafeElement.attr("ReadyForCommand")) {
            this.__SafeCommand(cmd, callback);//DefaultCommander içerisindeki herşeyi handle etmiş olmalı.
            return false;/*Handled*/
        }
        else if (window._WappPSSC && (!this.DefaultCommander || !this.DefaultCommander.IsKnown(cmd))) {
            //tumlesik
            $.ajax({
                type: "POST",
                url: HttpUtility.ResolveUrl('~/SystemTrayPSSC?op=WCommand'),
                data: { 'Cmd': JSON.stringify(cmd) },
                success: function (objR) {
                    if (callback)
                        callback(JSON.parse(objR));
                },
                dataType: 'json',
                OnAjaxError: function (a, b, c, d) {
                    if (cmd.Command == 'EditConnection' && window.StartConnectionEditorModal) {
                        StartConnectionEditorModal(cmd.ActiveConnection, function (connection) {
                            callback({ Response: connection });
                            return;
                        });
                        return true;
                    }
                    else
                        return false;
                }
            });


        }
        else if (this.DefaultCommander)
            return this.DefaultCommander.Command(cmd, callback, event);
        else {

            //document.location.href = "http://www.google.com"; //:OK
            //$("<a href=http://www.google.com target=_blank>TEST</a>").appendTo($("body")).click(); //:NOK
            //document.write("<a href=http://www.google.com target=ankara>TEST</a>");  //:OK

            //window.open("http://www.google.com");//:NOK

            alert("DefaultCommander required");//:NOK - GE altında alert çalışmıyor.
        }
    },
    OnResponse: function (obj) {
        var cmdObj = this.requestList[obj.RequestId];
        if (cmdObj && cmdObj.cmdcallback) {
            cmdObj.cmdcallback(obj, cmdObj.command);
            delete this.requestList[obj.RequestId];
        }

    },
};

function PCommunicationCommander_OnResponseJson(jsonStr) { window._PSCC.OnResponse(JSON.parse(jsonStr)); }

function ___PCommunicationCommander_OnMessageJson_Internal(obj, forceIframe) {
    $(window).trigger("PCCommand", obj);
    if (obj && obj.ForceRunIframeOption) forceIframe = obj.ForceRunIframeOption;
    if (window.DisableSendMessagesToIframe && !forceIframe)
        return;
    $("iframe").each(function (idx, frm) {
        try {
            var cnt = frm.contentWindow;//?
            if (cnt && cnt.___PCommunicationCommander_OnMessageJson_Internal) //IsAccessableParentObject(window, "___PCommunicationCommander_OnMessageJson_Internal")
                cnt.___PCommunicationCommander_OnMessageJson_Internal(obj);
        }
        catch (e) { }
    })
}




function PCommunicationCommander_OnMessageJson(jsonStr) {
    ___PCommunicationCommander_OnMessageJson_Internal(JSON.parse(jsonStr));
}

//window.PCommunicationCommander = window._PSCC;

window.HttpUtility = {
    GetValidatorValue: function () { return readCookie("DeceValidator"); },
    HtmlEncode: function (val) { if (!val || typeof (val) != "string") return val; return val.replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;'); }, //$('<div/>').text(val).html();
    HtmlDecode: function (val) { if (!val || typeof (val) != "string") return val; return window.HttpUtility.HtmlJsonDecode(val.replace(/&amp;/g, '&').replace(/&quot;/g, '"').replace(/&#39;/g, '\'').replace(/&lt;/g, '<').replace(/&gt;/g, '>')); },
    HtmlJsonDecode: function (val) { if (!val || typeof (val) != "string") return val; return val.replace(/\\u003c/g, '<').replace(/\\u003e/g, '>').replace(/\\u0027/g, '\''); }, 
    ResolveUrl: function (url, addVersionCode, notAddValidator,options) {
        var rtn = url;
        if (url && url.indexOf("~/") != -1) {
            rtn = url.replace("~/", $('#UrlRoot').val());
            if (options && options.FullUrl && window.URL)
                rtn = new URL(rtn, document.location.href).toString();
        }
        else
            rtn = url;

        if (!notAddValidator & rtn.indexOf("DeceValidator=") < 0) {

            var vl = readCookie("DeceValidator");
            if (vl) {
                rtn += rtn.indexOf("?") == -1 ? "?" : "&";
                rtn += "DeceValidator=" + vl;//url encode
            }
        }

        if (addVersionCode) {
            rtn += rtn.indexOf("?") == -1 ? "?" : "&";
            rtn += "v=" + HttpUtility.GetVersionCode();
        }
        return rtn;
    }, //?addVersionCode
    GetVersionCode: function () { return $('#UrlVersionCode').val(); },
    UrlEncode: function (val) { return window.encodeURIComponent ? window.encodeURIComponent(val) : val.replace(/&/g, '%26'); },
    UrlDecode: function (val) { return window.decodeURIComponent ? window.decodeURIComponent(val) : val.replace(/%26/g, '&'); },
    ToBase64: function (val, IgnoreBase64prefix) {
        var bs64Test = btoa(this.UrlEncode(val).replace(/%([0-9A-F]{2})/g, function (match, p1) { return String.fromCharCode('0x' + p1); }));
        if (!IgnoreBase64prefix)
            bs64Test = ".b64." + bs64Test;
        return bs64Test;
    },
    ScriptEncode: function (val, encodeEnter) { if (!val || typeof (val) != "string") return val; val = val.replace(/\\/g, '\\\\').replace(/'/g, '\\\'').replace(/"/g, '\\"'); if (encodeEnter) val = val.replace(/\r/g, '\\r').replace(/\n/g, '\\n'); return val; }
}


//Üst Pencere ile haberleşme
window._RMOneTimeArray = {};
function RunMetodOnRootWindow(fncname, objectparam, onetime) {
    RunMetodOnParentWindow(fncname, objectparam, onetime, true, true);
}

function RunMetodOnParentWindow(fncname, objectparam, onetime, run,onlyrootrun) {
    if (onetime) {
        if (window._RMOneTimeArray[fncname]) return;
        window._RMOneTimeArray[fncname] = true;
    }
    if (run && window[fncname]) {
        var norun=false;
        if (onlyrootrun)
            norun = (HasAccessableParentWindow(window) && IsAccessableParentObject(window, "RunMetodOnParentWindow") && IsAccessableParentObject(window, fncname));
        if (!norun) {
            try {
                window.___tmpEvent = objectparam.event;
                return window[fncname](objectparam && objectparam.__RunMetodOnParentWindowObj ? objectparam.Obj : objectparam);
            }
            finally {
                window.___tmpEvent = null;
            }
        }
    }
    if (HasAccessableParentWindow(window) && IsAccessableParentObject(window, "RunMetodOnParentWindow"))
        return window.parent.RunMetodOnParentWindow(fncname, { Obj: objectparam, event: GetEvent(), __RunMetodOnParentWindowObj:true}, false, true, onlyrootrun);

}

function RunMetodOnChildWindows(fncname, objectparam, recursive) {
    $("iframe").each(function (idx, frm) {
        try {
            var cnt = frm.contentWindow;//?
            if (cnt && cnt[fncname]) //IsAccessableParentObject(window, "___PCommunicationCommander_OnMessageJson_Internal")
                cnt[fncname](objectparam);
            if (cnt && recursive && cnt.RunMetodOnChildWindows) {
                cnt.RunMetodOnChildWindows(fncname, objectparam, recursive)
            }
        }
        catch (e) { }
    })
}



/*iphone*/
function ___UpdateCSSFrm(selector, cssKey, val) {
    var sel = $(selector);
    sel.css(cssKey, val);
}
function ___EvalInFrm(scrpt) {
    eval(scrpt);
}

/*Web Browserda buraya girer*/
window._PSCC.DefaultCommander = {
    IsKnown: function (cmd) {
        switch (cmd.Command) {
            case 'Relogin':
            case 'Download':
            case 'CloseWindow':
            case 'OpenWindow':
            case 'CloseAppTest':
            case 'IsOpenWindow':
            case 'FireWinEvent':
                return true;
                break;
        }
        return false;
    },
    Command: function (cmd, callback, event) {

        $(document.body).trigger("OnPCommand", cmd);
        if (cmd.Cancel) return;

        switch (cmd.Command) {
            case 'Relogin':
                var h = document.location.href;
                var lwi = h.toLowerCase().indexOf('loginwithguest=');
                var lwilng = h.toLowerCase().indexOf('language=');
                if (lwi > -1 || lwilng > -1) {
                    if (lwi > -1)
                        h = h.substr(0, lwi) + 'lwh' + h.substr(lwi + 14, h.length - (lwi + 14));
                    if (lwilng > -1)
                        h = h.substr(0, lwilng) + 'lgh' + h.substr(lwilng + 8, h.length - (lwilng + 8));
                    document.location.href = h;
                }
                else
                    document.location.reload();
                break;
            case 'Download':
                var url = cmd.Url;
                if (cmd.Args) {
                    if (url.indexOf("?") == -1) url += "?";
                    for (var key in cmd.Args)
                        url += "&" + key + "=" + HttpUtility.UrlEncode(cmd.Args[key]);
                }
                if ($("#downloaderIFrame").length == 0)
                    $(document.body).append($("<iframe id=downloaderIFrame style='position:absolute;width:10px;height:10px;left:-1000px;top:-1000px;'></iframe>"));
                $("#downloaderIFrame").attr("src", url);
                return false;//handled
                break;
            case 'CloseWindow':
                if (cmd.BrowserModeEnable)
                    window.close();
                break;
            case 'IsOpenWindow':
                if (callback) {
                    if (window.__dmmyWndList && window.__dmmyWndList[cmd.Name])
                        callback({ Response: true });
                    else
                        callback({ Response: false });
                }
                return false;
                break;
            case 'FireWinEvent':
                try {
                    if (window.opener) {
                        var cmd2 = JSON.parse(JSON.stringify(cmd));
                        cmd2.CallFromChild = true;
                        window.opener._PSCC.Command(cmd2);
                        if (!cmd.CallFromChild)
                            return false;
                    }
                }
                catch (e) {
                    if (!cmd.CallFromChild)
                        return false;
                }

                try {
                    cmd = JSON.parse(JSON.stringify(cmd));
                    cmd.Command = cmd.EventName;
                    ___PCommunicationCommander_OnMessageJson_Internal(cmd, true);
                }
                catch (e) { }
                return false;

                break;
            case 'OpenWindow':
                var wnd = null;
                if (cmd.DisableNewWindow) {
                    document.location.href = cmd.Url;
                    CancelEvent(event);
                    return;
                }
                var idg = cmd.NameIdGroup ? cmd.NameIdGroup : '';

                if (cmd.Dialog && !window.DisableWindowDialogs) {
                    var w = (cmd.Width > 0 ? cmd.Width : 400);
                    var h = (cmd.Height > 0 ? cmd.Height : 550);
                    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
                    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

                    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
                    var top = ((height / 2) - (h / 2)) + dualScreenTop;

                    wnd = window.open(cmd.Url, cmd.Name + idg,
                        "width=" + w + ",left=" + left + "," +
                        "height=" + h + ",top=" + top + "," +
                        "resizable=" + (cmd.Resizable ? 1 : 0) + "," +
                        "titlebar=0,status=0,menubar=0,location=0,"

                    );
                    if (!wnd)
                        return false;//error


                }
                else
                    wnd = cmd.Name == null ? window.open(cmd.Url) : window.open(cmd.Url, cmd.Name + idg);

                if (wnd) {
                    if (!window.__dmmyWndList) window.__dmmyWndList = {};
                    window.__dmmyWndList[cmd.Name + idg] = wnd;

                    if (!window.__isopTimer)
                        window.__isopTimer = setInterval(function () {
                            $.each(window.__dmmyWndList, function (i, j) {
                                if (j.closed) {
                                    delete window.__dmmyWndList[i];
                                }

                            })

                        }, 1000);
                }

                if (callback)
                    callback(wnd);
                CancelEvent(event);
                return false;/*Handled*/

                break;
        }
    }
}
function GetEvent(ev) {
    if (ev && (ev.stopPropagation || ev.preventDefault)) return ev;
    if (window.___tmpEvent) return window.___tmpEvent;
    try {
        return event;
    }
    catch (e) { }
    return window.event;
}
function CancelEvent(event) {
    var e = GetEvent(event);
    if (e && e.stopPropagation) e.stopPropagation();
    if (e && e.preventDefault) e.preventDefault();
}


function ListenWindowPostMessage(fnc) {
    if (fnc) {
        var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
        var eventer = window[eventMethod];
        var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
        eventer(messageEvent, function (e) {
            var key = e.message ? "message" : "data";
            var data = e[key];
            fnc(data, e);
        }, false);
    }
}




/*BrowserType*/
window.BrowserType = {
    IsGoogleEarth: function () {
        return (window.navigator && window.navigator.userAgent && window.navigator.userAgent.indexOf("Google Earth") != -1);
    },
    IsBrowser: function () {
        return !window.external || !window.external.IsObjectForScriptingClass;
    },
    IsWinApp: function () {
        return window._WappPSSC || !this.IsBrowser();
    },
    IsMobile: function () {
        if (window.GetUrlParameter && window.GetUrlParameter("MobileApp") == "1")
            return true;
        return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
    },
    IsSafari: function () {
        return navigator.userAgent.toLowerCase().indexOf("safari") != -1 && navigator.userAgent.toLowerCase().indexOf("chrome") == -1;
    },
    FlagValue: 0,

    
    getOS:function() {
        var osArray = ["android", "iphoneos", "win64","win32"]
        var uaArray = window.navigator.userAgent.split(';').splice(1, 2);
        for (var x = 0; x < uaArray.length; x++) {

            var replacedText = uaArray[x].toLocaleLowerCase();
            uaArray[x] = replacedText.trim().replace("CPU", "").replace(/ /g, '');

            for (var i = 0; i < osArray.length; i++) {
                if (uaArray[x].indexOf(osArray[i]) > -1) {
                    window.OsIndex = i;
                    return osArray[i];
                }
            }

        }
        return null;

    },

    getOSVersion:function () {
        var osArray = ["android", "iphoneos"]
        var uaArray = window.navigator.userAgent.split(';').splice(1, 2);
        var variable = uaArray[window.OsIndex].toLocaleLowerCase();
        for (var i = 0; i < osArray.length; i++) {
            if (variable.indexOf(osArray[i]) > -1) {
                variable = variable.toLocaleLowerCase();
                var searchedText = osArray[i].toLocaleLowerCase();
                if (uaArray[1].indexOf("like") > -1) {
                    var iosVersion = variable.split('like');
                    return parseFloat(iosVersion[0].replace(searchedText, ""));
                }
                return parseFloat(variable.replace(searchedText, ""));

            }

        }
    },
    _Update: function () {
        this.FlagValue = 0;
        if (this.IsGoogleEarth())
            this.FlagValue |= 1;//bit1
        if (this.IsBrowser())
            this.FlagValue |= 2;//bit2
        if (this.IsMobile())
            this.FlagValue |= 4;//bit3
        if (this.IsWinApp())
            this.FlagValue |= 8;
    }
}

window.BrowserType._Update();




$.ajaxSetup({
    beforeSend: function (xhr, op) {
        if (op && op.url && op.url.indexOf("http") == 0) {
            var uside = $('#UrlRoot').val();
            if (uside && ((op.url + "").indexOf(uside) != 0 && (op.url + "").indexOf(HttpUtility.ResolveUrl("~/", false, true, { FullUrl: true })) !=0))
                return;
          }
        xhr.setRequestHeader("BrowserType", window.BrowserType.FlagValue);
        var vl = HttpUtility.GetValidatorValue();
        if (vl)
            xhr.setRequestHeader("DeceValidator", vl);
    },
    cache: false
});

