﻿//window.DisableRememberExtent = false;

/* GENERIC TOOLS */
function imageToBase64(image) {
    var canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;

    var context = canvas.getContext("2d");
    context.drawImage(image, 0, 0);

    return canvas.toDataURL("image/png");
}



function _CreateWMSLayer(url, params, deceid) {
    var rtn1 = {};
    rtn1.wmsSource = new ol.source.TileWMS({
        url: url, //'wms?', //../../ olmamalı!
        params: params,
        wrapX: false
        //serverType: 'geoserver',
        //tilePixelRatio: hiDPI ? 2 : 1
    });

    rtn1.wmsLayer = new ol.layer.Tile({
        deceID: deceid,
        noHide:1,
        source: rtn1.wmsSource
    });
    return rtn1;
}

window.fitExtentFnc = function (view, bbox, mapSize, enableAnimate) {
    if (view.fitExtent)
        view.fitExtent(bbox, mapSize);
    else {

        if (!enableAnimate) {
            view.fit(bbox, { size: mapSize });
        }
        else {
            window.animatingFit = true;
            view.fit(bbox, {
                size: mapSize,
                duration: 500,
                callback: function (result) {
                    window.animatingFit = false;
                    if (window.rememberExtent)
                        window.rememberExtent();
                }
            })
        }
    }

}
window.encodeURIComponent_WMSFix = window.encodeURIComponent;/*openlayers LAYERS:,,, problem*/
window.encodeURIComponent = function (a, b, c, d, e) {
    var rtn = window.encodeURIComponent_WMSFix(a, b, c, d, e);
    if (rtn) return rtn.replace(/%2C/g, ",");
    return rtn;
}
window.RedrawLayer = function (layer) {
    var source = layer.getSource ? layer.getSource() : null;
    var layers = layer.getLayers ? layer.getLayers() : (source && source.getLayers ? source.getLayers() : null);
    if (layers) {
        var layers = layers.getArray();
        for (var i = 0; i < layers.length; i++)
            window.RedrawLayer(layers[i]);
        return;
    }
    if (source) source.setTileLoadFunction(source.getTileLoadFunction());
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function removeURLParameter(url, parameter) {
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);
        for (var i = pars.length; i-- > 0;) {
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url = urlparts[0] + '?' + pars.join('&');
        return url;
    } else {
        return url;
    }
}
window.wmsSource = null;

String.prototype.hashCode = function () {
    var hash = 0, i, chr, len;
    if (this.length == 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

var dictionary = getParameterByName('dictionary');
var param_style = getParameterByName('style');

function GetRandom() {
    return (new Date().toLocaleString() + HttpUtility.GetVersionCode() + Math.random()).hashCode();
}

window.__popUpWidth = 410;
window.__popUpHeight = 460;
function DCMapResize() {
    if (!window.__popUpHeightStart)
        window.__popUpHeightStart = window.__popUpHeight;
    var _wh = $(window).height()
    if (_wh > window.__popUpHeightStart)
        window.__popUpHeight = window.__popUpHeightStart;
    else
        window.__popUpHeight = Math.floor(_wh * 0.7) + 1;

    var fromId = HttpUtility.UrlDecode(GetUrlParameter('fromId'));
}

$(DCMapResize);
$(window).resize(DCMapResize);

function StartMap() {
    window.wms_wsName = __GetWorkspaceName();
    if (!window.DisableMapTitleChange) { if (!window.wms_wsName) window.wms_wsName = ''; else document.title = window.wms_wsName; }
    $('#wsNameLabel').html(window.wms_wsName);
    $(window).on("PCCommand",
        function (e, obj) {
            
            var IsWsOK = ((!obj.Arg && window.wms_wsName == "-") || obj.Arg == "*" || obj.Arg == window.wms_wsName);
            if ((obj.Command == "WSRunQuery" || obj.Command == "WSScanEnd" || obj.Command == "WSScanEndForMap") && IsWsOK) { //refresh olan window.wms_wsName değilse refresh gereksiz!

                if (obj.TargetWS && obj.TargetWS != window.wms_wsName && window.wms_wsName != "-")
                    return;
                window.UpdateMap(true, window.GetSupportExtentUpdate ? window.GetSupportExtentUpdate(obj.Command) : false);
                if (window.DefaultHighlight)
                    window.DefaultHighlight.Clear();
            }
            else if (obj.Command == "WSDeleted" && IsWsOK) { //refresh olan window.wms_wsName değilse refresh gereksiz!
                window.setTimeout(function () { _PSCC.Command({ Command: 'CloseWindow' }); }, 500);
            } else if (obj.Command == "GoMap" && IsWsOK) { //refresh olan window.wms_wsName değilse refresh gereksiz!

                //_PSCC.Command({ Command: 'FocusWindow' });
                GoMap(obj.Args);
            }
        });
    window.GoMap = function (args) {
        if (!args.BBOX || args.BBOX.length<4)
            return;
        var mLimit = function (val, min, max) {
            if (val < min) val = min;
            if (val > max) val = max;
            return val;
        }
        args.BBOX[0] = mLimit(args.BBOX[0], -180, 180);
        args.BBOX[1] = mLimit(args.BBOX[1], -89, 89);
        args.BBOX[2] = mLimit(args.BBOX[2], -180, 180);
        args.BBOX[3] = mLimit(args.BBOX[3], -89, 89);

        var IgnoreThisGoMap = false;
        if (args.Ignorable && window._GoMapInNotIgnoreable)
            IgnoreThisGoMap = true;


        if (!args.Ignorable) {
            window._GoMapInNotIgnoreable = true;
            window._GoMapTimeNotIgnoreable = new Date();
        }

        UpdateProjectionXY(args.BBOX, "EPSG:4326", window.view.getProjection().getCode(),
            function (bbox) {

                if (!args.Ignorable) {
                    window._GoMapInNotIgnoreable = false;
                    window._GoMapTimeNotIgnoreable = new Date();
                }
                else if (!IgnoreThisGoMap)
                    IgnoreThisGoMap = (window._GoMapInNotIgnoreable || (window._GoMapTimeNotIgnoreable && new Date() - window._GoMapTimeNotIgnoreable < 500));

                if (!bbox || bbox.length < 4 ||
                    isNaN(bbox[0]) || isNaN(bbox[1]) || isNaN(bbox[2]) || isNaN(bbox[3]))
                    return;

                var lDisableFitExtent = false;
                var inSetExtent = (args.SetExtent && window.wsExtent);

                var extentBbbox = bbox;
                if (args.Smart) {
                    //add 10%

                    if (window.wsExtentInit) {

                        var ks = [0,0,0,0];
                        ks[0] = Math.max(window.wsExtentInit[0], bbox[0]);
                        ks[1] = Math.max(window.wsExtentInit[1], bbox[1]);
                        ks[2] = Math.min(window.wsExtentInit[2], bbox[2]);
                        ks[3] = Math.min(window.wsExtentInit[3], bbox[3]);
                        var ksw = ks[3] - ks[1];
                        var ksh = ks[2] - ks[0];
                        if (ksw < 0 || ksh < 0) {

                            /*out of limit*/
                            inSetExtent = false;
                            return;
                        }

                        bbox = ks;
                        extentBbbox = bbox;
                    }


                    var viewArea = window.view.calculateExtent(GetMapSize());
                    if (viewArea) {

                        var ks = [0, 0, 0, 0];
                        ks[0] = Math.max(viewArea[0], bbox[0]);
                        ks[1] = Math.max(viewArea[1], bbox[1]);
                        ks[2] = Math.min(viewArea[2], bbox[2]);
                        ks[3] = Math.min(viewArea[3], bbox[3]);


                        var ksw = ks[3] - ks[1];
                        var ksh = ks[2] - ks[0];

                        if (ksw > 0 && ksh > 0) {

                            var allw = bbox[3] - bbox[1];
                            var allh = bbox[2] - bbox[0];
                            if (allw > 0 && allh > 0) {
                                var oall1 = ksw / allw;
                                var oall2 = ksh / allh;
                                if (oall1 > 0.3 && oall1 < 1.3 &&
                                    oall2 > 0.3 && oall2 < 1.3) { /*bbox/ksh intersection area <70% */
                                    var realw = viewArea[3] - viewArea[1];
                                    var realh = viewArea[2] - viewArea[0];
                                    if (realw > 0 && realh > 0) {
                                        var o1 = ksh / realw;
                                        var o2 = ksw / realh;
                                        if (o1 < 1.0 && o1 > 0.6 &&
                                            o2 < 1.0 && o2 > 0.6) { /*view/ksh intersection area <70% */
                                            lDisableFitExtent = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }

                if (inSetExtent && window.wsExtent) {
                    window.wsExtent[0] = extentBbbox[0];
                    window.wsExtent[1] = extentBbbox[1];
                    window.wsExtent[2] = extentBbbox[2];
                    window.wsExtent[3] = extentBbbox[3];
                };

                if (IgnoreThisGoMap || lDisableFitExtent)
                    return;


                fitExtentFnc(window.view, bbox, GetMapSize(), true);
                if (args.DrawData && window.DefaultHighlight) {
                    //style!
                    window.DefaultHighlight.Clear();
                    window.DefaultHighlight.AddFeatures(args.DrawData);
                }
            }
        )
        window.HidePopups();
        return true;
    }
    window.HidePopups = function (onlyPopup) {
        if (popup.isOpen()) popup.hide();
        if (!onlyPopup && window.LastDmmyModal && window.LastDmmyModal.modal) window.LastDmmyModal.modal("hide");
        if (!onlyPopup && IsAccessableParentObject(window, 'LastDmmyModal') && window.parent.LastDmmyModal.modal) window.parent.LastDmmyModal.modal("hide");
    }
    window.UpdateMap = function (ignoreLastExtent, updateExtent, noHidePopups) {
        if (!noHidePopups)
            window.HidePopups();
        wmsSource.updateParams(__GetParams());
        //wmsSource.refresh();
        if (updateExtent)
            window.UpdateExtentControl(ignoreLastExtent);
        if (window.OnUpdatingMap)
            window.OnUpdatingMap(true);

        //window.lastExtentControl

    }

    /* WMS AND MAP */


    // HiDPI support:
    //var hiDPI = ol.has.DEVICE_PIXEL_RATIO > 1;

    var wmsLayer = null;
    if (window.wms_wsName) {
        var wmst = _CreateWMSLayer(window.__wmsUrl, __GetParams(), "WmsLayer");
        window.wmsSource = wmst.wmsSource;
        wmsLayer = wmst.wmsLayer;
        window.wmsSourceLayer = wmsLayer;
    }

    if (dictionary)
        getDictionary(dictionary);

    window.layers = [];

    window.DeceLayerToOpenLayersLayerJS = function (deceLayer, lyr, noid) {
        var sourceObject = null;
        var layersObject = null;
        if (lyr.layers && lyr.layers.length > 0) {
            var lyrSubsJs = [];
            for (var i = 0; i < lyr.layers.length; i++) {
                var jsSubLayer = window.DeceLayerToOpenLayersLayerJS(deceLayer, lyr.layers[i], true);
                lyrSubsJs.push(jsSubLayer);
            }
            layersObject = "[" + lyrSubsJs.join(",") + "]";
            lyr.layers = "/*__LAYERS__*/";
        }


        if (lyr.source) {
            //lyr.source.tilePixelRatio = hiDPI ? 1 : 2;
            var typ = lyr.source.type;
            delete lyr.source.type;
            //if (!lyr.source) lyr.source = {};     lyr.source.wrapX = false;
            sourceObject = "new ol.source." + typ + "(" + JSON.stringify(lyr.source) + ")"; //type atılabilir ?
            lyr.source = "/*__SORUCE__*/";
        }


        if (!noid) {
            lyr.visible = false;
            lyr.deceID = deceLayer.ID;
        }


        var typ2 = lyr.layerType;
        delete lyr.layerType;
        var rtn = "new ol.layer." + typ2 + "(" + JSON.stringify(lyr).replace("\"/*__SORUCE__*/\"", sourceObject).replace("\"/*__LAYERS__*/\"", layersObject) + ")";
        rtn = rtn.__replaceAll("\"[FNC_STRING]", "").__replaceAll("[FNC_STRING]\"", "").__replaceAll("\\r", "\r").__replaceAll("\\n", "\n");
        return rtn;
    }
    String.prototype.__replaceAll = function (rpc, nwVal) {
        var str = this + ""; //str.replace(RegExp(rpc, 'g'),nwVal)
        var startIndex = str.indexOf(rpc);
        while (startIndex != -1) {
            str = str.replace(rpc, nwVal);
            startIndex = str.indexOf(rpc, startIndex + nwVal.length);
        }
        return str;
    }

    window.DeceLayerToOpenLayersLayer = function (deceLayer) {
        try {
            if (!deceLayer || !deceLayer.ClientLayer) return;
            var lyrScript = window.DeceLayerToOpenLayersLayerJS(deceLayer, deceLayer.ClientLayer);
            lyrScript = lyrScript.replace("{Random}", GetRandom());
            var rtnLayer = eval(lyrScript); //type atılabilir ?
            return rtnLayer;
        }
        catch (e) {
            return null;
        }
    }

    window.MapLayers = {};

    //CreateElement!

    var layerName = param_style ? param_style : window.__getBaseLayer();

    if (window.BaseMapLayers && window.BaseMapLayers.length > 0) {
        var noLayerImg = HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=emptyMap', true, true);

        var bLayerSelrea = window.CreateAddAction({
            id: "layerSelect",
            text: "[$layers:no_layer/js]",
            imgAttributes: "style=\"background-position: center center;background-image: url('" + noLayerImg + "');\"",
            click: function (ev1) {
                CancelEvent(ev1);
                var p1 = $("#mapLayerSelArea");
                if (p1.is(":visible"))
                    p1.fadeOut();
                else
                    p1.fadeIn();
            }
        });
        window.___bLayerSelrea = bLayerSelrea;
        bLayerSelrea.addClass("AutoFadeOuter");

        $("span", bLayerSelrea).css("background-size", "contain");
        var FixMapIco = function (lyr) {
            var icoName = lyr.data.IconName;
            if (!icoName || icoName == "autoMap") //fix
            {
                if (webLayer.AddingCondition == 2) {
                    icoName = "Map/" + webLayer.ID;
                    icoName = HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=' + icoName, true, true);
                }
                else if (lyr.old_source && lyr.old_type == "Image")
                    icoName = lyr.old_source.url;
                else {
                    icoName = "Map/Other";
                    icoName = HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=' + icoName, true, true);
                }
            }
            else
                icoName = HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=' + icoName, true, true);
            return icoName;
        }
        window.SelectLayer = function (layername) {
            $("#mapLayerSelArea").fadeOut();
            if (!layername || layername == 'none') {
                $("#mapLayerSelArea .active").removeClass("active")
                $("#map-sel-nolayer").addClass("active")
                $("#layerSelect").attr("title", "[$layers:no_layer/js]");
                $("#layerSelect span").css("background-image", 'url(\'' + noLayerImg + '\')');
                window.changeVisibleLayer(layername);
                return;
            }
            var lyr = window.MapLayers[layername];
            if (!lyr) return;

            $("#mapLayerSelArea .active").removeClass("active");
            $("#map-sel-l_" + lyr.data.ID).addClass("active");
            $("#layerSelect").attr("title", lyr.data.DisplayName);
            $("#layerSelect span").css("background-image", 'url(\'' + FixMapIco(lyr) + '\')');

            var projection = "EPSG:" + lyr.data.SRID;
            if (projection == "EPSG:0")
                projection = "EPSG:3857";
            var style = lyr.data.ID;
            if (projection && window.view.getProjection().getCode() != projection) {
                var url = removeURLParameter(window.location.href, 'style');
                url += url.indexOf('?') === -1 ? '?' : '&';
                url += "style=" + style;
                window.location.href = url;
                return;
            }
            window.changeVisibleLayer(style);
        };

        var sOff = $(bLayerSelrea).offset();
        var mHeight = $(window).height() - (sOff.top + 15);


        var sLayerSelrea = $("<div id=mapLayerSelArea class='mapLayerSelArea AutoFadeOut AutoFadeOuter' style='display:none;position:absolute;top:" + (sOff.top - 5) + "px;left:" + (sOff.left + 30) + "px;z-index:10;max-height: " + mHeight + "px; overflow-y: scroll;'></div>");
        $(document.body).append(sLayerSelrea);


        sLayerSelrea.append('<span class="map-layer-sel' + (layerName == "none" ? " active" : "") + '" id="map-sel-nolayer" onclick="window.SelectLayer(\'none\')"><button title="[$layers:no_layer/js]">' +
            '<img src="' + noLayerImg + '"/><span>[$layers:no_layer/js]</span></button></span>')

        //ws based custom layers

        var firstSel = !layerName;
        for (var i = 0; i < window.BaseMapLayers.length; i++) {
            var webLayer = window.BaseMapLayers[i];
            var old_source = webLayer && webLayer.ClientLayer ? webLayer.ClientLayer.source : null;
            var old_type = webLayer && webLayer.ClientLayer ? webLayer.ClientLayer.layerType : null;
            var nwLayer = DeceLayerToOpenLayersLayer(webLayer);
            if (nwLayer) {

                window.MapLayers[webLayer.ID] = { data: webLayer, layer: nwLayer, old_source: old_source, old_type: old_type };
                if (window.UpdateOpenlayersLayerForDece)
                    window.UpdateOpenlayersLayerForDece(nwLayer);

                var icoName = FixMapIco(window.MapLayers[webLayer.ID]);

                if (firstSel || layerName == webLayer.ID) {
                    if (firstSel)
                        layerName = webLayer.ID;
                    window.ProjectionStr = "EPSG:" + webLayer.SRID;
                    $("#layerSelect").attr("title", webLayer.DisplayName);
                    $("#layerSelect span").css("background-image", 'url(\'' + icoName + '\')');
                    firstSel = false;
                }

                sLayerSelrea.append('<span class="map-layer-sel' + ((layerName == webLayer.ID) ? " active" : "") + '" id="map-sel-l_' + webLayer.ID + '" onclick="window.SelectLayer(\'' + (webLayer.ID) + '\')"><button title="' + HttpUtility.ScriptEncode(webLayer.DisplayName) + '">' +
                    '<img src="' + icoName + '"/><span>' + HttpUtility.ScriptEncode(webLayer.DisplayName) + '</span></button></span>')
                window.layers.push(nwLayer);
            }
        }
        delete window.BaseMapLayers;
    }
    else if (window.editing || window.dictionary)   //|| true
    {
        var bingMap = new ol.layer.Tile({
            visible: true,
            source: new ol.source.BingMaps({
                key: 'Aj6XtE1Q1rIvehmjn2Rh1LR2qvMGZ-8vPS9Hn3jCeUiToM77JFnf-kFRzyMELDol',
                imagerySet: 'Aerial'
                //tilePixelRatio: hiDPI ? 2 : 1
            }),
            maxZoom: 19,
            minZoom: 1
        });
        window.layers.push(bingMap);
    }






    if (window.OnMapLayerListReady)
        OnMapLayerListReady();

    if (wmsLayer)
        window.layers.push(wmsLayer);

    
    //HARİTA
    window.map = new ol.Map({
        loadTilesWhileAnimating: true,
        loadTilesWhileInteracting: true,/*paning load*/
        layers: window.layers,
        target: 'map',
        controls: []
    });




    window.UpdateView = function (projectionStrValue) {
        //redirect olmadan çözülmedi.
        window.ProjectionStr = projectionStrValue;

        //changeprojection!
        var customProj = window.ProjectionStr ? ol.proj.get(window.ProjectionStr) : window.ProjectionStr;
        if (window.ProjectionStr && !customProj) {
            
            customProj = new ol.proj.Projection({ code: window.ProjectionStr, units: 'm' });
            if (!customProj || !customProj.getExtent())
                customProj = new ol.proj.Projection({ code: window.ProjectionStr, units: 'm', global: true, worldExtent: [-180, -85, 180, 85], extent: [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244] }); //+BBOX //, 
            /*if (!customProj || !customProj.getExtent())
                customProj = ol.proj.get("EPSG:3857");*/
        }

        var vObjParams = {
            projection: customProj,
            center: [0, 0],
            maxZoom: 19,
            minZoom: 1
        };
        if (window.OL_SetViewExtent && customProj && customProj.getExtent)
            vObjParams.extent = customProj.getExtent();
        window.view = new ol.View(vObjParams);
        map.setView(window.view);
    }

    if (window.ProjectionStr == "EPSG:0") window.ProjectionStr = "EPSG:3857";

    window.UpdateView(window.ProjectionStr);
    //window.map.addControl(new ol.control.Attribution());
    window.map.addControl(new ol.control.Zoom({ zoomInTipLabel: "[$map:zoom_in/js]", zoomOutTipLabel: "[$map:zoom_out/js]" }));

    window.changeVisibleLayer(layerName);
    window.WMSFirstCapability = true;

    if (window._UpdateSettingsForMap)
        window._UpdateSettingsForMap();
    window.UpdateExtentControl = function (ignoreLastExtent) {
        if (window.DisableRememberExtent)
            ignoreLastExtent = true;

        if (window.ReadRememberExtent)
            ignoreLastExtent = false;

        if (window.lastExtentControl)
            window.map.removeControl(window.lastExtentControl);
        var bboxForce = getParameterByName("BBOX");
        if (bboxForce) {
            bboxForce = eval("[" + bboxForce + "]");
            window.ReadRememberExtent = false;
        }

        var bboxLimit = getParameterByName("BBOXLimit");
        if (bboxLimit)
            bboxLimit = eval("[" + bboxLimit + "]");


        var lWMSFirstCapability = window.WMSFirstCapability;
        window.WMSFirstCapability = false;
        if (bboxLimit) {
            UpdateProjectionXY(bboxLimit, "EPSG:4326", window.view.getProjection().getCode(),
                function (bbox) {

                    window.wsExtent = bbox;
                    window.wsExtentInit = [window.wsExtent[0], window.wsExtent[1], window.wsExtent[2], window.wsExtent[3]];
                    window.lastExtentControl = new ol.control.ZoomToExtent({ extent: window.wsExtent , tipLabel: "[$map:fit_to_extent/js]" });
                    window.map.addControl(window.lastExtentControl);
                    $('.ol-zoom-extent button').html('<i class="fa fa-arrows-alt" style="margin-top:1px;"></i>');
                    fitExtentFnc(window.view, window.wsExtent, GetMapSize());

                    if (bboxForce) {
                        UpdateProjectionXY(bboxForce, "EPSG:4326", window.view.getProjection().getCode(),
                            function (bbox) {
                                fitExtentFnc(window.view, bbox, GetMapSize());
                            }
                        )

                    }
                    else
                        startupGoMap();
                    $('.ol-attribution button').html('<i class="fa fa-info"></i>');

                }
            )
        } else {
            if (window.wms_wsName) {
                var parser = new ol.format.WMSCapabilities();
                $.ajax(wmsSource.getUrls()[0] + (lWMSFirstCapability ? "&FirstCapability=1" : "") + "&SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities&wsName=" + HttpUtility.UrlEncode(window.wms_wsName) + "&Rnd=" + GetRandom() + (window.__GetCapabilityParams ? "&" + window.__GetCapabilityParams() : "")).then(function (response) {
                    var result = parser.read(response);
                    var mapProjection = window.view.getProjection().getCode();
                    $.each(result.Capability.Layer.BoundingBox,
                        function (idx, cpb) {
                            var inextent = cpb.extent;
                            if (cpb && cpb.crs == mapProjection) {
                                if (mapProjection == "EPSG:4326" && result.Capability.Layer.EX_GeographicBoundingBox)
                                    inextent = result.Capability.Layer.EX_GeographicBoundingBox;
                                if (!bboxForce) {
                                    var last_extent =
                                        ignoreLastExtent ? null : JSON.parse(window.localStorage.getItem((window.RememberPageName ? window.RememberPageName + ':' : '') + window.wms_wsName + '_' + window.view.getProjection().getCode() + '_LastExtent'));
                                    if (!last_extent)
                                        fitExtentFnc(window.view, inextent, GetMapSize());
                                    else
                                        fitExtentFnc(window.view, last_extent, GetMapSize());
                                }
                                window.wsExtent = inextent;
                            }
                        });

                    if (window.wsExtent)
                        window.wsExtentInit = [window.wsExtent[0], window.wsExtent[1], window.wsExtent[2], window.wsExtent[3]];
                    window.lastExtentControl = new ol.control.ZoomToExtent({ extent: window.wsExtent, tipLabel: "[$map:fit_to_extent/js]" });
                    window.map.addControl(window.lastExtentControl);
                    if (!bboxForce)
                        startupGoMap();
                    $('.ol-zoom-extent button').html('<i class="fa fa-arrows-alt" style="margin-top:1px;"></i>');
                });
            }
            else {

                window.lastExtentControl = new ol.control.ZoomToExtent({ tipLabel: "[$map:fit_to_extent/js]" });
                window.map.addControl(window.lastExtentControl);
                $('.ol-zoom-extent button').html('<i class="fa fa-arrows-alt" style="margin-top:1px;"></i>');
                if (!bboxForce) {
                    var last_extent = ignoreLastExtent ? null : JSON.parse(window.localStorage.getItem(window.wms_wsName + '_' + window.view.getProjection().getCode() + '_LastExtent'));
                    if (last_extent)
                        fitExtentFnc(window.view, last_extent, GetMapSize());
                    else
                        fitExtentFnc(map.getView(), view.getProjection().getExtent(), GetMapSize())
                }
            }
            if (bboxForce) {
                UpdateProjectionXY(bboxForce, "EPSG:4326", window.view.getProjection().getCode(),
                    function (bbox) {
                        fitExtentFnc(window.view, bbox, GetMapSize());
                    }
                )

            }
            else
                startupGoMap();
            $('.ol-attribution button').html('<i class="fa fa-info"></i>');
        }



    }

    window.UpdateExtentControl();

    //Son extenti hatırlar

    window.rememberExtent = function () {
        try {
            if (!window.DisableRememberExtent && !window.animatingFit)
                window.localStorage.setItem((window.RememberPageName ? window.RememberPageName + ':' : '') + window.wms_wsName + '_' + window.view.getProjection().getCode() + '_LastExtent', JSON.stringify(window.view.calculateExtent(GetMapSize())));
        }
        catch (ex) {
        }
    }

    window.view.on('change:resolution', rememberExtent);
    window.map.on('moveend', rememberExtent);
    window.__CloseMapContext = function () {
        $('#_mapcontextMenu').remove();
        window.___opencontextMenuTime = null;
        $(document.body).trigger("MAP_ClosePanels");
    }

    window.view.on('change:center', function () { window.__CloseMapContext(); });

    $(".ol-overlaycontainer-stopevent").append($("#dfMapBtnGrpC"));


    /* GEOLOCATION */
    if ((document.location.href.indexOf("https:") == 0 || (window.IsAccessableParentObject && window.IsAccessableParentObject(window, "GetCurrentGelocation"))) && ol3controls.GeolocationControl)
        window.map.addControl(new ol3controls.GeolocationControl({
            resolution: 1
        }));

    /*POPUP*/
    window.popup = new ol.Overlay.Popup();
    window.popup.OnClose = function () {
        $("#iframeContent").remove()
    }
    window.map.addOverlay(popup);

    if (!(/iphone|ipad|ipod/i).test(navigator.userAgent) && !window.BrowserType.IsMobile()) {
        $(window.map.getViewport()).on('contextmenu', function (e) {
            e.preventDefault();
            //e.stopPropagation();
            openContextMenu(e.pageX, e.pageY);
        });
    }
    else {
        $(window.map.getViewport())
            .on("touchstart", function (e) {
                if (e.originalEvent.touches.length > 1) {
                    clearTimeout(window.___timerLongTouch);
                }
                else {
                    var x = e.originalEvent.touches[0].pageX;
                    var y = e.originalEvent.touches[0].pageY;
                    window.___timerLongTouch = setTimeout(function () {
                        openContextMenu(x, y);
                    }, 1000);
                }
            })
            .on("touchmove", function (e) {
                clearTimeout(window.___timerLongTouch);
            })
            .on("touchend", function () {
                clearTimeout(window.___timerLongTouch);
            })
            .on("ontouchforcechange", function () {
                clearTimeout(window.___timerLongTouch);
            })
            .on("ontouchcancel", function () {
                clearTimeout(window.___timerLongTouch);
            });
    }

    window.map.on('click', doubleOrSingleClick);
    window.map.on('touchend', doubleOrSingleClick);    //singleclick

    var frameLoaded = false;

    var clicks = 0;

    function doubleOrSingleClick(e) {


        $(document.body).trigger("MAP_ClosePanels");
        if (window.___opencontextMenuTime) {

            if (window.BrowserType.IsMobile()) {
                if (new Date() - window.___opencontextMenuTime > 500)
                    window.__CloseMapContext();

            }
            else
                window.__CloseMapContext();
            return;
        }

        if (window.drawingNow)
            return false;
        if (!window.DisableMapGetFeature) {
            clicks++;
            if (clicks == 1) {
                setTimeout(function () {
                    if (clicks == 1) {
                        loadPopupFrameContent(e);
                    } else {
                        //  console.log('double click!');
                    }
                    clicks = 0;
                }, 200);
            }
        }
    }

    window.ContextMenuItems = [
        { ID: 'streetView', DisplayName: 'Street View', Icon: 'GUIJsonService?op=getIco&IconName=Action/streetview' },
        { ID: 'googleMaps', DisplayName: 'Google Maps', Icon: 'GUIJsonService?op=getIco&IconName=Action/googlemaps' }
    ];

    function openContextMenu(x, y) {
        window.__CloseMapContext();
        if (window.DisableMapContextMenu) return;
        var w = $(window);
        var mnItems = [];
        var mpOptions = window.StateForm ? StateForm.GetState("GUIMenuDisable") : null;
        for (var i = 0; i < ContextMenuItems.length; i++) {
            var mn = ContextMenuItems[i];
            if (mpOptions) {
                if (mn.ID == 'streetView' && mpOptions && ((mpOptions & 1) == 1)) continue;
                if (mn.ID == 'googleMaps' && mpOptions && ((mpOptions & 2) == 2)) continue;
            }
            var ico = (mn.Icon ? mn.Icon : 'GUIJsonService?op=getIco&IconName=empty');
            ico += (ico.indexOf('?') == -1 ? '?' : '&') + '&v=' + HttpUtility.GetVersionCode();

            var mn = '<li role="presentation"><a type="button" href="javascript:handleContexMenuEvent(\'' + mn.ID + '\', \'' + x + '\', \'' + y + '\');"><img src=' + ico + '/> ' + mn.DisplayName + ' </a></li>';
            mnItems.push(mn)
        }
        if (mnItems.length == 0)
            return;
        if (w.width() - x < 160) x = w.width() - 160;
        if (w.height() - y < (mnItems.length * 40)) y = w.height() - (mnItems.length * 40);
        window.___opencontextMenuTime = new Date();
        $('#map').append('<ul id="_mapcontextMenu" class="dropdown-menu" style="display:block;top: ' + y + 'px; left:' + x + 'px;">' +
            mnItems.join('') +
            '</ul>');
    }

    window.handleContexMenuEvent = function (option, x, y) {
        window.__CloseMapContext();

        if (option == 'streetView' ||
            option == 'googleMaps') {
            var location = map.getCoordinateFromPixel([x, y]);
            var data = [];
            UpdateProjectionXY(location, window.view.getProjection().getCode(), "EPSG:4326",
                function (coords) {
                    data[0] = coords[0];
                    data[1] = coords[1];

                    var url = "";

                    switch (option) {
                        case "streetView":
                            url = "~/GUI/Map/StreetViewer.html?pos=" + data[1] + "," + data[0] + "&zoomLevel=" + window.view.getZoom(); //url = "http://maps.google.com/maps?q=&cbll=" + data[1] + "," + data[0] + "&layer=c";
                            break;
                        case "googleMaps":
                            //url = "http://maps.google.com/maps?q=" + data[1] + "," + data[0] + "&z=" + window.view.getZoom(); //
                            url = "https://www.google.com/maps/@" + data[1] + "," + data[0] + "," + window.view.getZoom() + "z";
                            break;
                    }

                    var targetName = option;

                    _PSCC.Command({ Command: 'OpenWindow', Url: url, Name: targetName, MinWidth: 400, MinHeight: 400, Width: 800, Height: 600, ExecuteAsync: false, isInApp: (option == 'googleMaps' ? true : false) }, null, event);

                }
            );

        }
        else {
            for (var i = 0; i < ContextMenuItems.length; i++) {
                var mn = ContextMenuItems[i];
                if (mn.ID == option) {
                    if (mn.onclick)
                        mn.onclick(x, y);
                    return;
                }
            }

        }

    }

    window.createDumyModal = function (appendDom) {

        var html = 
            '<div class="modal fade modal-wide DmmyModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-hidden="true">'+
            '<div class="modal-dialog modal-lg">'+
            '<div class="modal-content">'+
            '<div class="modal-header" style="border-bottom:0; height:1px !important;">'+
            '<button type="button" class="close" style="margin-top: -10px;" data-dismiss="modal" aria-label="[$default:close/html]"><span aria-hidden="true">&times;</span></button>'+
        '</div>'+
        '<div class="modal-body"><div class="inPContent" style="padding:0;" class="iframe-holder"></div>'+
        '</div>'+
        '</div>'+
        '</div>'+
            '</div>';

        var mdl = $(html);
        mdl.on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
            mdl.remove();
            window.LastDmmyModal = null;
        });
        if (appendDom)
            $(appendDom).append(mdl);
        else $(document.body).append(mdl);
        return mdl;
    }

    function loadPopupFrameContent(evt) {
        if (window.__popupIsDisable)
            return;
        if (popup.isOpen()) {
            popup.hide();
            return;
        }
        //if (window.lastClickTime && new Date() - window.lastClickTime < 200) return;  window.lastClickTime = new Date();
        var coordinate = evt.coordinate;
        window.lastPopupCoord = coordinate;
        var mapProjection = window.view.getProjection();
        //var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(coordinate, mapProjection, 'EPSG:4326'));
        var viewResolution = /** @type {number} */ (window.view.getResolution());
        if (!wmsSource) return;
        window.frameUrl = wmsSource.getGetFeatureInfoUrl(
            evt.coordinate, viewResolution, mapProjection,
            {
                'INFO_FORMAT': 'text/html', 'IsMobil': (window.BrowserType.IsMobile() ? "1" : "0"), 'GeodiClient': '1'
            });


        var nwIframe = $('<iframe id="iframeContent" style="margin:5px; width:' + window.__popUpWidth + 'px; height: ' + window.__popUpHeight + 'px;" border="0" frameborder="0"></iframe>');
        frameLoaded = false;
        if (evt) {
            var eMouse = evt.a && evt.a.a ? evt.a.a : (evt.b && evt.b.b ? evt.b.b : null);
            if (eMouse)
                window.debugKeysPressed = (eMouse.ctrlKey && eMouse.shiftKey);
        }



        //window.frameUrl??

        var openPopup = function () {
            if (window.OnOpenPopup) {

                var resx = 12 * window.view.getResolution();  //6px2
                var pxa = [(window.lastPopupCoord[0] - resx), (window.lastPopupCoord[1] - resx),
                (window.lastPopupCoord[0] + resx), (window.lastPopupCoord[1] + resx)];

                if (window.OnOpenPopup(window.frameUrl,
                    view.getProjection().getCode(),
                    window.lastPopupCoord,
                    window.view.getResolution(),
                    pxa
                ))
                    return;
            }

            nwIframe.attr("src", window.frameUrl);

            if ($(window).width() > 768 && (window.DisableLargeMapModal || $(window).height() > 768)) {
                window.popupIsAvailable = true;
                $("#iframeContent").width(window.__popUpWidth + "px");
                $("#iframeContent").height(window.__popUpHeight + "px");
                var iframeIsNew = window.__iframeIsNew;
                //if (iframeIsNew)
                popup.setContent(nwIframe, coordinate);
                //else popup.TempCoord = coordinate;

                if (window.debugKeysPressed)
                    popup.show();
            }
            else {

                var mdl = null, reelWindow = window;
                window.popupIsAvailable = false;

               
                if (HasAccessableParentWindow(window)) {
                    mdl = createDumyModal(window.parent.document.body);
                    reelWindow = window.parent;
                    reelWindow.LastDmmyModal = mdl;
                    window.parent.QueryRunEnd = function (obj) {
                        var dataCount = obj.dataCount;
                        if (dataCount > 0) {
                            if (window.parent.LastDmmyModal)
                                window.parent.LastDmmyModal.modal("show");
                        }
                            
                    };
                }
                else {
                    mdl = createDumyModal();
                     window.LastDmmyModal = mdl;
                }
                if (!window.DisableLargeMapModal && $(window).width() > 900 && $(window).height() < 768)
                    mdl.find(".modal-content").addClass("modal-lg");


               

                if (window.debugKeysPressed)
                    mdl.modal('show');
                loadInModal(mdl, nwIframe, reelWindow);
            }
        }

        if (window.FastGetFeatureInfoChecker)
            window.FastGetFeatureInfoChecker(window.frameUrl,
                function () {
                    window.debugKeysPressed = true;
                    openPopup();
                });
        else openPopup();
    }

    function loadInModal(mdl, frame, reelWindow) {
        frame.css('width', '96%');
        var rWindow = (reelWindow ? reelWindow : window);
        var iframeHeight = $(rWindow).height() - 200;
        //if (iframeHeight > 410)
        //    iframeHeight = 410;
        frame.css('height', iframeHeight + 'px');
        //window.setTimeout(function () {
        frame.attr('src', frame.attr('src'));
        //}, 100);
        $('.inPContent', mdl).html('');
        $('.inPContent', mdl).append(frame);
    }

    // kullandığımız iframeden scroll yeteneğini aldık bu nedenle harita iframe içerisindeyse safari browserlarda scrollu devralsın/geri kalan browserlar devralıyor
    if (window.self != top && window.BrowserType.IsSafari())
        $('.ol-viewport').css('overflow', 'hidden');

    //İç iframe bir içerik bulduğunda burayı çağıracak.
    window.QueryRunEnd = function (obj) {
        dataCount = obj.dataCount;
        if (!frameLoaded && dataCount && dataCount > 0) {
            frameLoaded = true;
            /*if (window.parent != window && window.parent._PSCCInitalizer) {
                _PSCC.Command({ Url: window.frameUrl, Command: 'OpenWindow' });
            }
            else */if (window.popupIsAvailable)
                popup.show();
            else {
                if (HasAccessableParentWindow(window)) {
                    window.parent.LastDmmyModal.modal('show');
                }
                else window.LastDmmyModal.modal('show');
            }
           
        }
        else {
            //('Veri bulunamadı veya frame zaten yüklü');
        }
    }

    /* MOUSE POSITION */


    var promptIsOpen = true; //true iken bir daha açılmıyor bu nedenle ilk etapta kontrolün olmaması ihtimaline karşı true yapıldı
    if (!window.BrowserType.IsMobile()) {
        window.map.addControl(new ol.control.MousePosition({
            coordinateFormat: ol.coordinate.createStringXY(7),// : ol.coordinate.createStringXY(4),
            projection: "EPSG:4326",
            className: 'custom-mouse-position',
            //  target: document.getElementById('mouse-position'),
            undefinedHTML: '&nbsp;'
        }));
        promptIsOpen = false;
    }

    $(window).ready(function () {
        bootbox.addLocale('tr', {
            OK: '[$default:ok/js]',
            CANCEL: '[$default:cancel/js]',
            CONFIRM: '[$default:ok/js]'
        });
        if (!window.DisableMapTitleChange && (!window.wms_wsName || window.wms_wsName == '')) document.title = '[$default:map/js]';
        bootbox.setLocale('tr');
    });


    /* STYLE MANAGER */


    $(document).keydown(function (e) {
        if (!window.DisableMapCoordCopy && e.ctrlKey && e.which === 67 && !promptIsOpen) {
            promptIsOpen = true;
            if (window.ProjectionStr === "EPSG:4326") {
                var coord_html = $('.custom-mouse-position').html();
                var coordarray = coord_html.split(',');
                var format = "POINT ({x} {y})";
                var coord = [parseFloat(coordarray[0]), parseFloat(coordarray[1])];
                var out = ol.coordinate.format(coord, format, 7);
                bootbox.prompt({
                    animate: false,
                    backdrop: false,
                    title: "Copy",
                    value: out,
                    callback: function () { promptIsOpen = false; }
                });
                $('.bootbox-input').select();
                // window.prompt("Copy to clipboard: Ctrl+C, Enter", out);
            }
            else {
                bootbox.prompt({
                    animate: false,
                    backdrop: false,
                    title: "Copy",
                    value: $('.custom-mouse-position').html(),
                    callback: function () { promptIsOpen = false; }
                });
                $('.bootbox-input').select();
                //window.prompt("Copy to clipboard: Ctrl+C, Enter", $('.custom-mouse-position').html());
            }
        }
    });


};

//SAYISALLAŞTIRMA

window.vSource = new ol.source.Vector({ wrapX: false });

var vectorL = new ol.layer.Vector({
    source: window.vSource,
    style: new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
        }),
        stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    })
});

window.cleanDrawing = function () {
    if (dictionary)
        return;
    window.vSource.clear();
    checkDrawCount();
}

var editing = getParameterByName('editing');

function disableTypeSelect() {
    $(window.typeSelect).change();
    $(window.typeSelect).prop('disabled', 'disabled');
}

function enableTypeSelect() {
    $(window.typeSelect).prop('disabled', false);
}

function checkDrawCount() {
    var count = parseInt(editing);
    if (window.vSource.getFeatures().length === count)
        disableTypeSelect();
    else
        enableTypeSelect();
}

window.drawingNow = false;
function addInteraction() {
    var value = window.typeSelect.value;
    if (value !== 'None') {
        window.drawingNow = true;
        window.draw = new ol.interaction.Draw({
            source: window.vSource,
            type: /** @type {ol.geom.GeometryType} */ (value)
        });
        window.draw.on('drawend', function (dvEvent) {
            checkDrawCount();
            var ffList = [dvEvent.feature];
            toWKT(ffList,
                function (wktData) {
                    if (!editing)
                        startDictionaryRecord(wktData);
                    else
                        sendBackWkt(wktData);
                }
            );

        })
        window.map.addInteraction(window.draw);
    }
    else
        window.drawingNow = false;
}

function toFeature(wkt, srid) {
    if (!srid)
        srid = 4326;
    var dataprojSrid = window.view.getProjection().getCode().replace("EPSG:", "");//locakde çevrilen projeksiyonlar
    var format = new ol.format.WKT();
    var isClientConvertable =
        (srid == "4326" || srid == "3857") &&
        (dataprojSrid == "4326" || dataprojSrid == "3857");
    var features =
        isClientConvertable ?
            format.readFeature(wkt, { featureProjection: window.view.getProjection(), dataProjection: 'EPSG:' + srid }) :
            format.readFeature(wkt, { featureProjection: window.view.getProjection(), dataProjection: window.view.getProjection() });
    return features;
}
function toWKTPoint(pos0) {
    var f = new ol.Feature(new ol.geom.Point(pos0));
    return toWKTFeatures([f]);
}
function toWKTFeatures(features) {
    var format = new ol.format.WKT();
    return format.writeFeatures(features);
}
function toWKT(features, fnc, targetSrid) {

    var srid = window.currentDictionary ? window.currentDictionary.TableSchema.SRID : targetSrid;
    if (!srid)
        srid = 4326;

    var dataprojSrid = window.view.getProjection().getCode().replace("EPSG:", "");//locakde çevrilen projeksiyonlar
    var format = new ol.format.WKT();
    var isClientConvertable =
        (srid == "4326" || srid == "3857") &&
        (dataprojSrid == "4326" || dataprojSrid == "3857");
    var wkt =
        isClientConvertable ?
            format.writeFeatures(features, { featureProjection: window.view.getProjection(), dataProjection: 'EPSG:' + srid }) :
            format.writeFeatures(features, { featureProjection: window.view.getProjection(), dataProjection: window.view.getProjection() });
    if (isClientConvertable) {
        if (fnc)
            fnc(wkt);
    }
    else if (fnc) {
        $.post('GeoService?op=ChangeProjectionWKT',
            {
                "WKT": wkt,
                SourceEPSG: dataprojSrid,
                TargetEPSG: srid
            }
            ,
            function (wktData) {
                fnc(wktData);
            }
            , 'json');
    }
    return wkt;
}


function StartMap2() {
    window.typeSelect = document.getElementById('drawType');
    if (!window.typeSelect) return;
    window.typeSelect.onchange = function (e) {
        if (window.draw)
            window.map.removeInteraction(window.draw);
        addInteraction();
    };

    if (editing || dictionary) {
        $('.editingPanel').show();
        $('.layerPanel').removeClass('col-xs-10').addClass('col-xs-4');
        addInteraction();
        window.map.addLayer(vectorL);
        if (dictionary) {
            $('.cleanGroup').hide();
            $('.drawGroup').removeClass('input-group');
        }
    }
};

function GetMapSize() {
    var rtn = window.map.getSize();
    if ((!rtn || rtn[0] == 0) && IsAccessableParentObject(window, "GetSizeForMap")) {
        rtn = window.parent.GetSizeForMap();
    }
    return rtn;
}

function ShowExtentGeo() { //consol için

    var __XX_E = window.view.calculateExtent(GetMapSize());
    var __XY_E = [__XX_E[0], __XX_E[1], __XX_E[2], __XX_E[3]]
    UpdateProjectionXY(__XY_E, view.getProjection().getCode(), "EPSG:4326", function (t) {
        var mdl = createDumyModal();
        $('.inPContent', mdl).html(t[0] + ", " + t[1] + ", " + t[2] + ", " + t[3]);
        mdl.modal('show');
    })

}

function startupGoMap() {

    try {
        if (window._startupGoMapTimer)
            window.clearTimeout(window._startupGoMapTimer);

        if (window.parent && window.parent.window.CallMapBBOXData) {
            var wData = window.parent.window.CallMapBBOXData;
            window.parent.window.CallMapBBOXData = null;
            window._startupGoMapTimer = window.setTimeout(function () { window.GoMap(wData); }, 500);
        }
        else if (window.parent && window.parent.StartupView != "MAP" && window.parent.GeodiFacet && window.parent.GeodiFacet.BBOX)
            window._startupGoMapTimer = window.setTimeout(function () { window.GoMap({ BBOX: window.parent.GeodiFacet.BBOX, SetExtent: true }); }, 500);
    }
    catch (e) { }
}
$(window).ready(function () {
    RunMetodOnParentWindow("OnMapInit", { frame: window });
    window.setTimeout(function () { $(document.body).trigger("MAP_Init") }, 500);
    StartMap();
    StartMap2();
    if (window._Start3DMap && !window.Disable3D && window.olcs && !dictionary && (window._WappPSSC || !window.BrowserType.IsWinApp())) {
        if (!(window.BrowserType && window.BrowserType.getOS() === "android" && window.BrowserType.getOSVersion() < 7)) {
            window.CreateAddAction({
                id: "_3dStarterBtn",
                html: "<button><nobr id=btn3Dmap>3D</nobr></button>",
                click: function () {
                    _Start3DMap();
                }
            });
        }
    }

    if (window.map && window.CreateDefaultControllers)
        window.CreateDefaultControllers();
    RunMetodOnParentWindow("OnMapReady", { frame: window });
    window.setTimeout(function () { $(document.body).trigger("MAP_Ready") }, 500);

    startupGoMap();
    

})


