﻿window.keywordHighlightPreStr = '<span style="color:orange;font-weight:bold;" class="hgInner">'; //border:1px dashed red; fazla element ekliyor olabilir!
window.keywordHighlightPostStr = '</span>';
//window.keywordHihglihtactiveRoot = dışardan ayarlanıyor.
//window.keywordHihglihtactiveWsName = dışardan ayarlanıyor.

function KeywordHighlight() {
    this.Recognize = function (text) {
        if (!window.keywordHihglihtactiveRoot || !window.keywordHihglihtactiveWsName) return;

        var url;
        var params;
        var jsonmode;
        //jsonp GET ile çalışıyor.
        if (!window.keywordHighlightUsePost) {
            url = window.keywordHihglihtactiveRoot + 'DataExtractionHandler?op=Recognize&wsName=' + encodeURIComponent(window.keywordHihglihtactiveWsName) + '&Text=' + encodeURIComponent(text) + '&SortTextLength=true&AddFilterInfo=false';
            param = null;
            jsonmode = 'jsonp';
        }
        else {
            url = window.keywordHihglihtactiveRoot + 'DataExtractionHandler?op=Recognize&SortTextLength=true&AddFilterInfo=false';
            param = { 'wsName': window.keywordHihglihtactiveWsName, 'Text': text };
            jsonmode = 'json';
        }
      
        $.post(url, param,
            function (dataIn, textStatus) {
                var dataarray = [];
                for (var i = dataIn.Entries.length - 1; i > -1 ; i--) {
                    var str = dataIn.Entries[i].OriginalText;
                    //if (str.length > 3)
                    dataarray.push(escapeRegExp(str));
                }
                if (dataarray.length > 0) {
                    ReplaceForHighlight(dataarray.join("|"), null, true);
                    if (!window.keywordSearchHiglight)
                        GoAndBlink(".hgInner", 0, $(document.body), true, $("html, body"), 200);
                }
                if (window.keywordSearchHiglight) {
                    ReplaceForHighlight(window.keywordSearchHiglight, null, false, '<span style="color:red;font-weight:bold;text-decoration: underline;" class="hgInner hgInnerDefault">');

                    GoAndBlink(".hgInnerDefault", 0, $(document.body), true, $("html, body"), 200);

                }
            }
            ,
            jsonmode
            ).fail(function (xhr, status, error) {
                //alert(xhr.responseText);
            });
    }
}
function GoAndBlink(selector, pos, searchElement, DisableAddScrollTop, animateObject, exTopOffset,timeMs) {
    try {
        var all = $(selector, searchElement);
        if (pos >= all.length) pos = 0;
        var el = $($(selector)[pos]);
        
        if (el.offset()) {
            if (!animateObject)
                animateObject = searchElement;

            var tagetTop=((el.offset().top - (exTopOffset?exTopOffset:0)) + (DisableAddScrollTop ? 0 : searchElement.scrollTop()));
            animateObject.animate({
                scrollTop: tagetTop
            }, timeMs ? timeMs : 500,
            function () {
                var tagetTopNew = ((el.offset().top - (exTopOffset ? exTopOffset : 0)) + (DisableAddScrollTop ? 0 : searchElement.scrollTop()));
                if (Math.abs(tagetTopNew - tagetTop) > 10) {
                    animateObject.animate({
                        scrollTop: tagetTopNew
                    }, timeMs ? timeMs : 500);
                }

            }
            );

            for (var i = 0; i < 5; i++)
                el.fadeIn(200).fadeOut(200);
            el.fadeIn(500).fadeOut(500).fadeIn(750).fadeOut(750).fadeIn(1000); // blink
        }
    }
    catch (e) {
        
    }
}

function escapeRegExp(string) {
    return string.replace(/([*+?^=!:{}()|\[\]\/\\])/g, "\\$1").replace(/ /g, " ").replace("$", "[$]").replace(".", "[.]");
}

function ReplaceForHighlight(searchText, searchNode, escaped,customPreStr) {
    if (searchText)
    {
        if (!escaped) searchText = escapeRegExp(searchText);


        
        findAndReplace("(^|[\\W]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+)(" + (searchText) + ")($|[\\W]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+)",
            "$1" + (customPreStr?customPreStr:window.keywordHighlightPreStr) + "$2" + window.keywordHighlightPostStr + "$3", searchNode);
    }
        
}

function ReplaceForHighlightClear(searchNode)
{
    $(".hgInner", searchNode).each(function ()
    {
            var el = $(this)[0];
            var parent = el.parentNode;
            parent.replaceChild(el.firstChild, el);
            parent.normalize();
     });
}

function findAndReplace(searchText, replacement, searchNode) {
    if (!searchText || typeof replacement === 'undefined') {
        // Throw error here if you want...
        return;
    }
    var regex = typeof searchText === 'string' ?
                new RegExp(searchText, 'ig') : searchText,
        childNodes = (searchNode || document.body).childNodes,
        cnLength = childNodes.length,
        excludes = 'html,head,style,title,link,meta,script,object,iframe';
    while (cnLength--) {
        var currentNode = childNodes[cnLength];
        if (currentNode.nodeType === 1 &&
            (excludes + ',').indexOf(currentNode.nodeName.toLowerCase() + ',') === -1) {
            arguments.callee(searchText, replacement, currentNode);
        }
        if (currentNode.nodeType !== 3 || !regex.test(currentNode.data)) {
            continue;
        }
        var parent = currentNode.parentNode,
            frag = (function () {
                var html = currentNode.data.replace(regex, replacement),
                    wrap = document.createElement('div'),
                    frag = document.createDocumentFragment();
                wrap.innerHTML = html;
                while (wrap.firstChild) {
                    frag.appendChild(wrap.firstChild);
                }
                return frag;
            })();
        parent.insertBefore(frag, currentNode);
        parent.removeChild(currentNode);
    }
}


function  UrlDecodeTmp(val) { return window.decodeURIComponent ? window.decodeURIComponent(val) : val.replace(/%26/g, '&'); }

function GetUrlParameterTmp(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results === null ? null : results[1];
}

function ____CheckStart() {
    if (!window.keywordHihglihtactiveRoot || !window.keywordHihglihtactiveWsName) {

        window.keywordHihglihtactiveRoot = '<dcc:RequestValueWriter name="_geodiurl" />';
        window.keywordHihglihtactiveWsName = '<dcc:RequestValueWriter name="_geodiws" />'; 
        window.keywordSearchHiglight = '<dcc:RequestValueWriter name="_geodisearch" />'; 
        if (!window.keywordHihglihtactiveRoot || !window.keywordHihglihtactiveWsName)
        {
            
            window.keywordHihglihtactiveRoot = GetUrlParameterTmp("_geodiurl");
            window.keywordHihglihtactiveWsName = GetUrlParameterTmp("_geodiws");
            window.keywordSearchHiglight = GetUrlParameterTmp("_geodisearch");

            
            
            if (window.keywordHihglihtactiveRoot)
                window.keywordHihglihtactiveRoot = UrlDecodeTmp(window.keywordHihglihtactiveRoot);
            if (window.keywordHihglihtactiveWsName)
                window.keywordHihglihtactiveWsName = UrlDecodeTmp(window.keywordHihglihtactiveWsName);
            if (window.keywordSearchHiglight)
                window.keywordSearchHiglight = UrlDecodeTmp(window.keywordSearchHiglight);
            if (!window.keywordHihglihtactiveRoot || !window.keywordHihglihtactiveWsName)
                return;
        }
    }
    if (!window.$ || !window.$.post) {
        if (!window.___scriptAdded) {
            var sc2 = window.keywordHihglihtactiveRoot + "GUI/js/jquery-2.1.3.min.js";
            window.scElement = document.createElement("script");
            window.scElement.setAttribute("src", sc2);
            document.body.appendChild(window.scElement);
            window.___scriptAdded = true;
        }
        window.setTimeout(____CheckStart, 100);
        return;
    }
    if (window.recognizeOneTimeRun)
        return;
    window.recognizeOneTimeRun = true;

    
    var keywordHighlight = new KeywordHighlight();
    keywordHighlight.Recognize(document.body.innerText);
    
}
window.setTimeout(____CheckStart, 100);
