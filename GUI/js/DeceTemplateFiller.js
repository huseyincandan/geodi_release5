﻿window.fillterGlobalTypes = {};
/*
 fillterGlobalTypes.["name1"]=
    {
        "label": "name",
        "type":"text",
        "required":true,
        "readonly":false,
        "showif":"1==1"
    }
*/
//HttpUtility required



function TemplateFillerFormatParse(str, definedtypes) {
    var _DecodePercentForTemplate = function (str) {
        return str.replace(/PERCENTSPRT/g, '%')
    }

    if (!definedtypes) definedtypes = window.fillterGlobalTypes ? fillterGlobalTypes : {};
    var lastp = 0, m, r1 = new RegExp("(%%[^%]*%%)", "gi");
    var items = [];
    var fields = [];
    var fldRefs = {};
    var idx = 0;
    while ((m = r1.exec(str)) !== null) {
        if (m.index != lastp && m.index - lastp > 0)
            items.push({ val: _DecodePercentForTemplate(str.substr(lastp, m.index - lastp)), ToString: function (data, context) { return this.val; } });
        if (m[0].length > 0) {
            var t = str.substr(m.index, m[0].length);
            t = _DecodePercentForTemplate(t.substr(2, t.length - 4).trim());
            var fld = {
                isField: true,
                index: idx,
                ToString: function (data, context, options) {
                    var rtnS1 = null;
                    var vg = this["value-getter"];
                    if (vg != null && vg != undefined) {
                        if (typeof (vg) == "function")
                            rtnS1 = vg(this, data, context, options);
                        else
                            rtnS1 = vg;
                    }
                    else if (context && context.ValueFiller)
                        rtnS1 = context.ValueFiller(this, data, context);
                    else if (data && data.hasOwnProperty(this.model) && data[this.model]!=="")
                        rtnS1 = data[this.model];
                    else if (data && data.ValueFiller)
                        rtnS1 = data.ValueFiller(this, data, context);
                    else
                        return this.emptyvalue != undefined ? this.emptyvalue  : "";
                    var enc = this.encode ? this.encode : (options && options.encode ? options.encode : "js");
                    if (enc == "js")
                        rtnS1 = HttpUtility.ScriptEncode(rtnS1);
                    return rtnS1;
                }
            };
            if (t.indexOf(";") != -1 || t.indexOf(":") != -1)
                $.each(t.replace(/\\;/g, 'NSPN!SP0--').split(';'), function (i, sp) {
                    sp = sp.replace(/NSPN!SP0--/g, '\\;');
                    sp = sp.replace(/NSPN!SP1--/g, ';');
                    var v0 = sp.indexOf(":");
                    var k, v = true;
                    if (v0 != -1) {
                        k = sp.substr(0, v0).toLowerCase();
                        v = sp.substr(v0 + 1);
                        if (v.length > 1 && ((v[0] == "{" && v[v.length - 1] == "}") || (v[0] == "[" && v[v.length - 1] == "]")))
                            try { v = JSON.parse(v); } catch (ex) { }
                    }
                    else
                        k = sp.toLowerCase();
                    fld[k] = v;
                }
                );
            else if (definedtypes[t])
                fld = $.extend({}, definedtypes[t], fld);
            else
                fld.label = t;

            if (fld.type && definedtypes[fld.type])
                fld = $.extend({}, definedtypes[fld.type], fld);
            if (fld.set && definedtypes[fld.set])
                fld = $.extend({}, definedtypes[fld.set], fld);

            if (!fld.model && fld.id)
                fld.model = fld.id;
            if (!fld.model && fld.ID)
                fld.model = fld.ID;
            if (!fld.model)
                fld.model = "fld_" + fields.length;
            if (fld.model && definedtypes[fld.model])
                fld = $.extend({}, definedtypes[fld.model], fld);

            if (!fld.label)
                fld.label = fld.model;
            if (!fld.type)
                fld.type = "text";

            
            if (fldRefs[fld.model])
                fld = fldRefs[fld.model];
            else {
                fields.push(fld)
                fldRefs[fld.model] = fld;
            }
            items.push(fld);
            
            idx++;
        }
        lastp = r1.lastIndex = m.index + m[0].length;
    }
    if (str != null && str != undefined && lastp < str.length)
        items.push({ val: str.substr(lastp), ToString: function (data, context) { return this.val; } });
    var rtn1={
        all: items,
        fields: fields
    };
    rtn1.ToString= function (data, context,options) {
        var rtn = [];
        var fieldList = rtn1.all;
        if (options && options.fieldList)
            fieldList = options.fieldList;
        if (!options)
            options = {};
        if (!options.encode)
            options.encode = "js";
        $.each(fieldList, function (ix, fl) {
            var str = fl.ToString(data, context, options);
            rtn.push(str);
        })
        return rtn.join("");
    }

    return rtn1;
}

