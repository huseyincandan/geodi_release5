﻿
function GetUrlParameter(name, url) {
    if (!url) url = location.href
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}

function __GetWorkspaceName() {
    return "-";
}
function GetSupportExtentUpdate(Cmd) {
    return true;
}

function __GetMobidiMapQuery() {
    if (window.ForcedMobidiQueryJSON)
        return window.ForcedMobidiQueryJSON;
    var q = GetUrlParameter("mapQuery");
    if (q) return HttpUtility.UrlDecode(q);
    return null;
}


function __GetParams() {

    var recordID = window.ShowAllEntries ? null : GetUrlParameter("recordID");
    var wsName = __GetWorkspaceName();
    var rtn = { 'LAYERS': 'MOBIDI', 'Rnd': window.GetRandom(), 'wsName': wsName, "recordID": recordID };
    var query = window.ShowAllEntries ? null : __GetMobidiMapQuery();
    rtn.mapQuery = query;
    return rtn;
}

function __GetCapabilityParams() {
   var recordID = GetUrlParameter("recordID");
    if (recordID)
        return "recordID=" + recordID;

    var query = window.ShowAllEntries ? null : __GetMobidiMapQuery();
    if (query)
        return "mapQuery=" + HttpUtility.UrlEncode(query);
    return "";

}

function __getBaseLayer() {
    return DeceClientState.GetState("MobidiDefaultBaseMapLayerName", null, true);
}


$(function () {
     
    window.__popUpWidth = 600;
    window.__popUpHeight = 500;
    window.ShowAllEntries = false;
    var recordID = GetUrlParameter("recordID");
    
    //window.__wmsUrl = 'MobidiWMS?' +(recordID ? ("recordID="+recordID):"");
    window.__wmsUrl = 'MobidiWMS?';

    window.changeVisibleLayer = function (style) {
        var i, ii;
        for (i = 0, ii = window.layers.length; i < ii; ++i) {
            var lstyle = window.layers[i].get('deceID');
            var lnoHide = window.layers[i].get('noHide');
            if (!lnoHide && lstyle && lstyle != "WmsLayer")
                layers[i].set('visible', (lstyle == style));
        }
        window.DeceClientState.SetState('MobidiDefaultBaseMapLayerName', style, true);
    }


})

//DICTIONARY      

function getDictionary(id) {
    window.currentDictionaryRecognizerBase.GetGeodiDictionary(id, function (data, textStatus) {
        window.currentDictionary = data;
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.Polygon)) {
            $("#drawType option[value='Polygon']").remove();
        }
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.Point)) {
            $("#drawType option[value='Point']").remove();
        }
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.LineString)) {
            $("#drawType option[value='LineString']").remove();
        }
    });
}

function sendBackWkt(wkt) {
    window.parent.postMessage({ type: 'WKT', WKT: wkt }, '*');
}

function startDictionaryRecord(wkt) {
    $('.trash').hide();
    $('.cleanGroup').show();
    $('.loading').show();
    $('.drawGroup').addClass('input-group');
    disableTypeSelect();
    $('#DictionaryContent').html('');
    window.currentDictionary.__WKT = wkt;
    window.webEditorManagerMap.createEditor(window.currentDictionary, function (obj) {
        window.webEditorManagerMap.current = obj;
        $('#DictionaryModal').modal('show');
        // window.cleanDrawing();
        $('.loading').hide();
        $('.trash').show();
        $('.cleanGroup').hide();
        $('.drawGroup').removeClass('input-group');
        enableTypeSelect();
    }, $('#DictionaryContent'), 450);
}

window.webEditorManagerMap = new WebEditorManager();

window.currentEditorSave = function () {
    var val = window.webEditorManagerMap.current.IsValid(true);
    $.when(val).then(
         function (status) {
             if (status) {
                 window.opener.destroyGrid();
                 window.opener.initGrid();
                 $('#DictionaryModal').modal('hide');
             }
                 
         });
}

window.currentEditorClose = function () {
    window.vSource.removeFeature(window.lastFeature);
}