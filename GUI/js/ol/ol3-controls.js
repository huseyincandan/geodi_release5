﻿window.ol3controls = {};
var ol3controls = window.ol3controls;
function CreateGeolocation(proj, options) {
    var forcedgeolocation = (window.IsAccessableParentObject && window.IsAccessableParentObject(window, "GetCurrentGelocation"));
    if (forcedgeolocation)
        return new DeceGeolocation(proj);
    if (!options)
        options = {
            projection: proj,
            tracking: true,
            trackingOptions: {
                maximumAge: 10000,
                enableHighAccuracy: true,
                timeout: 600000
            }
        };
    else if (proj)
        options.projection = proj;
    return new ol.Geolocation(options);
}

function DeceGeolocation(proj) {
    this.proj = proj;
    this.on = function (key, fnc) {
        if (key == "change:position") {
            this.posfnc = fnc;
            this._once = false;
        }
        if (key == "error")
            this.errfnc = fnc;
    }
    this.once = function (key, fnc) {
        if (key == "change:position") {
            this.posfnc = fnc;
            this._once = true;
        }
    }
    this.setTracking = function (enb) {
        var that = this;
        window.clearTimeout(this.intrvl);

        if (enb) {
            var fnc1 = function () {
                window.parent.GetCurrentGelocation(function (data, evt) {
                    if (data) {
                        if (data.status == "error") {
                            if (that.errfnc) that.errfnc(data, evt);
                            //window.setTimeout(fnc1, 5000);
                        }
                        else {
                            that._last = data.location;
                            that._lastpos = [that._last.longitude, that._last.latitude]
                            if (that.proj) {
                                var sr1 = that.proj.getCode().replace("EPSG:", "")
                                if (sr1 != "4326") {
                                    var gm = new ol.geom.Point([that._lastpos[0], that._lastpos[1]]);
                                    gm.transform("EPSG:4326", that.proj);
                                    gm = gm.getCoordinates();
                                    that._lastpos = [gm[0], gm[1]];
                                }
                            }
                            if (that.posfnc) {
                                that.posfnc();
                                if (!that._once)
                                    window.setTimeout(fnc1,1000);
                            }
                        }
                    }
                })
            };

            this.intrvl = window.setTimeout(fnc1, 1000);
        }
    }
    this.getPosition = function () { return this._lastpos; }
    this.getAccuracy = function () { return this._last ? this._last.accuracy : 0; }
    this.getHeading = function () { return this._last ? this._last.heading : 0; }
    this.getSpeed = function () { return this._last ? this._last.speed : 0; }

    this.setTracking(true);
}

ol3controls.GeolocationControl = function (opt_options) {

    var options = opt_options || {};


    this._zoomTo = opt_options.zoom;

    var this_ = this;
    var element = document.createElement('div');
    element.className = 'ol-control geo-location ol-unselectable';

    this_.clickCount = 0;

    //TODO: İki buton gereksiz, tek butona düşürülecek
    var btnLocateMe = document.createElement('button');
    var span = document.createElement('i');
    span.className = 'fa fa-location-arrow';
    btnLocateMe.appendChild(span);
    function addLocateMe() {
        if (element.childElementCount > 0)
            element.removeChild(btnTrackMe);
        element.appendChild(btnLocateMe);
    }

    var btnTrackMe = document.createElement('button');
    var span2 = document.createElement('i');
    span2.className = 'fa fa-location-arrow control-rotate';
    btnTrackMe.appendChild(span2);
    function addTrackMe() {
        if (element.childElementCount > 0)
            element.removeChild(btnLocateMe);
        element.appendChild(btnTrackMe);
    }

    btnTrackMe.addEventListener('click', countClicks, false);
    btnTrackMe.addEventListener('touchstart', countClicks, false);
    btnLocateMe.addEventListener('click', countClicks, false);
    btnLocateMe.addEventListener('touchstart', countClicks, false);


    function countClicks(event) {
        this_.clickCount++;
        switch (this_.clickCount) {
            case 1:
                addTrackMe();
                startLocating(event, false);
                break;
            case 2:
                if (btnTrackMe.className.indexOf("selected-item") < 0)
                    btnTrackMe.className += "selected-item";
                stopLocating(event);
                startLocating(event, true);
                window.dragKey = this_.getMap().on('pointerdrag', function (e) { terminateLocate(e); });
                break;
            case 3:
                terminateLocate(event);
                break;
        }
    }

    var startLocating = function (e, trackPosition) {
        this_.startLocating(e, trackPosition);
    };

    var stopLocating = function (e) {
        this_.stopLocating(e);
    };

    var terminateLocate = function (event) {
        btnTrackMe.className = btnTrackMe.className.replace('selected-item', '');
        stopLocating(event);
        addLocateMe();
        this_.clickCount = 0;
        this_.getMap().unByKey(window.dragKey);
    }

    ol.control.Control.call(this,
    {
        element: element,
        map: options.map,
        target: options.target
    });

    addLocateMe();
};
ol.inherits(ol3controls.GeolocationControl, ol.control.Control);

ol3controls.GeolocationControl.prototype.startLocating = function (e, trackPosition) {
    e.preventDefault();

    var map = this.getMap();

    var view = map.getView();

    var this_ = this;

    var positions = new ol.geom.LineString([], ('XYZM'));

    var deltaMean = 500;


    function radToDeg(rad) {
        return rad * 360 / (Math.PI * 2);
    }

    function degToRad(deg) {
        return deg * Math.PI * 2 / 360;
    }

    function mod(n) {
        return ((n % (2 * Math.PI)) + (2 * Math.PI)) % (2 * Math.PI);
    }

    this_.markerEl = $('<img id="geolocation_marker" src="' + HttpUtility.ResolveUrl("~/GUI/img/geolocation_marker.png", false, true) + '"></img>');
    if (!this_.positionMarker)
        this_.positionMarker = new ol.Overlay({
            positioning: 'center-center',
            element: this_.markerEl[0],
            stopEvent: false
        });
    map.addOverlay(this_.positionMarker);

    function addPosition(position, heading, m, speed) {
        if (this_.trackingActive) {
            var x = position[0];
            var y = position[1];
            var fCoords = positions.getCoordinates();
            var previous = fCoords[fCoords.length - 1];
            var prevHeading = previous && previous[2];
            if (prevHeading) {
                var headingDiff = heading - mod(prevHeading);

                if (Math.abs(headingDiff) > Math.PI) {
                    var sign = (headingDiff >= 0) ? 1 : -1;
                    headingDiff = -sign * (2 * Math.PI - Math.abs(headingDiff));
                }
                heading = prevHeading + headingDiff;
            }
            positions.appendCoordinate([x, y, heading, m]);

            positions.setCoordinates(positions.getCoordinates().slice(-20));

            if (heading && speed) {
                this_.markerEl[0].src = '../../gui/img/geolocation_marker_heading.png';
            } else {
                this_.markerEl[0].src = '../../gui/img/geolocation_marker.png';
            }
        }
    }

    var previousM = 0;
    /*
    var fncbeforeRender = map.beforeRender?map.beforeRender:window.view.animate;
    fncbeforeRender(function (map, frameState) {
        if (frameState !== null && this_.trackingActive) {

            var m = frameState.time - deltaMean * 1.5;
            m = Math.max(m, previousM);
            previousM = m;

            var c = positions.getCoordinateAtM(m, true);
            var view = frameState.viewState;
            if (c) {
                view.center = getCenterWithHeading(c, -c[2], view.resolution);
                view.rotation = -c[2];
                this_.positionMarker.setPosition(c);
            }
        }
        return true;
    });      */

    function getCenterWithHeading(position, rotation, resolution) {
        var size = map.getSize();
        var height = size[1];

        return [
        position[0] - Math.sin(rotation) * height * resolution * 1 / 4,
        position[1] + Math.cos(rotation) * height * resolution * 1 / 4
        ];
    }


    function render() {
        map.render();
    }
    if (!this_.geolocation) {
        this_.geolocation = CreateGeolocation(view.getProjection());
        this_.geolocation.on('error', function (error) {
            //alert(error.message);
        });
    }
    else
        this_.geolocation.setTracking(true);

    map.on('postcompose', render);
    map.render();
    if (trackPosition) {
        this_.trackingActive = true;
        this_.geolocation.on('change:position', function () {
            var position = this_.geolocation.getPosition();
            var accuracy = this_.geolocation.getAccuracy();
            var heading = this_.geolocation.getHeading() || 0;
            var speed = this_.geolocation.getSpeed() || 0;
            var m = Date.now();
            addPosition(position, heading, m, speed);

            var coords = positions.getCoordinates();
            var len = coords.length;
            if (len >= 2) {
                deltaMean = (coords[len - 1][3] - coords[0][3]) / (len - 1);
            }
        });
    }
    else
        this_.geolocation.once('change:position', function () {
            this_.positionMarker.setPosition(this_.geolocation.getPosition());
            view.setCenter(this_.geolocation.getPosition());
            this_._toZoom = this_._toZoom ? this_._toZoom : 1;
            view.setResolution(this_._toZoom);
        });
};

ol3controls.GeolocationControl.prototype.stopLocating = function (e) {
    if (e)
        e.preventDefault();
    var this_ = this;
    if (this_.geolocation) {
        var map = this_.getMap();

        this_.geolocation.on('change:position', function () {
            return false;
        });
        this_.geolocation.once('change:position', function () {
            return false;
        });
        this_.geolocation.setTracking(false);
        delete this_.geolocation;
        delete this_.lastLocationZoom;
        map.removeOverlay(this_.positionMarker);
        delete this_.positionMarker;
        this_.trackingActive = false;
    }
};