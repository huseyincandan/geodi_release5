window.olDistance= {
	toRadians:function(angleInDegrees) {
	  return angleInDegrees * Math.PI / 180;
	},
	getDistance:function(c1, c2) {
	  const radius = 6371008.8;
	  const lat1 = window.olDistance.toRadians(c1[1]);
	  const lat2 = window.olDistance.toRadians(c2[1]);
	  const deltaLatBy2 = (lat2 - lat1) / 2;
	  const deltaLonBy2 = window.olDistance.toRadians(c2[0] - c1[0]) / 2;
	  const a = Math.sin(deltaLatBy2) * Math.sin(deltaLatBy2) + Math.sin(deltaLonBy2) * Math.sin(deltaLonBy2) * Math.cos(lat1) * Math.cos(lat2);
	  return 2 * radius * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	},
	getLengthInternal:function(coordinates) {
	  let length = 0;
	  const radius = 6371008.8;
	  for (let i = 0, ii = coordinates.length; i < ii - 1; ++i) {
		length += window.olDistance.getDistance(coordinates[i], coordinates[i + 1]);
	  }
	  return length;
	},
	getLength:function(geometry, sourceProjection) {

	  const type = geometry.getType();
	  if (type !== 'GeometryCollection' && sourceProjection && sourceProjection!='EPSG:4326')
		geometry = geometry.clone().transform(sourceProjection, 'EPSG:4326');
	 
	  let length = 0;
	  let coordinates, coords, i, ii, j, jj;
	  switch (type) {
		case 'Point':
		case 'MultiPoint':
		  {
			break;
		  }
		case 'LineString':
		case 'LinearRing':
		  {
			coordinates = geometry.getCoordinates();
			length = window.olDistance.getLengthInternal(coordinates);
			break;
		  }
		case 'MultiLineString':
		case 'Polygon':
		  {
			coordinates = geometry.getCoordinates();
			for ((i = 0, ii = coordinates.length); i < ii; ++i) {
			  length += window.olDistance.getLengthInternal(coordinates[i]);
			}
			break;
		  }
		case 'MultiPolygon':
		  {
			coordinates = geometry.getCoordinates();
			for ((i = 0, ii = coordinates.length); i < ii; ++i) {
			  coords = coordinates[i];
			  for ((j = 0, jj = coords.length); j < jj; ++j) {
				length += window.olDistance.getLengthInternal(coords[j]);
			  }
			}
			break;
		  }
		case 'GeometryCollection':
		  {
			const geometries = geometry.getGeometries();
			for ((i = 0, ii = geometries.length); i < ii; ++i) {
			  length += window.olDistance.getLength(geometries[i], sourceProjection);
			}
			break;
		  }
		default:
		  {
			throw new Error('Unsupported geometry type: ' + type);
		  }
	  }
	  return length;
	},

	getAreaInternal:function(coordinates) {
	  let area = 0;
	  const radius = 6371008.8;
	  const len = coordinates.length;
	  let x1 = coordinates[len - 1][0];
	  let y1 = coordinates[len - 1][1];
	  for (let i = 0; i < len; i++) {
		const x2 = coordinates[i][0];
		const y2 = coordinates[i][1];
		area += window.olDistance.toRadians(x2 - x1) * (2 + Math.sin(window.olDistance.toRadians(y1)) + Math.sin(window.olDistance.toRadians(y2)));
		x1 = x2;
		y1 = y2;
	  }
	  return area * radius * radius / 2.0;
	},
	getArea:function(geometry, sourceProjection) {
	  const type = geometry.getType();
	 if (type !== 'GeometryCollection' && sourceProjection && sourceProjection!='EPSG:4326')
		geometry = geometry.clone().transform(sourceProjection, 'EPSG:4326');
	 
	  let area = 0;
	  let coordinates, coords, i, ii, j, jj;
	  switch (type) {
		case 'Point':
		case 'MultiPoint':
		case 'LineString':
		case 'MultiLineString':
		case 'LinearRing':
		  {
			break;
		  }
		case 'Polygon':
		  {
			coordinates = geometry.getCoordinates();
			area = Math.abs(window.olDistance.getAreaInternal(coordinates[0]));
			for ((i = 1, ii = coordinates.length); i < ii; ++i) {
			  area -= Math.abs(window.olDistance.getAreaInternal(coordinates[i]));
			}
			break;
		  }
		case 'MultiPolygon':
		  {
			coordinates = geometry.getCoordinates();
			for ((i = 0, ii = coordinates.length); i < ii; ++i) {
			  coords = coordinates[i];
			  area += Math.abs(window.olDistance.getAreaInternal(coords[0]));
			  for ((j = 1, jj = coords.length); j < jj; ++j) {
				area -= Math.abs(window.olDistance.getAreaInternal(coords[j]));
			  }
			}
			break;
		  }
		case 'GeometryCollection':
		  {
			const geometries = geometry.getGeometries();
			for ((i = 0, ii = geometries.length); i < ii; ++i) {
			  area += window.olDistance.getArea(geometries[i],sourceProjection);
			}
			break;
		  }
		default:
		  {
			throw new Error('Unsupported geometry type: ' + type);
		  }
	  }
	  return area;
	}
}