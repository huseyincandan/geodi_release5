﻿var ___cloud = function () {
    this.getDropboxAuthUrl = function (obj, requestId, callBackFunction) {
        $.post('DropboxContentEnumeratorWebHandler?op=getValidateUrl', { 'input': JSON.stringify(obj), 'clientRequestId': requestId }, callBackFunction, 'json');
    }
    this.isDropboxOK = function (requestId, callBackFunction) {
        $.post('DropboxContentEnumeratorWebHandler?op=isOK', { 'clientRequestId': requestId }, callBackFunction, 'json');
    }
    this.setDropboxToken = function (obj, requestId, callBackFunction) {
        $.post('DropboxContentEnumeratorWebHandler?op=setAccessToken', { 'input': JSON.stringify(obj), 'clientRequestId': requestId }, callBackFunction, 'json');
    }
    this.renewDropboxFolders = function (obj, callBackFunction) {
        $.post('DropboxContentEnumeratorWebHandler?op=renewFolders', { 'input': JSON.stringify(obj) }, callBackFunction, 'json');
    }
    this.renewWebDAVFolders = function (obj, callBackFunction) {
        $.post('WebDAVContentEnumeratorWebHandler?op=renewFolders', { 'input': JSON.stringify(obj) }, callBackFunction, 'json');
    }
    this.isWebDAVValid = function (obj, callBackFunction) {
        $.post('WebDAVContentEnumeratorWebHandler?op=isValid', { 'input': JSON.stringify(obj) }, callBackFunction, 'json');
    }
    this.renewFTPFolders = function (obj, callBackFunction) {
        $.post('FTPContentEnumeratorWebHandler?op=renewFolders', { 'input': JSON.stringify(obj) }, callBackFunction, 'json');
    }
    this.isFTPValid = function (obj, callBackFunction) {
        $.post('FTPContentEnumeratorWebHandler?op=isValid', { 'input': JSON.stringify(obj) }, callBackFunction, 'json');
    }
}

window.CloudManager = new ___cloud();




