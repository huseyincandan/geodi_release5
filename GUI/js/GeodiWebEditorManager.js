﻿if (!window.enum)
    window.enum = new Object();
window.enum.addingCondition = {
    Default: 0, Hide: 1, SingleInstance: 2
}

window.__lidVal = 0;
var WebEditorManager = function (editors) {
    this.editors = editors;
    this.current = null;
    this.currentIndex = -1;

    this.createList = function (target, onClickFunction, options) {
        if (!options)
            options = {}; //DisableGroups:false     , RootIcoSize , MenuIcoSize
        var orgEditors = [];
        var groupEditorIds = {};
        var inIndex = 0;
        for (var i = 0; i < this.editors.length; i++) {
            if (editors[i].__WebEditorMode == 3) //editörü gizli
                continue;
            this.editors[i].__lid = window.__lidVal++;
            if (this.editors[i].GroupName && !options.DisableGroups) {
                if (!groupEditorIds[this.editors[i].GroupName]) {
                    groupEditorIds[this.editors[i].GroupName] = inIndex++;
                    orgEditors.push({ __GrpOrg: true, GroupName: this.editors[i].GroupName, editors: [this.editors[i]] });
                }
                else {
                    orgEditors[groupEditorIds[this.editors[i].GroupName]].editors.push(this.editors[i]);
                }
            }
            else {
                orgEditors.push(this.editors[i]);
                inIndex++;
            }
        }
        var CreateIdIn = function (val, simple) {
            if (simple)
                return val.replace(/ /g, "_").replace(/\./g, "_").replace(/:/g, "_");
            //var vailds="ABC..."
            var v = val + "";
            return "id_" + v.hashCode();
        }
        var addbtn = function (edt, intarget, isDropdownItem) {

            var name = edt.DisplayName;
            var lid = edt.__lid;
            delete edt.ID;       //option!!!!, Singleton !!!!!
            var ico = "";
            var lIcoSize = isDropdownItem ? options.MenuIcoSize : options.RootIcoSize;
            if (lIcoSize > 0) {
                if (edt.IconName)
                    ico = "<img src='" + HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=' + edt.IconName, true, true) + "' width=" + lIcoSize + " height=" + lIcoSize + ">  ";
                else
                    ico = "<img src='" + HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=empty', true, true) + "' width=" + lIcoSize + " height=" + lIcoSize + ">  ";
            }
            var btnDom =
                isDropdownItem ?
                 $('<li id="' + CreateIdIn(edt.__type) + '" data-id="' + lid + '"><a href="#">' + ico + name + '</a></li>') :
                $('<button style="margin-right:5px;" type="button" id="btn_' + CreateIdIn(edt.__type, true) + '" data-id="' + lid + '" class="btn btn-sm btn-default" >' + ico + name + '</button>');
            btnDom.click(function () { onClickFunction($(this).data('id')); });
            intarget.append(btnDom);
        }
        for (var i = 0; i < orgEditors.length; i++) {
            var edt = orgEditors[i];
            if (edt.__GrpOrg) {

                if (edt.editors.length == 1) {
                    addbtn(edt.editors[0], target);
                }
                else {
                    var btnDom = $(' <div class="btn-group"></div>');
                    btnDom.append($('<button style="margin-right:5px;" type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" >' +
                         (options.RootIcoSize > 0 ? "<img src='" + HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=empty', true, true) + "' width=1 height=" + options.RootIcoSize + "> " : "") +
                            edt.GroupName + ' <span  style="margin-top:-3px;" class="caret"></span></button>'));

                    var btnNewTarget = $('<ul class="dropdown-menu" role="menu"></ul>');

                    btnDom.append(btnNewTarget);
                    for (var j = 0; j < edt.editors.length; j++)
                        addbtn(edt.editors[j], btnNewTarget, true);
                    target.append(btnDom);
                }

            }
            else
                addbtn(edt, target);
        }
    };

    this.close = function () { this.current.Dispose(); };

    this.createEditor = function (obj, onReadyFunction, targetDom, frameHeight) {
        if (!frameHeight)
            frameHeight = 400;
        var url0 = obj.forcedEditorUrl ? obj.forcedEditorUrl : HttpUtility.ResolveUrl('~/WebEditorLoader?type=' + obj.__type);
        var frame =
            $('<iframe src="' + url0 + '" style="width:100%; height: ' + frameHeight.toString() + 'px;" border="0" frameborder="0"></iframe>');

        $(targetDom).append(frame);

        function editorLoaded() {
            var inwnd = frame.get(0).contentWindow;
            if (inwnd && inwnd.WebEditorObject) {
                var editorObj = {
                    frameWindow: function () { return frame.get(0).contentWindow },
                    editorObject: frame.get(0).contentWindow.WebEditorObject,
                    IsValid: function (save, original) { return this.frameWindow().WebEditorObject.IsValid(save, original ? JSON.stringify(original) : null); },
                    SetObject: function (obj, editorObjController) {
                        var jsonStr = JSON.stringify(obj);
                        this.frameWindow().WebEditorObject.SetObject(jsonStr, editorObjController);

                        //height ayarını karşıya bind et
                        frame.height(frameHeight);
                        $(targetDom).height(frameHeight);
                        this.frameWindow().WebEditorObject.SetHeight = editorObjController.SetHeight;

                        if (obj && obj.__ReadonlyForEdit) {
                            var that1 = this;
                            window.setTimeout(function () {
                                var bdy = that1.frameWindow().document.body;
                                $("input,button,select", bdy).prop("disabled", true);
                            }, 10)
                        
                        }
                    },
                    GetObject: function (original) {
                        var JsonStr = this.frameWindow().WebEditorObject.GetObject(original ? JSON.stringify(original) : null);
                        return JSON.parse(JsonStr);
                    },
                    SetHeight: function (height) {
                        window.setTimeout(
                            function () {
                                frame.height(height);
                                $(targetDom).height(height);
                            }, 100
                            );

                    },
                    Dispose: function () { if (this.editorObject.Dispose) this.frameWindow().WebEditorObject.Dispose(); },
                    Update: function (editorObjController) {
                        this.SetObject(obj, editorObjController);
                    },
                    GetHelp: function (xType) {
                        if (this.editorObject.GetHelp) return this.frameWindow().WebEditorObject.GetHelp(xType);
                    },
                    OnFinalize: function () {
                        if (this.editorObject.OnFinalize) return this.frameWindow().WebEditorObject.OnFinalize();
                    },
                    Invoke: function (functionName, arg1, arg2, arg3) {
                        if (this.editorObject[functionName]) return this.frameWindow().WebEditorObject[functionName](arg1, arg2, arg3);
                    },
                    domObject: frame
                };

                editorObj.Update(editorObj);
                onReadyFunction(editorObj);
            }
            else window.setTimeout(editorLoaded, 100);
        }
        editorLoaded();
    };

    this.cloneObject = function (obj) {
        return jQuery.extend(true, {}, obj);
    };

    this.findObjectByType = function (type) {
        for (var i = 0; i < this.editors.length; i++)
            if (this.editors[i].__type == type)
                return this.editors[i];
    };

    this.findObject = function (id, orObjID, orType) {
        if (id != undefined & id != null)
        for (var i = 0; i < this.editors.length; i++)
            if (this.editors[i].__lid == id)
                    return this.editors[i];
        if (orObjID != undefined & orObjID != null)
            for (var i = 0; i < this.editors.length; i++)
                if (this.editors[i].ID == orObjID)
                    return this.editors[i];
        if (orType != undefined & orType != null)
            return this.findObjectByType(orType);
    };
}