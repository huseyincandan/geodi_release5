﻿/*DECE*/

function SetDynFormHeight(prm) {
    if (prm && prm.id && prm.height) {
        if (prm.maxheight)
            $("#" + prm.id).css("max-height", prm.maxheight);
        $("#" + prm.id).height(prm.height);
    }
}
$(function () {
    if (window.Selectize)
        Selectize.define('custom_class', function (options) {
            var favoriteOptionsClass = function (thisRef, options) {
                var self = thisRef;
                console.log({ 'self': self })
                var original = self.setup;
                self.setup = (function () {
                    return function () {
                        var addTo = function (html, option) {
                            var $html = $(html);
                            $html.addClass(option.type)
                            return $('<div>').append($html.clone()).html();
                        }
                        var option_render = self.settings.render.option;
                        self.settings.render.option = function (option) {
                            return addTo(option_render.apply(self, arguments), option);
                        }

                        var item_render = self.settings.render.item;
                        self.settings.render.item = function (option) {
                            return addTo(item_render.apply(self, arguments), option);
                        }
                        original.apply(self, arguments);
                    };
                })();
            }
            favoriteOptionsClass(this, options);
            return;
        });
});

function setToSelectize(element, field, $scope, parentElement, fieldType) {
    var vl = "";
    if ($scope && $scope.formData) {
        vl = $scope.formData[field.model];
    }
    else
        vl = $(element).val();
    $(element).val(vl);
    $(element).selectize({ plugins: ['remove_button', 'custom_class']   });
}
function ___setDynamicSelectClick(obj) {
    $("[eval-script=1]", $(obj)).each(function () {
        var v = $(this).html();
        try { eval(v); }
        catch (ex) { }
    })
}

function setDynamicSelect(element, field, $scope, parentElement, fieldType) {
    var vl = "";
    if ($scope && $scope.formData) {
        vl = $scope.formData[field.model];
    }
    else
        vl = $(element).val();
    var rd0 = $(element).prop("readonly") || $(element).attr("ng-readonly") == "true";

    var $select = $(element).selectize({
        plugins: ['remove_button','custom_class'],
        valueField: 'value',
        labelField: 'text',
        searchField: 'HTML',
        score: function (search) {
            return function () { return 1; };//not support(-)
        },
        render: {
            option: function (data, escape) {
                return data.HTML;
            }
        }
    });

    
    var dom = $select[0];

    if (rd0 && dom && dom.selectize) {
        if (dom.selectize.$control_input)
            dom.selectize.$control_input.prop('readonly', true);
        if (dom.selectize.$control && dom.selectize.$control.off)
            dom.selectize.$control.off();
    }

    var serliazeCurrent = function (str) {
        var lastFrmData = {};
        if (str)
            lastFrmData["__qStr"] = str;
        if ($scope.formData)
            $.each($scope.formData, function (id1, vl1) {
                if (id1 != field.model)
                    lastFrmData[id1] = vl1;
            });
        return JSON.stringify(lastFrmData);
    }

    var addOps = function (str) {
        dom.__last = serliazeCurrent(str);
        dom.__lastSet = true;
        if (dom._latq && dom._latq.abort)
            try { dom._latq.abort(); } catch (ex1) { }

        var activeFormData = $scope.formData;
        if (activeFormData) {
            activeFormData = JSON.parse(JSON.stringify(activeFormData));


            for (key in activeFormData) {
                var inputElement = $('input[name="' + key + '"]');
                switch (inputElement.attr("type")) {
                    case 'datetime':
                        activeFormData[key] = (inputElement.val() ? inputElement.datepicker().data('datepicker').lastSelectedDate : null);
                        break;
                    case 'date':
                        activeFormData[key] = (inputElement.val() ? inputElement.datepicker().data('datepicker').lastSelectedDate : null);
                        break;
                    case 'time':
                        var tPicker = (inputElement.val() ? inputElement.data('timepicker') : null);
                        activeFormData[key] = tPicker.getTime(true).toString();
                        break;
                    default:
                        var val = activeFormData[key];
                        if (Array.isArray(val))
                            val = val.join(',');
                        activeFormData[key] = val;
                        break;
                }

            }
        }

        

        dom._latq=$.post(HttpUtility.ResolveUrl('MobidiServerSyncHandler?op=DynamicSelectQuery'), {
            'element_id': field.model,
            'query': str,
            'formData': activeFormData ? JSON.stringify(activeFormData) : null,
            'pageSize':60
        },
            function (data) {
                if (data) {
                    dom.selectize.clearOptions();//not support(-)
                    var ops = dom.selectize.options;
                    for (var i = 0; i < data.Items.length; i++) {
                        var item = data.Items[i];
                        if (item.value && !ops[item.value]) {
                            dom.selectize.addOption(item);
                        }

                    }
                    dom.selectize.refreshOptions();
                }
            }, 'json');
    }

    /*custom event*/
    dom.selectize.__onOptionSelect = dom.selectize.onOptionSelect;
    dom.selectize.onOptionSelect = function (a, b, c) {
        if (a && a.currentTarget)
            ___setDynamicSelectClick(a.currentTarget);
        dom.selectize.__onOptionSelect(a, b, c);
    }

    dom.selectize.on("focus", function () {
        if (dom.__lastSet && dom.__last == serliazeCurrent())
            return;

        if (dom.selectize.items && dom.selectize.items.length > 0) {
            $.each(dom.selectize.options, function (id1, vl1) {
                if (dom.selectize.items.indexOf(id1) == -1)
                    dom.selectize.removeOption(id1);
            })
        }
        else
            dom.selectize.clearOptions();
        addOps();
    });
    dom.selectize.on("type", function (str) {
        if (str) {
            window.clearTimeout(dom.__typeThread);
            dom.__typeThread = window.setTimeout(function () {
                addOps(str);
            }, 500);
        }
    });
  

    if (vl)
        $.post(HttpUtility.ResolveUrl('MobidiServerSyncHandler?op=DynamicSelectQueryItem'), {
            'element_id': field.model,
            'Value': JSON.stringify(vl),
        },
            function (data) {
                var ops = dom.selectize.options;
                if (data && data.length > 0) {
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        if (item.value) {
                            if (!ops[item.value])
                                dom.selectize.addOption(item);
                            dom.selectize.addItem(item.value);
                        }
                    }
                    dom.selectize.refreshItems();
                }
            }, 'json');

}
function setMobidiUserSelect(element, field, $scope, parentElement, fieldType) {
    var rd0 = $(element).prop("readonly") || $(element).attr("ng-readonly") == "true";
    var $select = $(element).selectize({
        plugins: ['remove_button','custom_class'],
        valueField: 'value',
        labelField: 'text'
    });
    var dom = $select[0];

    if (rd0 && dom && dom.selectize) {
        if (dom.selectize.$control_input)
            dom.selectize.$control_input.prop('readonly', true);
        if (dom.selectize.$control && dom.selectize.$control.off)
            dom.selectize.$control.off();
    }

    dom.__allSel = {};
    var selectMode = 1;
    if (field && field.attributes.userselect_mode != undefined)
        selectMode = parseInt(field.attributes.userselect_mode);
    dom.selectize.on("type", function (str) {
        if (str) {
            window.clearTimeout(dom.__typeThread); 
            dom.__typeThread = window.setTimeout(function () {
                $.post(HttpUtility.ResolveUrl('DeceServerManagerHandler?op=SuggestUsers'), { 'keyword': str, FillMode: selectMode},
                    function (data) {
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                var item = data[i];
                                if (item.value && !dom.__allSel[item.value]) {
                                    dom.__allSel[item.value] = true;
                                    dom.selectize.addOption(item);
                                }
                            }
                            dom.selectize.refreshOptions();
                        }
                    }, 'json');

            }, 300);
        }
    });
    var vl = "";
    if ($scope && $scope.formData) {
        vl = $scope.formData[field.model];
    }
    else
        vl = $(element).val();

    
    
        $.post(HttpUtility.ResolveUrl('DeceServerManagerHandler?op=UpdateSuggestDisplayNames'), {
            'clientValues': (vl ? JSON.stringify(vl.split(',')) : null),
            'appendDefaults': true,
            'AllowFillMode': selectMode,
            'SupportShowCurrentUser': window.SupportShowCurrentUser ? true : false
        },
            function (data) {
                if (data) {
                    for (var i = 0; i < data.DefaultList.length; i++) {
                        var item = data.DefaultList[i];
                        if (item.value) {
                            if (!dom.__allSel[item.value]) {
                                dom.__allSel[item.value] = true;
                                dom.selectize.addOption(item);
                            }
                        }
                    }

                    for (var i = 0; i < data.SuggestList.length; i++) {
                        var item = data.SuggestList[i];
                        if (item.value) {
                            if (!dom.__allSel[item.value]) {
                                dom.__allSel[item.value] = true;
                                dom.selectize.addOption(item);
                            }
                            dom.selectize.addItem(item.value);
                        }
                    }
                    dom.selectize.refreshItems();
                }
            }, 'json');

    
}


function addUrlOpenButton(element, field, $scope, parentElement, fieldType) {
    if ($scope && $scope.isAllowQuery)
        return;
    element.wrap("<span class='input-group ngInput'/>");
    var el2P = element.parent();
    element.addClass("noWithChange");
    element.width("83%;");
    element.width("calc(100% - 30px)");
    
    var btn1 = $('<a href=# style="float:right;margin:8px;"><i  class="fa fa-link"></i></a>');
    el2P.append(btn1);
    btn1.click(function (ev) {
        var url = $("input", $(this).parent()).val();
        if (url == null || url.length == 0)
            return;
        if (ev.ctrlKey)
            window.open(url);
        else
            document.location.href = url;
    })

}
/**
* DynamicForms - Build Forms in AngularJS From Nothing But JSON
* @version v0.0.4 - 2014-11-16
* @link http://github.com/danhunsaker/angular-dynamic-forms
* @license MIT, http://opensource.org/licenses/MIT
*/

/**
* Dynamically build an HTML form using a JSON array/object as a template.
*
* @todo Properly describe this directive.
* @param {mixed} [template] - The form template itself, as an array or object.
* @param {string} [templateUrl] - The URL to retrieve the form template from; template overrides.
* @param {Object} ngModel - An object in the current scope where the form data should be stored.
* @example <dynamic-form template-url="form-template.js" ng-model="formData"></dynamic-form>
*/
angular.module('dynform', [])
  .directive('dynamicForm', ['$q', '$parse', '$http', '$templateCache', '$compile', '$document', '$timeout', function ($q, $parse, $http, $templateCache, $compile, $document, $timeout) {
      var supported = {
          //  Text-based elements
          'text': { element: 'input', type: 'text', editable: true, textBased: true },
          'date': { element: 'input', type: 'datetime', editable: true, textBased: true }, /*MD-1792*/
          'datetime': { element: 'input', type: 'datetime',hasTime:true,editable: true, textBased: true },
          'datetime-local': { element: 'input', type: 'datetime-local', editable: true, textBased: true },
          'email': { element: 'input', type: 'text', editable: true, textBased: true },
          'month': { element: 'input', type: 'month', editable: true, textBased: true },
          'number': { element: 'input', type: 'number', editable: true, textBased: true },
          'password': { element: 'input', type: 'password', editable: true, textBased: true },
          'search': { element: 'input', type: 'search', editable: true, textBased: true },
          'tel': { element: 'input', type: 'tel', editable: true, textBased: true },
          'textarea': { element: 'textarea', editable: true, textBased: true },
          'time': { element: 'input', type: 'time', editable: true, textBased: true },
          'url': { element: 'input', type: 'text', editable: true, textBased: true, oninit: addUrlOpenButton},
          'week': { element: 'input', type: 'week', editable: true, textBased: true },
          //  Specialized editables
          'checkbox': { element: 'input', type: 'checkbox', editable: true, textBased: false },
          'color': { element: 'input', type: 'color', editable: true, textBased: false },
          'file': { element: 'input', type: 'file', editable: true, textBased: false },
          'range': { element: 'input', type: 'range', editable: true, textBased: false },
          'select': { element: 'select', editable: true, textBased: false },
          //  Pseudo-non-editables (containers)
          'checklist': { element: 'div', editable: false, textBased: false },
          'fieldset': { element: 'fieldset', editable: false, textBased: false },
          'radio': { element: 'div', editable: false, textBased: false },
          //  Non-editables (mostly buttons)
          'button': { element: 'button', type: 'button', editable: false, textBased: false },
          'hidden': { element: 'input', type: 'hidden', editable: false, textBased: false },
          'image': { element: 'input', type: 'image', editable: false, textBased: false },
          'legend': { element: 'legend', editable: false, textBased: false },
          'reset': { element: 'button', type: 'reset', editable: false, textBased: false },
          'submit': { element: 'button', type: 'submit', editable: false, textBased: false },
          // Non-editables drawing
          'signature': { element: 'image', type: 'signature', editable: false, textBased: true },
          'drawing': { element: 'image', type: 'drawing', editable: false, textBased: true },

          /*mobile->select*/
          'user_assignselect': { element: 'input', type: 'text', editable: true, textBased: true, oninit: setMobidiUserSelect },
          'dynamic_select': { element: 'select', type: 'text', editable: true, textBased: true, oninit: setDynamicSelect }
      };
     
      
      return {
          restrict: 'E', // supports using directive as element only
          link: function ($scope, element, attrs) {
              //  Basic initialization
              var newElement = null,
                newChild = null,
                optGroups = {},
                cbAtt = '',
                foundOne = false,
                iterElem = element,
                model = null;

             
              //  Check that the required attributes are in place
              if (angular.isDefined(attrs.ngModel) && (angular.isDefined(attrs.template) || angular.isDefined(attrs.templateUrl)) && !element.hasClass('dynamic-form')) {
                  model = $parse(attrs.ngModel)($scope);
                  //  Grab the template. either from the template attribute, or from the URL in templateUrl
                  (attrs.template ? $q.when($parse(attrs.template)($scope)) :
                    $http.get(attrs.templateUrl, { cache: $templateCache }).then(function (result) {
                        return result.data;
                    })
                  ).then(function (template) {
                      var setProperty = function (obj, props, value, lastProp, buildParent) {
                          props = props.split('.');
                          lastProp = lastProp || props.pop();

                          for (var i = 0; i < props.length; i++) {
                              obj = obj[props[i]] = obj[props[i]] || {};
                          }

                          if (!buildParent) {
                              obj[lastProp] = value;
                          }
                      },
                      bracket = function (model, base) {
                          props = model.split('.');
                          return (base || props.shift()) + (props.length ? "['" + props.join("']['") + "']" : '');
                      },
                      buildFields = function (field, id) {

                          /*DECE*/
                          if (field.virtual) return;
                          /*DECE*/

                          if (String(id).charAt(0) === '$') {
                              // Don't process keys added by Angular...  See GitHub Issue #29
                              return;
                          }
                          else if (!angular.isDefined(supported[field.type]) || supported[field.type] === false) {
                              //  Unsupported.  Create SPAN with field.label as contents
                              newElement = angular.element('<span></span>');
                              if (angular.isDefined(field.label)) { angular.element(newElement).html("<span class=dynfrmLabel>" + field.label + "</span>"); }
                              angular.forEach(field, function (val, attr) {
                                  if (["label", "type"].indexOf(attr) > -1) { return; }
                                  newElement.attr(attr, val);
                              });
                              this.append(newElement);
                              newElement = null;
                          }
                          else {
                              //  Supported.  Create element (or container) according to type
                              if (!angular.isDefined(field.model)) {
                                  field.model = id;
                              }

                              /*DECE*/
                              var fieldIsTextSelect = (field.type === "select" && $scope.forceSelectize);
                              if (!fieldIsTextSelect && field.type === "select" && angular.isDefined(field.options)) {
                                  var lngthSelect = 0;
                                  for (var vKey in field.options) {
                                      if (!field.options.hasOwnProperty(vKey)) continue;
                                      lngthSelect++;
                                      if (lngthSelect > 100) break;
                                      
                                  }
                                  if (lngthSelect > 100 || (field.attributes && field.attributes.suggestMode)) { /*DECE*/
                                      fieldIsTextSelect = true;
                                  }
                              }


                              newElement = angular.element($document[0].createElement(supported[field.type].element));
                              if (angular.isDefined(supported[field.type].type)) {
                                  newElement.attr('type', supported[field.type].type);
                              }
                              $(newElement).addClass("ngInput");
                              if (field.type == "date" || field.type == "datetime" )
                                  $(newElement).attr("autocomplete", "off");
                              if (supported[field.type].hasTime)
                                  $(newElement).data("timepicker",true);

                              
                              //  Editable fields (those that can feed models)
                              if (angular.isDefined(supported[field.type].editable) && supported[field.type].editable) {
                                  newElement.attr('name', bracket(field.model));
                                  newElement.attr('ng-model', bracket(field.model, attrs.ngModel));
                                  // Build parent in case of a nested model
                                  setProperty(model, field.model, {}, null, true);

                                  if (angular.isDefined(field.readonly)) {
                                      newElement.attr('ng-readonly', field.readonly);
                                      if (field.readonly && field.type == "select")
                                          newElement.prop("disabled", true);
                                  }
                                  if (angular.isDefined(field.required)) { newElement.attr('ng-required', field.required); }
                                  if (angular.isDefined(field.val)) {
                                      setProperty(model, field.model, angular.copy(field.val));
                                      newElement.attr('value', field.val);
                                  }
                              }

                              //  Fields based on input type=text
                              if (angular.isDefined(supported[field.type].textBased) && supported[field.type].textBased) {
                                  if (angular.isDefined(field.minLength)) { newElement.attr('ng-minlength', field.minLength); }
                                  if (angular.isDefined(field.maxLength)) { newElement.attr('ng-maxlength', field.maxLength); }
                                  if (angular.isDefined(field.validate)) { newElement.attr('ng-pattern', field.validate); }
                                  if (angular.isDefined(field.placeholder)) { newElement.attr('placeholder', field.placeholder); }
                              }

                              /*DECE*/
                              if (fieldIsTextSelect)
                                  field.type = "select";
                              //  Special cases
                              if (field.type === 'number' || field.type === 'range') {
                                  if (angular.isDefined(field.minValue)) { newElement.attr('min', field.minValue); }
                                  if (angular.isDefined(field.maxValue)) { newElement.attr('max', field.maxValue); }
                                  if (angular.isDefined(field.step)) { newElement.attr('step', field.step); }
                              }
                              else if (['text', 'textarea'].indexOf(field.type) > -1) {
                                  if (angular.isDefined(field.splitBy)) { newElement.attr('ng-list', field.splitBy); }
                                  newElement.css("resize", "vertical");
                              }
                              else if (field.type === 'checkbox')
                              {

                                  if (angular.isDefined(field.isOn)) { newElement.attr('ng-true-value', field.isOn); }
                                  if (angular.isDefined(field.isOff)) { newElement.attr('ng-false-value', field.isOff); }
                                  if (angular.isDefined(field.slaveTo)) { newElement.attr('ng-checked', field.slaveTo); }
                              }
                              else if (field.type === 'checklist') {
                                  if (angular.isDefined(field.val)) {
                                      setProperty(model, field.model, angular.copy(field.val));
                                  }
                                  if (angular.isDefined(field.options)) {
                                      if (!(angular.isDefined(model[field.model]) && angular.isObject(model[field.model]))) {
                                          setProperty(model, field.model, {});
                                      }
                                      angular.forEach(field.options, function (option, childId) {

                                          newChild = angular.element('<input type="checkbox" />');
                                          newChild.attr('name', bracket(field.model + '.' + childId));
                                          newChild.attr('ng-model', bracket(field.model + "." + childId, attrs.ngModel));
                                          if (angular.isDefined(option['class'])) { newChild.attr('ng-class', option['class']); }
                                          if (angular.isDefined(field.disabled)) { newChild.attr('ng-disabled', field.disabled); }
                                          if (angular.isDefined(field.readonly)) { newChild.attr('ng-readonly', field.readonly); }
                                          if (angular.isDefined(field.required)) { newChild.attr('ng-required', field.required); }
                                          if (angular.isDefined(field.callback)) { newChild.attr('ng-change', field.callback); }
                                          if (angular.isDefined(option.isOn)) { newChild.attr('ng-true-value', option.isOn); }
                                          if (angular.isDefined(option.isOff)) { newChild.attr('ng-false-value', option.isOff); }
                                          if (angular.isDefined(option.slaveTo)) { newChild.attr('ng-checked', option.slaveTo); }
                                          if (angular.isDefined(option.val)) {
                                              setProperty(model, field.model, angular.copy(option.val), childId);
                                              newChild.attr('value', option.val);
                                          }


                                          if (angular.isDefined(option.label)) {

                                              newChild = newChild.wrap('<label></label>').parent();
                                              newChild.append(document.createTextNode(' ' + option.label));
                                          }
                                          newElement.append(newChild);
                                      });
                                  }
                              }
                              else if (field.type === 'radio') {
                                  if (angular.isDefined(field.val)) {
                                      setProperty(model, field.model, angular.copy(field.val));
                                  }
                                  if (angular.isDefined(field.values)) {
                                      angular.forEach(field.values, function (label, val) {
                                          newChild = angular.element('<input type="radio" />');
                                          newChild.attr('name', bracket(field.model));
                                          newChild.attr('ng-model', bracket(field.model, attrs.ngModel));
                                          if (angular.isDefined(field['class'])) { newChild.attr('ng-class', field['class']); }
                                          if (angular.isDefined(field.disabled)) { newChild.attr('ng-disabled', field.disabled); }
                                          if (angular.isDefined(field.callback)) { newChild.attr('ng-change', field.callback); }
                                          if (angular.isDefined(field.readonly)) { newChild.attr('ng-readonly', field.readonly); }
                                          if (angular.isDefined(field.required)) { newChild.attr('ng-required', field.required); }
                                          newChild.attr('value', val);
                                          if (angular.isDefined(field.val) && field.val === val) { newChild.attr('checked', 'checked'); }
                                          if (field.cStyle) {
                                              newChild.attr("style", field.cStyle);
                                          }
                                          if (label) {
                                              newChild = newChild.wrap('<label></label>').parent();
                                              newChild.append(document.createTextNode(' ' + label));
                                              newChild = newChild.wrap('<div class="radio"></<div>').parent();
                                          }

                                          newElement.append(newChild);
                                      });
                                  }
                              }
                              else if (field.type === 'select') {



                                  if (angular.isDefined(field.multiple) && field.multiple !== false) { newElement.attr('multiple', 'multiple'); }
                                  if (angular.isDefined(field.empty) && field.empty !== false) { newElement.append(angular.element($document[0].createElement('option')).attr('value', '').html(field.empty)); }

                                  if (angular.isDefined(field.autoOptions)) {
                                      newElement.attr('ng-options', field.autoOptions);
                                  }
                                  else if (angular.isDefined(field.options)) {

                                      angular.forEach(field.options, function (option, childId) {
                                          newChild = angular.element($document[0].createElement('option'));
                                          newChild.attr('value', childId);

                                          if (angular.isDefined(option.disabled)) { newChild.attr('ng-disabled', option.disabled); }
                                          if (angular.isDefined(option.slaveTo)) { newChild.attr('ng-selected', option.slaveTo); }
                                          if (angular.isDefined(option.label)) { newChild.html(option.label); }
                                          if (angular.isDefined(option.group)) {
                                              if (!angular.isDefined(optGroups[option.group])) {
                                                  optGroups[option.group] = angular.element($document[0].createElement('optgroup'));
                                                  optGroups[option.group].attr('label', option.group);
                                              }
                                              optGroups[option.group].append(newChild);
                                          }
                                          else {
                                              newElement.append(newChild);
                                          }

                                      });

                                      if (!angular.equals(optGroups, {})) {
                                          angular.forEach(optGroups, function (optGroup) {
                                              newElement.append(optGroup);
                                          });
                                          optGroups = {};
                                      }
                                  }

                              }
                              else if (field.type === 'image') {
                                  if (angular.isDefined(field.label)) { newElement.attr('alt', field.label); }
                                  if (angular.isDefined(field.source)) { newElement.attr('src', field.source); }
                              }
                              else if (field.type === 'hidden') {
                                  newElement.attr('name', bracket(field.model));
                                  newElement.attr('ng-model', bracket(field.model, attrs.ngModel));
                                  if (angular.isDefined(field.val)) {
                                      setProperty(model, field.model, angular.copy(field.val));
                                      newElement.attr('value', field.val);
                                  }
                              }
                              else if (field.type === 'file') {
                                  if (angular.isDefined(field.multiple)) {
                                      newElement.attr('multiple', field.multiple);
                                  }
                              }
                              else if (field.type === 'fieldset') {
                                  if (angular.isDefined(field.fields)) {


                                      var workingElement = newElement;
                                      angular.forEach(field.fields, buildFields, newElement);
                                      var dvContainer = $("<div></div>");
                                      dvContainer.append(workingElement);
                                      workingElement = dvContainer
                                      newElement = workingElement;

                                  }
                              }
                              else if (field.type === 'signature' || field.type === 'drawing')
                              {
                                  newElement = $("<img/>", { src: field.val });
                                  $(newElement).addClass("ngInput");
                                  $(newElement).addClass("ngImgInput");
                                  let imEl = newElement;
                                  $(imEl).click(function () {
                                      var sb = imEl.clone();
                                      if (!sb.attr("src"))
                                          return;
                                      sb.removeClass("ngImgInput").addClass("ngImgInputPopup");
                                      showMessage($('<div>').append(sb).html(), $(".dynfrmLabel", imEl.parent()).html());
                                  });
                              }
                              
                              if (field.style) {
                                  newElement.attr("style", field.style);
                              }
                              //  Common attributes; radio already applied these...
                              if (field.type !== "radio") {
                                  if (angular.isDefined(field['class'])) { newElement.attr('ng-class', field['class']); }
                                  //  ...and checklist has already applied these.
                                  if (field.type !== "checklist") {
                                      if (angular.isDefined(field.disabled)) { newElement.attr('ng-disabled', field.disabled); }
                                      if (angular.isDefined(field.callback)) {
                                          //  Some input types need listeners on click...
                                          if (["button", "fieldset", "image", "legend", "reset", "submit"].indexOf(field.type) > -1) {
                                              cbAtt = 'ng-click';
                                          }
                                              //  ...the rest on change.
                                          else {
                                              cbAtt = 'ng-change';
                                          }
                                          newElement.attr(cbAtt, field.callback);
                                      }
                                  }
                              }

                              //  If there's a label, add it.
                              if (angular.isDefined(field.label)) {
                                  //  Some elements have already applied their labels.
                                  if (["image", "hidden"].indexOf(field.type) > -1) {
                                      angular.noop();
                                  }
                                      //  Fieldset elements put their labels in legend child elements.
                                  else if (["fieldset"].indexOf(field.type) > -1) {
                                      newElement.prepend(angular.element($document[0].createElement('legend')).html("<span class=dynfrmLabel>"+field.label+"</span>"));
                                  }
                                      //  Button elements get their labels from their contents.
                                  else if (["button", "legend", "reset", "submit"].indexOf(field.type) > -1) {
                                      if (field.report) {
                                          newElement = newElement.wrap('<div style="margin-bottom: 5px;"></div>').parent();
                                          var $label = angular.element("<label/>", { html: field.label, class: "dynfrmLabel" });
                                          angular.element(newElement).prepend($label);
                                          angular.element(newElement).find("button").attr("onclick", field.attributes.onclick).addClass(field.attributes.class).html(field.label);
                                         
                                          field.attributes.class = null;
                                          field.attributes.onclick = null;
                                          if (field.report.come_open) {
                                              angular.element(newElement).find("button").remove();
                                              var url0 = field.report.url;
                                              var wid = null;
                                              if (url0 != null) {
                                                  if (!window.___dyn_iframeid)
                                                      window.___dyn_iframeid = 0;
                                                  window.___dyn_iframeid++;
                                                  wid = "dynIfrm" + window.___dyn_iframeid;
                                                  url0 += (url0.indexOf("?") == -1 ? "?" : "&") + "frameID=" + wid;
                                              }
                                              var $iframe = angular.element("<iframe/>", { src: url0, class: "reportFrame" });
                                              $iframe.attr("id", wid);
                                              angular.element(newElement).append($iframe);
                                              var $loading = angular.element("<div/>", { class: "reportLoading", style: "left: " + (($("#data-attributes").width() / 2) - 24) + "px; bottom: 173px" });
                                              angular.element(newElement).append($loading);
                                              
                                              $iframe[0].onload = function () {
                                                  angular.element($iframe[0].contentDocument).find("body").css({"margin": "0", "padding": "0"});
                                                  angular.element($iframe[0].contentDocument).find("#All").css("padding", "0");
                                                  angular.element($iframe[0].contentDocument).find("#All .rpBlockFull:eq(0)").css("margin", "0");
                                                  $loading.remove();
                                              };
                                             
                                          }
                                         
                                      }
                                      else
                                        newElement.html(field.label);
                                  }
                                  else {

                                      /*DECE*/
                                      var $label = '<label></label>';
                                      if (field.labelStyle) {
                                          $label = '<label style="' + field.labelStyle + '"></label>';
                                      }
                                      if ($scope.isAllowQuery && !field.virtual) {
                                          var dElem = null;
                                          if ((field.type === 'datetime' || field.type === 'date') && !$scope.disableRangeFieldCreate) {
                                              newElement.attr('class', 'col-xs-6');
                                              dElem = newElement.clone();
                                              if (supported[field.type].hasTime)
                                                  $(dElem).data("timepicker", true);
                                              dElem.attr('ng-model', "formData['" + field.model + "_v'" + "]");
                                              dElem.attr('name', field.model + "_v");

                                              var $div = $('<div/>', { style: "padding: 0px;" });
                                              if (!$scope.previewMode) 
                                                  $div.attr("class", "col-xs-9");
                                              
                                              newElement = newElement.wrap($div).parent();

                                              //var betweenElem = $("<div class='col-xs-2'></div>");

                                              if (dElem) {
                                                  //newElement.append(betweenElem);
                                                  newElement.append(dElem);
                                              }


                                          }
                                          else 
                                              newElement.attr('class', 'col-xs-9 form-value-single ngInput');
                                          

                                          newElement = newElement.wrap($label).parent();

                                          var newEl = $("<div class='control-label col-xs-3' style='padding-left:0px;'>" + "<span class=dynfrmLabel>" + field.label + "</span>" + "</div>");
                                          newElement.prepend(newEl);
                                      }
                                          /*DECE*/

                                          //  Everything else should be wrapped in a label tag.
                                      else {
                                          newElement = newElement.wrap($label).parent();
                                         
                                          /*DECE*/
                                        
                                          var sp = $("<span/>", { class: "dynfrmLabel", html: field.label, title: field.label });
                                          if (field.spanClass)
                                          {
                                              var dataArr = field.spanClass.split(' ');
                                              dataArr.forEach(function (cls) {
                                                  sp.addClass(cls);
                                              });
                                          }
                                          
                                          newElement.prepend(sp);
                                      }
                                  }
                              }

                              // Arbitrary attributes
                              if (angular.isDefined(field.attributes)) {
                                  angular.forEach(field.attributes, function (val, attr) {
                                      newElement.attr(attr, val);
                                  });
                              }


                             
                              // Add the element to the page
                              this.append(newElement);

                              var endElement = $(".ngInput", newElement);
                              if (supported[field.type].oninit)
                                  supported[field.type].oninit(endElement, field, $scope, newElement, supported[field.type]);
                              else if (fieldIsTextSelect)
                                  setToSelectize(endElement, field, $scope, newElement, supported[field.type]);
                              if (window.dynamicElementOnInit)
                                  window.dynamicElementOnInit(endElement, field, $scope,newElement, supported[field.type]);

                              newElement = null;
                          }
                      };

                      angular.forEach(template, buildFields, element);

                      //  Determine what tag name to use (ng-form if nested; form if outermost)

                      while (!angular.equals(iterElem.parent(), $document) && (iterElem[0].innerHTML !== $document[0].documentElement.innerHTML)) {
                          if (['form', 'ngForm', 'dynamicForm'].indexOf(attrs.$normalize(angular.lowercase(iterElem.parent()[0].nodeName))) > -1) {
                              foundOne = true;
                              break;
                          }
                          iterElem = iterElem.parent();
                      }
                      if (foundOne) {
                          newElement = angular.element($document[0].createElement('ng-form'));
                      }
                      else {
                          newElement = angular.element("<form></form>");
                      }

                      //  Psuedo-transclusion
                      angular.forEach(attrs.$attr, function (attName, attIndex) {
                          newElement.attr(attName, attrs[attIndex]);
                      });
                      newElement.attr('model', attrs.ngModel);
                      newElement.removeAttr('ng-model');
                      angular.forEach(element[0].classList, function (clsName) {
                          newElement[0].classList.add(clsName);
                      });
                      
                      newElement.addClass('dynamic-form');
                      newElement.append(element.contents());

                      //  onReset logic
                      newElement.data('$_cleanModel', angular.copy(model));
                      newElement.bind('reset', function () {
                          $timeout(function () {
                              $scope.$broadcast('reset', arguments);
                          }, 0);
                      });
                      $scope.$on('reset', function () {
                          $scope.$apply(function () {
                              $scope[attrs.ngModel] = {};
                          });
                          $scope.$apply(function () {
                              $scope[attrs.ngModel] = angular.copy(newElement.data('$_cleanModel'));
                          });
                      });
                    
                     
                      //  Compile and update DOM
                      $compile(newElement)($scope);
                      element.replaceWith(newElement);
                  });
              }
          }
      };
  }])
  //  Not a fan of how Angular's ngList is implemented, so here's a better one (IMO).  It will ONLY
  //  apply to <dynamic-form> child elements, and replaces the ngList that ships with Angular.
  .directive('ngList', [function () {
      return {
          require: '?ngModel',
          link: function (scope, element, attr, ctrl) {
              var match = /\/(.*)\//.exec(element.attr(attr.$attr.ngList)),
                separator = match && new RegExp(match[1]) || element.attr(attr.$attr.ngList) || ',';

              if (element[0].form !== null && !angular.element(element[0].form).hasClass('dynamic-form')) {
                  return;
              }

              ctrl.$parsers.splice(0, 1);
              ctrl.$formatters.splice(0, 1);

              ctrl.$parsers.push(function (viewValue) {
                  var list = [];

                  if (angular.isString(viewValue)) {
                      //  Don't have Angular's trim() exposed, so let's simulate it:
                      if (String.prototype.trim) {
                          angular.forEach(viewValue.split(separator), function (value) {
                              if (value) list.push(value.trim());
                          });
                      }
                      else {
                          angular.forEach(viewValue.split(separator), function (value) {
                              if (value) list.push(value.replace(/^\s*/, '').replace(/\s*$/, ''));
                          });
                      }
                  }

                  return list;
              });

              ctrl.$formatters.push(function (val) {
                  var joinBy = angular.isString(separator) && separator || ', ';

                  if (angular.isArray(val)) {
                      return val.join(joinBy);
                  }

                  return undefined;
              });
          }
      };
  }])
  //  Following code was adapted from http://odetocode.com/blogs/scott/archive/2013/07/05/a-file-input-directive-for-angularjs.aspx
  .directive('input', ['$parse', function ($parse) {
      return {
          restrict: 'E',
          require: '?ngModel',
          link: function (scope, element, attrs, ctrl) {
              if (!ctrl) {
                  // Doesn't have an ng-model attribute; nothing to do here.
                  return;
              }
              if (attrs.type === 'file') {
                  var modelGet = $parse(attrs.ngModel),
                    modelSet = modelGet.assign,
                    onChange = $parse(attrs.onChange),
                    updateModel = function () {
                        scope.$apply(function () {
                            modelSet(scope, element[0].files);
                            onChange(scope);
                        });
                    };

                  ctrl.$render = function () {
                      element[0].files = this.$viewValue;
                  };
                  element.bind('change', updateModel);
              }
              else if (attrs.type === 'range') {
                  ctrl.$parsers.push(function (val) {
                      if (val) {
                          return parseFloat(val);
                      }
                  });
              }
          }
      };
  }])
  //  Following code was adapted from http://odetocode.com/blogs/scott/archive/2013/07/03/building-a-filereader-service-for-angularjs-the-service.aspx
  .factory('fileReader', ['$q', function ($q) {
      var onLoad = function (reader, deferred, scope) {
          return function () {
              scope.$apply(function () {
                  deferred.resolve(reader.result);
              });
          };
      },
        onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.error);
                });
            };
        },
        onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast('fileProgress',
                  {
                      total: event.total,
                      loaded: event.loaded,
                      status: reader.readyState
                  });
            };
        },
        getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };

      return {
          readAsArrayBuffer: function (file, scope) {
              var deferred = $q.defer(),
                reader = getReader(deferred, scope);
              reader.readAsArrayBuffer(file);
              return deferred.promise;
          },
          readAsBinaryString: function (file, scope) {
              var deferred = $q.defer(),
                reader = getReader(deferred, scope);
              reader.readAsBinaryString(file);
              return deferred.promise;
          },
          readAsDataURL: function (file, scope) {
              var deferred = $q.defer(),
                reader = getReader(deferred, scope);
              reader.readAsDataURL(file);
              return deferred.promise;
          },
          readAsText: function (file, scope) {
              var deferred = $q.defer(),
                reader = getReader(deferred, scope);
              reader.readAsText(file);
              return deferred.promise;
          }
      };
  }]);

/*  End of dynamic-forms.js */

function generateDynamicForm(options) {
    if (options.elements) {
        var tDom = $(options.targetDom);
        
        tDom.attr("ng-app", "MobidiApp");
        tDom.attr("ng-controller", "formCtrl" + options.controllerId);
        var html = '<dynamic-form template="formTemplate" ng-model="formData">' +
            '</dynamic-form>';
        tDom.append(html);
        tDom.append("<div class='frmData' ng-click='reloadData()'>{{ formData }}</div>");
        angular.module('MobidiApp', ['dynform']).controller('formCtrl' + options.controllerId, function ($scope, $compile) {
            $scope.windowObj = window;
            $scope.evalHelper = {
                Eval: function (str) {
                    return eval(str);
                }
            }
            $scope.isAllowQuery = options.isQuery ? true : false;
            $scope.disableRangeFieldCreate = options.disableRangeFieldCreate ? true : false;
            $scope.forceSelectize = options.forceSelectize ? true : false;
            $scope.formTemplate = options.elementsUpdater ? options.elementsUpdater(options.elements, options.controllerId) : options.elements;
            var _inUpdateFormData = function (parsed) {
                for (key in parsed) {
                    var val = parsed[key];
                    var inElement = options.elements ? options.elements.filterAll(function (f) { return f.model == key }) : null;
                    if (inElement && inElement.length > 0) inElement = inElement[0];
                    if (inElement && inElement.multiple && inElement.type == "select") {
                        if (val)
                            parsed[key] = val.split(',');
                        else
                            parsed[key] = null;
                    }
                }
                return parsed;
            }

            $scope.formData = options.formDataGetter ? _inUpdateFormData(options.formDataGetter()) : {};
            $scope.reloadData = function () {
                $scope.formData = options.formDataGetter ? _inUpdateFormData(options.formDataGetter()) : {};
            }

        });
        angular.bootstrap(tDom, ['MobidiApp']);
        //angular.element(document).ready(function () {
        //    angular.bootstrap(document, ['MobidiApp']);
        //});
        var dateFormatStr = 'mm.dd.yyyy';
        if (window.DeceClientState && window.DeceClientState.GetLanguage().split('-')[0] == "tr") 
            dateFormatStr = 'dd.mm.yyyy'
        $("input[type=datetime]").each(function (index) {
            var item = $(this);
            if (item.datepicker) {
                item.attr("data-language", "current");
                item.datepicker({
                    position: 'bottom center',
                    dateFormat: dateFormatStr,
                    timeFormat: "hh:ii",
                    autoClose: true,
                    onSelect: function (formattedDate, date, inst) {
                        $("input[type=datetime]").change();
                    }
                });
                var vlDate = item.val();
                if (vlDate && !isRelativeDate(vlDate)) {
                    var dateTemp = new Date(item.val());
                    item.datepicker().data('datepicker').date = dateTemp;
                    item.datepicker().data('datepicker').selectDate(dateTemp);
                }
            }
        });
    }

}