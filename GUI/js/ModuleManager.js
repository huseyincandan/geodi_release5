﻿$(function () {
    if (window.DeceClientState && typeof moment !== 'undefined') moment.locale(window.DeceClientState.GetLanguage());
})
function getSuffix(id) {
    return id.replace('{', '').replace('}', '').replace('-', '');
}

function check_eraseSearchTxtDiv() {
    var elErase = $('#eraseSearchTxtDiv');
    if ($("#searchTxt").val()) {
        if (!elErase.is(":visible"))
            elErase.fadeIn();
    }
    else {
        if (elErase.is(":visible"))
            elErase.fadeOut();
    }
}
function clearSearchTxt() {
    $("#searchTxt").val('');
    Run();
}


function Run(startIndx) {
    if (window._RunTimeout) window.clearTimeout(window._RunTimeout);
    getInstalledModulesForRender(
        function (installedModulesForRender) {
            if (window._RunTimeout) window.clearTimeout(window._RunTimeout);
            var searchTxt = $("#searchTxt").val();
            var installedModuleExists;
            if (!startIndx) {
                delete window.__CanShowList;
                $("#response").empty();
                $("#loading").fadeIn();

                installedModulesForRender = filterModules(installedModulesForRender, searchTxt);
                if (installedModulesForRender.length > 0) {
                    var installedHtml = TemplateToHtml($("#template1"), installedModulesForRender, {});
                    $("#response").append($(installedHtml));
                    installedModuleExists = true;
                }
            }

            var q = new GeodiQuery();
            q.wsName = GetWsName();
            q.ServerUrl = GetServerName();
            q.Token = GetToken();

            if (!startIndx) {
                q.StartIndex = 0;
                delete window._endIndex;
            }
            else
                q.StartIndex = startIndx;

            q.EndIndex = window._endIndex = q.StartIndex + 6;

            q.SearchString = searchTxt;
            q.FillOptions = 524288;
            q.SearchStringsHiden = [GetSearchStringsHiden()];
            q.UseRank = true;
            q.OnAjaxError = function (event, data, settings, argd, callBackFunction) {
                settings.RunCallBack = true;
                return false;
            }
            if (!HttpUtility.ResolveUrl)
                HttpUtility.ResolveUrl = q.ResolveUrl;
            RenderDLV(
                {
                    Query: q,
                    QueryOptions: {
                        languagetemp: window.DeceClientState?window.DeceClientState.GetLanguage():null
                    },
                    DisableSummary: true,
                    TargetDiv: "#response",
                    Template: $("#template1").html(),
                    OnEndRender: function (args) {
                        if (args.Data && args.Data.length)
                            checkScroll();
                        else
                            $("#loading").fadeOut();

                        initStatus();
                        getStatus();

                        if (installedModuleExists && (!args.Data || !args.Data.length)) {
                            var el = $(".norecord");
                            if (el.is(":visible"))
                                el.hide();
                        }
                    }
                }
            )
            // html1 += TemplateToHtml($("#template_opt"), lstOpt, { All: modules, GetModuleIconName: GetModuleIconName, GetModuleKey: GetModuleKey, Items: items, ItemsCount: itemsCount, Page: page });
        });
    initStatus();
}
function filterModules(modules, searchTxt) {
    if (!searchTxt)
        return modules;
    var arr = [];
    searchTxt = searchTxt.toLocaleLowerCase();

    if (searchTxt[0] !== '*')
        searchTxt = '*' + searchTxt;
    if (searchTxt[searchTxt.length] !== '*')
        searchTxt = searchTxt + '*';

    for (var i = 0; i < modules.length; i++) {
        var mod = modules[i];
        var txt = (mod.JSONData.Description + " " + mod.JSONData.ModuleName).toLocaleLowerCase();
        if (MatchRule(txt, searchTxt))
            arr.push(mod);
    }
    return arr;
}

$(window).ready(function () {
    $('#searchTxt').keypress(function (e) {
        if (e.which == 13) {
            Run();
            return false;
        }
    });
    $('#searchTxt').on('input propertychange paste', function () {
        if (window._RunTimeout) window.clearTimeout(window._RunTimeout);
        window._RunTimeout = window.setTimeout(
            function () {
                Run();
            }, 500);
        check_eraseSearchTxtDiv();
    });
    $(window).scroll(function () {
        checkScroll();
    });

    getStatusID();
    Run();
    window._statusIdInterval = window.setInterval(function () { getStatusID(); }, 5000);
});
function checkScroll() {
    var el = $("#loading");
    if (el.is(":visible") && isScrolledIntoView(el))
        Run(window._endIndex);
}
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height() - 10;

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

function uninstallModule(moduleId) {
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=UninstallModuleAsync'), { 'moduleId': moduleId },
        function (ok) {
            initStatus();
            getStatus();
        });
    initStatus();
}
function installModule(dataContentIdentifier) {
    var downloadUrl = GetServerName() + "/GeodiJSONService?op=downloadDoc&FromBackup=1&wsName=" + GetWsName() + "&UserSession=" + HttpUtility.UrlEncode(GetToken()) + "&content_id=" + dataContentIdentifier;
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=InstallOrUpdateModuleAsync'), { 'moduleXUrl': downloadUrl },
        function (ok) {
            initStatus();
            getStatus();
        });
    initStatus();
}

function canShow(data) {
    if (!window.__CanShowList) window.__CanShowList = {};
    if (window.__CanShowList[data.JSONData.ID]) return false;
    window.__CanShowList[data.JSONData.ID] = true;
    return !data.JSONData.Hidden;
}
function canInstall(data) {
    var module = data.JSONData;
    if (!IsLicenseOK(module))
        return false;
    return IsVersionCompatibleWithMainModule(module);
}


function getInstalledModulesForRender(callback) {
    if (window.__installedModulesForRender) {
        callback(window.__installedModulesForRender);
        return;
    }

    var dic = GetInstalledModulesDic();

    var q = new GeodiQuery();
    q.wsName = GetWsName();
    q.ServerUrl = GetServerName();
    q.Token = GetToken();
    q.SearchString = "";
    q.SearchStringsHiden = [GetSearchStringsHiden()];
    q.EndIndex = 100000;
    q.OnAjaxError = function (event, data, settings, argd, callBackFunction) {
        settings.RunCallBack = true;
        return false;
    }

    $.each(dic, function (k, v) {
        q.SearchString += " ID:(" + k + ") or ";
    })
    if (q.SearchString) q.SearchString = q.SearchString.substr(0, q.SearchString.length - 4);

    var id = -1;
    q.GetDocuments2(function (data) {
        for (var i = 0; i < data.length; i++) {
            var doc = data[i];
            doc.JSONData = JSON.parse(doc.AdditionalValues.FCD_JsonData);
            if (dic[doc.JSONData.ID])
                dic[doc.JSONData.ID].ServerData = doc;
        }
        var alldata = [];
        $.each(dic, function (k, v) {
            var nw = v.ServerData;
            var iconUrl = HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetIcon&moduleID=' + k);
            if (!nw) {
                nw = {
                    AdditionalValues: {
                        //ContentDate: { UTC: "2019-08-19T13:28:07.000Z" },
                        ContentSize: 1,
                        ContentTypeKey: GetContentTypeKey(),
                        //Date: "2019-08-19T13:28:07.966Z",
                        DisplayName: v.ModuleName,
                        FCD_Icon: iconUrl,
                        ID: k
                    },
                    ContentType: GetContentTypeKey(),
                    ContentIdentifier: --id,
                    DisplayName: v.ModuleName,
                    JSONData: v
                };
            }
            else {
                if (!nw.AdditionalValues.FCD_Icon)
                    nw.AdditionalValues.FCD_Icon = iconUrl;
                if (!nw.AdditionalValues.JSONData)
                    nw.AdditionalValues.JSONData = v;
            }
            nw.GetDocItemAttr = function () { return "" };
            nw.GetHrefFull = function () { return "" };

            nw.GetIcon = function () { return this.AdditionalValues.FCD_Icon; };
            nw.IsInstalledModule = true;

            alldata.push(nw);
        });

        window.__installedModulesForRender = alldata;
        callback(window.__installedModulesForRender);
    },
        {
            languagetemp: window.DeceClientState ? window.DeceClientState.GetLanguage() : null
        }
    )
}

function getIcon(data) {
    var ico;
    if (data.GetIcon)
        ico = data.GetIcon();
    if (!ico)
        ico = HttpUtility.ResolveUrl("~/GUI/img/module.png");      
    return ico;
}

function initStatus() {
    delete window._lastStatusID;
}

function getStatusID() {
    var url = HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetStatusID');
    $.post(url,
        function (id) {
            if (window._lastStatusID != id) {
                window._lastStatusID = id;

                if (window._statusIdInterval) {
                    window.clearInterval(window._statusIdInterval);
                    delete window._statusIdInterval;
                }
                getStatus();
                window._statusTimeout = window.setTimeout(function () { getStatus(); }, 2000);
            }
        });
}
function getStatus() {
    if (window._freezeUpdateModulesStatus)
        return;
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetStatus'),
        function (status) {
            if (window._freezeUpdateModulesStatus)
                return;
            var continueGetStatus;
            if (status && status.length == 2) {

                var id = status[0];
                if (window._lastStatusID != id) {
                    window._lastStatusID = id;
                    continueGetStatus = true;
                }

                var downloadValue = 0;
                var downloadTotal = 0;

                var items = status[1];
                if (items && items.length) {
                    for (var i = 0; i < items.length; i++) {
                        var stat = items[i];
                        var modID = stat.ID;
                        var suffix = getSuffix(modID);

                        var elDownloading = $("#downloading" + suffix);
                        var elDownloadFinished = $("#downloadFinished" + suffix);
                        var elUninstalled = $("#uninstalled" + suffix);
                        var elCancelled = $("#cancelled" + suffix);
                        var elError = $("#error" + suffix);

                        var elBtnInstall = $("#btnInstall" + suffix);
                        var elBtnUninstall = $("#btnUninstall" + suffix);

                        switch (stat.StType)//None = 0, Downloading = 1, DownloadFinished = 2, Uninstalled = 3, Cancelled= 4, Error = 5
                        {
                            case 0://none
                                if (elDownloading.is(":visible")) elDownloading.fadeOut();
                                if (elDownloadFinished.is(":visible")) elDownloadFinished.fadeOut();
                                if (elUninstalled.is(":visible")) elUninstalled.fadeOut();
                                if (elCancelled.is(":visible")) elCancelled.fadeOut();
                                if (elError.is(":visible")) elError.fadeOut();

                                if (!elBtnInstall.is(":visible")) elBtnInstall.fadeIn();
                                if (!elBtnUninstall.is(":visible")) elBtnUninstall.fadeIn();
                                break;
                            case 1://downloading
                                if (elDownloadFinished.is(":visible")) elDownloadFinished.fadeOut();
                                if (elUninstalled.is(":visible")) elUninstalled.fadeOut();
                                if (elCancelled.is(":visible")) elCancelled.fadeOut();
                                if (elError.is(":visible")) elError.fadeOut();
                                if (!elDownloading.is(":visible")) elDownloading.fadeIn();

                                if (elBtnInstall.is(":visible")) elBtnInstall.fadeOut();
                                if (elBtnUninstall.is(":visible")) elBtnUninstall.fadeOut();

                                var elDownloadingTxt = $("#downloadingTxt" + suffix);

                                continueGetStatus = true;
                                var statusItem = stat.Details;
                                var percent;
                                if (statusItem.PercentStatus) {
                                    percent = statusItem.PercentStatus.TotalValue / statusItem.PercentStatus.Count * 100;

                                    downloadValue += statusItem.PercentStatus.TotalValue;
                                    downloadTotal += statusItem.PercentStatus.Count;
                                }

                                elDownloadingTxt.html(statusItem.DisplayName);

                                var pgEl = $('.pb' + suffix);
                                if (percent) {
                                    if (!pgEl.is(":visible"))
                                        pgEl.fadeIn();
                                    pgEl.css("width", percent + '%');
                                    progressExists = true;
                                }
                                break;
                            case 2://downloadFinished
                                if (elDownloading.is(":visible")) elDownloading.fadeOut();
                                if (elUninstalled.is(":visible")) elUninstalled.fadeOut();
                                if (elCancelled.is(":visible")) elCancelled.fadeOut();
                                if (elError.is(":visible")) elError.fadeOut();
                                if (!elDownloadFinished.is(":visible")) {
                                    elDownloadFinished.fadeIn();
                                    if (stat.Details == 1) {
                                        $("#downloadFinished_resetRequired" + suffix).fadeIn();
                                        window._resetRequired = true;
                                    }
                                    else
                                        $("#downloadFinished_resetNotRequired" + suffix).fadeIn();
                                }

                                if (elBtnInstall.is(":visible")) elBtnInstall.fadeOut();
                                if (elBtnUninstall.is(":visible")) elBtnUninstall.fadeOut();
                                break;

                            case 3://uninstalled
                                if (elDownloading.is(":visible")) elDownloading.fadeOut();
                                if (elDownloadFinished.is(":visible")) elDownloadFinished.fadeOut();
                                if (elCancelled.is(":visible")) elCancelled.fadeOut();
                                if (elError.is(":visible")) elError.fadeOut();
                                if (!elUninstalled.is(":visible")) {
                                    elUninstalled.fadeIn();
                                    if (stat.Details == 1) {
                                        $("#uninstalled_resetRequired" + suffix).fadeIn();
                                        window._resetRequired = true;
                                    }
                                    else
                                        $("#uninstalled_resetNotRequired" + suffix).fadeIn();
                                }

                                if (elBtnInstall.is(":visible")) elBtnInstall.fadeOut();
                                if (elBtnUninstall.is(":visible")) elBtnUninstall.fadeOut();
                                break;

                            case 4://cancelled
                                if (elDownloading.is(":visible")) elDownloading.fadeOut();
                                if (elDownloadFinished.is(":visible")) elDownloadFinished.fadeOut();
                                if (elUninstalled.is(":visible")) elUninstalled.fadeOut();
                                if (elError.is(":visible")) elError.fadeOut();
                                if (!elCancelled.is(":visible")) {
                                    elCancelled.fadeIn();
                                    if (stat.Details == 1) {
                                        $("#cancelled_resetRequired" + suffix).fadeIn();
                                        window._resetRequired = true;
                                    }
                                    else
                                        $("#cancelled_resetNotRequired" + suffix).fadeIn();
                                }

                                if (!elBtnInstall.is(":visible")) elBtnInstall.fadeIn();
                                if (!elBtnUninstall.is(":visible")) elBtnUninstall.fadeIn();
                                break;

                            case 5://error
                                if (elDownloading.is(":visible")) elDownloading.fadeOut();
                                if (elDownloadFinished.is(":visible")) elDownloadFinished.fadeOut();
                                if (elUninstalled.is(":visible")) elUninstalled.fadeOut();
                                if (elCancelled.is(":visible")) elCancelled.fadeOut();
                                if (!elError.is(":visible")) {
                                    elError.fadeIn();
                                    $('#errorMessage' + suffix).html(stat.Details);
                                }

                                if (!elBtnInstall.is(":visible")) elBtnInstall.fadeIn();
                                if (!elBtnUninstall.is(":visible")) elBtnUninstall.fadeIn();
                                break;
                        }
                    }
                }

                if (downloadTotal) {
                    updateResetAppBtn(false);
                    var percent = downloadValue * 100.0 / downloadTotal;
                    var el = $('#downloadingMain');
                    if (!el.is(":visible")) {
                        el.fadeIn();
                        $('.main').css('padding-bottom', el.css('height'));
                    }
                    $('.pgAll').css("width", percent + '%');

                    var toBeDownloaded = downloadTotal - downloadValue;
                    $('#progressTotalMsg').html("[$ModuleExtract:downloaded/js]: " + HumanFileSize(downloadValue) + " - " + "[$ModuleExtract:remaining/js]: " + HumanFileSize(toBeDownloaded));
                }
                else {
                    $('#progressTotalMsg').html("");

                    var el = $('#downloadingMain');
                    if (el.is(":visible")) {
                        el.fadeOut();

                        if (!$('#restartAppBtn').is(':visible')) {
                            $('.main').css('padding-bottom', '0px');
                        }
                    }
                    updateResetAppBtn(window._resetRequired);
                }
            }

            if (window._statusIdInterval) {
                window.clearInterval(window._statusIdInterval);
                delete window._statusIdInterval;
            }
            window.clearTimeout(window._statusTimeout);

            if (continueGetStatus) {
                window._statusTimeout = window.setTimeout(function () { getStatus(); }, 2000);
            }
            else {
                window._statusIdInterval = window.setInterval(function () { getStatusID(); }, 5000);
            }
        });
}

function updateResetAppBtn(showResetAppButton) {
    var el = $('#restartAppBtn');
    var visible = el.is(':visible');
    if (!showResetAppButton) showResetAppButton = false;
    if (visible != showResetAppButton) {
        if (visible)
            el.fadeOut();
        else {
            CanRestartApp(function (ok) {
                if (ok) {
                    el.fadeIn();
                    $('.main').css('padding-bottom', el.css('height'));
                }
            });
        }
    }
}


function onRestartAppClick() {
    CanRestartApp(function (ok) {
        if (ok) {
            $('#restartingMain').fadeIn();
            window._freezeUpdateModulesStatus = true;
            SetPauseHandleAjaxError(true);
            $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=RestartApp'),
                function (res) {
                    if (res && (res != "OK")) {
                        SetPauseHandleAjaxError(false);
                        showMessage(res, '[$default:warning/js]');
                    }
                    else
                        checkAppStart();
                })
                .fail(function (response) {
                    checkAppStart();
                });
        }
    });
}
function checkAppStart() {
    window.setTimeout(
        function () {
            try {
                LoadModules(
                    function (modules) {
                        SetPauseHandleAjaxError(false);
                        delete window._freezeUpdateModulesStatus;
                        location.reload();
                    },
                    function (jqXHR, textStatus, errorThrown) {//fail
                        if (jqXHR && jqXHR.status == 401)
                            location.reload();
                    });
            }
            catch (ex) { }
            if (window.PauseHandleAjaxError)
                checkAppStart();
        }, 1000);
}
