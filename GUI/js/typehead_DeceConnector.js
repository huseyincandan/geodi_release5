﻿/*SUGGEST*/

function AppendLocalSuggest(historyName, sText) {
    if (window.localStorage) {
        var wsHsName = "wsh.:" + historyName;
        var strs = window.localStorage[wsHsName];
        if (!strs) strs = []; else strs = JSON.parse(strs);
        for (var i = strs.length - 1; i > -1; i--)
            if (strs[i].toLocaleLowerCase() == sText.toLocaleLowerCase())
                strs.splice(i, 1);
        strs.push(sText);
        if (strs.length > 1000)
            strs.splice(0, 1);

        window.localStorage[wsHsName] = JSON.stringify(strs);
    }
}
function deleteSuggest(obj, event, historyName) {
    CancelEvent();
    var sText = $(".tttx", $(obj).parent()).text();
    $(obj).parent().remove();

    if (window.localStorage) {
        var wsHsName = "wsh.:" +historyName;
        var strs = window.localStorage[wsHsName];
        if (!strs) strs = []; else strs = JSON.parse(strs);
        for (var i = strs.length - 1; i > -1; i--)
            if (strs[i].toLocaleLowerCase() == sText.toLocaleLowerCase())
                strs.splice(i, 1);
        window.localStorage[wsHsName] = JSON.stringify(strs);
    }

}

function getSuggestHistory(q, cb, historyName, forGeodIOptions) {
    var matches;
    matches = [];
    if (window.localStorage) {
        var wsHsName = "wsh.:" + historyName;
        var strs = window.localStorage[wsHsName];
        if (strs && strs.length > 0) {
            strs = JSON.parse(strs);
            substrRegex = null;
            try {
                if (q)
                    q = q.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
                substrRegex = new RegExp(q, 'i');
            }
            catch (err) {
                cb(matches);
                return;
            }
            for (var i = strs.length - 1; i > -1; i--)
                if (substrRegex.test(strs[i])) {

                    var d = {
                        Text: strs[i],
                        Type: 2
                    };
                    if (forGeodIOptions && forGeodIOptions.SuggestDataUpdater)
                        forGeodIOptions.SuggestDataUpdater(d);

                    matches.push(d);
                    if (matches.length > 3)
                        break;
                }
        }
    }

    cb(matches);
}

function getSuggestFTSForGEODI(q, cb, forGeodIOptions) {
    if (!q) {
        cb(null);
        return;
    }

    if (window.__suggestTm)
        window.clearTimeout(window.__suggestTm);

    window.__suggestTm = window.setTimeout(function () {
        var qSuggest = window.CurrentQueryContainer?window.CurrentQueryContainer.CurrentQuery.Clone():new GeodiQuery();//wsName
        qSuggest.SearchString = q;
        qSuggest.DisableGetContentKeywords = true;
        qSuggest.StartIndex = 0;
        qSuggest.EndIndex = 5; //test
        if (forGeodIOptions.UpdateQuery)
            forGeodIOptions.UpdateQuery(qSuggest);

        if (window._suggestSh)
            window._suggestSh.abort();
        window._suggestSh = $.post(HttpUtility.ResolveUrl('~/GeodiJSONService?op=getFTSSuggest'),
            {
                query: JSON.stringify(qSuggest),
                wsName: forGeodIOptions.wsName
            },
            function (data) {
                window._suggestSh = null;
                var matches;
                matches = [];
                if (data && data.length > 0)
                    for (var i = 0; i < data.length; i++) {
                        var d = data[i];
                        

                        var itm = { Text: d.Suggest };


                        if (d.ForDoc) {
                            if (forGeodIOptions.DisableDocs)
                                continue;
                            itm.ImgSrc = HttpUtility.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + d.Ref.ContentType, true, true);
                            if (window.CurrentQueryContainer)
                                itm.ImgTitle = HttpUtility.HtmlEncode(window.CurrentQueryContainer.CurrentWSInfo.GetContentTypeTitleFromId(d.Ref.ContentType));

                        }
                        else {
                            if (forGeodIOptions.DisableKeywords)
                                continue;
                            if (window.CurrentQueryContainer)
                                itm.ImgSrc = HttpUtility.ResolveUrl('~/GUIJsonService?op=getIco&IconName=' + window.CurrentQueryContainer.CurrentWSInfo.GetRecognizerIconFromId(d.Ref.RecognizerID), true, true);
                            itm.ImgTitle = (d.Ref.RecognizerName ? d.Ref.RecognizerName : '');

                        }
                        if (forGeodIOptions && forGeodIOptions.SuggestDataUpdater)
                            forGeodIOptions.SuggestDataUpdater(itm);
                        matches.push(itm);
                    }
                cb(matches);
            }, "json");
    }, 500);
}


function InitTypeHeadForGEODI(targetDom,onselect,historyName,forGeodIOptions) {
    window.typeheadNoTab = window.typeheadNoEnter = true;
    targetDom.typeahead(null,
        {
            name: 'select-history',
            displayKey: 'Text',
            source: function (a, b) { getSuggestHistory(a, b, historyName,forGeodIOptions) },
            templates: {
                suggestion: function (d) {
                    var txt = d.Text;
                    if (forGeodIOptions && forGeodIOptions.UpdateHistoryText) txt = forGeodIOptions.UpdateHistoryText(d.Text, d);
                    return $('<span class="tttxico img-suggestion-2"></span><span class="ttxDelete" onclick="deleteSuggest(this,event,\'' + historyName.replace(/'/g, "_").replace(/"/g, "_") + '\')">X</span><div class=tttx>' + (txt ? HttpUtility.HtmlEncode(txt) : txt) + '</div>');
                }
            }
        },
        {
            name: 'select-keyword',
            displayKey: 'Text',
            source: function (a, b) { getSuggestFTSForGEODI(a, b, forGeodIOptions) },
            templates: {
                suggestion: function (d) {
                    var txt = d.Text;
                    if (forGeodIOptions && forGeodIOptions.UpdateSuggestText) txt = forGeodIOptions.UpdateSuggestText(d.Text, d);
                    var imgHtml = d.ImgSrc ? '<img class="tttxico" src="' + d.ImgSrc + '" title="' + d.ImgTitle + '"/>' : '';
                    return $(imgHtml+'<div class=tttx>' + (txt?HttpUtility.HtmlEncode(txt):txt) + '</div>');
                }
            }
        }
    ).on("typeahead:selected", onselect);
}