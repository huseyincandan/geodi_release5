﻿function __GetWorkspaceName() {
    var ws = StateForm.GetState("wsName");
    if (ws)
        $("#txtSearchMap").show();
    return ws;
}
function GetSupportExtentUpdate(Cmd) {
    return false;
}


function __GetParams() {
    var rtn = { 'LAYERS': 'Geodi', 'wsName': window.wms_wsName, 'Rnd': window.GetRandom() };
    rtn.IsWidget = false;
    if (CheckWidget()) {
        //GetActiveQuery
        rtn.mapQuery = JSON.stringify(GetActiveQuery());
        rtn.IsWidget = true;
    }
    return rtn;
    //return { 'LAYERS': 'Geodi', 'wsName': window.wms_wsName, 'Rnd': window.GetRandom() }; // , 'CRS': 'EPSG:4326' //Rnd sürekli gelmemeli.
}

function __getBaseLayer() {
    return StateForm.GetState("DefaultBaseMapLayerName");
}
function FastGetFeatureInfoChecker(url, callback) {
    $.get(url + "&FastCheck=1", function (data) {
        if (data == "1")
            callback();
        else if (window.__RefWSLayers) {
            url=url.replace("&wsName=", "_t=");
            for (var i = 0; i < window.__RefWSLayers.length; i++) {
                var s = window.__RefWSLayers[i];
                if (callback) {
                    let url2 = url + "&wsName=" + HttpUtility.UrlEncode(s.server.wsName);
                    $.get(url2 + "&FastCheck=1", function (data) {
                        if (data == "1" && callback) {
                            window.frameUrl = url2;
                            var c = callback; callback = null; c();
                        }

                    });
                }
            }
        }
    });

}


function OnMapLayerListReady() {
    try {
        if (window.parent && window.parent.DefaultServerSourceList) {
            window.__RefWSLayers = [];
            //+setquery
            var inwsName = null; try { StateForm.GetState("wsName"); } catch (e0){ }
            for (var i = 0; i < window.parent.DefaultServerSourceList.length; i++) {
                var server = window.parent.DefaultServerSourceList[i];
                if (server && !server.ServerUrl && !server.Token && server.wsName && server.wsName!=inwsName) {
                    var prms = __GetParams();
                    prms.wsName = server.wsName;
                    var url = window.__wmsUrl; //server.ServerUrl
                    var wmst = _CreateWMSLayer(url, prms, "WmsLayer2");
                    window.layers.push(wmst.wmsLayer);
                    wmst.server = server;
                    window.__RefWSLayers.push(wmst);
                }
            }
            function updateGeodiWMSLayers(targetWS) {
                if (!targetWS)
                    return;
                for (var i = 0; i < window.__RefWSLayers.length; i++) {
                    var s = window.__RefWSLayers[i];
                    if (s.server.wsName == targetWS) {
                        var prms = __GetParams();
                        prms.wsName = s.server.wsName;
                        s.wmsSource.updateParams(prms);
                        //s.wmsSource.refresh();
                    }
                }
            }

            $(window).on("PCCommand",
                function (e, obj) {
                    //parent ?
                    if (obj.Command == "WSRunQuery") {
                        try {
                            updateGeodiWMSLayers(obj.TargetWS)
                        }
                        catch (e2) { }
                    }
                });


            //+onchange
        }
    }
    catch (e1) {

    }
}

function _UpdateSettingsForMap() {
    if (window.StateForm && StateForm.GetState("DisableRememberExtent"))
        window.DisableRememberExtent = true;
}

$(function () {



    window.__wmsUrl = 'wms?';

    window.changeVisibleLayer = function (style) {
        var i, ii;
        for (i = 0, ii = window.layers.length; i < ii; ++i) {
            var lstyle = window.layers[i].get('deceID');
            var lnoHide = window.layers[i].get('noHide');
            if (!lnoHide && lstyle && lstyle != "WmsLayer")
                layers[i].set('visible', (lstyle == style));
        }
        if (window.wms_wsName)
            $.post(HttpUtility.ResolveUrl('~/GeodiJSONService?op=SetDefaultBaseMapLayerName'), { 'wsName': window.wms_wsName, 'layerName': style },
                function (data, textStatus) { }
                , 'json');
    }


})


function OnMapSearchStart(text) {
    window.MapSearchData = null;
    UpdateMapSearchGUI();
    if (text) {
        var wsName = StateForm.GetState("wsName");
        if (wsName)
            $.post(HttpUtility.ResolveUrl('~/GeodiJSONService?op=QueryGeom'), { 'wsName': wsName, 'SearchString': text },
                function (data, textStatus) {
                    if (data && data.length > 0) {
                        GoMap({ 'wsName': wsName, 'BBOX': data[0].BBOX })
                        window.MapSearchData = data;
                        window.MapSearchDataIndex = 0;
                        UpdateMapSearchGUI();
                    }
                }
                , 'json');
    }
}

function MapPrevLastSearch() {
    window.MapSearchDataIndex--;
    if (window.MapSearchDataIndex < 0) window.MapSearchDataIndex = window.MapSearchData.length - 1;
    UpdateMapSearchGUI()
    var wsName = StateForm.GetState("wsName");
    GoMap({ 'wsName': wsName, 'BBOX': window.MapSearchData[window.MapSearchDataIndex].BBOX });
}
function MapNextLastSearch() {
    window.MapSearchDataIndex++;
    if (window.MapSearchDataIndex >= window.MapSearchData.length) window.MapSearchDataIndex = 0;
    UpdateMapSearchGUI()
    var wsName = StateForm.GetState("wsName");
    GoMap({ 'wsName': wsName, 'BBOX': window.MapSearchData[window.MapSearchDataIndex].BBOX });
}
function UpdateMapSearchGUI() {
    if (window.MapSearchData && window.MapSearchData.length > 1) {
        $("#mapSearchPrev").show();
        $("#mapSearchNext").show();
        $("#mapSearchPos").show();
        $("#mapSearchPos").html((window.MapSearchDataIndex + 1) + "/" + window.MapSearchData.length);
        var data = window.MapSearchData[window.MapSearchDataIndex];
        $("#mapSearchPos").attr("title", data.kWHigh ? data.kWHigh : data.Text);
    }
    else {
        $("#mapSearchPrev").hide();
        $("#mapSearchNext").hide();
        $("#mapSearchPos").hide();
    }

}

//DICTIONARY

function getDictionary(id) {
    window.currentDictionaryRecognizerBase.GetGeodiDictionary(id, function (data, textStatus) {
        window.currentDictionary = data;
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.Polygon)) {
            $("#drawType option[value='Polygon']").remove();
        }
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.Point)) {
            $("#drawType option[value='Point']").remove();
        }
        if (!window.TableGeometryType.Has(parseInt(data.TableSchema.GeometryType), window.TableGeometryType.LineString)) {
            $("#drawType option[value='LineString']").remove();
        }
    });
}

function sendBackWkt(wkt) {
    window.parent.postMessage({ type: 'WKT', WKT: wkt }, '*');
}

function startDictionaryRecord(wkt) {
    $('.trash').hide();
    $('.cleanGroup').show();
    $('.loading').show();
    $('.drawGroup').addClass('input-group');
    disableTypeSelect();
    $('#DictionaryContent').html('');
    window.currentDictionary.__WKT = wkt;
    window.webEditorManagerMap.createEditor(window.currentDictionary, function (obj) {
        window.webEditorManagerMap.current = obj;
        $('#DictionaryModal').modal('show');
        // window.cleanDrawing();
        $('.loading').hide();
        $('.trash').show();
        $('.cleanGroup').hide();
        $('.drawGroup').removeClass('input-group');
        enableTypeSelect();
    }, $('#DictionaryContent'), 450);
}

window.webEditorManagerMap = new WebEditorManager();

window.currentEditorSave = function () {
    var val = window.webEditorManagerMap.current.IsValid(true);
    $.when(val).then(
         function (status) {
             if (status)
                 $('#DictionaryModal').modal('hide');
         });
}

window.currentEditorClose = function () {
    window.vSource.removeFeature(window.lastFeature);
}