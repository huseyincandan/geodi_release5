﻿function TableSchema(id) {
    this.ID = id;

    this.GetTableSchemaList = function (callBackFunction) {
        $.post('TableSchemaService?op=GetTableSchemaList', { }, callBackFunction, 'json');
    }
}

window.currentTableSchema = new TableSchema('');
