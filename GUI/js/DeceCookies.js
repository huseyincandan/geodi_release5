﻿function DeceCookies() {

    this.getCookie = function (name) {
        if (name && name !== '') {
            var nameEQ = name + "=";
            var cookies = document.cookie.split("; ");
            for (var i = 0; i < cookies.length; i++) {
                var cookie = cookies[i];
                if (cookie.indexOf(nameEQ) == 0) {
                    return cookie.substring(nameEQ.length, cookie.length);
                }
            }
        }
        return null;
    };

    this.setCookie = function (key, value, expiresInMinutes, pathValue, domainValue) {
        if (key) {
            var expires = "";
            var path = " path=";
            var domain = "";
            if (expiresInMinutes && !isNaN(parseInt(expiresInMinutes))) {
                if (parseInt(expiresInMinutes) < 0)
                    expires = " expires=" + new Date(+new Date + 1000 * 86400 * expiresInMinutes).toUTCString();
                else
                    expires = " expires=" + new Date(+new Date + 1000 * 60 * expiresInMinutes).toUTCString();
            }

            if (!pathValue)
                path = path + "/";
            else
                path = path + pathValue;

            if (domainValue)
                domain = " domain=" + domainValue;

            document.cookie = key + "=" + value + ";" + expires + ";" + path + ";" + domain;
        }
    }

    this.clearCookie = function (key, pathValue, domainValue) {
        if (key) {
            this.setCookie(key, '', -1, pathValue, domainValue);
        }
    }

}

window.DeceCookies = new DeceCookies();