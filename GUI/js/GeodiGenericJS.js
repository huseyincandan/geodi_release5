﻿
delete window.PointerEvent;
window.navigator.msPointerEnabled = false;


function GeodiService() {
    this.GetRecognizers = function (callBackFunction, params) {
        if (!params) params = {}
        $.post("GeodiJSONService?op=getRecognizers", params, callBackFunction, 'json');
    }
    this.GetWebBaseMapLayers = function (callBackFunction, params) {
        if (!params) params = {}
        $.post("LayerService?op=GetWebBaseMapLayers", params, callBackFunction, 'json');
    }
    this.GetAllSupportedEPSG = function (callBackFunction) {
        $.post("GeoService?op=GetAllSupportedEPSG", {}, callBackFunction, 'json');
    }
    this.FolderExists = function (folderPath, callBackFunction,user,pass) {
        $.post("GUIJsonService?op=FolderExists", { 'folderPath': folderPath ,user:user,pass:pass}, callBackFunction, 'json');
    }
    this.AnyFolderExists = function (folderPaths, callBackFunction, user, pass) {
        $.post("GUIJsonService?op=AnyFolderExists", { 'folderPaths': JSON.stringify(folderPaths), user: user, pass: pass }, callBackFunction, 'json');
    }
    this.GetAvailableLanguages = function (callBackFunction) {
        $.post("LanguageManager?op=getAvailableLanguages", {}, callBackFunction, 'json');
    }
    this.GetContentReaders = function (callBackFunction) {
        $.post("GeodiJSONService?op=GetContentReaders", {}, callBackFunction, 'json');
    }
    this.GetContentEnumerators = function (callBackFunction,params) {
        if (!params) params = {}
        $.post("GeodiJSONService?op=getContentEnumarators", params, callBackFunction, 'json');
    }
    this.GetStorageManagers = function (typeFilter,callBackFunction) {
        $.post("GeodiJSONService?op=getStorageManagers", { "type": typeFilter }, callBackFunction, 'json');
    }
    
   
    this.getEnumerators = function (wsName, callBackFunction) {
        $.post('GeodiJSONService?op=getEnumerators', { 'wsName': wsName }, callBackFunction, 'json');
    }
    this.getSuggestionForQuery = function (searchText, wsName, callBackFunction) {
        $.post("GeodiJSONService?op=getSuggestionForQuery", { 'searchText': searchText, 'wsName': wsName }, callBackFunction, 'json');
    }
}

window.GeodiServiceObj = new GeodiService();
function PostAborter() {
    this._Lst = [];
    this.Add = function (handler) {
        this._Lst.push(handler);
    }
    this.Remove = function (handler) {
        var idx = this._Lst.indexOf(handler);
        if (idx > -1)
            this._Lst[idx] = null; //this._Lst.splice(idx, 1);
    }
    this.AbortAll = function () {
        for (i = 0; i < this._Lst.length; i++) {
            var h = this._Lst[i];
            if (h) {
                try{
                    h.abortDC = 1;
                }
                catch (e){}
                h.abort();
            }
        }
        this._Lst = [];
    }
}

// Mesaj kutusu gösterir.
//  text: Input etiketinde görünecek yazı.
//  title: Mesaj kutusunun başlığı
//  bodyContent: mesajın body'sinde gösterilecek HTMl içerik.
function messageBoxEx(type, title, bodyContent, buttons, onCloseCallBack, onShownCallBack, dialogID,forcedHelpValue,dialogOptions,options) {
    if (!dialogID)
        dialogID = 'modalDialog';
    if (dialogOptions && dialogOptions.buttons) {
        if (!buttons) buttons = [];
        buttons = buttons.concat(dialogOptions.buttons);
    }
    var onClosingCallBack = options ? options.onClosingCallBack : null;

    if ($('#' + dialogID).length == 0) {
        $('body').append($('<div class="modal fade" id="' + dialogID + '" tabindex="-1" role="dialog" aria-labelledby="' + dialogID + 'Label" aria-hidden="true"><div class="modal-dialog"><div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<span id="' + dialogID + 'Help" style="display:none;"></span>' +
            '<h4 class="modal-title" id="' + dialogID + 'Label">-</h4></div><div class="modal-body">-</div>' +
            '<div class="modal-footer"><button type="button" class="btn btn-sm btn-default" data-dismiss="modal">[$default:close/js]</button></div></div></div></div>'));
        $('#' + dialogID).on('hide.bs.modal', function (e) {
            if (window.__activeMsgBxonClosingCallBack && window.__activeMsgBxonClosingCallBack[dialogID])
                return window.__activeMsgBxonClosingCallBack[dialogID](e);
        })
        $('#' + dialogID).on('hidden.bs.modal', function () {
            if (window.__activeMsgBxonCloseCallBack && (!$('#' + dialogID + 'Label').attr('onCloseHandled') || $('#' + dialogID + 'Label').attr('onCloseHandled') == "false"))
                window.__activeMsgBxonCloseCallBack();
        })
        $('#' + dialogID).on('shown.bs.modal', function () {
            //event yayınlanmıyor.
            if (window.__activeMsgBxononShownCallBack)
                window.__activeMsgBxononShownCallBack();
        })
    };

    var d1 = $(".modal-dialog", $('#' + dialogID));
    d1.attr("class", "modal-dialog");
    if (dialogOptions && dialogOptions.dialogClass)
        d1.addClass(dialogOptions.dialogClass);
    //modal-lg

    $('#' + dialogID + 'Label').attr('onCloseHandled', false);
    if (!type) type = 'default';
    if (!title) title = 'Mesaj';
    var d1 = $('#' + dialogID).attr("dece-class");
    if (d1)
        $('#' + dialogID).removeClass(d1);
    $('#' + dialogID).attr("dece-class", type);
    $('#' + dialogID).addClass("dece-modal-", type);

    $('#' + dialogID + 'Label').html(title);
    $('#' + dialogID + ' .modal-body').html('');

    if (forcedHelpValue != undefined && forcedHelpValue != null) {
        $('#' + dialogID + 'Help').replaceWith('<span id="' + dialogID + 'Help" class="pull-right" style="padding:0px;padding-right:5px;display:inline-flex;vertical-align:top; color:#ffffff;" data-help="none"  data-placement="bottom" ><span class="directValue" style="display:none">' + forcedHelpValue + '</span></span>');
    }
    else {
        if (!forcedHelpValue) forcedHelpValue = "-";
        $('#' + dialogID + 'Help').replaceWith('<span id="' + dialogID + 'Help" class="pull-right" style="padding:0px;padding-right:5px;display:none;vertical-align:top" data-help="none"  data-placement="bottom" ><span class="directValue" style="display:none">' + forcedHelpValue + '</span></span>');

    }
    
    
    $('#' + dialogID + ' .modal-body').append(bodyContent);
    $('#' + dialogID + ' .modal-footer').html((dialogOptions && dialogOptions.FooterHtml) ? dialogOptions.FooterHtml : '');


    if (dialogOptions && dialogOptions.hideFooter)
        $('#' + dialogID + ' .modal-footer').hide();
    else
        $('#' + dialogID + ' .modal-footer').show();
    

    if (buttons) {
        for (var i = 0; i < buttons.length; i++)
            if(!buttons[i].hide)
            {
                if (buttons[i].Creator)
                {
                    var buttonElem = buttons[i].Creator(buttons[i], $('#' + dialogID), options);
                    if(buttonElem)
                        $('#' + dialogID + ' .modal-footer').append(buttonElem);
                }
                else {
                    if (!buttons[i].className)
                        buttons[i].className = 'btn btn-default';

                    var buttonElem = $('<button type="button" class="' + buttons[i].className + '"' + (buttons[i].closeOnClick ? ' data-dismiss="modal"' : '') + ' >' + buttons[i].text + '</button>');
                    if (buttons[i].onClickCallBack)
                        buttonElem.on('click', buttons[i].onClickCallBack);
                    $('#' + dialogID + ' .modal-footer').append(buttonElem);
                }
        }
    }
    if (!window.__activeMsgBxonClosingCallBack) window.__activeMsgBxonClosingCallBack = {};
    window.__activeMsgBxonClosingCallBack[dialogID] = onClosingCallBack;
    window.__activeMsgBxonCloseCallBack = onCloseCallBack;
    window.__activeMsgBxononShownCallBack = onShownCallBack;
    if ($('#' + dialogID).embedHelp)
        $('#' + dialogID).embedHelp(true);

    if (dialogOptions)
        $('#' + dialogID).modal(dialogOptions);
    else
        $('#' + dialogID).modal({ });
    

    $('#' + dialogID).modal('show');

    if (options && options.OnShown)
        options.OnShown($('#' + dialogID), options);
    if (options && options.OnShown2)
        options.OnShown2($('#' + dialogID), options);

}

function ShowModalUrl(url, title, buttonsArray, closeOnclickButtons, callback, dialogOptions, iframeoptions, onCloseCallBack, onShownCallBack,options) {
    url = HttpUtility.ResolveUrl(url, false, true);
    window.lastUrlModalCallback = null;
    var lwidth = (iframeoptions && iframeoptions.width) ? iframeoptions.width : "100%";
    var lheight = (iframeoptions && iframeoptions.height) ? iframeoptions.height : "350px";
    if (!dialogOptions) dialogOptions = { backdrop: 'static', keyboard: true };
    var text = '<iframe style="width:' + lwidth + ';height:' + lheight + ';" frameborder=0 src="' + url + '" ' + (iframeoptions && iframeoptions.attr ? iframeoptions.attr:'') + '></iframe>'
    if (!title) title = " ";
    var buttons = null;
    if (buttonsArray) {
        buttons = [];
        var incallback = callback ? callback : function (v) {
            if (window.lastUrlModalCallback)
                window.lastUrlModalCallback(v);
            else
                HideModalUrl();
        };
        $.each(buttonsArray, function (i, v) {
            buttons.push(createModalButton(v, incallback, closeOnclickButtons ? false : true));
        })
    }
    messageBoxEx(null, title, text, buttons, onCloseCallBack, onShownCallBack, "urlDialog", null, dialogOptions, options);
}

function SetTitleModalUrl(title) {
    $('#urlDialogLabel').text(title);
}

function SetCallbackModalUrl(callbackmtd) {
    window.lastUrlModalCallback = callbackmtd;
}
function SetHelpModalUrl(helpData) {
    var target = $('.modal-header', '#urlDialog');
    if (target.embedHelp) {
        $('.directValue', target).html(helpData);
        target.embedHelp(true);
    }
}

function HideModalUrl() {
    $('#urlDialog').modal('hide');
}


function createModalButton(btnType, callback, disableCloseOnClick) {
    var btnText = '';
    switch (btnType) {
        case 'yes': btnText = '[$default:yes/js]'; break;
        case 'no': btnText = '[$default:no/js]'; break;
        case 'ok': btnText = '[$default:ok/js]'; break;
        case 'close': btnText = '[$default:close/js]'; break;
        case 'save': btnText = '[$default:save/js]'; break;
        default:
            btnText = btnType;
            break;
    }
    var closeOnClick= disableCloseOnClick?false:true;
    return {
        text: btnText, className: 'btn btn-sm btn-default', closeOnClick: closeOnClick, onClickCallBack: function () {
            $('#modalDialogLabel').attr('onCloseHandled', true);
            if (callback) callback(btnType);
        }
    };
}

function showMessage(text, title, type, btnYes, btnNo, btnOK, callBack, shownCallback, dialogID, dialogOptions, helpText = null) {
    if (!type) type = 'default';
    var buttons = [];
    if (btnYes)
        buttons.push(createModalButton('yes', callBack));
    if (btnNo)
        buttons.push(createModalButton('no', callBack));
    if (btnOK)
        buttons.push(createModalButton('ok', callBack));

    if (!btnYes && !btnNo && !btnOK) {
        buttons.push(createModalButton('close',callBack));
    }
    messageBoxEx(type, title, text, buttons,
        function () {
            if (callBack)
                callBack('none');
        }
        , shownCallback, dialogID, helpText, dialogOptions
    );
}

function inputJSON(jsonObject, title, helpText, callBack) {
    inputBoxExtra(
        {
            title: title + (helpText?('<span data-help="none" data-ns="none"><span class="directValue" style="display:none">'+helpText+'</span></span>'):''),
            callBack: function (result, text, html) {
                if (result == 'ok') {
                    if (text)
                        try {
                            var rtn = JSON.parse(text);
                            callBack("ok", rtn);
                        }
                        catch (e) {
                            showMessage(e.message);
                            callBack("error",text,e);
                        }
                }
                else
                    callBack(result, text);
            },
            defaultValue: JSON.stringify(jsonObject ? jsonObject : {}, null, "\t"),
            useTextarea: true,
            textAreaHeight: $(window).height() * 0.6,
            SupportHtml: false,
            OnStart: function () {
                $(".advSetDialog .row").removeClass("row");
                $(".advSetDialog textarea").height("50vh");
                $(".advSetDialog textarea").attr("spellcheck", "false");

            },
            DialogOptions: { backdrop: 'static', keyboard: true, dialogClass: 'advSetDialog' }
        }
    )
}

// Input girmek için modal açar
//  text: Input etiketinde görünecek yazı.
//  title: Mesaj kutusunun başlığı
//  callback: Input'un callback'i
//  defaultValue: Input'un varsayılan değeri.
//  useTextArea: text alanının kullanıp kullanılmayacağı
//  textAreaHeight: useTextArea true'ysa text alanının yüksekliğini ayarlar. pozitif olmayan sayılar dikkate alınmaz
function inputBox(text, title, callBack, defaultValue, useTextarea, textAreaHeight,dialogOptions) {
    inputBoxExtra({
        text: text,
        title: title,
        callBack:callBack,
        defaultValue:defaultValue,
        useTextarea:useTextarea,
        textAreaHeight: textAreaHeight,
        DialogOptions: dialogOptions
        });
}
function inputSetEditing(htmlEditableContent, options) {
    if (options.OnInitElement)
        options.OnInitElement(htmlEditableContent, htmlEditableContent[0]);
    if (options.OnEditing) {

        htmlEditableContent.on({
            focus: function () {
            },
            blur: function () {
            },
            //paste
            keyup: function (e) {
                var key = e.keyCode;
                if (e.ctrlKey || e.altKey || (key < 41 && key != 32 && key != 8 && key != 9))//key == 13 || key == 37 ||  key == 38 || key == 39 || key == 40 || key == 33 || key == 34 || key == 35 || key == 36
                    return;
                //advance!!
                var jqEl = $(this);
                var el = jqEl[0];
                el.lastTime = new Date();
                if (el.tmr)
                    window.clearTimeout(el.tmr);
                if (jqEl.text() == "")
                    jqEl.html("");
                else if (options.OnEditing) {
                    el.tmr = window.setTimeout(function () {
                        options.OnEditing($(el), el);
                    }, 400);
                }
            }
        });

    }

    if (options.OnStart)
        options.OnStart(htmlEditableContent, htmlEditableContent[0]);
}
function inputBoxExtra(options) {
    var bodyContent = null;
    var labelText = options.text ? '<p>' + options.text + '</p>' : '';
    if (options.useTextarea)
    {
        var $divRow = $("<div/>", { class: "row" }), $divCol = $("<div/>", { }), style = "";
        if (options.SupportHtml) {
            if (!options.textAreaHeight) options.textAreaHeight = 200;
            style = 'style="height:' + options.textAreaHeight + 'px !important;padding:10px !important;overflow:auto;"'; //auto
            $divCol.append(labelText + '<div type="text"  class=inEditableArea ' + (options.hideOK ? '' : 'contenteditable=true') + ' class="form-control"' + style + '>' + (options.defaultValue ? options.defaultValue.replace(/\r\n/g, '\r').replace(/\r/g, '<br>') : '') + '</div>');
        }
        else {
            style = options.textAreaHeight && options.textAreaHeight > 0 ? 'style="height:' + options.textAreaHeight + 'px !important;"' : '';
            $divCol.append(labelText + '<textarea type="text" class="form-control"' + style + ' ' + (options.hideOK ? 'disabled' : '') + '>' + (options.defaultValue ? options.defaultValue : '') + '</textarea>');
            
        }
        $divRow.append($divCol);
        var $div = $("<div/>", {});
        $div.append($divRow);
        bodyContent = $div.html();
        $div.remove();
    }
    else if(options.SupportHtml) 
        bodyContent = (labelText + '<div type="text" class=inEditableArea style="padding:10px !important;" class="form-control" ' + (options.hideOK ? '' : 'contenteditable=true') + '>' + (options.defaultValue ? options.defaultValue.replace(/\r\n/g, '\r').replace(/\r/g, '<br>') : '') + '</div>');
    else
        bodyContent = (labelText + '<input type="text" class="form-control" value="' + (options.defaultValue ? window.HttpUtility.HtmlEncode(options.defaultValue) : '') + '" ' + (options.hideOK ? 'disabled' : '') + '/>');

    if (options.BodyFormat) {
        bodyContent = options.BodyFormat.replace("{body}", bodyContent);
        bodyContent = $(bodyContent);
    }
    else
        bodyContent = $(bodyContent);
    if (options.Extra)
    {
        if (options.Extra.Content) {
            var bodyDummy = $("<div></div>");
            bodyDummy.append(bodyContent);
            var contentIn = typeof (options.Extra.Content) == 'function' ? options.Extra.Content() : options.Extra.Content;
            bodyDummy.append(contentIn);
            bodyContent = bodyDummy;
        }
    }

    var buttons = [
        {
            hide: options.hideClose,
            text: '[$default:close/js]',
            className: 'btn btn-sm btn-default',
            closeOnClick: true
        },
        {
        hide:options.hideOK,
            text: '[$default:ok/js]',
        className: 'btn btn-sm btn-primary',
        closeOnClick: true,
        onClickCallBack: function () {
            $('#modalDialogLabel').attr('onCloseHandled', true);
            if (options.callBack) {
                var value=null;
                var htmlValue=null;
                if(options.SupportHtml) {
                    var htmlEditableContent=$(".inEditableArea", $("#modalDialog"));
                    value=htmlEditableContent.text();
                    htmlValue = htmlEditableContent.html().replace(/<br>/g, "\r\n").replace(/<div>/g, "\r\n").replace(/<[/]div>/g, "").replace(/<p>/g, "\r\n").replace(/<[/]p>/g, "");
                }
                else htmlValue=value=options.useTextarea ?
                        $('#modalDialog .modal-body textarea[type="text"]').val() :
                        $('#modalDialog .modal-body input[type="text"]').val();
                options.callBack('ok',
                        value,
                        htmlValue
                    );
            }
        }
    }
        
    ];

    if (options.ButtonsUpdater)
        buttons = options.ButtonsUpdater(buttons);

    messageBoxEx('default', options.title, bodyContent, 
        buttons,
        function () {
            if (options.callBack)
                options.callBack('', '');
        },
        function () {
            if(options.SupportHtml)
            {
                var htmlEditableContent=$(".inEditableArea", $("#modalDialog"));
                htmlEditableContent.focus();

                inputSetEditing(htmlEditableContent,options);
                
                return;
            }
            var jqEl = null;
             if (options.useTextarea)
                jqEl = $("textarea", $("#modalDialog"));
            else
                 jqEl = $("input", $("#modalDialog"));
             jqEl.focus();
             if (options.OnStart)
                 options.OnStart(jqEl, jqEl[0]);

        }, null, null, options.DialogOptions,options);
}
/*Ajax Error Handle*/

window.GlobalAjaxErrorBuffer = [];
function ShowErrorMessageInRoot(body, status, tt,dialogOptions) {
    hideWaitDialog();
    try {
        if (HasAccessableParentWindow(window)  &&
            IsAccessableParentObject(window, "ShowErrorMessageInRoot")) {
            window.parent.ShowErrorMessageInRoot(body, status, tt, dialogOptions);
            try { window.parent.hideWaitDialog(); }catch (e2){}
            return;
        }
    }
    catch (e) { }
    showMessage(body, status, tt, null, null, null, null, null,null,dialogOptions);

}

$(document).ajaxError(function (event, data, settings)
{
    if (window.PauseHandleAjaxError)
        return;

    if (window.OnAjaxError && data.statusText != "abort" && !data.abortDC)
        window.OnAjaxError();
    if (settings.OnAjaxError) {
        if (settings.OnAjaxError(event, data, settings))
            return;//Handled
    }

    if (data.status == 0 && data.statusText != "abort" && !data.abortDC) {
        if (settings.DisableErrors)
            return;
        if (!settings.__rtr)
            settings.__rtr = 0;
        settings.__rtr++;
        var retryCount = 8;
        if (settings.__rtr < retryCount) { //retry 4 times
            window.setTimeout(
                function () { $.ajax(settings); }, 50
                );
            return;
        }
        else {
            if (settings.DisableErrorsEnd)
                return;
            data = {
                status: "",
                statusText: '[$default:connection_lost/js]',
                responseText: '[$default:reconnect_text/js]'
            };
            if (!window.GlobalAjaxErrorRetryList)
                window.GlobalAjaxErrorRetryList = [];
            window.GlobalAjaxErrorRetryList.push(settings);
        }
    }

    if (data.status == 200) {
        console.log('Parse Error');
        return;
    }

    if (window.GlobalAjaxErrorBuffer && window.GlobalAjaxErrorBuffer.length > 0) {
        var lastData = window.GlobalAjaxErrorBuffer[window.GlobalAjaxErrorBuffer.length - 1];
        if (lastData.status == data.status &&
            lastData.statusText == data.statusText &&
            lastData.responseText == data.responseText)
            return;
    }
    window.GlobalAjaxErrorBuffer.push(data); //settings no message ise veya request no mesaj ise göstermeyebiliriz.(return)

    if (window.ShowGlobalAjaxMessageTimer) window.clearTimeout(window.ShowGlobalAjaxMessageTimer);
    window.ShowGlobalAjaxMessageTimer = window.setTimeout(function () {
        var v2 = window.GlobalAjaxErrorBuffer;
        var v2Retry = window.GlobalAjaxErrorRetryList;
        delete window.GlobalAjaxErrorRetryList;
        window.GlobalAjaxErrorBuffer = [];
        if (v2.length < 1) return;
        if (v2.length == 1) {
            if (v2[0].responseText) {
                if (window.OnAjaxErrorDialog)
                    window.OnAjaxErrorDialog();
                var dialogOptions = null;
                if (v2Retry && v2Retry.length>0) {
                    window.GlobalErrorRetry = v2Retry;
                    dialogOptions = {
                        backdrop: 'static', keyboard: false,
                        FooterHtml: '<a href="javascript:_RetryConnect()" class="pull-left">[$default:reconnect/js]</a>',
                        dialogClass: "modal-lg"
                    }
                }

                if (v2[0].responseText.indexOf('errorTitle') > -1) {
                    var errorElem = $('<div></div>').html(v2[0].responseText);
                    var NewTtl = '';
                    errorElem.find('img').attr('align', 'left');
                    if (errorElem.find('#sErrorId').length > 0) {
                        errorElem.find('#sErrorId').text(v2[0].status + " " + v2[0].statusText);
                    }
                    NewTtl = errorElem.find('#errorTitle').prop("outerHTML");
                    if (NewTtl != '' && NewTtl != null && NewTtl != undefined && NewTtl != 'undefined') {
                        errorElem.find('#errorTitle').remove();
                        if (errorElem.find('#sErrorId').length > 0)
                            errorElem.find('#sErrorId').remove();
                        v2[0].responseText = errorElem.prop("outerHTML");
                        ShowErrorMessageInRoot(v2[0].responseText, NewTtl, 'danger', dialogOptions);
                    }
                }
                else
                ShowErrorMessageInRoot(v2[0].responseText, v2[0].status + " " + v2[0].statusText, 'danger', dialogOptions);//mesaj açıksa beklenebilir.
            }
        }
        else {
            var verrList = [];
            var fndErr = false;
            verrList.push('<div class="panel-group" id="gerrList" role="tablist">');

            for (var i = 0; i < v2.length; i++)
                if (v2[i].responseText) {
                    var NewTtl = '';
                    var Newbdy = '';
                    if (v2[i].responseText.indexOf('errorTitle') > -1) {
                        var errorElem = $('<div></div>').html(v2[i].responseText);
                        errorElem.find('img').attr('align', 'left');
                        if (errorElem.find('#sErrorId').length > 0) {
                            errorElem.find('#sErrorId').text(v2[i].status + " " + v2[i].statusText);
                        }
                        NewTtl = errorElem.find('#errorTitle').prop("outerHTML");
                        if (NewTtl != '' && NewTtl != null && NewTtl != undefined && NewTtl != 'undefined') {
                            errorElem.find('#errorTitle').remove();
                            if (errorElem.find('#sErrorId').length > 0)
                                errorElem.find('#sErrorId').remove();
                            Newbdy = errorElem.prop("outerHTML");
                        }
                    }

                    fndErr = true;
                    verrList.push('<div class="panel panel-default">');
                    verrList.push('<div class="panel-heading" style="cursor:pointer;" role="tab" id="hderr' + i + '" data-toggle="collapse" data-parent="#gerrList" href="#hderrbdy' + i + '" aria-expanded="true" aria-controls="hderrbdy' + i + '">');
                    verrList.push(NewTtl != '' && NewTtl != null && NewTtl != undefined && NewTtl != 'undefined' ? NewTtl : v2[i].status + " " + v2[i].statusText);
                    verrList.push('</div>');
                    verrList.push('<div id="hderrbdy' + i + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="hderr' + i + '">');
                    verrList.push('<div class="panel-body">');
                    verrList.push(Newbdy != '' && Newbdy != null && Newbdy != undefined && Newbdy != 'undefined' ? Newbdy :v2[i].responseText);
                    verrList.push('</div>');
                    verrList.push('</div>');
                    verrList.push('</div>');
                }

            if (fndErr) {
                verrList.push('</div>');
                if (window.OnAjaxErrorDialog)
                    window.OnAjaxErrorDialog();
                var dialogOptions = null;
                if (v2Retry && v2Retry.length > 0) {
                    window.GlobalErrorRetry = v2Retry;
                    dialogOptions = {
                        backdrop: 'static', keyboard: false,
                        FooterHtml: '<a href="javascript:_RetryConnect()" class="pull-left">[$default:reconnect/js]</a>',
                        dialogClass: "modal-lg"
                    }
                }
                ShowErrorMessageInRoot(verrList.join(''), v2.length + " " + "[$default:error/js]", 'danger', dialogOptions);//mesaj açıksa beklenebilir.
            }
        }

    }, 500);
});


$(window).on("PCCommand",
  function (e, obj) {
      if (obj.Command == "_RetryConnectCmd") {
          if (window.GlobalErrorRetry) {
              var vr = window.GlobalErrorRetry;
              delete window.GlobalErrorRetry;
              for (var i = 0; i < vr.length; i++)
                      $.ajax(vr[i]);
          }
      }
  });


function _RetryConnect() {
    if (window.___PCommunicationCommander_OnMessageJson_Internal)
        ___PCommunicationCommander_OnMessageJson_Internal({ Command: "_RetryConnectCmd" });
    $('#modalDialog').modal('hide');
}

function showWaitDialog(title, text) {
    if (!text) text = "";
    if (!title) title = "";

    $(".loadingTextData", $('#modalWaitDialog')).remove();
    if ($('#modalWaitDialog').length == 0) {
        $('body').append($('<div class="modal fade" id="modalWaitDialog" tabindex="-1" role="dialog" aria-labelledby="modalWaitDialogLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header" ' + (title ? '' : 'style="display:none;"') + '><h4 class="modal-title" id="modalWaitDialogLabel" ' + (title ? '' : 'style="display:none;"') + '>' + title + '</h4></div><div class="modal-body" style="border-radius: 10px;"><div id="modalWaitDialogText">' + text + '</div><div ><div class="progress progress-striped active" style="margin-top:10px;margin-bottom:10px;"><div class="progress-bar" id=modalProgress style="width: 100%;"></div></div></div></div></div></div></div>'));
    }
    else {
        $("#modalProgress").empty();
        $("#modalWaitDialogText", $('#modalWaitDialog')).html(text);
        var ttl = $("#modalWaitDialogLabel", $('#modalWaitDialog'));
        ttl.html(title);
        if (!title) ttl.parent().hide(); else ttl.parent().show();
    }
    $('#modalWaitDialog').modal({ backdrop: 'static', keyboard: false });
}
function hideWaitDialog() {
    $('#modalWaitDialog').modal('hide');
}

// Generic Functions
function CreateId(val, simple) {
    if (simple)
        return val.replace(/ /g, "_").replace(/\./g, "_");
    //var vailds="ABC..."
    var v = val + "";
    return "id_" + v.hashCode();
}
function FormatSecond(Second,dcount,orderStrings) {
   // daha kısa stringlere ihtiyaç duyulduğu için orderString parametresi eklendi. localization için de kullanılabilir.
    var orders = orderStrings ? orderStrings : ["[$Calendar:Year/js]", "[$Calendar:Month/js]", "[$Calendar:DayName/js]", "[$Calendar:Hour/js]", "[$Calendar:Minute/js]", "[$Calendar:Second/js]"];
    return _FormatForArray(Second, orderStrings ? 60 : [12, 30, 24, 60, 60, 60], orders, "0 second", dcount);
}

function FormatBytes(Size, dcount, orderStrings) {
    var scale = 1024;
    var orders = orderStrings ? orderStrings : ["GB", "MB", "KB", "Bytes"];
    return _FormatForArray(Size, scale, orders, "0 byte", dcount);
}
function _FormatForArray(Value, scale, orders, defaultVal, dcount) {
    if (Value == 0)
        return defaultVal;
    var max = 1,scaleV1=1;
    if (typeof scale == "number")
        max = Math.pow(scale, orders.length - 1);
    else
        $.each(scale, function (i1, v1) { if (i1!=scale.length-1) max *= v1; })
    for (var order = 0; order < orders.length; order++) {
        if (Value > max || orders.length - 1 == order)
            return FormatNumber(Value / max, dcount) + " " + orders[order];
        
        scaleV1 = (typeof scale == "number") ? scale : scale[order];
        max /= scaleV1;
        
    }
    
}

function FormatNumber(val,dcount,withoutZero) {
    if (!val)
        return "0";
    if (typeof val !== "string" && typeof val !== "number")
        return val;

    if (!Number(val))
        return val;

    var rtn = "";

    if (dcount >= 0) {
        try { val = eval(val); } catch (e) { }
        try { val = val.toFixed(dcount); } catch (e) { }
    }
   
    val += "";
   
    var sp = (1.1).toLocaleString().substring(1, 2);
    var vlRightr = val.split(".");
    var vlRight="";

    if (vlRightr.length == 2) {
        vlRight = vlRightr[1];
        if (dcount>=0 && vlRight.length>dcount) {
            vlRight=vlRight.substr(0,dcount);
        }
        if (vlRight)
            vlRight = sp + vlRight;
        val = vlRightr[0];
    }
    sp = sp == ',' ? '.' : ',';
    
    for (var i = 0; i < val.length;i++)
    {
        var c=val[val.length-i-1];
        if (i != 0 && i % 3 == 0)
            rtn = sp + rtn;
        rtn = c + rtn;
    }
    if (withoutZero) {
        val = Number(vlRight.replace(",", "."));
        if (val == 0)
            return rtn;
    }
    
    return rtn + vlRight;
}

function UpdateStrForSort(val,padMax) {
    if (val == undefined || val == null || !(val+"").length) return val;
    val += "";
    var rtn = [];
    var nm = [];
    if (!padMax) padMax = 20;
    for (var i = 0; i < val.length; i++) {
        var c = val[i]
        if (c >= '0' && c <= '9') {
            nm.push(c);
        } else {
            rtn.push(pad(nm.join(""), padMax,"0"));
            nm = [];
            rtn.push(c);
        }
    }
    rtn.push(pad(nm.join(""), padMax, "0"));
    return rtn.join("");
}


function pad(num, size,pStr) {
    var s = num + "";
    while (s.length < size) s = (pStr ? pStr : "0") + s;
    return s;
}
function parseNetDate(value) {
    if (value.UTC) return new Date(value.UTC);
    //return value;
    return typeof (value) == "string" ? new Date(value) : value;
}
function toNetDateString(value) {
    var o = value;
    if (isNaN(o.getUTCMonth()))
        return { UTC: null };

    var month = o.getUTCMonth() + 1,
					day = o.getUTCDate(),
					year = o.getUTCFullYear(),
					hours = o.getUTCHours(),
					minutes = o.getUTCMinutes(),
					seconds = o.getUTCSeconds(),
					milli = o.getUTCMilliseconds();

    if (month < 10) {
        month = '0' + month;
    }
    if (day < 10) {
        day = '0' + day;
    }
    if (hours < 10) {
        hours = '0' + hours;
    }
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    if (milli < 100) {
        milli = '0' + milli;
    }
    if (milli < 10) {
        milli = '0' + milli;
    }
    var utcFormat = year + '-' + month + '-' + day + 'T' +
        hours + ':' + minutes + ':' + seconds +
        '.' + milli + 'Z';

    return { "UTC": utcFormat };
}
function netDateToStr(value, humanReadable) {
    if (!value || (typeof (value.UTC) == "string" && !value.UTC)) return "&nbsp;";
    var dateObj = parseNetDate(value);
    if (humanReadable)
        return moment(dateObj).fromNow();
    else
        return pad(dateObj.getDate().toString(), 2) + '.' + pad((dateObj.getMonth() + 1).toString(), 2) + '.' + dateObj.getFullYear().toString() + ' ' + pad(dateObj.getHours().toString(), 2) + ':' + pad(dateObj.getMinutes().toString(), 2);
}

function preventBodyScroll(elem) {
    var toolbox = elem;
    var height = toolbox.height();
    var scrollHeight = toolbox.get(0).scrollHeight;
    if (height != scrollHeight) {
        toolbox.bind('mousewheel', function (e, d) {
            if ((this.scrollTop === (scrollHeight - height) && d < 0) || (this.scrollTop === 0 && d > 0)) {
                e.preventDefault();
            }
        });
    }
}




if (typeof enums == "undefined")
    enums = new Object();
enums.TypeCodeEnum = {
    Empty: 0,
    Object: 1,
    DBNull: 2,
    Boolean: 3,
    Char: 4,
    SByte: 5,
    Byte: 6,
    Int16: 7,
    UInt16: 8,
    Int32: 9,
    UInt32: 10,
    Int64: 11,
    UInt64: 12,
    Single: 13,
    Double: 14,
    Decimal: 15,
    DateTime: 16,
    String: 18,
}

function IsTypeCodeNumeric(typeCode) {
    switch (typeCode) {
        case enums.TypeCodeEnum.Double:
        case enums.TypeCodeEnum.Int16:
        case enums.TypeCodeEnum.UInt16:
        case enums.TypeCodeEnum.Int32:
        case enums.TypeCodeEnum.UInt32:
        case enums.TypeCodeEnum.Int64:
        case enums.TypeCodeEnum.UInt64:
        case enums.TypeCodeEnum.Single:
        case enums.TypeCodeEnum.Decimal:
            return true;
            break;
    }
    return false;
}
function IsTypeCodeInteger(typeCode) {
    switch (typeCode) {
        case enums.TypeCodeEnum.Int16:
        case enums.TypeCodeEnum.UInt16:
        case enums.TypeCodeEnum.Int32:
        case enums.TypeCodeEnum.UInt32:
        case enums.TypeCodeEnum.Int64:
        case enums.TypeCodeEnum.UInt64:
            return true;
            break;
    }
    return false;
}
/*Hashcode*/
String.prototype.hashCode = function () {
    var hash = 0, i, chr, len;
    if (this.length == 0) return hash;
    for (i = 0, len = this.length; i < len; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};



//bir array içerisindeki nesnenin pozisyonunu değiştirir
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    for (var i = 0; i < this.length; i++) {
        if (this[i] == undefined) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

Array.prototype.unique = function () {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

var protoTypeSetting = function(own,predicate,maxCount)
{
    this.ownObject = own;
    this.predicate = predicate;
    this.maxCount = (typeof maxCount === 'undefined' ? 100000 : maxCount),//100K
    this.remove = false;
    this.updatedItem = null;
    this.setUpdate = function (item)
    {
        if (typeof item !== 'undefined') {
            this.updatedItem = item;
        }
        return this;
    }
    this.setRemove = function () {
        this.remove = true;
        return this;
    }
};


var multipleProtoType = function (_protoTypeSettings)
{
    
    var result = [];
  
    function run(list)
    {
        
        for (var i = 0; i < list.length; i++)
        {
            var item = list[i];
            if (typeof item === 'undefined') return;
            for (var key in item) {
                if (Array.isArray(item[key])) {
                    run(item[key]);
                    if (result.length >= _protoTypeSettings.maxCount) break;
                }
            }
            try {
                if (_protoTypeSettings.predicate(item)) {
                    if (_protoTypeSettings.remove) {
                        list.splice(i, 1);
                        i--;
                    }
                    if (_protoTypeSettings.updatedItem !== null)
                        $.extend(list[i], _protoTypeSettings.updatedItem);
                    result.push(item);
                    if (result.length >= _protoTypeSettings.maxCount) break;
                }
            } catch (e) {

            }
            
        }
        
    };
    run(_protoTypeSettings.ownObject);
    return result;
};

Array.prototype.filterAll = function (predicate,maxCount)
{
    var settingObj = new protoTypeSetting(this,predicate, maxCount);
    return multipleProtoType(settingObj);
};
Array.prototype.remove = function (predicate)
{
    var settingObj = new protoTypeSetting(this, predicate).setRemove();
    return multipleProtoType(settingObj);
};
Array.prototype.update = function (predicate, updatedItem)
{
    var settingObj = new protoTypeSetting(this, predicate).setUpdate(updatedItem);
    return multipleProtoType(settingObj);
};

Array.prototype.filterFirst = function (predicate) {
    var rtn = this.filterAll(predicate,1);
    return (rtn.length == 1) ? rtn[0] : null;
}



//bir array içerisindeki nesneyi, başka bir arraye veirlen pozisyona gelecek şekilde taşır
Array.prototype.moveOut = function (obj, newArray, position) {
    var index = this.indexOf(obj);
    if (this == newArray) {
        var oi = this.indexOf(obj);
        this.move(oi, position);
    }
    else if (newArray.indexOf(obj) === -1) {
        var obj = this[index];
        newArray.push(obj);
        this.splice(index, 1);
        var ni = newArray.indexOf(obj);
        newArray.move(ni, position);
    }
    else {
        var oi = newArray.indexOf(obj);
        if (oi > -1)
            newArray.move(oi, position);
    }
    for (var i = 0; i < this.length; i++) {
        if (this[i] == undefined) {
            this.splice(i, 1);
            i--;
        }
    }
    for (var i = 0; i < newArray; i++) {
        if (newArray[i] == undefined) {
            newArray.splice(i, 1);
            i--;
        }
    }
    return this;
};
Array.prototype.max = function () {
    return Math.max.apply(null, this);
};

Array.prototype.min = function () {
    return Math.min.apply(null, this);
};

//timer,msg

function StartLoadingInfoTimer(loadingDom, textList) {
    loadingDom = $(loadingDom);
    $(".loadingTextData", loadingDom).remove();
    if (!textList || textList.length==0)
        textList = [
            {
                Sleep: 1000,
                Text: ""
            },
            {
                Sleep: 3000,
                Text: "[$default:waiting_text_1/js]"
            }
            ,
            {
                Sleep: 10000,
                Text: "[$default:waiting_text_2/js]"
            }
            ,
            {
                Sleep: 20000,
                Text: "[$default:waiting_text_3/js]"
            }
        ]
    var tmr = loadingDom.data("tDataTimer");
    if (tmr) window.clearTimeout(tmr);
    loadingDom.append("<div class=loadingTextData align=center style='display:none'></div>");
    loadingDom.data("tData", textList);
    loadingDom.data("tDataCurrent",-1);
    __NextLoadingInfoTimer(loadingDom);
}

function __NextLoadingInfoTimer(loadingDom) {
    var textList = loadingDom.data("tData");
    var tCurrent = loadingDom.data("tDataCurrent");
    if (!tCurrent) tCurrent = 0;
    tCurrent++;
    var tmr = $(loadingDom).data("tDataTimer");
    if (tmr) window.clearTimeout(tmr);
    if (tCurrent >= textList.length)
        return;
    var dm1 = $(".loadingTextData", loadingDom);
    dm1.html(textList[tCurrent].Text);
    if (textList[tCurrent].Text)
        dm1.show();
    else
        dm1.hide();
    loadingDom.data("tDataCurrent", tCurrent);
    loadingDom.data("tDataTimer", window.setTimeout(function () {
        __NextLoadingInfoTimer(loadingDom)
    }, textList[tCurrent].Sleep));
}

function EndLoadingInfoTimer(loadingDom) {
    var tmr = loadingDom.data("tDataTimer");
    if (tmr) window.clearTimeout(tmr);
    $(".loadingTextData", loadingDom).remove();
}

//guid üretir
function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

window.LayerHelpers = {
    Find: function (layers, callback) {
        if (!callback) return;
        for (var j = 0; j < layers.length; j++) {
            callback(layers[j]);
            if (layers[j].Layers)
                return this.Find(layers[j].Layers, callback);
        }
    },
    FirstOrDefault: function (layers, condition) {
        if (!condition) return;
        for (var j = 0; j < layers.length; j++) {
            if (condition(layers[j]))
                return layers[j];
            if (layers[j].Layers) {
                var rtn = this.FirstOrDefault(layers[j].Layers, condition);
                if (rtn) return rtn;
            }
        }
        return null;
    }
}

function UsageLogItem() {
    try {
        this.WSName = StateForm.GetState("wsName");
    }
    catch (err) { this.WSName = ''; }
    this.Ticks = -1;
    this.Action = '';
    this.Mode = 3;
    this.Info = '';
    this.Detail = '';
}
function UsageLogger() {
    this.AddLog = function (logItem, callBackFunction,noasync) {
        $.ajax({
            type: 'POST',
            url: 'UsageLogService?op=AddLog',
            data:  { 'logItem': JSON.stringify(logItem) },
            success: callBackFunction,
            dataType: "json",
            async: noasync ? false : true
        });
        //$.post('UsageLogService?op=AddLog', { 'logItem': JSON.stringify(logItem) }, callBackFunction, 'json');
    }
    this.AddLogSimple = function (action, mode, callBackFunction, noasync) {
        this.AddLogWithInfo(action, mode, '', callBackFunction, noasync);
    }
    this.AddLogWithInfo = function (action, mode, info, callBackFunction, noasync){
        if (!mode)
            mode = 3;
        var lgItem = new UsageLogItem();
        lgItem.Action = action;
        lgItem.Mode = mode;
        lgItem.Info = info;
        this.AddLog(lgItem, callBackFunction, noasync);
    }
}
window.DefaultUsageLogger = new UsageLogger();

function GetUrlParameter(name, url) {
    if (!url) url = location.href
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}

function SearchInTable(inputElem, tableElem) {
    inputElem.keyup(function () {
        SearchInTableIn(inputElem, tableElem);
    });
}

function SearchInTableIn(inputElem, tableElem) {
        tableElem.children('tbody').children('tr').each(function () {
            var cells = $(this).children('td');
            if (cells.length > 0) {
                var searchString = inputElem.val().toLocaleUpperCase();
                if (window.HasCheckedFilterForSearchInTable && $("input:checked", this).length == 0)
                    $(this).hide();
                else if (cells.first().html().toLocaleUpperCase().indexOf(searchString) >= 0)
                    $(this).show();
                else
                    $(this).hide();
            }
        });
}

if (!Array.prototype.find) {
    Object.defineProperty(Array.prototype, 'find', {
        value: function (predicate) {

            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }

            var o = Object(this);
            var len = o.length >>> 0;
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }


            var thisArg = arguments[1];

            var k = 0;
            while (k < len) {

                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return kValue;
                }
                k++;
            }

            return undefined;
        },
        configurable: true,
        writable: true
    });
}
if (!Array.prototype.findIndex) {
    Object.defineProperty(Array.prototype, 'findIndex', {
        value: function (predicate) {
            if (this == null) {
                throw new TypeError('"this" is null or not defined');
            }
            var o = Object(this);
            var len = o.length >>> 0;
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate must be a function');
            }
            var thisArg = arguments[1];
            var k = 0;
            while (k < len) {
                var kValue = o[k];
                if (predicate.call(thisArg, kValue, k, o)) {
                    return k;
                }
                k++;
            }
            return -1;
        },
        configurable: true,
        writable: true
    });
}

function UserIs(AuthObj, users, disableGroupCheck, disableUserNameCheck) {
    if (!AuthObj || !users || users.length == 0) return false;
    if (!Array.isArray(users)) users = [users];
    for (var i = 0; i < users.length; i++)
        if (users[i])
            users[i] = ((users[i]) + '').toLowerCase();
    if (!disableGroupCheck && AuthObj.Groups) {
        if (AuthObj.Groups.find(function (g) { return users.indexOf(g.ID.toLowerCase()) != -1 || users.indexOf(g.DisplayName.toLowerCase()) != -1 } ))
            return true;
    }
    if (!disableUserNameCheck && AuthObj.CurrentUser && AuthObj.CurrentUser.Name&& users.indexOf(AuthObj.CurrentUser.Name.toLowerCase()) != -1)
        return true;
    return false;
}

/*projection*/

function UpdateProjectionXY(XYArray, sourceepsg, targetepsg, fnc) {
    sourceepsg = sourceepsg.replace("EPSG:", "");
    targetepsg = targetepsg.replace("EPSG:", "");
    if (sourceepsg == targetepsg) fnc(XYArray);
    else {
        if (window.ol && (sourceepsg == "4326" || sourceepsg == "3857") && (targetepsg == "4326" || targetepsg == "3857")) {
            var tmpArray = [];
            for (var i = 0; i < XYArray.length - 1; i += 2)
                tmpArray.push([XYArray[i], XYArray[i + 1]]);
            var geom = new ol.geom.LineString(tmpArray);
            geom.transform("EPSG:" + sourceepsg, "EPSG:" + targetepsg);
            tmpArray = geom.getCoordinates();

            XYArray = [];

            for (var i = 0; i < tmpArray.length ; i++) {
                XYArray.push(tmpArray[i][0]);
                XYArray.push(tmpArray[i][1]);
            }
            fnc(XYArray);
        }
        else
            $.post('GeoService?op=ChangeProjectionXY',
                    {
                        "Coords": JSON.stringify(XYArray),
                        SourceEPSG: sourceepsg,
                        TargetEPSG: targetepsg
                    }
                    ,
                    function (result) {
                        fnc(result);
                    }
                    , 'json');
    }

}


function isRelativeDate(dateVal) {
    if (!dateVal || typeof (dateVal) != "string")
        return false;
    var relKeys = ["y", "s", "m", "a", "w", "h", "d", "g", "wd", "ig", "wm", "ia", "min", "dk", "hour", "saat"];
    for (var i = 0; i < relKeys.length; i++)
        if (dateVal.indexOf(relKeys[i]) != -1)
            return true;
    return false;
}

function SingleBind(dm, evnt, fnc) {
    dm = $(dm);
    if (dm.attr('data1_' + evnt)) return;
    dm.attr('data1_' + evnt, "1");
    dm.bind(evnt, fnc);
}

