﻿if (!window.FacetAlwaysShowList) window.FacetAlwaysShowList = {};
function GeodiFacetLink(params, pThis) {
    var wsInfoItem = params.wsInfoItem;
    var displayName = (params.wsInfoItem ? (HttpUtility.HtmlEncode(wsInfoItem.DisplayName)) : (params.Title ? params.Title : ""));
    this.icon = (params.noIcon ? null : (params.wsInfoItem ? { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=" + wsInfoItem.IconName, true, true) } : null));
    this.title = displayName;
    this.html = displayName;
    this.class = "facetLink FacetEnum";
    this.panel = params.panel;
    return this;
}
function Facet() {
    this.FacetArea = "#facetArea";
    this.FacetItems = [];
    this.FacetPanels = [];
    this.OnQueryStarting = function (query) {
        $(document.body).trigger("GUI_FacetQueryOnStarting", query);
    };
    this.Helpers = {
        pThis: this,
        CurrentQuery: function () { return window.CurrentQueryContainer.CurrentQuery; },
        CreateLink: function (properties)
        {
            
            var propOnClick = (properties.onclick ? this.LinkDeserialize(properties.onclick) : false), panelEmptyVisibleValue = null;
            if (propOnClick && !propOnClick.remove) {
                panelEmptyVisibleValue = window.GeodiFacet.Settings.PanelEmptyVisibleValues.find(function (f) { return f.panelId === properties.panel.prop("id") });
            }
            
            
            var childItems = null;

            var proptext = properties.html;
            if (proptext && proptext.toLowerCase().indexOf("gqcomment") == 0) {
                var px0 = proptext.indexOf(":(");
                var px = proptext.indexOf(") ", px0);
                if (px0 != -1 && px != -1)
                    proptext = proptext.substr(px0 + 2, (px - px0 - 2));
            }
                

            if (proptext.indexOf("\\") > -1) {
                properties.group = proptext;
                proptext = proptext.substr(proptext.indexOf("\\"), proptext.length - 1);
                proptext = proptext.split('\\').join("");
                properties.title = proptext;
            }

            
            var class2 = "";
            if (properties.remove)
                class2 = "_ttl_rm";
            if (properties.countBar)
                class2 = properties.countBar.style ? "_ttl" : "_ttl_2";

            var $innerHtmlSpan = $("<span/>", { class: class2, style: properties.htmlStyle, html: (typeof proptext !== 'undefined' ? proptext : "") });
            var $removeIcon = $("<span/>", { class: "img-delete16 img-button16", style: "vertical-align: middle;" });
            var elmProp = { class: properties.class, category: properties.category, style: properties.style, onclick: (properties.onclick ? properties.onclick : ""), title: (properties.title ? properties.title : "") };
            for (var key in elmProp) {
                if (elmProp[key] === "" || elmProp[key] === "undefined")
                    delete elmProp[key];
            }
            if (properties.onchange)
                elmProp.onchange = properties.onchange;

            var $element = $("<div/>", elmProp);

            $element.attr("itemid", properties.itemid);

            if (properties.checkBox) {
                properties.checkBox.type = "checkbox";
                properties.checkBox.style = "position: relative; top: 3px;";
            }

            var $checkBox = $("<input/>", properties.checkBox);

            if (typeof properties.icon !== 'undefined' && properties.icon) {
                var $iconImg = $("<img/>", { src: properties.icon.src, class: "img-default" });
                $element.append($iconImg);
            }
            if (properties.preElement)
                $element.append(properties.preElement);

            if ($innerHtmlSpan.text().length > 0) {
                $element.append($innerHtmlSpan);
            }

            if (properties.remove) {
                $element.attr("onclick", properties.onclick);
                $element.prepend($removeIcon);
            }
            else if (!properties.remove && properties.checkBox) {
                var $elementDv = $("<div/>", { class: "checkbox" });
                var $elementLabel = $("<label>", {});
                $elementLabel.append($checkBox);
                $elementLabel.append($('<span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>'));
                $elementLabel.append($element.html());
                $elementDv.append($elementLabel);
                $element.empty().append($elementDv);
            }
            if (typeof properties.countBar !== 'undefined') {
                var count = parseInt(properties.countBar.html);
                var flag = ((properties.countBar.html && count === NaN) || count === 0 ? false : true);
                var alwaysShow = (count == 0 && window.FacetAlwaysShowList && properties.cType && window.FacetAlwaysShowList[properties.cType.replace("enumarator","enumerator") + "_" + properties.itemid]);
                if (alwaysShow) {
                    properties.countBar.html = "0";
                    flag = true;
                }
                if (flag) {
                    var $countBar = $("<span/>", properties.countBar);
                    $countBar.html(properties.countBar.html);
                    $element.append($countBar);
                }
                if (!alwaysShow && count === 0) {
                    $element.addClass("facetEmptyItem");
                    properties.panel.header.$spanShowFacetEmptyItemBtn.show();
                    if (panelEmptyVisibleValue && panelEmptyVisibleValue.isVisible)
                        $element.show();
                }
            }
            
            if (properties.group && !properties.remove) {
                
                var pGroupTitle = properties.group.substr(0, properties.group.indexOf("\\"));
                var pGroup = properties.panel.find("div[title='" + pGroupTitle + "']").not(".facetSubItem");

                var groupData = window.GeodiFacet.Settings.GroupData.find(function (f) { return f.name === pGroupTitle; });
                if (!groupData) {
                    groupData = { name: pGroupTitle, expand: false, items: [{ name: $element.attr("title"), checked: (properties.checkBox.checked === "checked" || properties.checkBox.checked ? true : false) }] };
                    window.GeodiFacet.Settings.GroupData.push(groupData);
                }
                else {
                    var groupItem = groupData.items.find(function (f) { return f.name === $element.attr("title"); });
                    if (!groupItem) {
                        groupItem = { name: $element.attr("title"), checked: (properties.checkBox.checked === "checked" || properties.checkBox.checked ? true : false), items:[]};
                        groupData.items.push(groupItem);
                    }
                    else groupItem.checked = (properties.checkBox.checked === "checked" || properties.checkBox.checked ? true : false);
                }

                if (pGroup.length === 0) {
                    var countBarClone = $element.find("span.facetCountR");
                    if (countBarClone.length === 0) {
                        countBarClone = $("<span/>", { class: "facetCountR facetCountRPBar facetCountRPBarLarge"});
                    }
                    else {
                        countBarClone = countBarClone.clone();
                        countBarClone.find("span.facetCountR").attr("class", "facetCountR facetCountRPBar facetCountRPBarLarge");
                    }
                    pGroup = $("<div/>", { class: "facetLink facetLinkPLine checkboxGroup", title: pGroupTitle});

                    if (panelEmptyVisibleValue && panelEmptyVisibleValue.isVisible)
                        pGroup.show();
                    else
                        pGroup.hide();
                    var $pI = $("<i/>", { class: "checkboxGroupExpand " + (groupData && groupData.expand ? "negative" : "plus"), onclick: this.LinkSerialize({ functionName: "Helpers.ExpandCheckBoxGroup", category: pGroupTitle }), html: "&nbsp;" });
                    pGroup.append($pI);
                    var $checkboxDiv = $("<div/>", {class: "checkbox"});
                    var $label = $("<label/>", {});
                    var $checkbox = $("<input/>",  {type: "checkbox", onclick: 'GeodiFacet.CheckorUnChecked({ "id": "' + pGroupTitle + '", "type": "virtual", "remove": false},this)', style: "position: relative; top: 3px"});
                    var $spanCr = $("<span/>", {class: "cr", html: "<i class='cr-icon glyphicon glyphicon-stop notCheckedStyle'></i>"});
                    $label.append($checkbox);
                    $label.append($spanCr);
                    $label.append("<span class='_ttl_2'>" + pGroupTitle + "</span>");
                    $checkboxDiv.append($label);
                    pGroup.append($checkboxDiv);
                    countBarClone.attr("class", "facetCountR facetCountRPBar facetCountRPBarLarge");
                    countBarClone.attr("groupcount", countBarClone.html());
                    countBarClone.html("<b>"+countBarClone.html()+"</b>");
                    if (properties.countSum)
                        pGroup.append(countBarClone);
                    var $childItems = $("<div/>", {class: "childItems", style: (groupData && groupData.expand ? "" : "display: none")});
                    pGroup.append($childItems);

                }
                else {
                    if (properties.countSum) {
                        var val = $element.find("span.facetCountR").html();
                        if (!val) val = "";
                        var cNumb = Number(val.replace(/[.,]/g, '')) + Number(pGroup.find("span[groupcount]").attr("groupcount").replace(/[.,]/g, ''));
                        pGroup.find("span[groupcount]").attr("style", this.CreateCountBarStyle({ count: cNumb, foundDocument: cNumb }));
                        pGroup.find("span[groupcount]").attr("groupcount", cNumb);
                        pGroup.find("span[groupcount]").html(cNumb.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.'));
                    }
                    
                }
                var totalChecked = 0;
                groupData.items.filter(function (f) { if (f.checked) totalChecked++; });
                if (groupData.items.length > 0 && groupData.items.length === totalChecked)
                    this.FullChecked(pGroup);
                else if (groupData.items.length > 0 && totalChecked > 0 && groupData.items.length > totalChecked)
                    this.HalfChecked(pGroup);
                else if (totalChecked === 0)
                    this.NotChecked(pGroup);
                properties.panel.body.append(pGroup);
                childItems = pGroup.find("div.childItems");
                //$element.removeAttr("onclick");
                $element.addClass("facetSubItem");
                childItems.append($element);

                if(!$element.hasClass("facetEmptyItem"))
                    pGroup.show();
                childItems = null;
                if (pGroup.find(".facetEmptyItem").length === groupData.items.length)
                    pGroup.addClass("facetEmptyItem");
                if (properties.panel.body.find(".checkboxGroup:visible").length > 0)
                    properties.panel.body.addClass("facetLeftSpace");
                else properties.panel.body.removeClass("facetLeftSpace");
                delete properties.group;
            }
            return $element;
        },
        CreatePanel: function (properties) {
            var pThis = this.pThis;

            var $panel = $("<div/>", { class: "panel panel-default", id: properties.id.replace("#", "") });
            if (properties.style)
                $panel.attr("style", properties.style);

            $panel.header = $("<div/>", { class: "panel-heading", id: properties.id.replace("#", "") + "_header" });
            $panel.header.title = $("<span/>");
            $panel.body = $("<div/>", { class: "panel-body", id: properties.id.replace("#", "") + "_body" });
            $panel.body.addItem = function (link) {
                if ($panel.body.find("div[title='" + HttpUtility.ScriptEncode(link.attr("title")) + "']").length === 0)
                    $panel.body.append(link);
                $(document.body).trigger("facetPanelChange", [{ target: $panel, properties: properties }]);
            };
            if (properties.body) {
                if (properties.body.class)
                    $panel.body.addClass(properties.body.class);
            }
            if (properties.priorty)
                $panel.priorty = properties.priorty

            if (typeof properties.header !== 'undefined') {
                if (properties.header.title) {
                    $panel.header.html(properties.header.title);
                    $panel.header.append($panel.header.title);
                }
                if (properties.header.html)
                    $panel.header.append(properties.header.html);
                if (properties.chart) {
                    $panel.header.$chartSpan = $("<span/>", { class: "facetCountR", style: "cursor: pointer; top: 5px;", onclick: "GeodiFacet.Helpers.ShowPieChart('" + properties.id.replace("#", "") + "_chartArea" + "')" });
                    $panel.header.$chartSpan.html("<i class='fa fa-pie-chart'></i>");
                    $panel.header.append($panel.header.$chartSpan);
                }
                if (properties.visibleEmptyBtn) {
                    $panel.header.css("position", "relative");
                    $panel.header.$spanShowFacetEmptyItemBtn = $("<span/>", { class: "showFacetEmptyItemBtn", onclick: "window.GeodiFacet.Helpers.ShowOrHideFacetEmptyItem(\"" + properties.id + "\")", html: '<i class="fa fa-eye-slash" style="font-size: 20px !important"></i>' });
                    $panel.header.$spanShowFacetEmptyItemBtn.insertBefore($panel.header.$chartSpan);
                }

                $panel.append($panel.header);
            }
            $panel.append($panel.body);

            if (properties.chart) {
                $panel.chart = $("<div/>", { id: properties.id.replace("#", "") + "_chartArea", class: "panel-body chartArea", style: "display: none;" });
                $panel.append($panel.chart);
            }
            if (properties.clear) {
                $panel.body.clear = function () {
                    $panel.body.empty();
                    $(document.body).trigger("facetPanelChange", [{ target: $panel, properties: properties}]);
                };
                if (properties.chart)
                    $panel.chart.clear = function () {
                        $panel.chart.empty();
                    };
            }

            var facetArea = this.pThis.FacetArea;

            $panel.priorty = (properties.priorty ? properties.priorty : 5);
            var fIndex = pThis.FacetPanels.findIndex(function (panel) { return panel.prop("id") === $panel.prop("id") });
            if (fIndex < 0)
                pThis.FacetPanels.push($panel);
            else
                pThis.FacetPanels[fIndex] = $panel;

            pThis.FacetPanels.AddedPriorties = [];
            $(facetArea).empty();
            pThis.FacetPanels.sort(function (a, b) { return b.priorty - a.priorty }).forEach(function (panel) {
                pThis.FacetPanels.AddedPriorties.push(panel.priorty);
                if (pThis.FacetPanels.AddedPriorties.max() === panel.priorty)
                    $(facetArea).prepend(panel);
                else
                    $(facetArea).append(panel);

            });
            
            return $panel;
        },
        ConvertObjToArray: function (rObj) {
            var arr = [];
            for (var key in rObj) {
                var obj = {};
                obj.id = key;
                obj.value = rObj[key];
                arr.push(obj);
            }
            return arr;
        },
        CreateCountBarStyle: function (params) {
            var pStyle = '', pValue = '';
            if (params.foundDocument)
                pValue = (params.count / params.foundDocument) * 100;
            if (!params.direction) params.direction = "to right";
            var val = "linear-gradient(" + params.direction + ", " + (window.FacetPercentColor ? window.FacetPercentColor : "orange") + " " + (pValue) + "%, #DEDEDE " + (pValue) + "%)" //rgb(48, 228, 48)
            if (!params.onlyVal)
                val = "background: " + val + ";";
            return val;
        },
        CreateChart: function (params) {
            if (!params.data || params.data.length < 1) {
                $(params.chart).parent("div.panel-default").find("div.panel-heading").children("span.facetCountR").hide();
                return;
            }
            else
                $(params.chart).parent("div.panel-default").find("div.panel-heading").children("span.facetCountR").show();
            var pieArea = $(params.chart);
            var pieDom = pieArea[0];
            if (!params.chartType) params.chartType = 'pie';
            if (pieDom.PieChart)
                pieDom.PieChart.destroy();

            var cdata = [];
            params.data.forEach(function (f) {
                cdata.push(f.ChartItem);
            });

            pieDom.LinkData = params.data;

            pieDom.PieChart = c3.generate({
                bindto: "#" + params.chart.prop("id"),
                size: {
                    height: 270,
                    width: 280
                },
                data: {
                    columns: cdata,
                    type: params.chartType,
                    order: 'desc',
                    onclick: function (d, el) {

                        var dm = $(el).parents(".chartArea");
                        var data = dm[0].LinkData.find(function (f) { return f.ChartItem[0] === d.id });

                        if (data) {
                            var linkData = data.LinkData;
                            FacetLink(linkData.id, linkData.Category, false, true);
                        }

                    }
                },
                legend: {
                    show: params.hideLegend ? false : true
                }
            });
        },
        GetCurrentNamesOrderList: function (namesOrder) {
            var findNamesOrder = window.GeodiFacet.Settings.NamesOrderList.find(function (f) { return f.namesOrder === namesOrder });
            if (!findNamesOrder) {
                findNamesOrder = { namesOrder: namesOrder, totalCheckbox: 0, totalChecked: 0 };
                window.GeodiFacet.Settings.NamesOrderList.push(findNamesOrder);
            }
            return findNamesOrder;
        },
        ContentTypeData: function (params) {
            var returnObj = [];
            var data = window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders;
            if (data && data.NamesOrder.length > 0) {
                data.NamesOrder.forEach(function (namesOrder) {
                    var names = data.Names[namesOrder];
                    if (names.AllTyes && names.AllTyes.length > 0)
                        returnObj.push({ namesOrder: namesOrder, names: data.Names[namesOrder] });
                });
            }
            return returnObj;
        },
        CreateNamesOrderList: function (params) {
            var arr = [];
            this.ContentTypeData(params).forEach(function (item) {
                var namesOrder = { namesOrder: item.namesOrder, totalCheckbox: 0, totalChecked: 0 };
                arr.push(namesOrder);
            });
            return arr;
        },
        ResetNamesOrderList: function () {
            window.GeodiFacet.Settings.NamesOrderList.update(function (f) { return f; }, { totalCheckbox: 0, totalChecked: 0, checked: false });
        },
        CurrentQueryAddorRemove: function (params, pThis) {
            var currentQuery = this.CurrentQuery();
            if (!params.id) return;
            if (!currentQuery[params.CurrentQueryItemKey]) currentQuery[params.CurrentQueryItemKey] = (params.CurrentQueryItemKey === "RecognizerIDs" ? "" : []);

            if (!params.noArray) {
                var items = (!Array.isArray(currentQuery[params.CurrentQueryItemKey]) ? (currentQuery[params.CurrentQueryItemKey] === "" ? [] : currentQuery[params.CurrentQueryItemKey].split(',')) : currentQuery[params.CurrentQueryItemKey]);
                var fItems = items.filter(function (f) { return f === (typeof items[0] === "number" ? params.id : params.id.toString()); });


                if (params.remove) {
                    if (fItems.length > 0) {
                        for (var i = 0; i < fItems.length; i++) {
                            var index = items.findIndex(function (f) { return f === (typeof fItems[0] === "number" ? fItems[i] : fItems[i].toString()); });
                            if (index > -1)
                                items.splice(index, 1);
                        }
                        if (!Array.isArray(currentQuery[params.CurrentQueryItemKey]))
                            currentQuery[params.CurrentQueryItemKey] = items.join(',');
                        else
                            currentQuery[params.CurrentQueryItemKey] = items;
                    }
                    return true;
                }
                else {
                    if (fItems.length === 0) {
                        items.push(params.id);
                        if (!Array.isArray(currentQuery[params.CurrentQueryItemKey]))
                            currentQuery[params.CurrentQueryItemKey] = items.join(',');
                        else
                            currentQuery[params.CurrentQueryItemKey] = items;
                        return true;
                    }
                }
            } else currentQuery[params.CurrentQueryItemKey] = params.id;
            
           

        },
        FacetModeSwap: function (panel, obj) {

            var allArea = $("#" + panel);
            var listArea = $("#" + panel + "_body");
            var pieArea = $("#" + panel + "_chartArea");

            if (allArea.attr("fmode") === 0) {
                listArea.hide();
                pieArea.fadeIn("fast");
                $(obj).css("color", "black");
                /*if (pieArea[0].PieChart)
                    pieArea[0].PieChart.flush();*/
                allArea.attr("fmode", 1);
            }
            else {
                pieArea.hide();
                $(obj).css("color", "gray");
                listArea.fadeIn("fast");
                allArea.attr("fmode", 0);
            }
        },
        LinkSerialize: function (params) {
            var fncName = "GeodiFacet.";
            fncName += (params.functionName ? params.functionName : "AddorRemove");
            if (typeof params.remove !== 'undefined' && !params.remove)
                params.remove = false;
            delete params.functionName;
            return fncName + "(" + JSON.stringify(params) + ",this)";
        },
        LinkDeserialize: function (fncData) {
            var data = fncData.split('({');
            var rSplitData = "";
            for (var i = 1; i < data.length; i++)
            {
                rSplitData += data[i];
                if ((data.length - 1) === i) return JSON.parse("{" + rSplitData.replace(",this)", ""));

            }
        },
        ShowPieChart: function (chartElement) {
            var element = $("#" + chartElement);
            if (element.css("display") === "none" && element.children().length > 0) {
                element.parent().find("div.panel-body").eq(0).hide();
                element.show();
            } else {
                element.hide();
                element.parent().find("div.panel-body").eq(0).show();
            }
        },
        GetLinkChecked: function (params) {
            if (params.queryData)
                return params.queryData.indexOf(params.id) > -1;
            return false;
        },
        ExpandCheckBoxGroup: function (params, from) {
            var doExpand = false;
            var groupData = window.GeodiFacet.Settings.GroupData.find(function (f) { return f.name === params.category });
            if (!groupData) {
                groupData = { name: params.category, expand: false, items:[] };
                window.GeodiFacet.Settings.GroupData.push(groupData);
            }

            if (!groupData.expand) doExpand = true;
            else doExpand = false;


            if (doExpand) {
                $(from).removeClass("plus").addClass("negative");
                groupData.expand = true;
                $(from).parent().find(".childItems").fadeIn("fast");

            } else {
                $(from).removeClass("negative").addClass("plus");
                groupData.expand = false;
                $(from).parent().find(".childItems").fadeOut("fast");
            }

        },
        ShowOrHideFacetEmptyItem: function (id) {
            var $panel = this.pThis.FacetPanels.find(function (f) { return f.prop("id") === id; });
            if (!$panel) return;

            var panelEmptyVisibleValue = window.GeodiFacet.Settings.PanelEmptyVisibleValues.find(function (f) { return f.panelId === id; });
            if (!panelEmptyVisibleValue) {
                panelEmptyVisibleValue = { panelId: id, isVisible: false };
                window.GeodiFacet.Settings.PanelEmptyVisibleValues.push(panelEmptyVisibleValue);
            }

            var doVisibleEmptyItem = false;
            if (!panelEmptyVisibleValue.isVisible)
                doVisibleEmptyItem = true;

            if (doVisibleEmptyItem) {
                $panel.body.find(".facetEmptyItem").fadeIn("fast");
                $panel.body.find(".checkboxGroup").fadeIn("fast");
                $panel.header.$spanShowFacetEmptyItemBtn.find("i").attr("class", "fa fa-eye");
                panelEmptyVisibleValue.isVisible = true;
            }
            else {
                panelEmptyVisibleValue.isVisible = false;
                $panel.body.find(".facetEmptyItem").fadeOut("fast");
                $panel.body.find(".checkboxGroup").each(function () { if ($(this).find(".facetLink").not(".facetEmptyItem").length > 0) $(this).fadeIn("fast"); else $(this).fadeOut("fast") });
                $panel.header.$spanShowFacetEmptyItemBtn.find("i").attr("class", "fa fa-eye-slash");
            }
        },
        HalfChecked: function (elm) {
            var checkbox = ($(elm).prop("tagName").toLowerCase() === "input" && $(elm).prop("type") === "checkbox") ? elm : elm.find("[type=\"checkbox\"]:eq(0)");
            checkbox.removeAttr("checked");
            checkbox.addClass("halfCheckedCheckbox");
            checkbox.parent().find("span.cr i").attr("class", "cr-icon glyphicon glyphicon-stop halfCheckedGroup");
        },
        FullChecked: function (elm) {
            var checkbox = ($(elm).prop("tagName").toLowerCase() === "input" && $(elm).prop("type") === "checkbox") ? elm : elm.find("[type=\"checkbox\"]:eq(0)");
            var sParams = this.LinkDeserialize($(checkbox).attr("onclick"));
            sParams.remove = true;
            sParams.functionName = "CheckorUnChecked";
            checkbox.attr("checked", true);
            checkbox.attr("onclick", this.LinkSerialize(sParams));
            checkbox.removeClass("halfCheckedCheckbox");
            checkbox.parent().find("span.cr i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");

        },
        NotChecked: function (elm) {
            var checkbox = ($(elm).prop("tagName").toLowerCase() === "input" && $(elm).prop("type") === "checkbox") ? elm : elm.find("[type=\"checkbox\"]:eq(0)");
            checkbox.removeAttr("checked");
            checkbox.removeClass("halfCheckedCheckbox");
            checkbox.parent().find("span.cr i").attr("class", "cr-icon glyphicon glyphicon-stop notCheckedStyle");
        },
        GetGUITotalChecked: function (fnamesOrder, from) {
            fnamesOrder.totalChecked = 0;
            $(from).parents(".childItems").find("input[type=checkbox]").each(function () {
                if ($(this)[0].checked)
                    fnamesOrder.totalChecked++;
            });
            return fnamesOrder;
        }
    };
    this.Settings = { GroupData: [], NamesOrderList: [], PanelEmptyVisibleValues: [] };
    this.Init = {};
    this.Clear = function () {
        this.FacetPanels.forEach(function (panel) {
            if (panel.body.clear) {
                panel.body.clear();
            }
            if (panel.chart) {
                panel.chart.clear();
            }
        });
        this.FacetItems.forEach(function (facetItem) {
            if (facetItem.ChartData)
                facetItem.ChartData = [];
        });
    };
    this.AddFacetItem = function (newFacetItem) {
        this.FacetItems.push(newFacetItem);
        return newFacetItem;
    };
    this.Render = function (params, callerQuery, callerQueryIsEmpty,forUpdate) {
        if (!params)
            params = this.__LastParams;
        else
            this.__LastParams = params;

        if (forUpdate) {
            this.BBOX = params && params.data && params.data.BBOX ? [params.data.BBOX.MinX, params.data.BBOX.MinY, params.data.BBOX.MaxX, params.data.BBOX.MaxY] : null;
            if (this.BBOX) {
                var lDisableFitExtent = (window.StartupView == "MAP" && !this.__IsNextGoMap);
                var that = this;
                window.setTimeout(function () {
                    ___PCommunicationCommander_OnMessageJson_Internal({ Command: "GoMap", Arg: "*", Args: { BBOX: that.BBOX, SetExtent: true, Ignorable: true, DisableFitExtent: lDisableFitExtent, Smart: true } }, true);
                }, 400);
                
                this.__IsNextGoMap = true;
            }
        }

        if (!params) return;
        if (!window.CurrentQueryContainer.FullFacet)
            window.CurrentQueryContainer.FullFacet = {};
        if (callerQuery && !window.CurrentQueryContainer._FullFacetReady) {
            if (window.DefaultServerSourceList && window.DefaultServerSourceList.length > 0) {
                window.CurrentQueryContainer._FullFacetReady = true;
                var fct = {};
                for (var index = 0; index < window.DefaultServerSourceList.length; index++) {
                    var qe = new GeodiQuery();
                    qe.CurrentSourceIndex = index;
                    qe.SourceList = window.DefaultServerSourceList;
                    qe.UpdateSource();
                    qe.GetFacet(function (data) {
                        if (data) {
                            ExtendObjectWithDecimalAdd(fct, data);
                            window.CurrentQueryContainer.FullFacet = fct;
                            GeodiFacet.Render();
                        }
                    });
                }

            }
            else if (callerQueryIsEmpty && params && params.data) {
                window.CurrentQueryContainer.FullFacet = params.data;
                window.CurrentQueryContainer._FullFacetReady = true;
            }
            else {
                var qe = new GeodiQuery();
                if (callerQuery.wsName) qe.wsName = callerQuery.wsName;
                qe.GetFacet(function (data) {
                    window.CurrentQueryContainer.FullFacet = data;
                    window.CurrentQueryContainer._FullFacetReady = true;
                    GeodiFacet.Render();
                });
            }
        }
        if (this.Settings.NamesOrderList.length === 0)
            this.Settings.NamesOrderList = this.Helpers.CreateNamesOrderList(params);
        else this.Helpers.ResetNamesOrderList();

        var pThis = this;
        pThis.Clear();
        if (!window.CurrentQueryContainer.FullFacet.DocForContentType)
            window.CurrentQueryContainer.FullFacet.DocForContentType = {};
        this.FacetItems.forEach(function (facetItem) {
            if (params && params.data && params.data.DocForContentType) {
                for (var key in params.data.DocForContentType) {
                    var val = params.data.DocForContentType[key];
                    if (!window.CurrentQueryContainer.FullFacet.DocForContentType.hasOwnProperty(key))
                        window.CurrentQueryContainer.FullFacet.DocForContentType[key] = val;
                }
            }
            facetItem.Render($.extend(params, { serverInfo: params.data, fullFacet: window.CurrentQueryContainer.FullFacet, clientTime: params.cTime, serverTime: params.sTime }), pThis);
        });
        $(document.body).trigger("GUI_FacetReady");
    };
    this.CheckorUnChecked = function (params, target) {
        var pThis = this;
        var pThisHelper = this.Helpers;
        if (params.type === "virtual") {
            var grp = this.Settings.GroupData.find(function (f) { return f.name === params.id });
            if (grp && grp.items)
                $.each(grp.items, function (a, b) { b.checked = params.remove ? false : true; });
            if (params.remove) {
                $(target).parents(".facetLink").find(".childItems input[type=checkbox]").each(function () {
                    if ($(this).prop("checked")) $(this).click();
                    $(this).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok notCheckedStyle");
                });
                $(target).next("span.cr").find("i").attr("class", "cr-icon notCheckedStyle");
                $(target).removeAttr("checked");
            }
            else {
                $(target).parents(".facetLink").find(".childItems input[type=checkbox]").each(function () {
                    if (!$(this).prop("checked")) $(this).click();
                    $(this).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");

                });
                $(target).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");
                $(target).attr("checked", true);
            }

            params.remove = !params.remove;
            $(target).attr("onclick", pThisHelper.LinkSerialize(params));

            return;
        }
        params.functionName = "CheckorUnChecked";

        if (params.type === "cgroup") {
            var fnamesOrder = window.GeodiFacet.Settings.NamesOrderList.find(function (f) { return f.namesOrder === params.id });
            if (params.remove) {

                $(target).next("span.cr").find("i").attr("class", "cr-icon notCheckedStyle");
                $(target).removeAttr("checked");
                $(target).parents("div.checkboxGroup").find("div.facetLink input[type=checkbox]").each(function () {
                    var sParams = pThisHelper.LinkDeserialize($(this).attr("onclick"));
                    sParams.remove = true;
                    $(this).removeAttr("checked");
                    $(this).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok notCheckedStyle");
                    var facetItem = pThis.FacetItems.find(function (f) { return f.Name === "ctype" });
                    if (facetItem)
                        facetItem.Remove(sParams, pThis);
                });
                fnamesOrder.checked = false;
                fnamesOrder.totalChecked = 0;
            }
            else {


                $(target).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");
                $(target).attr("checked", true);

                $(target).parents("div.checkboxGroup").find("div.facetLink input[type=checkbox]").each(function () {
                    var sParams = pThisHelper.LinkDeserialize($(this).attr("onclick"));
                    sParams.remove = true;
                    $(this).removeAttr("onclick").attr("onclick", pThisHelper.LinkSerialize(sParams));
                    $(this).attr("checked", "checked");
                    $(this).next("span.cr").find("i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");
                    var facetItem = pThis.FacetItems.find(function (f) { return f.Name === "ctype" });
                    if (facetItem)
                        facetItem.Remove(sParams, pThis);
                    fnamesOrder.totalChecked++;
                });
                fnamesOrder.checked = true;


            }

        }
        else if (params.type === "ctype")
        {
            var parentCheckbox = $(target).parents("div.checkboxGroup").find("div.checkbox:eq(0) input[type=checkbox]:eq(0)");
            var fnamesOrder = window.GeodiFacet.Settings.NamesOrderList.find(function (f) { return f.namesOrder === $(target).parents("div.checkboxGroup").attr("title") });
            var parentCheckboxParams = pThisHelper.LinkDeserialize(parentCheckbox.attr("onclick"));
            if (params.remove) {
                fnamesOrder.checked = false;
                if (fnamesOrder.totalChecked > 0)
                    fnamesOrder.totalChecked--;

                var facetItem = window.GeodiFacet.FacetItems.find(function (f) { return f.Name === "cgroup" });
                if (facetItem)
                    facetItem.Remove(parentCheckboxParams, pThis);

                $(target).parents(".childItems").find("input[type=checkbox]").each(function () {
                    var sParams = pThisHelper.LinkDeserialize($(this).attr("onclick"));
                    var facetItem = pThis.FacetItems.find(function (f) { return f.Name === "ctype" });

                    if ($(this)[0].checked) {
                        if (facetItem)
                            facetItem.Add(sParams, pThis);
                    } else {
                        if (facetItem)
                            facetItem.Remove(sParams, pThis);
                    }


                });

            }
            else {
                fnamesOrder.totalChecked++;
                if (fnamesOrder.totalChecked === fnamesOrder.totalCheckbox)
                    fnamesOrder.checked = true;
                $(target).attr("checked", "checked");
                $(target).next("span.cr").children("i").attr("class", "cr-icon glyphicon glyphicon-ok checkedStyle");

            }



            var totalGUIChecked = pThisHelper.GetGUITotalChecked(fnamesOrder, target).totalChecked;

            if (totalGUIChecked === fnamesOrder.totalCheckbox) {
                pThisHelper.FullChecked(parentCheckbox);
                parentCheckboxParams.remove = false;

            }
            else if (totalGUIChecked > 0 && totalGUIChecked < fnamesOrder.totalCheckbox) pThisHelper.HalfChecked(parentCheckbox);
            else if (totalGUIChecked < 1) pThisHelper.NotChecked(parentCheckbox, "cgroup");



        }

        this.AddorRemove(params, target);
        params.remove = !params.remove;
        $(target).attr("onclick", pThisHelper.LinkSerialize(params));
    };
    this.AddorRemove = function (params, target) {
        var pThis = this;

        var checkBox = null;

        if (target) {
            checkBox = $(target).find("input[type=checkbox]:eq(0)");
            if ($(target).prop("tagName").toLowerCase() === "input" && $(target).prop("type") == "checkbox")
                checkBox = $(target);
        }



        var checkBoxVal = null;
        if (checkBox && checkBox.length > 0)
            checkBoxVal = $(checkBox)[0].checked;


        var sParams = { id: params.id, Type: params.type, Op1: params.op1, Op2: params.op2, From: target };

        var facetItem = this.FacetItems.find(function (f) { return f.Name === params.type });

        params.remove = (checkBoxVal === null ? params.remove : !checkBoxVal);

        if (facetItem) {
            var changed = false;
            if (params.remove)
                changed = facetItem.Remove(sParams, pThis);
            else if (!params.remove)
                changed = facetItem.Add(sParams, pThis);

            if (changed) {
                if (window.DisableFacetTimeout) {
                    doQueryText(true);
                }
                else {
                    window.clearTimeout(window.__TFC);
                    window.__TFC = window.setTimeout(function () {
                        doQueryText(true);
                    },
                    window.FacetTimeout ? window.FacetTimeout : 800);
                }
            }

        }


    };
    $(document.body).on("facetPanelChange", function (e, arg) {
        if (arg.properties && arg.properties.id === "fc_enumarator_p" && $(".NotHidable", arg.target.body).length === 0 && $(".FacetEnum", arg.target.body).length === 1) {
            $(arg.target).hide();
            return;
        }
        if ($(arg.target.body).find("div").length < 1) $(arg.target).hide();
        else $(arg.target).show();
    });
}
$(document).ready(function () {

    if (!window.GeodiFacet)
        window.GeodiFacet = new Facet();
    else {
        window.GeodiFacet.FacetItems = [];
        window.GeodiFacet.FacetPanels = [];
    }
    window.GeodiFacet.detailPanel = window.GeodiFacet.Helpers.CreatePanel({ id: "fc_detail_p", priorty: 10, clear: true, header: { title: "[$query:content_properties/js]" } });
    var contentTypePanel = window.GeodiFacet.Helpers.CreatePanel({
        id: "fc_contentType_p", priorty: 20, clear: true, chart: true, style: "padding-bottom: 15px;", header: { title: "[$query:type/js]" }, visibleEmptyBtn: true
    });
    window.GeodiFacet.QueryPanel = window.GeodiFacet.Helpers.CreatePanel({ id: "fc_query_p", clear: true, priorty: 50, style: "display: none", header: { title: "[$default:query_page/js]" } });
    window.GeodiFacet.AddFacetItem({
        Name: "status",
        Target: window.GeodiFacet.Helpers.CreatePanel({ id: "fc_status_p", priorty: 60, body: { class: "fc-status" } }),
        Render: function (params, pThis) {
            var body = this.Target.body;
            $(body).empty();
            if (!window.IsTestMode) {
                var clientTimeTitle = "";
                if (params.clientTime >= 0)
                    clientTimeTitle = (params.clientTime / 1000).toFixed(2) + " s network";
                if (params.serverTime >= 0)
                    $(body).append("<div>" + (params.serverTime / 1000).toFixed(2) + " s <span style='color:#AAA;font-size:12px;'>( " + clientTimeTitle + " )</span></div>");
                else if (clientTimeTitle)
                    $(body).append("<div>" + clientTimeTitle + " </div>");
            }

            var fnsSiteStr = "[$query:format_total_site_count/js]";
            if (params.serverInfo.SiteGroupCount && fnsSiteStr)
                fnsSiteStr = ", " + fnsSiteStr.replace("{0}", FormatNumber(params.serverInfo.SiteGroupCount));
            else
                fnsSiteStr = "";

            var formatTxt = "[$query:format_total_info/js]";
            if (params.serverInfo.FoundDocument > 1)
                formatTxt = "[$query:format_total_info_multiple/js]";
            $(body).append("<div>" + formatTxt.replace("{0}", FormatNumber(params.serverInfo.FoundDocument)).replace("{1}", FormatNumber(params.serverInfo.TotalDocument)) + fnsSiteStr + "  </div>");


            var pStyle = pThis.Helpers.CreateCountBarStyle({ count: params.serverInfo.FoundDocument, foundDocument: params.serverInfo.TotalDocument, onlyVal: true });
            $(body).css("background", pStyle);
        }

    });
    window.GeodiFacet.AddFacetItem({
        Name: "enumarator",
        Target: window.GeodiFacet.Helpers.CreatePanel({
            id: "fc_enumarator_p", priorty: 40, header: { title: "[$query:enumarators/js]" }, clear: true, chart: true, visibleEmptyBtn: true
        }),
        ChartData: [],
        Helpers: {
            GetWsInfoData: function () {
                if (window.CurrentQueryContainer.CurrentWSInfo && window.CurrentQueryContainer.CurrentWSInfo.Enumarators)
                    return window.CurrentQueryContainer.CurrentWSInfo.Enumarators;
                return null;
            },
            GetWsInfoItem: function (id) {
                return window.CurrentQueryContainer.CurrentWSInfo.Enumarators.find(function (f) { return f.ItemHashCode === id; });
            },
            AddChartData: function (enumarator, fThis) {
                if (!enumarator.value || enumarator.value === 0) return;
                var displayName = enumarator.DisplayName;
                if (displayName.indexOf("\\") > -1)
                    displayName = displayName.substr(displayName.indexOf("\\"), displayName.length - 1).split('\\').join("");

                var obj = { LinkData: { Id: enumarator.ItemHashCode, Category: fThis.Name }, ChartItem: [displayName, enumarator.value] };
                fThis.ChartData.push(obj);
            }
        },
        CurrentQueryItemKey: "Enumarators",
        Render: function (params, pThis) {
            var fThis = this, linkProperties = null, serverInfo = params.serverInfo, currentQuery = pThis.Helpers.CurrentQuery();
            this.GetLinkProperties = function (wsInfoItem) {
                if (typeof wsInfoItem === "string" || typeof wsInfoItem === "number")
                    wsInfoItem = this.Helpers.GetWsInfoItem(wsInfoItem);
                params.Name = fThis.Name;
                params.wsInfoItem = (wsInfoItem ? wsInfoItem : { IconName: "layer/geodi", DisplayName: ' ? ', ItemHashCode: null });
                params.panel = fThis.Target;
                linkProperties = new GeodiFacetLink(params, pThis);
                if (params.wsInfoItem.SupportFolderFeed)
                    linkProperties.class += " NotHidable";
                linkProperties.chart = true;
                linkProperties.itemid = params.wsInfoItem.ItemHashCode;
                linkProperties.checkBox = { checked: pThis.Helpers.GetLinkChecked({ id: params.wsInfoItem.ItemHashCode, queryData: (currentQuery[this.CurrentQueryItemKey] ? currentQuery[this.CurrentQueryItemKey] : null) }) };
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: params.wsInfoItem.ItemHashCode, type: fThis.Name });
                return linkProperties;
            };

            var data = this.Helpers.GetWsInfoData();
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var enumarator = data[i];
                    if ((!params.serverInfo.DocForEnumarator || !params.serverInfo.DocForEnumarator.hasOwnProperty(enumarator.ItemHashCode)) &&
                        (!window.CurrentQueryContainer.FullFacet.DocForEnumarator || !window.CurrentQueryContainer.FullFacet.DocForEnumarator.hasOwnProperty(enumarator.ItemHashCode))) {
                        if ((i + 1) > (data.length - 1))
                            pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                        continue;
                    }


                    linkProperties = fThis.GetLinkProperties(enumarator);
                    if (linkProperties) {
                        enumarator.value = (params.serverInfo.DocForEnumarator ? params.serverInfo.DocForEnumarator[enumarator.ItemHashCode] : 0);
                        linkProperties.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: enumarator.value, foundDocument: serverInfo.FoundDocument }), html: FormatNumber(enumarator.value) };
                        linkProperties.cType = fThis.Name;
                        linkProperties.countSum = true;
                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);


                        fThis.Target.body.addItem($facetLink);
                        fThis.Helpers.AddChartData(enumarator, fThis);

                    }
                    if (i === (data.length - 1))
                        pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                }
            }
          
            if (fThis.Target.body.find(".facetEmptyItem").length === 0)
                fThis.Target.header.$spanShowFacetEmptyItemBtn.hide();
           

            if (currentQuery[fThis.CurrentQueryItemKey]) {
                currentQuery[fThis.CurrentQueryItemKey].forEach(function (enumaratorId) {

                    var linkProperties = fThis.GetLinkProperties(enumaratorId);
                    if (linkProperties) {
                        linkProperties.remove = true;
                        linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: enumaratorId, type: fThis.Name, remove: true });
                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                        window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                    }
                });
            }
            
        },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "recognizer",
        Target: window.GeodiFacet.Helpers.CreatePanel({
            id: "fc_recognizer_p", priorty: 30, header: { title: "[$query:layers/js]" }, clear: true, chart: true, visibleEmptyBtn: true,
        }),
        ChartData: [],
        Helpers: {
            GetWsInfoData: function () {
                if (window.CurrentQueryContainer.CurrentWSInfo && window.CurrentQueryContainer.CurrentWSInfo.Recognizers)
                    return window.CurrentQueryContainer.CurrentWSInfo.Recognizers;
                return null;
            },
            GetWsInfoItem: function (id) {
                return window.CurrentQueryContainer.CurrentWSInfo.Recognizers.find(function (f) { return f.ItemHashCode == id });
            },
            AddChartData: function (recognizer, fThis) {
                if (!recognizer.value || recognizer.value == 0) return;
                var displayName = recognizer.DisplayName;
                if (displayName.indexOf("\\") > -1)
                    displayName = displayName.substr(displayName.indexOf("\\"), displayName.length - 1).split('\\').join("");
                var obj = { LinkData: { Id: recognizer.ItemHashCode, Category: fThis.Name }, ChartItem: [displayName, recognizer.value] };
                fThis.ChartData.push(obj);
            }
        },
        CurrentQueryItemKey: "RecognizerIDs",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this, linkProperties = null, serverInfo = params.serverInfo;
            var panelEmptyVisibleValue = window.GeodiFacet.Settings.PanelEmptyVisibleValues.find(function (f) { return f.panelId === fThis.Target.prop("id") });
            this.GetLinkProperties = function (wsInfoItem) {
                if (typeof wsInfoItem === "string")
                    wsInfoItem = this.Helpers.GetWsInfoItem(wsInfoItem);
                params.Name = fThis.Name;
                params.wsInfoItem = (wsInfoItem ? wsInfoItem : { IconName: "layer/geodi", DisplayName: ' ? ', ItemHashCode: null });
                params.panel = fThis.Target;
                linkProperties = new GeodiFacetLink(params, pThis);
                linkProperties.chart = true;
                linkProperties.checkBox = {};
                linkProperties.itemid = params.wsInfoItem.ItemHashCode;
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: params.wsInfoItem.ItemHashCode, type: fThis.Name });
                return linkProperties;
            };

            var data = this.Helpers.GetWsInfoData();
            if (data)
            {
                for (var i = 0; i < data.length; i++)
                {
                    var recognizer = data[i];
                    if ((!params.serverInfo.DocForRecongizer || !params.serverInfo.DocForRecongizer.hasOwnProperty(recognizer.ItemHashCode)) &&
                        (!window.CurrentQueryContainer.FullFacet.DocForRecongizer || !window.CurrentQueryContainer.FullFacet.DocForRecongizer.hasOwnProperty(recognizer.ItemHashCode))) {
                        if ((i + 1) > (data.length - 1)) 
                            pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                        continue;
                    }

                    linkProperties = fThis.GetLinkProperties(recognizer);
                    if (linkProperties) {

                        var varKwValue = (params.serverInfo.KWForRecongizer ? params.serverInfo.KWForRecongizer[recognizer.ItemHashCode] : null);
                        var varDocValue = (params.serverInfo.DocForRecongizer ? params.serverInfo.DocForRecongizer[recognizer.ItemHashCode] : null);

                        recognizer.value = (params.serverInfo.KWForRecongizer ? varKwValue : varDocValue);
                        recognizer.checked = currentQuery[fThis.CurrentQueryItemKey] && currentQuery[fThis.CurrentQueryItemKey].indexOf(recognizer.ItemHashCode) > -1;
                        linkProperties.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: recognizer.value, foundDocument: serverInfo.FoundDocument }), html: FormatNumber(recognizer.value) };
                        linkProperties.cType = fThis.Name;
                        if (recognizer.checked) linkProperties.checkBox.checked = "checked";

                        if (params.serverInfo.KWForRecongizer && varDocValue)
                            linkProperties.html += "<br> <small class=doc-count>" + ("[$query:in_count_query/js]".replace("{0}", varDocValue)) + " </small>";

                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                        if (params.serverInfo.KWForRecongizer)
                            $(".img-default", $facetLink).addClass("has-doc-dount");

                        fThis.Target.body.addItem($facetLink);
                        fThis.Helpers.AddChartData(recognizer, fThis);

                    }
                    if (i === (data.length - 1))
                        pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                }
               
            }


            if (fThis.Target.body.find(".facetEmptyItem").length === 0)
                fThis.Target.header.$spanShowFacetEmptyItemBtn.hide();
            
            if (currentQuery[fThis.CurrentQueryItemKey]) {
                data = currentQuery[fThis.CurrentQueryItemKey].split(',');
                data.forEach(function (recognizerId) {
                    var linkProperties = fThis.GetLinkProperties(recognizerId);
                    if (linkProperties) {
                        linkProperties.remove = true;
                        linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: recognizerId, type: fThis.Name, remove: true });
                        var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                        window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                    }
                });
            }

        },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "datequery",
        Target: window.GeodiFacet.detailPanel,
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = {
                icon: { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=Layer/date", true, true) },
                class: "facetLink FacetEnum"
            };
            var txt = "";
            if (currentQuery.DocDateStart && currentQuery.DocDateStart.UTC) {
                var d1 = new moment(parseNetDate(currentQuery.DocDateStart.UTC));
                txt = d1.format("L");
            }
            if (currentQuery.DocDateEnd && currentQuery.DocDateEnd.UTC) {
                var d2 = new moment(parseNetDate(currentQuery.DocDateEnd.UTC));
                txt += (txt ? " - " : "") + d2.format("L");
            }
            if (txt) {
                linkProperties.html = txt;
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ type: fThis.Name, remove: true });
                linkProperties.remove = true;
                var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink);
            }
        },
        Add: function (params, pThis) {
            return false;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            delete currentQuery.DocDateStart;
            delete currentQuery.DocDateEnd;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "hidenquery",
        Target: window.GeodiFacet.detailPanel,
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            if (currentQuery.SearchStrings && currentQuery.SearchStrings.length > 0) {
                for (var i = 0; i < currentQuery.SearchStrings.length; i++) {

                    var linkProperties = {
                        class: "facetLink FacetEnum"
                    };
                    linkProperties.html = currentQuery.SearchStrings[i];
                    linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: i, type: fThis.Name, remove: true });
                    linkProperties.remove = true;
                    var $facetLink = pThis.Helpers.CreateLink(linkProperties);
                    window.GeodiFacet.QueryPanel.body.addItem($facetLink);
                }
            }
        },
        Add: function (params, pThis) {
            return false;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            try {
                currentQuery.SearchStrings.splice(params.id, 1);
            } catch (e) { }
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "geom",
        Target: window.GeodiFacet.detailPanel,
        CurrentQueryItemKey: "GeometryStatus",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = {
                icon: { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=Layer/geography", true, true) },
                class: "facetLink FacetEnum",
                panel: window.GeodiFacet.detailPanel
            };


            if (currentQuery[fThis.CurrentQueryItemKey] !== 1) {
                linkProperties.html = "<span class='img-not12'>&nbsp;</span>  [$query:geometry_status_null/js]";
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: 1, type: fThis.Name });
                var $facetLink1 = pThis.Helpers.CreateLink(linkProperties);
                fThis.Target.body.addItem($facetLink1);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] !== 2) {
                linkProperties.html = "[$query:geometry_status_not_null/js]";
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: 2, type: fThis.Name });
                var $facetLink2 = pThis.Helpers.CreateLink(linkProperties);
                fThis.Target.body.addItem($facetLink2);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] === 1) {
                linkProperties.html = "<span class='img-not12'>&nbsp;</span>  [$query:geometry_status_null/js]";
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: 2, type: fThis.Name, remove: true });
                linkProperties.remove = true;
                var $facetLink3 = pThis.Helpers.CreateLink(linkProperties);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink3);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] === 2) {
                linkProperties.html = "[$query:geometry_status_not_null/js]";
                linkProperties.onclick = pThis.Helpers.LinkSerialize({ id: 1, type: fThis.Name, remove: true });
                linkProperties.remove = true;
                var $facetLink4 = pThis.Helpers.CreateLink(linkProperties);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink4);
            }

        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = params.id;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = 3;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "label",
        Target: window.GeodiFacet.detailPanel,
        CurrentQueryItemKey: "LabelStatus",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = function () {
                this.icon = { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=geodi:.geodinote", true, true) },
                this.class = "facetLink FacetEnum";
                this.panel = window.GeodiFacet.detailPanel;
            };


            var linkProp = new linkProperties();
            linkProp.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: null, html: FormatNumber(params.serverInfo.TotalHasNote) }

            if (params.serverInfo.TotalHasNote > 0 && currentQuery[fThis.CurrentQueryItemKey] != 1 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "[$query:fav_status_not_null/js]";
                linkProp.onclick = pThis.Helpers.LinkSerialize({ id: 1, type: fThis.Name });
                linkProp.countBar.style = pThis.Helpers.CreateCountBarStyle({ count: params.serverInfo.TotalHasNote, foundDocument: params.serverInfo.FoundDocument });
                var $facetLink1 = pThis.Helpers.CreateLink(linkProp);
                fThis.Target.body.addItem($facetLink1);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] != 2 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                var pNotNoteCount = (params.serverInfo.FoundDocument - params.serverInfo.TotalHasNote);
                if (params.serverInfo.DocForContentType && params.serverInfo.DocForContentType["geodi:.geodinote"])
                    pNotNoteCount -= params.serverInfo.DocForContentType["geodi:.geodinote"];
                if (pNotNoteCount) {
                    linkProp.html = "<span class='img-not12'>&nbsp;</span>  [$query:fav_status_null/js]";
                    linkProp.onclick = pThis.Helpers.LinkSerialize({ id: 2, type: fThis.Name });
                    linkProp.countBar.style = pThis.Helpers.CreateCountBarStyle({ count: pNotNoteCount, foundDocument: params.serverInfo.FoundDocument });
                    linkProp.countBar.html = FormatNumber(pNotNoteCount);

                    var $facetLink2 = pThis.Helpers.CreateLink(linkProp);
                    fThis.Target.body.addItem($facetLink2);
                }

            }


            linkProp = new linkProperties();
            if (params.serverInfo.TotalHasNote > 0 && currentQuery[fThis.CurrentQueryItemKey] == 1 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "[$query:fav_status_not_null/js]";
                linkProp.onclick = pThis.Helpers.LinkSerialize({ id: 1, type: fThis.Name, remove: true });
                linkProp.remove = true;
                var $facetLink3 = pThis.Helpers.CreateLink(linkProp);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink3);
            }

            if (currentQuery[fThis.CurrentQueryItemKey] == 2 && !GUISearchOptions.Is(GUISearchOptions.HideNoteIcon)) {
                linkProp.html = "<span class='img-not12'>&nbsp;</span>  [$query:fav_status_null/js]";
                linkProp.onclick = pThis.Helpers.LinkSerialize({ id: 2, type: fThis.Name, remove: true });
                linkProp.remove = true;
                var $facetLink4 = pThis.Helpers.CreateLink(linkProp);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink4);
            }


        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = params.id;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = 3;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "nums",
        Target: window.GeodiFacet.detailPanel,
        CurrentQueryItemKey: "GetNumberOfOccurrences",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            var linkProperties = function () {
                this.icon = { src: HttpUtility.ResolveUrl("~/GUIJsonService?op=getIco&IconName=Action/shownumbers", true, true) },
                    this.class = "facetLink FacetEnum",
                    this.html = "[$query:show_number_of_occurrences/js]",
                    this.onclick = pThis.Helpers.LinkSerialize({ id: true, type: fThis.Name });
                this.panel = window.GeodiFacet.detailPanel;
            };
            if (!currentQuery[fThis.CurrentQueryItemKey]) {
                var linkProp1 = new linkProperties();
                var $facetLink1 = pThis.Helpers.CreateLink(linkProp1);
                fThis.Target.body.addItem($facetLink1);
            }
            if (currentQuery[fThis.CurrentQueryItemKey]) {
                var linkProp2 = new linkProperties();
                linkProp2.remove = true;
                linkProp2.onclick = pThis.Helpers.LinkSerialize({ id: true, type: fThis.Name, remove: true });
                var $facetLink2 = pThis.Helpers.CreateLink(linkProp2);
                window.GeodiFacet.QueryPanel.body.addItem($facetLink2);
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = true;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery[this.CurrentQueryItemKey] = false;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "cgroup",
        Target: contentTypePanel,
        CurrentQueryItemKey: "ContentReaders",
        ChartData: [],
        Helpers: {
            GetData: function () { return window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders; },
            GetGroupItem: function (params, pThis) {
                var currentQuery = pThis.Helpers.CurrentQuery(), wsInfo = this.GetData();
                if (!window.CurrentQueryContainer.CurrentWSInfo || !wsInfo)
                    return;

                if (!currentQuery.ContentReaders)
                    currentQuery.ContentReaders = [];

                var item = null;
                if (wsInfo.OtherCategory && wsInfo.OtherCategory.Category.Text === params.id)
                    item = wsInfo.OtherCategory;
                else {
                    var namesOrder = wsInfo.NamesOrder.find(function (f) { return f === params.id });
                    if (namesOrder)
                        item = wsInfo.Names[namesOrder];
                }
                return item;
            }
        },
        Render: function (params, pThis) {
            var fThis = this, currentQuery = pThis.Helpers.CurrentQuery(), wsInfo = this.Helpers.GetData();
            var panelEmptyVisibleValue = window.GeodiFacet.Settings.PanelEmptyVisibleValues.find(function (f) { return f.panelId == fThis.Target.prop("id") });
            var linkProperties = function (namesOrder) {
                this.class = "facetLink facetLinkPLine";
                this.panel = contentTypePanel;
            };

            if (wsInfo) {

                wsInfo.NamesOrder.forEach(function (namesOrder) {
                    var currentNamesOrderWindow = pThis.Helpers.GetCurrentNamesOrderList(namesOrder);
                    var names = wsInfo.Names[namesOrder];
                    var titleElmProperties = { $titleElement: null, count: 0 };
                    var fnamesOrder = window.GeodiFacet.Settings.NamesOrderList.find(function (f) { return f.namesOrder === namesOrder;});
                    var filterAddedGroups = [];
                    if (currentQuery[fThis.CurrentQueryItemKey] && currentQuery[fThis.CurrentQueryItemKey].length > 0 && wsInfo && wsInfo.NamesOrder.length > 0) {

                        if (names && names.SubList) {
                            names.SubList.forEach(function (subList) {
                                var index = currentQuery[fThis.CurrentQueryItemKey].findIndex(function (f) { return f === subList.__type; });
                                if (index > -1 && filterAddedGroups.findIndex(function (f) { return f === namesOrder; }) === -1) {
                                    var linkProp = new linkProperties();
                                    linkProp.title = (HttpUtility.HtmlEncode(namesOrder));
                                    linkProp.html = "<span class=\"img-default\">&nbsp;</span>" + namesOrder;
                                    linkProp.remove = true;
                                    linkProp.onclick = pThis.Helpers.LinkSerialize({ id: namesOrder, type: fThis.Name, remove: true });
                                    pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
                                    filterAddedGroups.push(namesOrder);
                                }
                            });
                        }

                    }
                    var i = 0;
                    if (names.AllTyes && names.AllTyes.length > 0)
                        names.AllTyes.forEach(function (type) {
                            var contentType = null;
                            var contentTypeTxt = type.split(':')[1];

                            if (currentQuery.ContentTypes && currentQuery.ContentTypes.length > 0) {
                                contentType = currentQuery.ContentTypes.find(function (f) { return f === type; });
                                if (contentType) {
                                    var linkQueryProp = {
                                        class: "facetLink",
                                        title: (HttpUtility.HtmlEncode(contentTypeTxt)),
                                        html: contentTypeTxt,
                                        onclick: pThis.Helpers.LinkSerialize({ id: type, type: "ctype", remove: true }),
                                        icon: { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=" + type, true, true) },
                                        remove: true,
                                        panel: contentTypePanel,
                                        category: namesOrder
                                    };

                                    pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkQueryProp));

                                }
                            }


                            if (params.fullFacet.DocForContentType[type]) {
                                if (!titleElmProperties.$titleElement) {
                                    var readerHasQuery = filterAddedGroups.find(function (f) { return f === namesOrder });

                                    var contentReaderLink =
                                    {
                                        countBar: { class: "facetCountR facetCountRPBar facetCountRPBarLarge", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", },
                                        title: (HttpUtility.HtmlEncode(namesOrder)),
                                        html: namesOrder,
                                        checkBox: { checked: (readerHasQuery ? true : (fnamesOrder && fnamesOrder.checked ? true : false)), onclick: pThis.Helpers.LinkSerialize({ id: namesOrder, type: fThis.Name, remove: (readerHasQuery ? true : (fnamesOrder && fnamesOrder.checked ? true : false)), functionName: "CheckorUnChecked" }) },
                                        class: "facetLink facetLinkPLine",
                                        panel: contentTypePanel
                                    };
                                    var groupData = window.GeodiFacet.Settings.GroupData.find(function (f) { return f.name === namesOrder; });
                                    if (!groupData) {
                                        groupData = { name: namesOrder, expand: false , items:[]};
                                        window.GeodiFacet.Settings.GroupData.push(groupData);
                                    }
                                    titleElmProperties.$titleElement = pThis.Helpers.CreateLink(contentReaderLink);
                                    titleElmProperties.$titleElement.addClass("checkboxGroup");
                                    titleElmProperties.$titleElement.find(".checkbox");
                                    titleElmProperties.$titleElement.hide();
                                    fThis.Target.body.addItem(titleElmProperties.$titleElement);
                                    titleElmProperties.$titleElement.append("<div class='childItems' style=\"" + (groupData && groupData.expand ? '' : 'display: none') + "\"></div>");
                                    titleElmProperties.$titleElement.checked = contentReaderLink.checkBox.checked;
                                    $("<i/>", { html: '&nbsp;', class: "checkboxGroupExpand " + (groupData && groupData.expand ? 'negative' : 'plus'), onclick: pThis.Helpers.LinkSerialize({ functionName: "Helpers.ExpandCheckBoxGroup", category: namesOrder }) }).prependTo(titleElmProperties.$titleElement);

                                }
                                var count = (params.serverInfo.DocForContentType && params.serverInfo.DocForContentType[type] ? params.serverInfo.DocForContentType[type] : 0);
                                titleElmProperties.count += count;


                                var contentTypeLinkChecked = (titleElmProperties.$titleElement.checked ? true : contentType ? true : false);
                                var contentTypeLink =
                                {
                                    class: "facetLink facetSubItem",
                                    title: (HttpUtility.HtmlEncode(contentTypeTxt)),
                                    html: contentTypeTxt,
                                    icon: { src: HttpUtility.ResolveUrl("~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=" + type, true, true) },
                                    chart: true,
                                    panel: contentTypePanel,
                                    checkBox: { category: namesOrder, checked: contentTypeLinkChecked, onclick: pThis.Helpers.LinkSerialize({ id: type, type: "ctype", functionName: "CheckorUnChecked", remove: contentTypeLinkChecked }) }
                                };
                                if (count > 0) {
                                    contentTypeLink.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", style: pThis.Helpers.CreateCountBarStyle({ count: count, foundDocument: params.serverInfo.FoundDocument }), html: FormatNumber(count) };
                                    var obj = { LinkData: { Id: type, Category: "ctype" }, ChartItem: [contentTypeTxt, count] };
                                    fThis.ChartData.push(obj);
                                }
                                else {
                                    contentTypePanel.header.find(".showFacetEmptyItemBtn").show();
                                    contentTypeLink.class += " facetEmptyItem";
                                }

                                var linkItem = pThis.Helpers.CreateLink(contentTypeLink);

                                if (linkItem.hasClass("facetEmptyItem") && panelEmptyVisibleValue && panelEmptyVisibleValue.isVisible)
                                    linkItem.show();
                                titleElmProperties.$titleElement.find("div.childItems").append(linkItem);

                                currentNamesOrderWindow.totalCheckbox++;
                                if (contentTypeLink.checkBox.checked)
                                    currentNamesOrderWindow.totalChecked++;
                            }
                            if (i === names.AllTyes.length - 1) {
                                if (titleElmProperties.$titleElement) {
                                    if (currentNamesOrderWindow.totalCheckbox > 0 && currentNamesOrderWindow.totalCheckbox === currentNamesOrderWindow.totalChecked)
                                        pThis.Helpers.FullChecked(titleElmProperties.$titleElement);
                                    else if (currentNamesOrderWindow.totalCheckbox > 0 && currentNamesOrderWindow.totalChecked > 0 && currentNamesOrderWindow.totalCheckbox > currentNamesOrderWindow.totalChecked)
                                        pThis.Helpers.HalfChecked(titleElmProperties.$titleElement);
                                    else if (currentNamesOrderWindow.totalChecked === 0)
                                        pThis.Helpers.NotChecked(titleElmProperties.$titleElement);
                                }


                            }
                            i++;
                        });


                    if (titleElmProperties.$titleElement) {
                        if (titleElmProperties.count > 0) {
                            $(titleElmProperties.$titleElement).show();
                            var pStyle = pThis.Helpers.CreateCountBarStyle({ count: titleElmProperties.count, foundDocument: params.serverInfo.FoundDocument });
                            $(titleElmProperties.$titleElement).find("span.facetCountRPBarLarge").attr("style", pStyle).html("<b>" + FormatNumber(titleElmProperties.count) + "</b>");
                        } else {
                            $(titleElmProperties.$titleElement).find("span.facetCountRPBarLarge").remove();
                            contentTypePanel.header.find(".showFacetEmptyItemBtn").show();
                            $(titleElmProperties.$titleElement).addClass("facetEmptyItem");
                            if (panelEmptyVisibleValue && panelEmptyVisibleValue.isVisible)
                                $(titleElmProperties.$titleElement).show();

                        }
                    }

                    if (wsInfo.NamesOrder[wsInfo.NamesOrder.length - 1] === namesOrder)
                        pThis.Helpers.CreateChart({ chartType: "pie", chart: fThis.Target.chart, data: fThis.ChartData });
                });
                if (fThis.Target.body.find(".facetEmptyItem").length === 0)
                    fThis.Target.header.$spanShowFacetEmptyItemBtn.hide();

            }


        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), item = this.Helpers.GetGroupItem(params, pThis);

            if (item && item.SubList)
                item.SubList.forEach(function (subList) {
                    var typeName = subList.__type;
                    var index = currentQuery.ContentReaders.findIndex(function (f) { return f == typeName });
                    if (index < 0)
                        currentQuery.ContentReaders.push(typeName);
                });

            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), item = this.Helpers.GetGroupItem(params, pThis);
            var fnamesOrder = window.GeodiFacet.Settings.NamesOrderList.find(function (f) { return f.namesOrder == params.id });
            if (fnamesOrder)
                fnamesOrder.checked = false;
            if (item && item.SubList)
                item.SubList.forEach(function (subList) {
                    var typeName = subList.__type;
                    var index = currentQuery.ContentReaders.findIndex(function (f) { return f == typeName });
                    if (index > -1)
                        currentQuery.ContentReaders.splice(index, 1);
                });

            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "ctype",
        Target: contentTypePanel,
        GetData: function () { return window.CurrentQueryContainer.CurrentWSInfo.GroupContentReaders },
        CurrentQueryItemKey: "ContentTypes",
        ChartData: [],
        Render: function (params, pThis) { },
        Add: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = false;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        },
        Remove: function (params, pThis) {
            params.CurrentQueryItemKey = this.CurrentQueryItemKey;
            params.remove = true;
            return pThis.Helpers.CurrentQueryAddorRemove(params, pThis);
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "goparent",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            if (currentQuery.DocParentsTree && currentQuery.DocParentsTree.length > 0) {

                var linkProp = { class: "facetLink", remove: true };
                var txtInExtTitle = "[$query:has_parent_filter/js]";
                var txtIn = txtInExtTitle;
                var txtImgText = "<span class=\"img-default\">&nbsp;</span> ";

                if (currentQuery.DocParentsSingleTxt) {
                    txtIn = currentQuery.DocParentsSingleTxt;
                    txtInExtTitle = '\r\n' + txtInExtTitle;
                }
                if (currentQuery.DocParentsSingleContentType)
                    linkProp.icon = { src: '"' + HttpUtility.ResolveUrl('~/GeodiJSONService?op=getContentTypeIco&ContentTypeKey=' + currentQuery.DocParentsSingleContentType, true, true) + '"', class: "img-default" };
                linkProp.html = txtIn;
                linkProp.title = txtIn + txtInExtTitle;
                linkProp.onclick = pThis.Helpers.LinkSerialize({ id: null, type: fThis.Name, remove: true });
                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.DocParentsTree = [params.id];
            currentQuery.DocParentsSingleTxt = params.Op1;
            currentQuery.DocParentsSingleContentType = params.Op2;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.DocParentsTree = null;
            return true;
        }
    });
    window.GeodiFacet.AddFacetItem({
        Name: "gosimilarto",
        Render: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery(), fThis = this;
            if (currentQuery.SimilarTo) {
                var linkProp = { class: "facetLink", remove: true };
                linkProp.preElement =$("<img/>", { src: "" + HttpUtility.ResolveUrl('~/GeodiJSONService?op=GetSimilarToIcon&tmpFn=' + currentQuery.SimilarTo) + "" });
                linkProp.html = "&nbsp;" + "[$query:similar/js]: " + (currentQuery.SimilarDisplayName ? currentQuery.SimilarDisplayName:"");
                linkProp.onclick = pThis.Helpers.LinkSerialize({ id: null, type: fThis.Name, remove: true });
                pThis.QueryPanel.body.addItem(pThis.Helpers.CreateLink(linkProp));
            }
        },
        Add: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.SimilarTo = params.id;
            currentQuery.SimilarDisplayName = params.Op1;
            return true;
        },
        Remove: function (params, pThis) {
            var currentQuery = pThis.Helpers.CurrentQuery();
            currentQuery.SimilarTo = null;
            return true;
        }
    });
});

/*recognizer facet*/
function AddRecognizerKeywordsFacet(OnlyKeywordsID,DefaultQueryText, Target, options, forceRender) {
    if (!window.GeodiFacet) return;

    if (Target!=null && typeof (Target) == "string") {
        var opText = options && options.TargetText ? options.TargetText : Target;
        Target = window.GeodiFacet.Helpers.CreatePanel({ id: "customFP" + "type" + OnlyKeywordsID, priorty: 40, clear: true, chart: false, style: "padding-bottom: 15px;", header: { html: $("<span/>", { class: "queryListTitle", html: opText }) }, visibleEmptyBtn: true });
    }
    else if (Target == null || !Target.body) {
        var opText = options && options.TargetText ? options.TargetText : DefaultQueryText;
        Target = window.GeodiFacet.Helpers.CreatePanel({ id: "customFP" + "type" + OnlyKeywordsID, priorty: 40, clear: true, chart: false, style: "padding-bottom: 15px;", header: { html: $("<span/>", { class: "queryListTitle", html: opText }) }, visibleEmptyBtn: true });
    }





    var inFacetItem = {
        Name: "type" + OnlyKeywordsID,
        Target: Target,
        GetData: function () {
            var fThis = this;
            var $defer = $.Deferred();
            var query = window.CurrentQueryContainer.CurrentQuery;
            query = query.Clone();
            query.DisableGetContentKeywords = true;
            query.UseRank = false;
            query.GetNumberOfOccurrences = true;
            query.DisableUpdater = true;
            query.OptimizeForSuggest = false;
            query.StartIndex = 0;
            query.EndIndex = 1000;

            query.FillOptions = 262144; //AutoRetrieveOnlyParent//

            if (query.SearchString)
                query.SearchString = "(" + query.SearchString + ") and (" + DefaultQueryText + ")";
            else
                query.SearchString = DefaultQueryText;
            if (options && options.QueryUpdater)
                query = options.QueryUpdater(query);

            query.GetKeywordsForDocs([null], true, function (data) {
                var dt1 = [];
                if (options && options.DataUpdater)
                    data = options.DataUpdater(data);

                if (data && data.NODATA)
                    for (var i = 0; i < data.NODATA.length; i++) {
                        if (options && options.DataItemUpdater)
                            data.NODATA[i] = options.DataItemUpdater(data.NODATA[i]);

                        var d0 = data.NODATA[i];
                        if (parseInt(d0[3]) > 0)
                            dt1.push({ name: d0[1], query: 'GQComment' + OnlyKeywordsID + ':(' + (options && options.DataFacetTextUpdater ? options.DataFacetTextUpdater(d0[1]) : d0[1]) + ') keywordid:(' + d0[0] + ') ', addToCurrentQuery: false, count: d0[3] })
                    }
                //getQueryCounts
                //dt1=dt1.sort(function(a,b) { return a.name.localeCompare(b.name) });
                dt1 = dt1.sort(function (a, b) { return b.count - a.count; });
                $defer.resolve(dt1); //data
            })
            return $defer.promise();
        },
        Render: function (params, pThis) {

            var fThis = this;
            var getLinkProperties = function (query) {
                params.Name = fThis.Name;
                params.noIcon = true;
                linkProperties = new GeodiFacetLink(params, pThis);
                delete params.noIcon;
                linkProperties.chart = true;
                linkProperties.itemid = query.name.trim();
                linkProperties.onclick = window.GeodiFacet.Helpers.LinkSerialize({ id: query.name.trim(), type: params.Name });
                return linkProperties;
            };

            fThis.GetData().then(function (data) {
                fThis.Target.body.empty();
                for (var i = 0; i < data.length; i++) {
                    var query = data[i];
                    var linkProperties = getLinkProperties(query);
                    if (linkProperties) {
                        linkProperties.title = query.name;
                        linkProperties.html = query.name;
                        linkProperties.onclick = 'window.GeodiFacet.AddorRemove({"id":"' + query.name.trim() + '","type":"' + fThis.Name + '"},this)';
                        linkProperties.countBar = { class: "facetCountR facetCountRPBar", onclick: "GeodiFacet.Helpers.FacetModeSwap(\"" + fThis.Target.prop("id") + "\",this)", html: query.count };
                        var $facetLink = window.GeodiFacet.Helpers.CreateLink(linkProperties);
                        $facetLink.data(query);
                        fThis.Target.body.addItem($facetLink);
                    }

                    if (i == (data.length - 1) && data.length > 4) {

                        fThis.Target.find(".facetLink").hide();
                        fThis.Target.find(".facetLink:lt(4)").show();
                        var dm = $("<div class='facetLink forMore' forMore=1 ><a class='moreSideLink' href='javascript:void(0);'>[$default:More/js]</a></div>");
                        fThis.Target.find('.panel-body').append(dm);
                        dm.click(function () {
                                    var forMore = $(this).attr("forMore");
                                    if (forMore == 1) {
                                        fThis.Target.find(".facetLink").not(".forMore").show();
                                        $(this).attr("forMore", "0");
                                        $(this).text("[$default:close/js]");
                                    }
                                    else {
                                        fThis.Target.find(".facetLink").not(".forMore").hide();
                                        fThis.Target.find(".facetLink:not(.forMore):lt(4)").show();
                                        $(this).attr("forMore", "1");
                                        $(this).text("[$default:More/js]");
                                    }
                                });
                    }
                }
            });


        },
        Add: function (params) {
            var item = $(params.From);
            return FieldLink(item.data().query, null, false, null, options ? options.QueryTargetArrayName : null);
        }
    };

    window.GeodiFacet.AddFacetItem(inFacetItem);

    if (forceRender)
        window.GeodiFacet.Render();
}