﻿function CreateAddAction(op) {
    $("#" + op.id).remove();
    var grp = $("#dfMapBtnGrp");
    var mobile = window.BrowserType.IsMobile();
    if (!grp || grp.length == 0) {
        grp = $('<div id="dfMapBtnGrpC" ' + (mobile ? 'class="ol-touch"' : '') + ' style="position:absolute;z-index:10;left:7px;top:' + (mobile ? 104 : 100) + 'px;"><span id="dfMapBtnGrp" class="ol-unselectable ol-control"></span></div>');
        $(document.body).append(grp);
        grp = $("#dfMapBtnGrp");
    }
    var btn = $( op.html?op.html:'<button id="' + op.id + '" ><span class="' + (op.imgClass ? op.imgClass : '') + ' img-default" ' + (op.imgAttributes ? op.imgAttributes : '') + '></span></button>');
    window._MpActTop = Math.max(op.top, window._MpActTop ? window._MpActTop : 0);
    var that = this;
    if (op.text)
        btn.attr("title", op.text);
    btn.click(op.click)
    grp.append(btn);
    return btn;
}

function DrawController(vSource, vLayer) {
    this.VectorSource = vSource;
    this.VectorLayer = vLayer;
    this.AddLayer = function () {
        window.map.removeLayer(this.VectorLayer);
        window.map.addLayer(this.VectorLayer);
    }
    //add style, add layer
    this.AddFeatures = function (features) {
        var that = this;
        $.each(features, function (i, d) {
            if (d.wkt) {
                //wkt callback ?
                var f = toFeature(d.wkt);
                //style
                f.set("data", d);
                if (d.style)
                    f.setStyle(d.style);
                f.on("change", function () {
                    f.get("data").changed = true;
                });
                that.VectorSource.addFeature(f);
            }
        })
        that.AddLayer();
    }

    this.Clear = function () {
        this.VectorSource.clear();
    }

    this.GetFeatures = function () {
        var flist = this.VectorSource.getFeatures();
        var rtn = [];
        for (var i = 0; i < flist.length; i++) {
            var f = flist[i];
            var d = f.get("data");
            if (d.changed) {
                //changed
                d.wkt = toWKT([f], null, 4326);
                rtn.push(d);
            }
            else {
                rtn.push(d);
            }

        }

        //edited ??

        return rtn;
    }
    this.CreateAddAction = function (op) {
        var that = this;
        op.click = function () {
            var md = that.modify;
            if (that.draw) {
                //end
                that.EndEdit();
                if (md)
                    that.StartEdit();
            }
            else
                that.StartAdd(op.geoType, op.style, function (dvEvent) {
                    op.changed = true;
                    if (op.RemoveBtnOnAdd)
                        $("#" + op.id).remove();
                    dvEvent.feature.set("data", op);
                    if (op.style)
                        dvEvent.feature.setStyle(op.style);
                    that.EndEdit();
                    if (md)
                        that.StartEdit();
                });
        }
        window.CreateAddAction(op);
    }
    this.StartAdd = function (geoType, style, callback) {
        var that = this;
        if (that.draw)
            return; //?
        if (!geoType)
            geoType = "Point";//Polygon,LineString
        that.AddLayer();
        that.draw = new ol.interaction.Draw({
            source: that.VectorSource,
            type: geoType,
            style: style
        });

        window.map.addInteraction(that.draw);
        if (callback)
            that.draw.on('drawend', function (dvEvent) {
                callback(dvEvent);
            });
    }
    this.StartEdit = function () {
        var that = this;
        if (that.modify)
            return;
        that.AddLayer();
        that.modify = new ol.interaction.Modify({
            source: that.VectorSource,
            insertVertexCondition: function () { return true; }

        });
        window.map.addInteraction(that.modify);

        that.snap = new ol.interaction.Snap({ source: that.VectorSource });
        window.map.addInteraction(that.snap);
    }

    this.EndEdit = function () {
        var that = this;
        if (that.modify)
            window.map.removeInteraction(that.modify);
        if (that.snap)
            window.map.removeInteraction(that.snap);
        if (that.draw)
            window.map.removeInteraction(that.draw);
        delete that.modify;
        delete that.snap;
        delete that.draw;
    }
}


function CreateDefaultControllers() {
    if (window.map) {
        if (!window.DefaultDraw)
            window.DefaultDraw = CreateOrGetDrawController("default");
        if (!window.DefaultHighlight)
            window.DefaultHighlight = CreateOrGetDrawController("highlight",
                new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 0.7)',
                        width: 4
                    }),
                    fill: new ol.style.Fill({
                        color: 'rgba(0, 0, 255, 0.1)'
                    }),
                    image: new ol.style.Circle({
                        radius: 12,
                        stroke: new ol.style.Stroke({
                            color: 'rgba(255, 0, 0, 0.7)',
                            width: 4
                        }),
                    })
                })
                );
    }
}
function CreateDefaultDrawStyle() {
    return new ol.style.Style({
        fill: new ol.style.Fill({
            color: 'rgba(255, 255, 255, 0.2)'
        }),
        stroke: new ol.style.Stroke({
            color: '#ffcc33',
            width: 2
        }),
        image: new ol.style.Circle({
            radius: 7,
            fill: new ol.style.Fill({
                color: '#ffcc33'
            })
        })
    })
}

function CreateOrGetDrawController(id, defaultStyle) {
    if (!window._vSourceList)
        window._vSourceList = {};
    if (window._vSourceList[id])
        return window._vSourceList[id];
    var vs = window.vSource;
    var vl = window.vectorL;
    if (!defaultStyle) {
        defaultStyle = CreateDefaultDrawStyle();
    }
    vs = new ol.source.Vector({ wrapX: false });
    vl = new ol.layer.Vector({
        source: vs,
        deceID: id,
        noHide: true,
        style: defaultStyle
    });
    window._vSourceList[id] = new DrawController(vs, vl);
    return window._vSourceList[id];
}
CreateDefaultControllers();


