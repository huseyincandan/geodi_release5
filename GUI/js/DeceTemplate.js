﻿function TemplateToTree(str) {
    var lastp = 0, m, r1 = new RegExp("(<(\\/|)d:[^<>]*>)|({{[^\\\\}]*}})", "gi");
    var parseAtt = function (txt) {
        var attrs = {}; var tmp = $(txt);
        $.each(tmp[0].attributes, function (index, attribute) {
            var val = attribute.value;
            if (val.length > 1 && (val[0] == val[val.length - 1] && (val[0] == "'" || val[0] == '"'))) val = val.substr(1, val.length - 2);
            attrs[attribute.name] = val
        });
        tmp.remove();
        return attrs;
    }
    var tree = { items: [], ToHtml: function (data, context) { var h = []; for (var i = 0; i < this.items.length; i++) h.push(this.items[i].ToHtml(data, context)); return h.join(''); } };
    var current = tree;
    while ((m = r1.exec(str)) !== null) {
        if (m.index != lastp && m.index - lastp > 0)
            current.items.push({ val: str.substr(lastp, m.index - lastp), parent: current, ToHtml: function (data, context) { return this.val; } });
        if (m[0].length > 0) {
            var t = str.substr(m.index, m[0].length);
            if (t.indexOf("<d:r") == 0) {
                var cn = {
                    items: [], parent: current, attr: parseAtt(t),
                    ToHtml2: function (data, context) {
                        var h = [];
                        for (var i = 0; i < this.items.length; i++)
                            h.push(this.items[i].ToHtml(data, context));
                        return h.join('');
                    },
                    ToHtml: function (data, context) {
                        if (this.attr["visible"] && !eval(this.attr["visible"])) return "";
                        if (this.attr["data"]) {
                            var h = [];
                            var parentdata = data;
                            data = eval(this.attr["data"]);
                            var that = this;
                            if (data)
                                $.each(data, function (i, d) {
                                    var cs0 = null;
                                    if (context && context.TemplateToHtmlSettings)
                                        cs0 = context.TemplateToHtmlSettings;;
                                    if (!cs0 || !cs0.DisableParentDataAdd)
                                        d.parentdata = parentdata;
                                    if (!cs0 || !cs0.DisableRepeaterIndexAdd)
                                        d.repeaterIndex = i;
                                    var h2 = that.ToHtml2(d, context);
                                    if (!cs0 || !cs0.DisableParentDataDelete)
                                        delete d.parentdata;
                                    if (!cs0 || !cs0.DisableRepeaterIndexAdd)
                                        delete d.repeaterIndex;

                                    h.push(h2);
                                });
                            return h.join('');
                        }
                        return this.ToHtml2(data, context);
                    }
                };
                current.items.push(cn);
                current = cn;
            }
            else if (t.indexOf("</d:r") == 0) {
                if (current.parent != null)//parse error
                    current = current.parent;
            }
            else
                current.items.push({ val: t, parent: current, ToHtml: function (data, context) { return eval(this.val); } });
        }
        lastp = r1.lastIndex = m.index + m[0].length;
    }
    if (str != null && str != undefined && lastp < str.length)
        tree.items.push({ val: str.substr(lastp), ToHtml: function (data, context) { return this.val; } });
    return tree;
}

function TemplateToHtml(tepmlate, data, context) {
    if (!window.__templateCache) window.__templateCache = {};
    var html = tepmlate.html ? tepmlate.html() : (tepmlate.innerHTML ? tepmlate.innerHTML : tepmlate);
    if (!window.__templateCache[html]) window.__templateCache[html] = TemplateToTree(html);
    return window.__templateCache[html].ToHtml(data, context);
}