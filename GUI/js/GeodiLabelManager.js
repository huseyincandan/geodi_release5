﻿window.RecognizeFormatter = {
    escapeRegExp: function (string) {
        return string.replace(/([*+^=!:${}()|\[\]\/\\])/g, "\\$1").replace(/ /g, "\\s*").replace("$", "[$]").replace(".", "[.]");
    },
    ReplaceAllKeywords: function (src, key, rplc) {
        if (!key)
            return src;
        key = key.trim();
        if (key.length == 0)
            return src;
        var allIsWord = key == null || key.replace(/\b.*\b/, "").trim().length == 0;
        return src.replace(/&nbsp;/g, " ").replace(new RegExp("(^|[\\" + (allIsWord ? "W" : "s") + "]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+|[<>]+)(" + RecognizeFormatter.escapeRegExp(key) + ")($|[\\" + (allIsWord ? "W" : "s") + "]+|[,.)(\"“”'’?–_+:;=\/\\~}{\^\$\!\|\-]+|[<>]+)", 'gi'), rplc).replace(/  /g, "&nbsp; ");
    },
    OnHtmlEditing: function (jqEl, el, wsName, force) {
        if (window.___disableOnHtmlRecognizerEditing) return;

        var text = $(el).html(); //.html();
        $.post('DataExtractionHandler?op=Recognize', { 'Text': text, 'wsName': wsName, SortTextLength: true, AddFilterInfo: false },
            function (data) {
                if (window.___disableOnHtmlRecognizerEditing) return;
                if (!force) {
                    var currentTime2 = new Date();
                    if (el.lastTime && currentTime2 - el.lastTime < 50)
                        return;
                }
                RecognizeFormatter.formatText(el, data ? data.Entries : null);
            },
            'json');//.fail(function () { alert("error"); });
    },
    HtmlToText: function (text) {
        if (text) {
            for (var i = 0; i < 3; i++)
                text = text.replace(/<span[\s\S]*?>([\s\S]*?)<\/span>/g, "$1").replace(/<font[\s\S]*?>([\s\S]*?)<\/font>/g, "$1");
        }
        return text;
    },

    formatText: function (el, entries) {


        var sel = rangy.getSelection();
        var savedSel = sel.saveCharacterRanges(el);

        var txtSummaryStart = el.innerHTML;
        var txtSummary = txtSummaryStart.replace(/<span[\s\S]*?>([\s\S]*?)<\/span>/g, "$1").replace(/<font[\s\S]*?>([\s\S]*?)<\/font>/g, "$1");


        if (entries && entries.length > 0 && txtSummary) {
            txtSummary = " " + txtSummary + " ";
            if (entries) {
                var rplist = {};
                for (var ix = 0; ix < entries.length; ix++)
                    if (!rplist[entries[ix].OriginalText]) {
                        rplist[entries[ix].OriginalText] = true;
                        txtSummary = RecognizeFormatter.ReplaceAllKeywords(txtSummary, entries[ix].OriginalText, "$1<span class='highlight'>$2</span>$3"); //" + entries[ix].OriginalText + " //lightHighlight
                    }
            }
            else
                txtSummary = RecognizeFormatter.ReplaceAllKeywords(txtSummary, activeData.Keyword, "$1<span class='highlight'>$2</span>$3"); //" + activeData.Keyword + "

            if (stringStartsWith(txtSummary, "&nbsp;"))
                txtSummary = txtSummary.substr(6);
            if (!stringStartsWith(txtSummary, " ")) txtSummary = " " + txtSummary;
            if (!stringEndsWith(txtSummary, " ")) txtSummary = txtSummary + " ";
            var nhtml = txtSummary.substr(1, txtSummary.length - 2);
            if (nhtml && nhtml.substr(nhtml.length - 1) == ">") {
                var lOf = nhtml.lastIndexOf("<");
                if (lOf > 1 && nhtml.substr(lOf - 1, 1) == " ")
                    nhtml = nhtml.substr(0, lOf - 1) + "&nbsp;" + nhtml.substr(lOf, nhtml.length - lOf);
            }
            $(el).html(nhtml);
        }
        else
            $(el).html(txtSummary);//clear

        sel.restoreCharacterRanges(el, savedSel);
    },


    RegisterRecognizerSuggestion: function (jqel, el, wsName, enumID) {
        if (!jqel.atwho)
            return;
        jqel.atwho({
            at: "@",
            data: [],
            limit: 200,
            insertTpl: "${name}",
            callbacks: {
                /*beforeInsert: function (a, b) {
                    a = a;
                    //beforeInsert
                },  */
                remoteFilter: function (query, render_callback) {
                    //add cache
                    if (window.__RecognizerSuggestDisableForCurrentWs) {
                        render_callback([]);
                        window.___disableOnHtmlRecognizerEditing = false;
                    }
                    else
                        $.post('SuggestionHandlerForNotes?op=SuggestRecognizers', { 'Text': query, 'wsName': wsName, 'enumID': enumID },
                            function (data) {
                                if (data) {
                                    if (data.RecognizerNotFound)
                                        window.__RecognizerSuggestDisableForCurrentWs = true;
                                    if (window.__RecognizerSuggestDisableForCurrentWs) {
                                        render_callback([]);
                                        window.___disableOnHtmlRecognizerEditing = false;
                                    }
                                    else {
                                        render_callback(data.Values);
                                    }
                                }
                            },
                            'json');//.fail(function () { alert("error"); });

                },
                afterMatchFailed: function (at, el) {
                }
            }
        });

        jqel.on('shown.atwho', function () {
            window.___disableOnHtmlRecognizerEditing = true;
            window.clearTimeout(window.__recSuggestTmr);
        });
        jqel.on('hidden.atwho', function () {
            window.clearTimeout(window.__recSuggestTmr);
            window.__recSuggestTmr = window.setTimeout(function () {
                window.___disableOnHtmlRecognizerEditing = false;
                RecognizeFormatter.OnHtmlEditing(jqel, el, wsName);
            }, 800)

        });

    }
};
function stringStartsWith(str, startText) {
    return str.indexOf(startText) == 0;
}
function stringEndsWith(str, endText) {
    return str.lastIndexOf(endText) == (str.length - endText.length);
}


var customItemTemplate = "<div><span />&nbsp;<small /></div>";

function elementFactory(element, e) {
    var template = $(customItemTemplate).find('span')
        .text('@' + e.val).end()
        .find('small')
        .text("(" + (e.meta || e.val) + ")").end();
    element.append(template);
};
/*Üst kod genelleşmeli*/


function GeodiLabelReference() {
    this.LabelId = -1;
    this.ReferenceId = -1;
    this.UserDescription = '';
    this.RegisterTime = new Date();
    this.LabelSection = { Attributes: {} }

}

function LabelManager() {
    var this_ = this;
    var ws = StateForm.GetState('wsName');

    this.toggleIcon = function ($el) {
        var iElem = $el.find('i');

        if (iElem.hasClass('fa-sticky-note') || iElem.hasClass('fa-sticky-note-o')) {
            if (iElem.hasClass('fa-sticky-note'))
                iElem.removeClass('fa-sticky-note').addClass('fa-sticky-note-o').toggleClass('fav-color');
            else
                iElem.addClass('fa-sticky-note').removeClass('fa-sticky-note-o').toggleClass('fav-color');
        }
        else
            $el.find('i').toggleClass('fa-star').toggleClass('fa-star-o').toggleClass('fav-color');
    }
    this.setFavorite = function ($target, forrefresh) {
        $target.find('.favLabel').each(function () {
            var $el = $(this);
            var lId = parseInt($el.data('label'));
            if (forrefresh) {
                var lin = $el.find('i');
                lin.removeClass('fa-sticky-note').removeClass('fa-sticky-note-o').removeClass('fav-color');
                if (lId > 0)
                    lin.addClass('fa-sticky-note').addClass('fav-color');
                else
                    lin.addClass('fa-sticky-note-o');

            }
            else if (lId > 0)
                this_.toggleIcon($el);
            if (!$el.attr("notitle"))
                $el.attr("title", '[$default:add_note/js]');
        });

        //$target.attr("title", this_.title);
    }

    this.confirmPopup = function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        var ws2 = this_.currentLabel && this_.currentLabel.util && this_.currentLabel.util.wsName ? geodiLabel.util.wsName : ws;
        this_.currentLabel.UserDescription = $('#UserDescription').val();
        if (this_.currentLabel.LabelId >= 0)
            LabelManagerComm.SetLabel(this_.currentLabel, ws2, function () {
                this_.currentElement.popover('toggle');
                return false;
            }, this_.currentLabel.util);
        else
            LabelManagerComm.AddLabel(this_.currentLabel, ws2, function (label) {
                if (label) {
                    this_.currentElement.data('label', label.LabelId);
                    this_.toggleIcon(this_.currentElement);
                }
                this_.currentElement.popover('toggle');
                return false;
            }, (this_.currentLabel ? this_.currentLabel.util : null));
    }
    this.removeFavorite = function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        var ws2 = this_.currentLabel && this_.currentLabel.util && this_.currentLabel.util.wsName ? this_.currentLabel.util.wsName : ws;
        LabelManagerComm.RemoveLabel(this_.currentLabel.LabelId, ws2, function (res) {
            if (res) {
                this_.toggleIcon(this_.currentElement);
                this_.currentElement.data('label', 0);
            }
            this_.currentElement.popover('toggle');
            return false;
        }, (this_.currentLabel ? this_.currentLabel.util : null));
    };
    this.cancel = function (e) {
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        this_.currentElement.popover('toggle');
        return false;
    };
    //$(document).on('click', ':not(.popover)', function () {
    //    if (this_.currentElement)
    //        this_.currentElement.popover('toggle');
    //});
}

var LabelManagerComm = {
    GetLabel: function (id, wsName, callBackFunction, util) {
        if (!util) util = HttpUtility;
        $.post(util.ResolveUrl('~/GeodiJSONService?op=GetLabel'), { 'wsName': wsName, "labelId": id },
            function (data, ex) {
                if (data) { //eski kodlar için destek
                    if (!data.LabelSection) data.LabelSection = { Attributes: {} };
                    else if (!data.LabelSection.Attributes) data.LabelSection.Attributes = {};
                }
                callBackFunction(data, ex)
            }
            , 'json');
    },
    SetLabel: function (label, wsName, callBackFunction, util) {
        if (!util) util = HttpUtility;
        $.post(util.ResolveUrl('~/GeodiJSONService?op=SetLabel'), { 'wsName': wsName, "label": JSON.stringify(label) }, callBackFunction, 'json');
    },
    AddLabel: function (label, wsName, callBackFunction, utilParam) {
        if (label && label.UserDescription && label.UserDescription.replace(/\s/g, '').length > 0) {//GEODI-4899
            var util = utilParam;
            if (!util) util = HttpUtility;
            $.post(util.ResolveUrl('~/GeodiJSONService?op=AddLabel'), { 'wsName': wsName, "label": JSON.stringify(label) },
                function (data, a, b) {
                    if (data && data.LabelId != -1)//GEODI-4899
                    {
                        if (data)
                            data.util = utilParam;
                        if (callBackFunction)
                            callBackFunction(data, a, b)
                    }
                }
                , 'json');
        }
    },
    RemoveLabel: function (id, wsName, callBackFunction, util) {
        if (!util) util = HttpUtility;
        $.post(util.ResolveUrl('~/GeodiJSONService?op=RemoveLabel'), { 'wsName': wsName, "labelId": id }, callBackFunction, 'json');
    },
    GetLabelListByRef: function (referenceId, referenceTextOld, referenceTypeOld, wsName, callBackFunction, util) {
        if (!util) util = HttpUtility;
        $.post(util.ResolveUrl('~/GeodiJSONService?op=GetLabelListByRef'), { 'referenceId': referenceId, 'wsName': wsName },
            function (data, ex) {
                if (data)
                    for (var i = 0; i < data.length; i++) { //eski kodlar için destek
                        if (!data[i].LabelSection) data[i].LabelSection = { Attributes: {} };
                        else if (!data[i].LabelSection.Attributes) data[i].LabelSection.Attributes = {};
                        data[i].util = util;
                    }
                callBackFunction(data, ex, util);
            }
            , 'json');
    },
    GetLabelDictionaryValues: function (wsName, contentID, callBackFunction, util) {
        if (!util) util = HttpUtility;
        $.post(util.ResolveUrl('~/GeodiJSONService?op=GetLabelDictionaryValues'), { 'wsName': wsName, "contentID": contentID }, callBackFunction, 'json');
    }
}



//startAddLabel({ X: x, Y: y, onAddLabelSuccess: function () { }, onAddLabelCancel: function () { }})
function startAddLabel(sectionInfo, geodiLabel, GUIExtraOptions) {


    if (window._hideAddLabel) return;
    if (!sectionInfo) sectionInfo = { Attributes: {} };

    if (!geodiLabel) {
        geodiLabel = new GeodiLabelReference();
        geodiLabel.UserDescription = '-';
        geodiLabel.ReferenceId = window.currentContent.ID;
        geodiLabel.LabelId = -1;
        geodiLabel.LabelSection = sectionInfo;
    }
    else if (!geodiLabel.LabelSection) geodiLabel.LabelSection = sectionInfo;

    var wsName = geodiLabel.util && geodiLabel.util.wsName ? geodiLabel.util.wsName : StateForm.GetState('wsName');

    inputBoxExtra(
        {
            //text: '[$docViewer:enter_text/js]',
            BodyFormat: _defaultBodyFormat,
            title: '[$docViewer:label_one/js]' + (geodiLabel.ReferenceIsOtherVersion ? ' <a  href="javascript:void(0)"  onclick="showContent(' + geodiLabel.ReferenceId + ',event.ctrlKey?\'?\':\'docViewerdocView\')"><small>[$default:OtherVersionLabel/js]</small></a> ' : '')  + '<span data-help="edit_note" data-ns="none"><span class="directValue" style="display:none">[$docViewer:help_edit_note/js]</span></span>',
            callBack: function (result, text, html) {
                text = RecognizeFormatter.HtmlToText(html);
                if (result == 'ok' && text != '') {
                    geodiLabel.UserDescription = text;
                    if (GUIExtraOptions && GUIExtraOptions.OnLabelSending)
                        GUIExtraOptions.OnLabelSending(geodiLabel);
                    if (geodiLabel.LP) {
                        geodiLabel.LP.SetPermissions();
                        delete geodiLabel.LP;
                    }
                    var ws = geodiLabel.util && geodiLabel.util.wsName ? geodiLabel.util.wsName : StateForm.GetState('wsName');
                    LabelManagerComm.AddLabel(geodiLabel, ws, function (data, textStatus) {
                        if (window.FireLabelAdd) window.FireLabelAdd(data)
                        if (GUIExtraOptions && GUIExtraOptions.OnLabelSended)
                            GUIExtraOptions.OnLabelSended(geodiLabel);

                    }, geodiLabel.util);
                }
                else if (sectionInfo.onAddLabelCancel) {
                    sectionInfo.onAddLabelCancel();
                }
            },
            ButtonsUpdater: function (buttons) {
                buttons.unshift({
                    Creator: function (btn, modal, options) {
                        return getPermArea();
                    }
                })
                return buttons;
            },
            defaultValue: ' ',
            useTextarea: true,
            textAreaHeight: 200,
            SupportHtml: true,
            OnEditing: function (jqEl, el) { RecognizeFormatter.OnHtmlEditing(jqEl, el, wsName) },//function
            OnInitElement: function (jqEl, el) { RecognizeFormatter.RegisterRecognizerSuggestion(jqEl, el, wsName, window._currentEnumIdLabel) },//function
            Extra: GUIExtraOptions,
            DialogOptions: { backdrop: 'static', keyboard: true, dialogClass: 'labelDialog' }
        }
    )
    loadLabelDictionaryValues(wsName, geodiLabel.ReferenceId, 200, geodiLabel.util);
    if (window.Auth && window.Auth.CurrentUser)
        geodiLabel.LP = new LabelPermissions(wsName, window._currentEnumIdLabel, geodiLabel, true, "#modalDialog");
}

// Etiketin içindeki notu editler. Eğer not silinirse etiketi de siler.
function editLabel(prms, GUIExtraOptions) {
    if (!prms) prms = {};
    var wsName = prms.geodiLabel && prms.geodiLabel.util && prms.geodiLabel.util.wsName ? prms.geodiLabel.util.wsName : StateForm.GetState('wsName');
    var canEditOrDelete = _canEditOrDeleteLabel(prms.geodiLabel);
    inputBoxExtra(
        {
            hideOK: !canEditOrDelete,
            BodyFormat: _defaultBodyFormat,
            //text: '[$docViewer:enter_text/js]',
            title: '[$docViewer:label_one/js]' + (prms.geodiLabel.ReferenceIsOtherVersion ? ' <a  href="javascript:void(0)"  onclick="showContent(' + prms.geodiLabel.ReferenceId + ',event.ctrlKey?\'?\':\'docViewerdocView\')"><small>[$default:OtherVersionLabel/js]</small></a> ':'') + '<span data-help="edit_note" data-ns="none"><span class="directValue" style="display:none">[$docViewer:help_edit_note/js]</span></span>',
            callBack: function (result, text, html) {
                text = RecognizeFormatter.HtmlToText(html);
                var rs = result;
                var tx = text;
                if (result == 'ok') {

                    var ws = prms.geodiLabel && prms.geodiLabel.util && prms.geodiLabel.util.wsName ? prms.geodiLabel.util.wsName : StateForm.GetState('wsName');

                    if (text != '') {
                        if (prms.geodiLabel.LP) {
                            prms.geodiLabel.LP.SetPermissions();
                            delete prms.geodiLabel.LP;
                        }
                        prms.geodiLabel.UserDescription = text;
                        if (GUIExtraOptions && GUIExtraOptions.OnLabelSending)
                            GUIExtraOptions.OnLabelSending(prms.geodiLabel);
                        LabelManagerComm.SetLabel(prms.geodiLabel, ws, function (data, textStatus) {
                            if (window.FireLabelUpdate) window.FireLabelUpdate(prms.geodiLabel);
                        }, (prms.geodiLabel ? prms.geodiLabel.util : null));
                    }
                    else {
                        LabelManagerComm.RemoveLabel(prms.geodiLabel.LabelId, ws, function (data, textStatus) {
                            if (window.FireLabelRemove) window.FireLabelRemove(prms.geodiLabel);
                        }, (prms.geodiLabel ? prms.geodiLabel.util : null));
                    }
                }
            },
            ButtonsUpdater: function (buttons) {
                buttons.unshift({
                    Creator: function (btn, modal, options) {
                        return getPermArea();
                    }
                })
                return buttons;
            },
            defaultValue: prms.geodiLabel.UserDescription,
            useTextarea: true,
            textAreaHeight: 200,
            SupportHtml: true,
            OnEditing: function (jqEl, el) { RecognizeFormatter.OnHtmlEditing(jqEl, el, wsName) },
            OnInitElement: function (jqEl, el) { RecognizeFormatter.RegisterRecognizerSuggestion(jqEl, el, wsName, window._currentEnumIdLabel) },//function
            OnStart: function (jqEl, el) { RecognizeFormatter.OnHtmlEditing(jqEl, el, wsName, true); },
            Extra: GUIExtraOptions,
            DialogOptions: { backdrop: 'static', keyboard: true, dialogClass: 'labelDialog' }
        }
    )
    if (canEditOrDelete)
        loadLabelDictionaryValues(wsName, prms.geodiLabel.ReferenceId, 200, prms.geodiLabel.util);
    if (window.Auth && window.Auth.CurrentUser)
        prms.geodiLabel.LP = new LabelPermissions(wsName, window._currentEnumIdLabel, prms.geodiLabel, canEditOrDelete, "#modalDialog");
}
var _defaultBodyFormat = '<div class="inLabelNotes hidden"></div>{body}';
function getPermArea() {
    return $("<div class='pull-left permArea hidden'></div>");
}

function LabelPermissions(wsName, enumID, geodiLabel, canEdit, container) {
    this._enumID = enumID;
    this._wsName = wsName;
    this._geodiLabel = geodiLabel;
    this._canEdit = canEdit;
    this._container = container;
    this._selectize;
    init(this);
    function init(that) {
        setTimeout(function () {
            initButton(that);
        }, 100);
    }
    function initButton(that) {
        var el = $('.permArea', $(that._container));
        if (el.hasClass("hidden")) {
            el.removeClass("hidden");
            el.on('mousedown',
                function (event) {
                    event.preventDefault();
                }
            );
        }
        var permState = getPermStateFromModel(that);
        var dsbHtml = that._canEdit ? "" : "disabled";
        var html =
            '<div class="btn-group dropup"  ' + dsbHtml + ' >' +
            '  <button ' + dsbHtml + ' type="button" class="btn btn-sm btn-default dropdown-toggle permBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" title="' + getTitle(permState) + '" >' +
            '    <img class="permIcon permImg" src="' + getIcon(permState) + '"style="cursor: pointer;"/>' +
            '    <span class="caret caret-right">' +
            '  </button>' +
            '  <ul class="dropdown-menu" >' +
            '    <li><button type="button" class="close" aria-label="Close">&times;</button></li>' +
            '    <li onclick="event.stopPropagation();"><a href="#">' +
            '      <input type="radio" class="opt opt0" id=_rd0 name="optradio" value=0 ' + ((permState == 0) ? 'checked' : '') + '><label for=_rd0>&nbsp;&nbsp;[$docViewer:perm_state_0/js]</label></input>' +
            '    </a></li>' +
            '    <li onclick="event.stopPropagation();"><a href="#">' +
            '      <input type="radio" class="opt opt1" id=_rd1 name="optradio" value=1 ' + ((permState == 1) ? 'checked' : '') + '><label for=_rd1>&nbsp;&nbsp;[$docViewer:perm_state_1/js]</label></input>' +
            '    </a></li>' +
            '    <li onclick="event.stopPropagation();"><a href="#">' +
            '      <input type="radio" class="opt opt2" id=_rd2 name="optradio" value=2 ' + ((permState == 2) ? 'checked' : '') + '><label for=_rd2>&nbsp;&nbsp;[$docViewer:perm_state_2/js]&nbsp;</label></input><div><input type="text" id="permBox"/></div>' +
            '    </a></li>' +
            '  </ul>' +
            '</div>';
        el.html(html);
        $(".opt", el).change(function () {
            updateButton(getPermStateFromView(that), that);
        })

        if (!el.hasClass('fadein')) {
            el.addClass('fadein');
        }
        setPermSelectize('#permBox', that);
        setTimeout(function () {
            updateButton(permState, that);
        }, 100);
    }
    function updateButton(permState, that) {
        var pnl = $(that._container);
        $('.permBtn', pnl).attr('title', getTitle(permState));
        $('.permImg', pnl).attr("src", getIcon(permState));
        $(".permArea .selectize-input", pnl).removeClass("disabled");
        if (permState != 2) {
            $(".permArea .selectize-input", pnl).addClass("disabled");
            that._selectize.disable();
        }
        else
            that._selectize.enable();
    }
    function getPermStateFromModel(that) {
        var perms = getPermissions(that);
        if (!perms || perms.length == 0)
            return 0;
        else {
            that._geodiLabel.editPermUser = perms[0];
            if (perms.length == 1)
                return 1;
            else
                return 2;
        }
    }
    function getPermStateFromView(that) {
        var pnl = $(that._container);
        if ($(".opt2", pnl).prop("checked"))
            return 2;
        else if ($(".opt1", pnl).prop("checked"))
            return 1;
        else
            return 0;
    }
    function getPermissions(that) {
        return (that._geodiLabel.Permissions && that._geodiLabel.Permissions.Permit && that._geodiLabel.Permissions.Permit.length > 0) ? that._geodiLabel.Permissions.Permit : null;
    }
    function getIcon(permState) {
        if (permState == 2)
            return HttpUtility.ResolveUrl("~/GUI/img/key24_2.png");
        else if (permState == 1)
            return HttpUtility.ResolveUrl("~/GUI/img/key24_1.png");
        else
            return HttpUtility.ResolveUrl("~/GUI/img/key24_0.png");
    }
    function getTitle(permState) {
        if (permState == 2)
            return "[$docViewer:perm_hlp_2/js]";
        else if (permState == 1)
            return "[$docViewer:perm_hlp_1/js]";
        else
            return "[$docViewer:perm_hlp_0/js]"
    }

    this.SetPermissions = function () {
        var permState = getPermStateFromView(this);
        if (permState == 0)
            this._geodiLabel.Permissions = null;
        else if (permState == 1)
            this._geodiLabel.Permissions = { Permit: [this._geodiLabel.editPermUser ? this._geodiLabel.editPermUser : window.Auth.CurrentUser.ID], Deny: null };
        else {
            var arr = this._selectize.items;
            if (arr && arr.length > 0) {
                if (this._geodiLabel.editPermUser) {
                    if (arr.indexOf(this._geodiLabel.editPermUser) < 0)
                        arr.unshift(this._geodiLabel.editPermUser);
                }
                else if (arr.indexOf(window.Auth.CurrentUser.ID) < 0)
                    arr.unshift(window.Auth.CurrentUser.ID);
                this._geodiLabel.Permissions = { Permit: arr, Deny: null };
            }
            else
                this._geodiLabel.Permissions = null;
        }
        return this._geodiLabel.Permissions;
    }
    function setPermSelectize(elem, that) {
        that._perm = getPermissions(that);
        setPermSelectizeStatic(elem, that);
    }
}


function setPermSelectizeStatic(elem, that) {
    var selDom = null;
    if (that._container) {
        var pnl = $(that._container);
        selDom = $(elem, pnl);
    }
    else selDom = $(elem);
    var selOptions = {
        plugins: ['remove_button'],
        valueField: 'value',
        labelField: 'text',
    };
    if (that._selOptionsUpdater)
        that._selOptionsUpdater(selOptions);
    var $select = selDom.selectize(selOptions);
    that._selectize = $select[0].selectize;
    that.__allSel = {};
    that._selectize.on("type", function (str) {

        if (str) {
            window.clearTimeout(that.__typeThread);
            that.__typeThread = window.setTimeout(function () {
                $.post(HttpUtility.ResolveUrl('SuggestionHandlerForNotes?op=GetUserSuggestionsAdv'), { 'wsName': that._wsName, 'enumID': that._enumID, 'keyword': str, 'ThrowError': (that._disableThrowError ? false : true), TextMode: (that._textMode?true:false) },
                    function (data) {
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                var item = data[i];
                                var id = item.ID ? item.ID : item.LoginProviderName + ":" + item.UserName;
                                var displayName = item.UserName;
                                if (that._textMode)
                                    id = displayName;
                                if (!that.__allSel[id]) {
                                    that.__allSel[id] = true;
                                    that._selectize.addOption({ value: id, text: displayName });
                                }
                            }
                            that._selectize.refreshOptions();
                        }
                    }, 'json');
            }, 300);
        }
    });

    if (that._perm) {
        var setSelItems = function (data) {
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    if (item) {
                        var id = item.ID ? item.ID : item.LoginProviderName + ":" + item.UserName;
                        var displayName = item.UserName;
                        if (that._textMode)
                            id = displayName;
                        if (!that.__allSel[id]) {
                            that.__allSel[id] = true;
                            that._selectize.addOption({ value: id, text: displayName });
                        }
                        that._selectize.addItem(id);
                    }
                    else {
                        if (!that.__allSel[that._perm[i]]) {
                            that.__allSel[that._perm[i]] = true;
                            that._selectize.addOption({ value: that._perm[i], text: that._perm[i] });
                        }
                        that._selectize.addItem(that._perm[i]);
                    }
                }
                that._selectize.refreshItems();
                that._selectize.refreshOptions();
            }
        }
        if (that._textMode) {
            var dmydata = [];
            for (var i = 0; i < that._perm.length; i++)
                dmydata.push({ UserName: that._perm[i] }); 
            setSelItems(dmydata);
        }
        else {
            $.post(HttpUtility.ResolveUrl('SuggestionHandlerForNotes?op=GetUserInfoByID'), { 'wsName': that._wsName, 'enumID': that._enumID, 'ids': JSON.stringify(that._perm), 'ThrowError': (that._disableThrowError ? false : true)},
                function (data) {
                    setSelItems(data);
                }, 'json');
        }
      
    }
}

function loadLabelDictionaryValues(wsName, contentID, textAreaHeight, util) {
    if (window._lastLabelDictionaryValues && window._lastLabelDictionaryValues[wsName + contentID]) {
        _setLabelDictionaryValues(window._lastLabelDictionaryValues[wsName + contentID], wsName, contentID, textAreaHeight);
        return;
    }
    setTimeout(function () {
        if (contentID)
            LabelManagerComm.GetLabelDictionaryValues(wsName, contentID, function (data, textStatus) {
                if (data && data.length > 0)
                    _setLabelDictionaryValues(data, wsName, contentID, textAreaHeight);
            }, util);
    }, 100);
}
function _setLabelDictionaryValues(data, wsName, contentID, textAreaHeight) {
    window._lastLabelDictionaryValues = {};
    window._lastLabelDictionaryValues[wsName + contentID] = data;

    var labels = '';
    sortLabels(wsName, data);

    var el = $('.inLabelNotes');
    var lblEventArgs = {
        wsName: wsName,
        Data: data,
        OutputHtml: null,
        container: el
    };

    $(document.body).trigger("GUI_LabelDictionaryRendering", lblEventArgs);
    if (lblEventArgs.Cancel) return;

    if (lblEventArgs.OutputHtml != null)
        labels = lblEventArgs.OutputHtml;
    else {
        labels = [];
        for (var i = 0; i < data.length; i++)
            labels.push('<span class="label label-default noselect inLabelNoteItem" onclick="_onClickLabelNotes(this, \'' + wsName + '\')">' + data[i] + '</span> ');
        labels = labels.join("");
    }



    if (el.hasClass("hidden")) {
        el.removeClass("hidden");
        var p1 = el.parent();
        if (!p1.hasClass("inLabelNotesModal"))
            p1.addClass("inLabelNotesModal")
        el.on('mousedown',
            function (event) {
                event.preventDefault();
            }
        );
    }
    setTimeout(function () {
        el.empty();
        el.append($(labels));
        if (!el.hasClass('fadein')) {
            if (textAreaHeight)
                el.css('max-height', textAreaHeight + 'px'); //set max height
            el.addClass('fadein');
        }
    }, 100);
}

function labelUsed(wsName, label) {
    if (window.localStorage && label) {
        label = label.toLocaleLowerCase();
        var wsHsName = "lblDict.:" + wsName;
        var strs = window.localStorage[wsHsName];
        if (!strs) strs = {}; else strs = JSON.parse(strs);
        if (!strs[label])
            strs[label] = 1;
        else
            strs[label]++;
        window.localStorage[wsHsName] = JSON.stringify(strs);
    }
}
function _getLabelUsedCount(strs, label) {
    label = label.toLocaleLowerCase();
    if (strs[label])
        return strs[label];
    else
        return 0;
}
function sortLabels(wsName, labels) {
    if (window.localStorage) {
        var wsHsName = "lblDict.:" + wsName;
        var strs = window.localStorage[wsHsName];
        if (strs) {
            strs = JSON.parse(strs);
            labels.sort(function (a, b) { return _getLabelUsedCount(strs, b) - _getLabelUsedCount(strs, a) });
        }
    }
}


function _onClickLabelNotes(item, wsName) {
    var txt = $(item).text();
    AddLabelTextToCurrentNote(txt, wsName);
}

function AddLabelTextToCurrentNote(txtOrHtmlObject, wsName) {
    var isDom = typeof txtOrHtmlObject == "object";
    insertAtCaret($('.inEditableArea'), isDom ? txtOrHtmlObject : ' ' + txtOrHtmlObject + ' ');
    if (window.RecognizeFormatter && wsName)
        RecognizeFormatter.OnHtmlEditing($('.inEditableArea'), $('.inEditableArea')[0], wsName);
    if (!isDom)
        labelUsed(wsName, txtOrHtmlObject);
}
function insertAtCaret(txtarea, textOrHtmlObject) {
    txtarea.focus();
    var sel = rangy.getSelection();
    if (sel.rangeCount) {
        var range = sel.getRangeAt(0);
        range.deleteContents();
        var isDom = typeof textOrHtmlObject == "object";
        var textNode = isDom ? textOrHtmlObject : document.createTextNode(textOrHtmlObject);
        if (isDom && textNode.promise) {
            if (textNode.length == 0) return;
            textNode = textNode[0]
        }
        range.insertNode(textNode);
        range.setStartAfter(textNode);
        sel.removeAllRanges();
        sel.addRange(range);
    }
}



// Görüntüleyici üzerindeki pinlerde gösterilecek yazı
function formatLabelMark(label) {
    var fromNow = '<div><span style="white-space: nowrap">(' + moment(label.RegisterTime.UTC).fromNow() + ')</span></div>';
    var description = '<div>' + label.UserDescription + (label.ReferenceIsOtherVersion ? ' <a  href="javascript:void(0)"  onclick="showContent(' + label.ReferenceId + ',event.ctrlKey?\'?\':\'docViewerdocView\')"><small>[$default:OtherVersionLabel/js]</small></a> ' : "")+'</div>';
    return fromNow + description;
};
function _canEditOrDeleteLabel(label) {
    if (!window._hideEditLabel)
        return true;
    else if (!window._hideAddLabel) {
        if (window.Auth && window.Auth.CurrentUser &&
            window.Auth.CurrentUser.UniqueName && label.UserInfo &&
            window.Auth.CurrentUser.UniqueName.toLowerCase() == label.UserInfo.toLowerCase())
            return true;
    }
    return false;
}
// Görüntüleyicideki sağdaki paneldeki 
function formatLabelList(label) {
    var userName = label.UserInfo;
    var index = userName.indexOf(':');
    if (index > 0)
        userName = userName.substring(index + 1, userName.length);
    var headerLines = [];
    headerLines.push('<p> ' + HttpUtility.HtmlEncode(userName) + (label.ReferenceIsOtherVersion ? ' <a  href="javascript:void(0)"  onclick="showContent(' + label.ReferenceId + ',event.ctrlKey?\'?\':\'docViewerdocView\')"><small>[$default:OtherVersionLabel/js]</small></a> ' : "") + '</p>');
    var registerMoment = moment(label.RegisterTime.UTC);
    headerLines.push('<p title="' + registerMoment.format('LLLL') + '">' + registerMoment.format('L') + ' (' + registerMoment.fromNow() + ')' + '</p>');
    if (window.getLabelAdditionalInfoForList) {
        var content = getLabelAdditionalInfoForList(label);
        if (content)
            headerLines.push(content);
    }

    
    var ret = '<div style="clear:both;min-height:28px;overflow:auto;color:#aaaaaa" class="labelHeaderArea"><div style="float:left" class="labelHeaderLeft">' + headerLines.join('') + '</div>' +
        '<div style="float:right" class="labelHeaderRight">';

    if (_canEditOrDeleteLabel(label))
        ret += '<span class="' +
            (window.BrowserType.IsMobile() ? "img-edit img-button24" : "img-edit16 img-button16") +
            '" title="' + '[$default:edit/js]' + '" onclick="btnEditLabelClick(this,event)"></span>' +
            (window.BrowserType.IsMobile() ? "&nbsp;" : "") +
            '<span class="' +
            (window.BrowserType.IsMobile() ? "img-delete img-button24" : "img-delete16 img-button16") +
            '" title="' + '[$default:delete/js]' + '" onclick="btnDeleteLabelClick(this,event)" style="margin-left:2px"></span>';


    ret += '</div></div>';

    ret += '<hr style="margin-top:3px;margin-bottom:6px;border-top-style:dashed"/><p style="font-size:10pt">' + label.UserDescription + '</p>';
    return ret;
}
function btnEditLabelClick(sender, e) {
    e.stopPropagation();
    var label = $(sender).parents('li').data('label-info');
    if (window.StartLabelEdit) {
        StartLabelEdit(label);
    }
    else
        editLabel({
            geodiLabel: label
        });
}
function btnDeleteLabelClick(sender, e) {
    e.stopPropagation();
    showMessage('[$default:confirm_delete/js]', '[$default:warning/js]', 'warning', true, true, false, function (result) {
        if (result == 'yes') {
            var label = $(sender).parents('li').first().data('label-info');
            var labelID = label.LabelId;
            var ws = label && label.util && label.util.wsName ? label.util.wsName : StateForm.GetState('wsName');
            LabelManagerComm.RemoveLabel(labelID, ws, function (data) {
                if (data) {
                    //window.__CurrentLabels = null;
                    if (window.FireLabelRemove) window.FireLabelRemove(label);
                    //GenerateLabelsList();
                }
            }, (label ? label.util : null));
        }
    });
}
function labelClick(sender) {
    if (window.OnLabelClick) {
        var label = $(sender).data('label-info');
        window.OnLabelClick(label, sender);
    }
}

//alt kısım düzenlenecek.
function UpdateLabels(parentElem) {
    if (!parentElem)
        parentElem = $('#slide_labels');
    if (!window.__CurrentLabels) return;
    if (window.OnLabelListUpdate) window.OnLabelListUpdate();
    var data = window.OnLabelsProcess ? window.OnLabelsProcess(window.__CurrentLabels) : window.__CurrentLabels;
    var ulLabelList = $('<ul class="list-group" id="ulLabelList"></ul>');


    parentElem.empty();
    parentElem.append(ulLabelList);
    if (data.length > 0) {
        $("#panelLabels").css("display", "");
        if (window.RegisterTimeCompare)
            data.sort(window.RegisterTimeCompare);
        for (var i = 0; i < data.length; i++) {
            if (window.OnLabelAddMark)
                window.OnLabelAddMark(data[i]);
            //var newElem = $('<a class="list-group-item" href=# onclick="simulateLabelClick(' + data[i].LabelId + ')" title="' + data[i].UserDescription + '" style="font-size:8pt">' +
            //    formatLabelList(data[i]) + '</a>');
            var newElem = $('<li class="list-group-item" style="font-size:8pt;padding:6px 6px;cursor:pointer" onclick="labelClick(this)">' + formatLabelList(data[i]) + '</li>');
            newElem.data('label-info', data[i]);
            ulLabelList.append(newElem);
        }

        $(".inrightLabelsCount").html(data.length)
        $(".inrightLabelsCount").css("display", "inline")
    }
    //if(window.ShowMarks)
    //    window.ShowMarks($("#markCheckBox").prop("checked"));
    else {
        $(".inrightLabelsCount").css("display", "none")
        var txt = "[$default:no_record/js]";
        if (window.CreateEmptyLabel && !window._hideAddLabel) {
            txt = "[$default:no_record_note/js]".replace("{0}", "<a href=javascript:void(0) onclick=___AddNoteForDoc()>");
            txt = txt.replace("{1}", "</a>");
        }



        parentElem.empty();
        //parentElem.append($('<p style="padding-left:5px;padding-top:5px">' + '[-$-default:no_record]' + '</p>'));
        parentElem.append($('<p style="padding-left:5px;padding-top:5px">' + txt + '</p>'));
        //$("#panelLabels").css("display", "none");
    }
}

function ___AddNoteForDoc() {
    var geodiLabel = window.CreateEmptyLabel();
    startAddLabel(null, geodiLabel);
}

