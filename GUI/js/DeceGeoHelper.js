﻿window.TableGeometryType = {
    Unknown: 0, /*Anyone*/
    NonSpatial: 1,
    Point: 2,
    LineString: 4,
    Polygon: 8,
    MultiPoint: 16,
    MultiLineString: 32,
    MultiPolygon: 64,
    GeometryCollection: 128,
    AllSpatial: this.Point | this.LineString | this.Polygon | this.MultiPoint | this.MultiLineString | this.MultiPolygon | this.GeometryCollection,
    Is: function (geotype, geotypeTarget) {
        return (geotype == geotypeTarget) || ((geotype & geotypeTarget) == geotypeTarget);
    },
    Has: function (geotype, geotypeTargets) {
        return (geotype == geotypeTargets) || ((geotype & geotypeTargets) != 0);
    },
    IsSpatial: function (geotype) {
        return geotype != this.NonSpatial;
    }
}
