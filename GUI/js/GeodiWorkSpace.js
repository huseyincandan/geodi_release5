﻿function WorkspaceSettings() {
    this.CheckUpperCaseForDictionary = false;
    this.DisableMultiTaskProcessing = false;
    this.NotUseGeoDisticntForTest = false;
}
window.progressbarTimeout = 10000;


function GetRuningStatusText(RuningStatus) {
    switch (RuningStatus) {
        case 1: return "[$setup_workspace_list:stop/js]"; break; //scan all
        case 2: return "[$setup_workspace_list:stopping/js]"; break;
        case 3: return "[$setup_workspace_list:start/js]"; break;
        case 4: return "[$setup_workspace_list:stop/js]"; break; //scan changes
    }
}
function GetRuningStatusClass(RuningStatus) {
    switch (RuningStatus) {
        case 1: return "stop"; break; //scan all
        case 2: return "loading24"; break; //waiting
        case 3: return "play"; break;
        case 4: return "stop"; break; //scan changes
    }
}

function GetWsPauseStatusText(RuningStatus) {
    if (RuningStatus)
        return "Project Pause";
    return ""; //none
}



function GetErrorTitleForWs(errObj) {
    if (errObj && errObj.TotalError)
        errObj = errObj.TotalError;
    var res = '';
    if (errObj) {
        if (errObj.TotalErrorCount > 0)
            res += FormatNumber(errObj.TotalErrorCount)+ " [$setup_workspace_list:workspace_error_info/js]";
        if (errObj.WarningCount && errObj.WarningCount > 0) {
            res += res.length > 0 ? ", " : "";
            res += FormatNumber(errObj.WarningCount) + " [$setup_workspace_list:workspace_warning_info/js]";
        }            
        if (errObj.InformationCount && errObj.InformationCount > 0) {
            res += res.length > 0 ? ", " : "";
            res += FormatNumber(errObj.InformationCount) + " [$setup_workspace_list:workspace_information_info/js]";
        }

        if (res.length > 0) {
            res += res.indexOf(",") > -1 ? "\r\n" : ". ";
            res += "[$setup_workspace_list:workspace_detail_info/js]";
        }
        
    }
    return res;
}



function avgTrengForWS(sz) {
    this.buffer = [];
    for (var i = 0 ; i < sz; i++)
        this.buffer[i] = 0;
    this.total = 0;
    this.count = 0;
    this.index = 0;
    this.maxsz = sz;
    this.update = function (kbps) {
        var total2 = this.total - this.buffer[this.index] + kbps;
        this.total = total2;
        this.buffer[this.index] = kbps
        this.index = (this.index + 1) % this.maxsz
        if (this.count < this.maxsz)
            this.count++;
        return this.total / this.count;
    }
}


function updateTotalStatusDivForWS(div, tsData, options, parallelScanStatusObj) {
    var hideChartDiv = false;

    if (tsData.All && tsData.All.hideChartDiv)
        hideChartDiv = true;
    updateChartForWs(div, tsData.speedTime, hideChartDiv,tsData.isScanActive,options);

    $(".ts-info", $(div)).remove();
    $(div).append(createInfoDivForWs(tsData, parallelScanStatusObj));
}
function _CreateChartyArray(cnt, val, lastVal) {
    var r = [];
    for (var i = 0; i < cnt; i++)
        r.push(val);
    if (lastVal != undefined)
    {
        if (r.length == 0)
            r.push(lastVal);
        else
            r[r.length - 1] = lastVal;
    }
    return r;
}


function updateChartForWs(div, speedTime, hideChartDiv, isScanActive, options) {
    if (!div) return;
    if (hideChartDiv) {
        if (div) {
            $("#c3" + div.id).remove();
        }
        return;
    }
    var chartDiv = $(".c3", $(div));
    if (speedTime && isScanActive) {
        if (!chartDiv.length) {
            //speedTime.speed, speedTime.time,avgTrendVal , avgTrendUseVal
            var chartId = "c3" + div.id;
            var chartDiv = $('<div class="scan-chart c3" id="' + chartId + '"><canvas id="cv' + chartId + '"/></div>');
            $(div).prepend(chartDiv);
            
            {
                var ctx = document.getElementById("cv"+chartId).getContext('2d');
                var chartData = {
                    type: options.chartType?options.chartType:'line',
                    data: {
                        labels: 
                            _CreateChartyArray(options.chartFirstFillCount, "", FormatSecond(speedTime.time / 1000, 0, ["[$setup_workspace_list:hour_short/js]", "[$setup_workspace_list:minute_short/js]", "[$setup_workspace_list:second_short/js]"])),
                            
                        datasets: [
                             {
                                 label: "Trend",
                                 data: _CreateChartyArray(options.chartFirstFillCount, 0, speedTime.avgTrendUseVal),
                                 backgroundColor: "rgba(54, 162, 235,0.5)",
                                 borderColor: "rgba(54, 162, 235,0.9)",
                                 borderWidth:2,
                                 pointRadius: 0,
                                 fill: true
                             },
                            {
                                label: "Ignore Trend",
                                data: _CreateChartyArray(options.chartFirstFillCount, 0, speedTime.avgTrendIgnoreVal),
                                backgroundColor: "rgba(75, 192, 192,0.5)",
                                borderColor: "rgba(75, 192, 192,0.9)",
                                borderWidth: 2,
                                pointRadius: 0,
                                fill: false
                            },
                            {
                                label: "Total Trend",
                                data: _CreateChartyArray(options.chartFirstFillCount, 0, speedTime.avgTrendVal),
                                backgroundColor: "rgba(255, 205, 86,0.5)",
                                borderColor: "rgba(255, 205, 86,0.9)",
                                borderWidth: 2,
                                pointRadius: 0,
                                fill: true,
                                hidden: true
                            },
                            {
                                label: "Avg",
                                data: _CreateChartyArray(options.chartFirstFillCount, 0, speedTime.speed),
                                backgroundColor: "rgba(255, 159, 64,0.5)",
                                borderColor: "rgba(255, 159, 64,0.9)",
                                pointRadius: 0,
                                borderWidth:2,
                                fill: false,
                                hidden: true
                            },
                             {
                                 label: "Opt. Trend",
                                 geodiExtP: options.OptMltFactor,
                                 data: _CreateChartyArray(options.chartFirstFillCount, 0, speedTime.OptmTrendVal),
                                 backgroundColor: "rgba(255, 99, 132,0.5)",
                                 borderColor: "rgba(255, 99, 132,0.9)",
                                 borderWidth: 2,
                                 pointRadius: 0,
                                 fill: false,
                                 hidden: true
                             }
                        ]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio:false,
                        elements: {
                            tension: 0.001
                        },
                        legend: {
                            position: 'bottom',
                            labels: {
                                boxWidth: 20,
                                fontSize: options.chartLegendFontSize?options.chartLegendFontSize:10
                            }

                        },
                        animation: {
                            animateRotate: true,
                            animateScale: true
                        },
                        scales: {
                            xAxes: [{
                                display: false
                            }],
                            yAxes: [{
                                display: true,
                                ticks: {
                                    min: 0
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: '[$setup_workspace_list:FileShort/js]/s'
                                }
                            }]
                        },
                        tooltips: {
                            position: "average",
                            mode: 'index',
                            intersect: false,
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    var dset = data.datasets[tooltipItem.datasetIndex];
                                    return dset.label + " : " + FormatNumber(tooltipItem.yLabel * (dset.geodiExtP > 0 ? dset.geodiExtP : 1), 2);
                                }
                            }
                        }
                    }
                };

                if (!window.DevelopmentDomain && !window.IsServerVersion) {
                    chartData.data.datasets.splice(1, chartData.data.datasets.length - 1);
                }
                var chartControl = new Chart(ctx, chartData);
                chartDiv.data("cData", chartData);
                chartDiv.data("cControl", chartControl);
            }

        }
        else {
            chartData=chartDiv.data("cData");
            chartControl=chartDiv.data("cControl");

            var dataset = chartData.data.datasets;
            if (chartDiv.attr("complete")) {
                chartData.data.labels = _CreateChartyArray(options.chartFirstFillCount, "");
                dataset[0].data = _CreateChartyArray(options.chartFirstFillCount, 0);
                if (window.DevelopmentDomain || window.IsServerVersion) {
                    dataset[1].data = _CreateChartyArray(options.chartFirstFillCount, 0);
                    dataset[2].data = _CreateChartyArray(options.chartFirstFillCount, 0);
                    dataset[3].data = _CreateChartyArray(options.chartFirstFillCount, 0);
                    dataset[4].data = _CreateChartyArray(options.chartFirstFillCount, 0);
                }
                chartDiv.attr("complete", null);
            }
            
            if (options.chartCount <= chartData.data.labels.length) {
                var l = options.chartCount - chartData.data.labels.length;
                chartData.data.labels.splice(0, l);
                dataset[0].data.splice(0, l);
                if (window.DevelopmentDomain || window.IsServerVersion) {
                    dataset[1].data.splice(0, l);
                    dataset[2].data.splice(0, l);
                    dataset[3].data.splice(0, l);
                    dataset[4].data.splice(0, l);
                }
            }
            chartData.data.labels.push(FormatSecond(speedTime.time / 1000, 0, ["[$setup_workspace_list:hour_short/js]", "[$setup_workspace_list:minute_short/js]", "[$setup_workspace_list:second_short/js]"]));
            dataset[0].data.push(speedTime.avgTrendUseVal)
            if (window.DevelopmentDomain || window.IsServerVersion) {
                dataset[1].data.push(speedTime.avgTrendIgnoreVal)
                dataset[2].data.push(speedTime.avgTrendVal)
                dataset[3].data.push(speedTime.speed)
                dataset[4].data.push(speedTime.OptmTrendVal)
            }
            chartControl.update();
        }
    }
    else if (!isScanActive && chartDiv.length) {
        chartDiv.attr("complete", true);
    }
}


function createInfoDivForWs(tsData, parallelScanStatusObj) {
    var div = $('<div class="ts-info"></div>');

    var sttDisplayName = $('<div class="col-xs-12 plabelfull">' + (tsData.displayName ? (tsData.displayName + (tsData.totalPercent ? (" ( " + FormatNumber(tsData.totalPercent.processed,0) + " / " + FormatNumber(tsData.totalPercent.total,0) + " ) "):"")) : "") + '&nbsp;</div>');
    appendRow(div).append(sttDisplayName);
    if (tsData && tsData.All && tsData.All.TotalExData) {
        var htmdsp = [];
        $.each(tsData.All.TotalExData, function (k, v) {
            if (v != null && v.Name)
                htmdsp.push(k + ":" + v.Name + " " + ((window.moment) ? "( " + moment(parseNetDate(v.LastChange)).fromNow() + ")" : ""));
            else if (v)
                htmdsp.push(k + ":" + v);
        })
        htmdsp = htmdsp.join("\r\n");
        sttDisplayName.attr("title", htmdsp);
    }
    var percent = tsData.totalPercent;
    appendRow(div).appendData([{
        key: "[$setup_workspace_list:ProgressStatusProcessNext/js]",
        value: percent ? FormatNumber(percent.total - percent.scanned) +"~": ""
    },
    {
        key: "[$setup_workspace_list:ProgressStatusTime/js]",
        value: tsData.time ? tsData.time : ""
    }]);

    appendRow(div).appendData([{
        key: "[$setup_workspace_list:ProgressStatusProcessed/js]",
        value: percent ? FormatNumber(percent.processed) : ""
    },
   {
       key: "[$setup_workspace_list:ProgressStatusIgnored/js]",
       value: percent ? FormatNumber(percent.ignored) : ""
   }]);

    if (window.DevelopmentDomain || window.IsServerVersion) 
        appendRow(div).appendData(
            [
                {
                    key: "Row",
                    value: percent ? FormatNumber(percent.C4) : ""
                },
                { key: "[$setup_workspace_list:ProgressStatusParsedKeyword/js]", value: tsData.counters?tsData.counters.parsedKw:0 }
            ]
            );
    else
        appendRow(div).appendData(
           [
                { key: "[$setup_workspace_list:ProgressStatusParsedKeyword/js]", value: tsData.counters ? tsData.counters.parsedKw : 0 }
           ]
           );

    if (tsData.counters) {
        

        appendRow(div).appendData([
            { key: "[$setup_workspace_list:ProgressStatusFileSize/js]", value: tsData.counters.fileSize },
            { key: "[$setup_workspace_list:ProgressStatusStringSize/js]", value: tsData.counters.stringSize }
        ]);
        
    }


    if (parallelScanStatusObj && (parallelScanStatusObj.TotalError && (parallelScanStatusObj.TotalError.TotalErrorCount ||
                                                                      (parallelScanStatusObj.TotalError.WarningCount && parallelScanStatusObj.TotalError.WarningCount) ||
                                                                      (parallelScanStatusObj.TotalError.InformationCount && parallelScanStatusObj.TotalError.InformationCount)))) {

        var res = '';
        if (errObj) {
            if (errObj.TotalErrorCount > 0)
                res += FormatNumber(errObj.TotalErrorCount) + " [$setup_workspace_list:workspace_error_info/js]";
            if (errObj.WarningCount && errObj.WarningCount > 0) {
                res += res.length > 0 ? ", " : "";
                res += FormatNumber(errObj.WarningCount) + " [$setup_workspace_list:workspace_warning_info/js]";
            }
            if (errObj.InformationCount && errObj.InformationCount > 0) {
                res += res.length > 0 ? ", " : "";
                res += FormatNumber(errObj.InformationCount) + " [$setup_workspace_list:workspace_information_info/js]";
            }

            if (res.length > 0) {
                res += res.indexOf(",") > -1 ? "\r\n" : ". ";
                res += "[$setup_workspace_list:workspace_detail_info/js]";
            }
        }

        var link = $('<a href="#"><small>' + res + '<small><a>');
        div.append(link);
        link.click(function (e) {
            e.preventDefault();
            var wsName = HttpUtility.UrlEncode(tsData.wsName);

            if (parallelScanStatusObj)
                _PSCCDownload('~/WorkspaceErrorViewer?wsName=' + HttpUtility.UrlEncode(wsName) + '&ParallelStatusID=' + HttpUtility.UrlEncode(parallelScanStatusObj.ID), null, { ShowOpenAs: false });

            return false;
        });
    }

    return div;

    function appendRow(div) {
        var row = $('<div class="row"></div>');
        div.append(row);
        row.appendData = function (data) {
            if (!data.length) data = [data];
            data.forEach(function (item, index) {
                var label = $('<div class="noVisibleInRecovery col-xs-3 plabel ' + (index == 1 ? "plabelright" : "") + '"></div>');
                row.append(label);
                label.text(item.key);

                var value = $('<div class="noVisibleInRecovery col-xs-3 pvalue"></div>');
                row.append(value);
                value.text(item.value);

                // (GEODI-2922) Dosya sayıları tooltip olarak gösterilecek.
                if (item.tooltip) {
                    value.attr("data-toggle", "tooltip");
                    value.attr("title", item.tooltip);
                }
            });
        };
        return row;
    }
}

window.__lastWsStatus = {};

function WorkSpace(pObject) {
    if (pObject) {
        if (typeof (pObject) != "object")
            pObject = JSON.parse(pObject);
        $.extend(this, pObject);
    }
    else {
        this.Name = '';
        this.ContentReaderEnumarators = [];
        this.Layers = [];
        this.Settings = new WorkspaceSettings();
        this.StorageManagers = [];
    }
    this.GetWorkSpace = function (workSpaceName, isTemplate, ForSimple, callBackFunction,options) {
        window.currentWorkspace = new WorkSpace();
        var ws = window.currentWorkspace;
        var cb = callBackFunction;
        ws.IsTemplate = isTemplate;
        var postData = { 'wsName': workSpaceName, 'isTemplate': isTemplate };

        if (options && options.DisableLocalize)
            postData.DisableLocalizeForce = "1";

        $.post('GeodiJSONService?op=' + (ForSimple ? 'getWorkspaceInfo' : 'getWorkspace'),
            postData
            , function (data, textStatus) {
            $.extend(ws, data);
            window.__lastloadedWsClone = JSON.parse(JSON.stringify(window.currentWorkspace));
            callBackFunction(data, textStatus);
        }, 'json');
    }
    this.saveWorkSpace = function (callBackFunction) {
        $.post('GeodiJSONService?op=saveWorkspace', { 'ws': HttpUtility.ToBase64(JSON.stringify(this)), 'WsName': this.Name }, callBackFunction, 'json');
    }
    this.startWorkspace = function (wsName, restart, callBackFunction,options) {
        var postData = { 'wsName': wsName, "restart": restart ? true : false }
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }

        $.post('GeodiWorkspaceManagerService?op=startWorkspace', postData, callBackFunction, 'json');
    }
    this.startRestartOCR = function (wsName, QueryText, callBackFunction, options) {
        var postData = { 'wsName': wsName, "Query": QueryText }
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }
        $.post('GeodiWorkspaceManagerService?op=startRescanOCRContents', postData, callBackFunction, 'json');
    }
    this.tryCalculateRescanOCRTime = function (wsName, QueryText, callBackFunction, options) {
        var postData = { 'wsName': wsName, "Query": QueryText }
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }
        return $.post( 'GeodiWorkspaceManagerService?op=tryCalculateRescanOCRContentTime', postData, callBackFunction, 'json');
    }

    this.tryCheckRescanOCRSupport = function (wsName, callBackFunction,options) {
        var postData = { 'wsName': wsName, };
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }
        return $.post( 'GeodiWorkspaceManagerService?op=tryCheckRescanOCRSupport', postData, callBackFunction, 'json');
    }
    
    this.workspaceMaintenance = function (wsName, mode, callBackFunction,options) {
        var postData = { 'wsName': wsName, "mode": mode };
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }
        $.post( 'GeodiWorkspaceManagerService?op=WorkspaceMaintenance', postData, callBackFunction, 'json');
    }
    this.repairWorkspace = function (wsName, mode, callBackFunction,options) {
        var postData = { 'wsName': wsName, "mode": mode };
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }
        $.post( 'GeodiWorkspaceManagerService?op=RepairWorkspace', postData, callBackFunction, 'json');
    }
    this.stopWorkspace = function (wsName, callBackFunction, options) {
        var postData = { 'wsName': wsName }
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }

        $.post('GeodiWorkspaceManagerService?op=stopWorkspace', postData, callBackFunction, 'json');
    }

    this.deleteWorkspace = function (wsName, callBackFunction, errorFunction,options) {
        var postData = { 'wsName': wsName }
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }

        $.post('GeodiWorkspaceManagerService?op=deleteWorkspace', postData, callBackFunction, 'json').fail(errorFunction);
    }
    this.setLastSelectedWorkspace = function (wsName, callBackFunction) {
        $.post('GeodiWorkspaceManagerService?op=setLastSelectedWs', { 'wsName': wsName }, callBackFunction, 'json');
    }
    this.getWorkspaceList = function (templates, callBackFunction, getIsInRecovery, options,OnAjaxError) {
        var postData = { 'templates': templates, 'GetIsInRecovery': getIsInRecovery ? true : false };
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }

        return $.ajax({
            type: "POST",
            url:  'GeodiJSONService?op=getWorkspaceList',
            data: postData,
            success: callBackFunction,
            dataType: 'json',
            OnAjaxError: OnAjaxError

        });

    }
    this.getWorkspaceListWithAuth = function (templates, callBackFunction, getIsInRecovery, options, OnAjaxError) {
        var postData = { 'templates': templates, 'GetIsInRecovery': getIsInRecovery ? true : false };
        if (options && options.ServerID) {
            postData.ServerID = options.ServerID;
            postData.ClientCacheKey = options.ClientCacheKey;
        }

        return $.ajax({
            type: "POST",
            url:  'GeodiJSONService?op=getWorkspaceListWithAuth',
            data: postData,
            success: callBackFunction,
            dataType: 'json',
            OnAjaxError: OnAjaxError

        });

    }
    this.isWSNameAvailable = function (wsName, callBackFunction) {
        $.post('GeodiJSONService?op=isWSNameAvailable', { 'wsName': wsName }, callBackFunction, 'json');
    }

    this.UpdateProgressForWs = function (ws, NoProgressUpdate, options, serverIndex, server) {
        if (ws.CommandStatus) {
            if (ws.RuningStatus != 3) {
                if (!ws.ParallelScanTotalStatuses)
                    ws.ParallelScanTotalStatuses = [];
                ws.TotalStatus.StatusName = "Scaning";
                ws.ParallelScanTotalStatuses.push(ws.TotalStatus);
            }
            ws.RuningStatus = 2;
            ws.CommandStatus.hideChartDiv = true;
            ws.TotalStatus = ws.CommandStatus;
        }

        if (!options) options = {};
        if (options.chartCount == undefined) options.chartCount = 720;//2h
        if (options.chartFirstFillCount == undefined) options.chartFirstFillCount = 0;
        if (options.OptMltFactor == undefined) options.OptMltFactor = 100;
        if (options.trendCount == undefined) options.trendCount = 9;

        var ids = CreateId(ws.Name + (serverIndex != null && serverIndex > -1? serverIndex:''));
        //silineni sil, ekleneni ekle.
        var container = $("#" + ids);

        if (ws.IsInRecoveryOrIndexUpdate) {
            container.addClass("IsInRecoveryOrIndexUpdate");
            ws.RuningStatus = 2; //Recovery/Update sırasında başlat butonu çıkmasın
            ws.TotalStatus.hideChartDiv = true;
        }
            
        else
            container.removeClass("IsInRecoveryOrIndexUpdate");

        var playPause = $("#PlayPause", container);

        
        $("#DtArea", container).html((window.IsTestMode ? 'TestMode' : netDateToStr(ws.WS_LastScanEndTime, false)));


        var progress = $(".progress-bar", container);
        var recoveryStatus = $(".recoveryStatus", container);
        if (ws.TotalStatus)
            recoveryStatus.html((ws.TotalStatus.DisplayName ? ws.TotalStatus.DisplayName : "") + " " + (ws.TotalStatus.InfoText ? ws.TotalStatus.InfoText : ""));
        else
            recoveryStatus.html("");
        var btnPlayPause = playPause.parent();
        var inPauseMode = $(".ws-pause-mode", btnPlayPause);
        if (ws.IsPausedProject) {
            inPauseMode.css("display","")
            inPauseMode.attr("title", GetWsPauseStatusText(ws.IsPausedProject));            
        }
        else {
            inPauseMode.hide();
        }
        btnPlayPause.attr("title", GetRuningStatusText(ws.RuningStatus));

        playPause.attr("wsStatus", ws.RuningStatus);
        playPause.attr("supportChangeScan", ws.IsScaned ? '1' : '0');

        playPause.attr("class", 'img-' + GetRuningStatusClass(ws.RuningStatus) + ' img-default');
        var projico = $("#wsico", container);

        projico.attr("class", 'img-default32' + (ws.TotalStatus.TotalError && (ws.TotalStatus.TotalError.TotalErrorCount > 0 ||
            (ws.TotalStatus.TotalError.WarningCount && ws.TotalStatus.TotalError.WarningCount > 0) ||
            (ws.TotalStatus.TotalError.InformationCount && ws.TotalStatus.TotalError.InformationCount > 0))? '-warning' : ''));
        if (ws.TotalStatus.TotalError && (ws.TotalStatus.TotalError.TotalErrorCount > 0 ||
            (ws.TotalStatus.TotalError.WarningCount && ws.TotalStatus.TotalError.WarningCount > 0) ||
            (ws.TotalStatus.TotalError.InformationCount && ws.TotalStatus.TotalError.InformationCount > 0)))
            $(".hasErrWsAction", container).show();
        else
            $(".hasErrWsAction", container).hide();

        projico.attr("title", GetErrorTitleForWs(ws.TotalStatus));

        var wsIcoName = ws.IconName;
        if (!wsIcoName) wsIcoName = "Workspace/workspace32";
        var ImgSrcurl = HttpUtility.ResolveUrl( '~/GUIJsonService?op=getIco&IconName=' + wsIcoName, true, true);

        if (server && server.ServerID)
            ImgSrcurl += '&ServerID=' + server.ServerID + '&ClientCacheKey=' + server.ClientCacheKey; //not supported url ??

        

        $("img", projico).attr("src", ImgSrcurl);

        if (!NoProgressUpdate)
            progress.attr("data-transitiongoal", (ws.TotalStatus.TotalPercent ? ws.TotalStatus.TotalPercent.Percent : 0)).progressbar();

        var getTSData = function (TotalStatus,ParallelScanPanel) {
            var tsData = { isScanActive: ParallelScanPanel ? true : ws.RuningStatus != 3, wsName: ws.Name };
            tsData.All = TotalStatus;
            if (TotalStatus && TotalStatus.TotalPercent) {
                if (TotalStatus.DisplayName) {
                    tsData.displayName = TotalStatus.DisplayName;
                }

                tsData.totalPercent = {
                    scanned: TotalStatus.TotalPercent.TotalValue,
                    total: TotalStatus.TotalPercent.Count,
                    processed: TotalStatus.TotalPercent.UsefulValue,
                    ignored: TotalStatus.TotalPercent.Value,
                    C4: TotalStatus.TotalPercent.Counters.C4,
                    C5: TotalStatus.TotalPercent.Counters.C5
                };

            }
            if (TotalStatus && TotalStatus.TotalError && TotalStatus.TotalError.TotalErrorCount > 0) {
                tsData.error = FormatNumber(TotalStatus.TotalError.TotalErrorCount);

            }

            if (TotalStatus && TotalStatus.TotalMS > 0) {
                tsData.time = FormatSecond((TotalStatus.TotalMS / 1000), 0, ["[$setup_workspace_list:hour_short/js]", "[$setup_workspace_list:minute_short/js]", "[$setup_workspace_list:second_short/js]"]);
            }

            if (TotalStatus && TotalStatus.TotalPercent && TotalStatus && TotalStatus.TotalMS > 0 &&
                (TotalStatus.TotalPercent.TotalValue > 0 || TotalStatus.TotalPercent.UsefulValue > 0)) {
                tsData.avg = FormatNumber(TotalStatus.TotalPercent.TotalValue / (TotalStatus.TotalMS / 1000), 3);

                if (ParallelScanPanel || ws.RuningStatus != 3) {

                    var lastStatusInfo = null;
                    if (ParallelScanPanel)
                        lastStatusInfo = (ParallelScanPanel.LastScanInfo) ? ParallelScanPanel.LastScanInfo : (ParallelScanPanel.LastScanInfo = new avgTrengForWS(options.trendCount));
                    else
                        lastStatusInfo = (window.__lastWsStatus[ids]) ? window.__lastWsStatus[ids] : (window.__lastWsStatus[ids] = new avgTrengForWS(options.trendCount));

                    if (lastStatusInfo.TotalStatus) {
                        var ttlFrk = (TotalStatus.TotalPercent.TotalValue + (TotalStatus.TotalPercent.Counters.C4 / 100)) - (lastStatusInfo.TotalStatus.TotalPercent.TotalValue + (lastStatusInfo.TotalStatus.TotalPercent.Counters.C4 / 100));
                        var ttlOptm = 0;
                        if (TotalStatus.DisplayName && TotalStatus.DisplayName.indexOf(":Optimizing") != -1) {
                            try {
                                var dsp = TotalStatus.DisplayName + "";
                                var idp = dsp.lastIndexOf("(");
                                if (idp != -1) {
                                    dsp = dsp.substr(idp + 1);
                                    var vl = eval(dsp.split("/")[0]);
                                    if (vl >= lastStatusInfo.OptVal)
                                        ttlOptm = (vl - lastStatusInfo.OptVal) / 10;
                                    lastStatusInfo.OptVal = vl;
                                }
                            }
                            catch (e) { }
                        }
                        else
                            lastStatusInfo.OptVal = 0;
                        var tm = TotalStatus.TotalMS - lastStatusInfo.TotalStatus.TotalMS;
                        if (tm > 0) {
                            var kbps = (ttlFrk > 0) ? 1000 / (tm / ttlFrk) : 0;//saniyde kaç adet
                            tsData.speedTime = { speed: kbps, time: TotalStatus.TotalMS };
                            var res = lastStatusInfo.update(kbps); //s
                            var strEx = "";
                            var tsDataEx = "";
                            if (lastStatusInfo.lastRes != undefined) {
                                var frkStime = (res - lastStatusInfo.lastRes);
                                strEx = " ( " + (frkStime == 0 ? "" : (frkStime > 0 ? "↑" : "↓")) + " " + FormatNumber(frkStime, 3) + " )"
                                if (frkStime > 0) tsDataEx = "(↑)";
                                else if (frkStime < 0) tsDataEx = "(↓)";
                            }
                            lastStatusInfo.lastRes = res;
                            tsData.avgTrend = FormatNumber(Math.max(res, 0), 3) + tsDataEx;
                            tsData.speedTime.avgTrendVal = Math.max(res, 0);


                            if (!lastStatusInfo.UsageTrend)
                                lastStatusInfo.UsageTrend = new avgTrengForWS(options.trendCount);
                            var ttlUsed = (TotalStatus.TotalPercent.UsefulValue + (TotalStatus.TotalPercent.Counters.C4 / 100)) - (lastStatusInfo.TotalStatus.TotalPercent.UsefulValue + (lastStatusInfo.TotalStatus.TotalPercent.Counters.C4 / 100));
                            kbps = (ttlUsed > 0) ? 1000 / (tm / ttlUsed) : 0;//saniyde kaç adet
                            tsData.speedTime.avgTrendUseVal = Math.max(lastStatusInfo.UsageTrend.update(kbps), 0);

                            if (!lastStatusInfo.IgnoreTrend)
                                lastStatusInfo.IgnoreTrend = new avgTrengForWS(options.trendCount);
                            ttlUsed = (TotalStatus.TotalPercent.Value) - (lastStatusInfo.TotalStatus.TotalPercent.Value);
                            kbps = (ttlUsed > 0) ? 1000 / (tm / ttlUsed) : 0;//saniyde kaç adet
                            tsData.speedTime.avgTrendIgnoreVal = Math.max(lastStatusInfo.IgnoreTrend.update(kbps), 0);


                            if (!lastStatusInfo.OptmTrend)
                                lastStatusInfo.OptmTrend = new avgTrengForWS(options.trendCount);
                            kbps = (ttlOptm > 0) ? 1000 / (tm / ttlOptm) : 0;//saniyde kaç adet
                            tsData.speedTime.OptmTrendVal = (Math.max(lastStatusInfo.OptmTrend.update(kbps), 0) / options.OptMltFactor);
                            lastStatusInfo.avgText = tsData.avgTrend;
                            lastStatusInfo.TotalStatus = TotalStatus;
                        }
                        else if (lastStatusInfo.avgText) {
                            tsData.avgTrend = lastStatusInfo.avgText;
                        }

                    }
                    else
                        lastStatusInfo.TotalStatus = TotalStatus;

                    var all = TotalStatus.TotalPercent.Count - TotalStatus.TotalPercent.UsefulValue; //TotalValue
                    var sTime = TotalStatus.TotalPercent.UsefulValue / (TotalStatus.TotalMS / 1000); // TotalValue

                    if (all > 1 && sTime > 0) {
                        var lLeft = all / sTime;
                        TotalStatus.TotalPercent.Count - TotalStatus.TotalPercent.TotalValue
                        tsData.leftTime = "~" + FormatSecond(lLeft, 0, ["[$setup_workspace_list:hour_short/js]", "[$setup_workspace_list:minute_short/js]", "[$setup_workspace_list:second_short/js]"]);
                    }
                }
                else if (!ParallelScanPanel)
                        window.__lastWsStatus[ids] = null;

            }

            if (TotalStatus && TotalStatus.TotalPercent && TotalStatus.TotalPercent.Counters) {
                tsData.counters = {
                    parsedKw: FormatNumber(TotalStatus.TotalPercent.Counters.C1),
                    stringSize: FormatBytes(TotalStatus.TotalPercent.Counters.C2, 0, ["GB", "MB", "KB", "[$setup_workspace_list:Bytes/js]"]),
                    fileSize: FormatBytes(TotalStatus.TotalPercent.Counters.C3, 0, ["GB", "MB", "KB", "[$setup_workspace_list:Bytes/js]"])
                };

            }

            return tsData;
        }
        
        
        var tsData = getTSData(ws.TotalStatus, null);
        progress.attr("class", 'progress-bar progress-bar-striped progress-bar-' + (tsData.isScanActive ? 'warning active' : 'info'));
        if (Object.keys(tsData).length > 1) {
            $(".ts-toggle", container).show();
            updateTotalStatusDivForWS(document.getElementById("ts" + ids), tsData, options);
        }
        
        var parallelScans = $(".parallelScans", container);
        if (parallelScans && parallelScans.length > 0) {
            var psHeaders = $(".parallelHeaders", parallelScans);
            var psPanels = $(".parallelPanels", parallelScans);
            var hideForce = false;
            if (ws.ParallelScanTotalStatuses && ws.ParallelScanTotalStatuses.length > 0) {
                var allParalllelScanId = [];
                $(".ps_span", psHeaders).removeClass("__check").addClass("__check");
                
                var renderAny = false;
                for (var i = 0; i < ws.ParallelScanTotalStatuses.length; i++) {
                    var ps = ws.ParallelScanTotalStatuses[i];
                    if (!ps.StatusName || !ps.ID)  //|| ps.StatusName.toLowerCase().indexOf("bg.") == 0
                        continue;
                    renderAny = true;
                    var psIds = CreateId(ps.ID);
                    var pnlHeader = $(".p_" + psIds, psHeaders);
                    if (pnlHeader.length == 0) {
                        pnlHeader = $("<a href='javascript:void(0);' class='ps_span p_" + psIds + "'></a>");
                        pnlHeader.data("statusID", psIds);
                        pnlHeader.html(ps.StatusName);
                        psHeaders.append(pnlHeader);
                        psPanels.append($("<div class='pp_panel pp_" + psIds + "' style='display:none' ></div>"));
                        pnlHeader.click(function () {
                            var obj = $(this);
                            var statusID = obj.data("statusID");
                            var pnl1 = $(".pp_" + statusID, psPanels);
                            $(".pp_panel", psPanels).not(pnl1).hide();
                            $(".ps_span", psHeaders).removeClass("ps_active");
                            if (pnl1.is(":visible")) {
                                pnl1.hide();
                            }
                            else {
                                pnl1.show();
                                obj.addClass("ps_active");
                            }
                        })
                    }
                    var tsDataSub = getTSData(ps, false, pnlHeader[0]);
                    updateTotalStatusDivForWS($(".pp_" + psIds, psPanels), tsDataSub, options, ps);
                    pnlHeader.removeClass("__check");
                }
                if (renderAny)
                    parallelScans.show();
                else
                    hideForce = true;
                
                $(".__check", psHeaders).each(function () {
                    var obj = $(this);
                    var statusID = obj.data("statusID");
                    obj.remove();
                    $(".pp_" + statusID, psPanels).remove();
                })
            }
            else
                hideForce = true;

            if (hideForce) {
                psHeaders.empty();
                psPanels.empty();
                parallelScans.hide();
            }
        }

        
        

    }
}

window.currentWorkspace = new WorkSpace();
