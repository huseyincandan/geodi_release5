﻿function AuthorizationManager() {
    this.GetSystemUserGroupsSimpleInfo = function (callBackFunction) {
        $.post('UserManagementService?op=GetSystemUserGroupsSimpleInfo', {}, callBackFunction, 'json');
    }
    this.GetSystemUserGroup = function (id, callBackFunction) {
        $.post('UserManagementService?op=GetSystemUserGroup', { 'id': id }, callBackFunction, 'json');
    }
    this.UpdateUserGroups = function (userGroups, callBackFunction) {
        $.post('UserManagementService?op=UpdateUserGroups', { 'userGroups': JSON.stringify(userGroups) }, callBackFunction, 'json');
    }
    this.DeleteGroups = function (ids, callBackFunction) {
        $.post('UserManagementService?op=DeleteGroups', { 'ids': JSON.stringify(ids) }, callBackFunction, 'json');
    }

    this.GetAllPermissionSet = function (callBackFunction) {
        var that = this;
        if (that.__GetAllPermissionSetLast)
            callBackFunction(that.__GetAllPermissionSetLast);
        else
            $.post('UserManagementService?op=GetAllPermissionSet', {}, function (data, b, c) {
                if (data)
                    that.__GetAllPermissionSetLast = data;
                    callBackFunction(data, b, c);
            }, 'json');
    }
    this.GetSystemUserGroupByDisplayName = function (displayName, callBackFunction) {
        $.post('UserManagementService?op=GetSystemUserGroupByDisplayName', { 'displayName': displayName }, callBackFunction, 'json');
    }
    this.GetUsers = function (filter, callBackFunction) {
        $.post('UserManagementService?op=GetUsers', { 'filter': filter }, callBackFunction, 'json');
    }
    this.GetUsersAdv = function (filter, callBackFunction, options) {
        $.post('UserManagementService?op=GetUsersAdv', { 'filter': filter, 'options': options? JSON.stringify(options):null }, callBackFunction, 'json');
    }
    this.GetSystemUserGroupList = function (filter, callBackFunction) {
        $.post('UserManagementService?op=GetSystemUserGroupList', { 'filter': filter }, callBackFunction, 'json');
    }
    this.GetSystemUserGroupListNames = function (filter, callBackFunction, clientCacheKey) {
        var that = this;
        if (clientCacheKey && that.__GetSystemUserGroupListNames && that.__GetSystemUserGroupListNames[clientCacheKey] && (new Date() - that.__GetSystemUserGroupListNames[clientCacheKey +"__date"])<20000)
            callBackFunction(that.__GetSystemUserGroupListNames[clientCacheKey]);
        else 
            $.post('UserManagementService?op=GetSystemUserGroupListNames', { 'filter': filter }, function (data, b, c) {
                if (clientCacheKey && data) {
                    if (!that.__GetSystemUserGroupListNames)
                        that.__GetSystemUserGroupListNames = {};
                    //time?
                    that.__GetSystemUserGroupListNames[clientCacheKey] = data;
                    that.__GetSystemUserGroupListNames[clientCacheKey + "__date"] = new Date();
                }
                callBackFunction(data, b, c);
            }, 'json');
    }
    this.GetUserSuggestions = function (keyword, callBackFunction) {
        $.post('UserManagementService?op=GetUserSuggestions', { 'keyword': keyword }, callBackFunction, 'json');
    }
    this.GetAllLoginProvider = function (callBackFunction) {
        $.post('UserManagementService?op=GetAllLoginProvider', {}, callBackFunction, 'json');
    }
    this.DeleteUser = function (providerName, userID, callBackFunction) {
        $.post('UserManagementService?op=DeleteUser', { 'providerName': providerName, 'userID': userID }, callBackFunction, 'json');
    }
}
window.authorizationManager = new AuthorizationManager();

function UserGroup() {
    this.ID = '';
    this.DisplayName = '';
    this.IsUserReference = false;
    this.IsPermissionReference = false;
    this.Users = [];
    this.Permissions = [];
}

function UserSimpleInfo(obj) {
    if (typeof (obj) == "string")
        obj = { "Name": obj };
    var str = obj.Name;
    this.Passive = obj.Passive;
    this.UserObject = obj;
    this.Attributes = obj.Attributes;
    var x = str.indexOf(':');
    if (x >= 0) {
        this.LoginProviderName = str.substring(0, x);
        this.UserName = str.substring(x + 1, str.length);
    }
    else {
        this.LoginProviderName = '';
        this.UserName = str;
    }
    this.ToString = function () {
        return this.LoginProviderName + ':' + this.UserName;
    }
}

function ShowModalForPemrissions(permissionGroupDisplayName, PermissionConatiner, options) {
    if (!PermissionConatiner.Permissions)
        PermissionConatiner.Permissions = [];
    window.authorizationManager.GetAllPermissionSet(function (data) {
        for (var i = 0; i < data.length; i++) {
            if (data[i].DisplayName == permissionGroupDisplayName) {
                setPermissions(PermissionConatiner.Permissions, data[i].Permissions, window.lbl_enumrator_authorization_label ? window.lbl_enumrator_authorization_label : '[$workspace_wizard:enumrator_authorization_label/js]', PermissionConatiner.DisplayName, options,'permDialog');
                break;
            }
        }
    });
}

function setPermissions(permissions, authorizationItems, labelText, titleText, options, clientCacheKeyForGetSystemUserGroupListNames) {
    if (!permissions)
        permissions = [];
    window.currentAuthorizationPermissions = permissions;
    window.authorizationManager.GetSystemUserGroupListNames('', function (data, textStatus) {
        var table = $('<table class="table table-hover fixed_header" id="tblAuthGroups"></table>');
        var modalContent = $('<div id="setPermission"></div>');
        modalContent.append($('<div class="tab-content permission-tab-content"><div role="tabpanel" class="tab-pane active" id="groups"></div></div>'));
        if (labelText)
            modalContent.find('#groups').append($('<p style="display:inline-block;margin-top: 10px;">' + labelText + '</p>'));
        

        modalContent.find('#groups').append($('<input id="txtEnumeratorAuthGroupSearch" style="display:inline-block" type="text" class="form-control" placeholder="[$default:search/js]">'));
        modalContent.find('#groups').append(table);



        var thead = $("<thead/>", {});
        table.append(thead);

        var row = $('<tr></tr>');
        thead.append(row);
        var cssWith = null;
        if (window.__lastPermWith)
            cssWith = { "width": window.__lastPermWith, "max-width": window.__lastPermWith, "min-width": window.__lastPermWith }

        var emptyTextColumn = $('<th>&nbsp;</th>');
        if (cssWith)
            emptyTextColumn.css(cssWith);
        row.append(emptyTextColumn);

        for (var j = 0; j < authorizationItems.length; j++) {
            var th = $('<th style="text-align:center" title="' + authorizationItems[j].DisplayName + '">' + authorizationItems[j].DisplayName + '</th>');
            if (cssWith)
                th.css(cssWith);
            th.data('item-data', authorizationItems[j]);
            row.append(th);

        }

        if (data) {
            for (var i = 0; i < data.length; i++) {
                var currentItem = null;
                for (var j = 0; j < window.currentAuthorizationPermissions.length; j++) {
                    if (window.currentAuthorizationPermissions[j].ID == data[i].ID) {
                        currentItem = window.currentAuthorizationPermissions[j];
                        break;
                    }
                }


                row = $('<tr></tr>');
                row.data('item-data', data[i]);
                var emptycell = $('<td title="' + data[i].DisplayName + '">' + data[i].DisplayName + '</td>');
                if (cssWith)
                    emptycell.css(cssWith);
                row.append(emptycell);

                table.append(row);
                for (var j = 0; j < authorizationItems.length; j++) {
                    var checked = '';
                    if (currentItem && currentItem.Permissions.indexOf(authorizationItems[j].ID) >= 0)
                        checked = 'checked';
                    var cell = $('<td style="text-align:center"><input type="checkbox"' + checked + '></input></td>');
                    cell.data('item-data', authorizationItems[j]);
                    if (cssWith)
                        cell.css(cssWith);
                    row.append(cell);
                }

            }

        }


        var saveFunction = function () {
            $('#modalDialogLabel').attr('onCloseHandled', true);
            window.currentAuthorizationPermissions.length = 0;

            $('#tblAuthGroups tr').each(function () {
                if ($(this).find('td input[type="checkbox"]:checked').length == 0)
                    return;
                var userGroup = new UserGroup();
                userGroup.ID = $(this).data('item-data').ID;
                userGroup.IsUserReference = true;
                userGroup.IsPermissionReference = false;

                $(this).find('td input[type="checkbox"]:checked').each(function () {
                    userGroup.Permissions.push($(this).parent().data('item-data').ID);
                })
                window.currentAuthorizationPermissions.push(userGroup);
            });

            if (window.checkProjectPermissionsGUI)
                checkProjectPermissionsGUI();
            if (options && options.OnSave)
                options.OnSave();
        };

        if (options)
            options.SaveFunction = saveFunction;

        var buttons = [];
        buttons.push({
            text: '[$default:close/js]', className: 'btn btn-sm btn-default', closeOnClick: true
        });
        buttons.push({
            text: '[$default:ok/js]', className: 'btn btn-sm btn-primary', closeOnClick: true, onClickCallBack: saveFunction
        });
        var op2 = options && options.DialogOptions ? options.DialogOptions : {};
        op2.OnShown2 = function () {
            var pm = $("#permDialog .modal-footer");

            if ($("#chkEnumeratorAuthGroupSearch", pm).length == 0) {
                var pm2 = $(".pull-left", pm);
                if (pm2.length > 0)
                    pm2.append($(' <input id="chkEnumeratorAuthGroupSearch" style="display:inline-block;" type="checkbox" ' + (window.HasCheckedFilterForSearchInTable ? "checked" : "") + ' onclick="window.HasCheckedFilterForSearchInTable=$(this).prop(\'checked\');SearchInTableIn($(\'#txtEnumeratorAuthGroupSearch\'), $(\'#tblAuthGroups\'))" ><label for=chkEnumeratorAuthGroupSearch>[$Authorization:showOnlyAuthorizedGroups/js]</label>'));
                else
                    pm.prepend($('<input id="chkEnumeratorAuthGroupSearch" style="display:inline-block;float:left;" type="checkbox" ' + (window.HasCheckedFilterForSearchInTable ? "checked" : "") + ' onclick="window.HasCheckedFilterForSearchInTable=$(this).prop(\'checked\');SearchInTableIn($(\'#txtEnumeratorAuthGroupSearch\'), $(\'#tblAuthGroups\'))" ><label for=chkEnumeratorAuthGroupSearch style="float:left;">[$Authorization:showOnlyAuthorizedGroups/js]</label>'));
            }
        }
        
        messageBoxEx('default',
            (window.lbl_user_groups ? window.lbl_user_groups : '[$workspace_wizard:user_groups/js]') +
            (titleText ? ' - <span style="text-overflow: ellipsis;overflow:hidden;">' + titleText + '</span>' : ''), modalContent, buttons, null, 
            null , "permDialog", null, { dialogClass: "modal-lg" },
            op2
        );

        

        SearchInTable($('#txtEnumeratorAuthUserSearch'), $('#tblAuthUsers'));
        SearchInTable($('#txtEnumeratorAuthGroupSearch'), $('#tblAuthGroups'));

        SearchInTableIn($('#txtEnumeratorAuthGroupSearch'), $('#tblAuthGroups'));

        window.setTimeout(function () {
            var maxWidth = parseInt($("table.fixed_header tbody").css("width"));
            var colmnWidth = (maxWidth / (authorizationItems.length + 1)) + "px";
            window.__lastPermWith = colmnWidth;
            $("table.fixed_header th, table.fixed_header td").css({ "width": colmnWidth, "max-width": colmnWidth, "min-width": colmnWidth });
        }, 300);

        $("table.fixed_header tbody").bind("scroll", function (e) {
            var tbodyMaxScrollLeft = $(this)[0].scrollWidth - $(this)[0].clientWidth;
            var theadMaxScrollLeft = $(this).parent().find("thead tr")[0].scrollWidth - $(this).parent().find("thead tr")[0].clientWidth;
            var tbodyPercentValue = ($(this)[0].scrollLeft / tbodyMaxScrollLeft) * 100;
            $(this).parent().find("thead tr")[0].scrollTo(((theadMaxScrollLeft / 100) * tbodyPercentValue), $(this).parent().find("thead tr")[0].scrollTop);

        });
    }, clientCacheKeyForGetSystemUserGroupListNames);
}