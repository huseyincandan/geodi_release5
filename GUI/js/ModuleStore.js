﻿function GetToken() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetToken")];
}
function GetServerName() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetServerName")];
}
function GetWsName() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetWsName")];
}
function GetSearchStringsHiden() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetSearchStringsHiden")];
}
function GetContentTypeKey() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetContentTypeKey")];
}
function CurAppIsTrial() {
    return [=c.RestApiJson("ModuleUpdateHandler", "CurrentApplicationIsTrial")] == true;
}

function CancelAllDownloads() {
    $('#downloadingMain').fadeOut();
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=CancelAllDownloads'),
        function (ok) {
        })
        .fail(function (response) {
        });
}
function CancelDownload(moduleId) {
    $('#downloadingMain').fadeOut();
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=CancelDownload'), { 'moduleId': moduleId },
        function (ok) {
        })
        .fail(function (response) {
        });
}

function VersionCompare(v1, v2, options) {
    if (!v1) v1 = "0.0.0.0";
    if (!v2) v2 = "0.0.0.0";
    //ref:https://stackoverflow.com/a/6832721/1266873
    var lexicographical = options && options.lexicographical,
        zeroExtend = options && options.zeroExtend,
        v1parts = v1.split('.'),
        v2parts = v2.split('.');

    function isValidPart(x) {
        return (lexicographical ? /^\d+[A-Za-z]*$/ : /^\d+$/).test(x);
    }

    if (!v1parts.every(isValidPart) || !v2parts.every(isValidPart)) {
        return NaN;
    }

    if (zeroExtend) {
        while (v1parts.length < v2parts.length) v1parts.push("0");
        while (v2parts.length < v1parts.length) v2parts.push("0");
    }

    if (!lexicographical) {
        v1parts = v1parts.map(Number);
        v2parts = v2parts.map(Number);
    }

    for (var i = 0; i < v1parts.length; ++i) {
        if (v2parts.length == i) {
            return 1;
        }

        if (v1parts[i] == v2parts[i]) {
            continue;
        }
        else if (v1parts[i] > v2parts[i]) {
            return 1;
        }
        else {
            return -1;
        }
    }

    if (v1parts.length != v2parts.length) {
        return -1;
    }

    return 0;
}
function MatchRule(str, rule) {
    var escapeRegex = function (str) { return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"); }
    return new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

function OnModulesStatusChanged() {
    RunMetodOnRootWindow("___PCommunicationCommander_OnMessageJson_Internal", { Command: "modulesStatusChanged", ForceRunIframeOption: true });
}
function OnModuleInstallStarted() {
    RunMetodOnRootWindow("___PCommunicationCommander_OnMessageJson_Internal", { Command: "moduleInstallStarted", ForceRunIframeOption: true });
}

function SetPauseHandleAjaxError(val) {
    if (!val) {
        window.PauseHandleAjaxError = false;
        RunMetodOnRootWindow("eval", "window.PauseHandleAjaxError = false; RunMetodOnChildWindows('eval','window.PauseHandleAjaxError = false',true);");
    }
    else {
        window.PauseHandleAjaxError = true;
        RunMetodOnRootWindow("eval", "window.PauseHandleAjaxError = true; RunMetodOnChildWindows('eval','window.PauseHandleAjaxError = true',true);");
    }
}
function _handleAjaxErr(jqXHR, exception) {
    if (jqXHR.status == 401)
        RunMetodOnRootWindow("eval", "_PSCC.Command({ Command: 'Relogin' })");
}

function GetModuleIconName(mod) {
    var iconName;
    try {
        if (mod.HasIcon) {
            if (mod.FCD_Icon)
                iconName = mod.FCD_Icon;
            else
                iconName = HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetIcon&moduleID=' + mod.ID);
        }
        else
            iconName = HttpUtility.ResolveUrl("~/GUI/img/module.png");
    }
    catch (err) {
        iconName = HttpUtility.ResolveUrl("~/GUI/img/module.png");
    }
    if (!iconName)
        iconName = HttpUtility.ResolveUrl("~/GUI/img/module.png");
    return iconName;
}
function GetPreviewIcoUrl(data) {
    if (data && data.JSONData && data.JSONData.ID)
        return HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetPreviewIcon&moduleID=' + data.JSONData.ID);
    return HttpUtility.ResolveUrl(GetServerName() + '/GeodiJSONService?op=getContentThumbnail&ContentIdentifier=' + data.ContentIdentifier + '&wsName=' + HttpUtility.UrlEncode(GetWsName()) + '&rnd=' + window.___DomStartDate
        + "&UserSession=" + HttpUtility.UrlEncode(GetToken()), false);
}

function IsLicenseOK(module) {
    var arr = module.Licenses;
    if (!arr || !arr.length)
        return true;

    var moduleLicenses = [=c.RestApiJson("ModuleUpdateHandler", "GetLicenses")];
    if (moduleLicenses && moduleLicenses.length)
        for (var i = 0; i < arr.length; i++)
            if (moduleLicenses.indexOf(arr[i]) >= 0)
                return true;

    return false;
}
function IsVersionCompatibleWithMainModule(module) {
    var appVerMain = "[=c.AppVersion]";
    var appVerMod = module.MainModuleVersion;
    if (VersionCompare(appVerMain, appVerMod) >= 0)
        return true;
    else
        return false;
}

function IsInstalled(moduleId) {
    return GetInstalledModulesDic().hasOwnProperty(moduleId);
}
function GetInstalledModule(moduleId) {
    return GetInstalledModulesDic()[moduleId];
}
function CanUpdate(mod) {
    var moduleId = mod.ID;
    var dic = GetInstalledModulesDic();
    if (dic.hasOwnProperty(moduleId)) {
        var mod0 = dic[moduleId];
        if (mod0.ModuleVersion && mod.ModuleVersion)
            return VersionCompare(mod.ModuleVersion, mod0.ModuleVersion) > 0;
    }
    return false;
}

function GetInstalledModulesDic() {
    if (!window.__installedModulesDic)
        window.__installedModulesDic = _GetInstalledModulesDic();
    return window.__installedModulesDic;
}
function _GetInstalledModules() {
    return [=c.RestApiJson("ModuleUpdateHandler", "GetInstalledModules")];
}
function _GetInstalledModulesDic() {
    var modules = _GetInstalledModules();
    var dic = {};
    if (modules)
        for (var i = 0; i < modules.length; i++) {
            var mod = modules[i];
            dic[mod.ID] = mod;
        }
    return dic;
}

function _getStatus(callback, failCallback) {
    $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=GetStatus'),
        function (status) {
            if (status && status.length == 2) {
                //var statusId = status[0];
                callback(status[1]);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            failCallback(jqXHR, textStatus, errorThrown);
        });
}

function LoadModules(callback, failCallback) {
    var q = new GeodiQuery();
    q.wsName = GetWsName();
    q.ServerUrl = GetServerName();
    q.Token = GetToken();

    q.StartIndex = 0;
    q.EndIndex = 100000;

    //q.SearchString = searchTxt;
    q.SearchStringsHiden = [GetSearchStringsHiden()];
    q.UseRank = true;
    if (!HttpUtility.ResolveUrl)
        HttpUtility.ResolveUrl = q.ResolveUrl;
    q.OnAjaxError = function (event, data, settings) {
        settings.DisableErrorsEnd = true;
        return false;
    }
  

    RenderDLV(
        {
            Query: q,
            QueryOptions: {
                languagetemp: window.DeceClientState ? window.DeceClientState.GetLanguage() : null
            },
            DisableSummary: true,
            //TargetDiv: "#response",
            //Template: $("#template1").html(),
            OnEndRender: function (args) {
                var dic = _GetInstalledModulesDic();
                if (args.Data && args.Data.length) {
                    for (var i = 0; i < args.Data.length; i++) {
                        var data = args.Data[i];
                        var mod = data.JSONData;

                        if (data.AdditionalValues) {
                            if (data.AdditionalValues.FCD_Icon) {
                                mod.HasIcon = true;
                                mod.FCD_Icon = data.AdditionalValues.FCD_Icon;
                            }
                            if (data.AdditionalValues.ContentSize)
                                mod.UpdateLinkFileSize = data.AdditionalValues.ContentSize;
                        }

                        mod.UpdateLink = GetServerName() + "/GeodiJSONService?op=downloadDoc&FromBackup=1&wsName=" + GetWsName() + "&UserSession=" + HttpUtility.UrlEncode(GetToken()) + "&content_id=" + data.ContentIdentifier;

                        if (!dic.hasOwnProperty(mod.ID)) {
                            dic[mod.ID] = mod;
                        }
                        else {
                            var mod0 = dic[mod.ID];
                            if (VersionCompare(mod.ModuleVersion, mod0.ModuleVersion) > 0) {
                                mod.OldModuleVersion = mod0.ModuleVersion;//bu kurulu demek
                                dic[mod.ID] = mod;
                            }
                        }
                    }
                }
                for (var k in dic) {
                    if (dic.hasOwnProperty(k)) {
                        var mod = dic[k];
                        if (mod.DL_Downloading) delete mod.DL_Downloading;
                        if (mod.DL_Finished) delete mod.DL_Finished;
                        if (mod.DL_Cancelled) delete mod.DL_Cancelled;
                        if (mod.DL_Error) delete mod.DL_Error;
                    }
                }

                _getStatus(function (items) {
                    if (items && items.length) {
                        for (var i = 0; i < items.length; i++) {
                            var stat = items[i];
                            var modID = stat.ID;
                            var mod = dic[modID];
                            mod.STAT = stat;
                            switch (stat.StType)//None = 0, Downloading = 1, DownloadFinished = 2, Uninstalled = 3, Cancelled= 4, Error = 5
                            {
                                case 0://none
                                    break;
                                case 1://downloading
                                    mod.DL_Downloading = true;
                                    mod.DL_Downloaded = stat.Details.PercentStatus.TotalValue;
                                    break;
                                case 2://downloadFinished
                                    mod.DL_Finished = true;
                                    mod.DL_Downloaded = mod.UpdateLinkFileSize;
                                    if (mod.Reset)
                                        mod.InstallRequested = true;
                                    break;
                                case 3://uninstalled
                                    mod.DL_Finished = true;
                                    if (mod.Reset)
                                        mod.UninstallRequested = true;
                                    break;
                                case 4://cancelled
                                    break;
                                case 5://error
                                    mod.DL_Error = stat.Details;
                                    break;
                            }
                        }
                    }

                    var isTrial = CurAppIsTrial();
                    var modules = [];
                    for (var k in dic) {
                        if (dic.hasOwnProperty(k)) {
                            var mod = dic[k];
                            mod.AutoInstall = isTrial ? mod.AutoInstall_Trial : mod.AutoInstall_Licensed;
                            mod.LicenseIsNotOK = !IsLicenseOK(mod);
                            mod.UpdateVersionNotCompatible = !IsVersionCompatibleWithMainModule(mod);
                            modules.push(mod);
                        }
                    }
                    callback(modules);
                }, failCallback);
            }
        }
    )
}

function CheckModuleAutoUpdates(callback) {
    LoadModules(
        function (modules) {
            if (modules && modules.length) {
                var res = { MandatoryExists: false, OptionalExists: false };
                for (var i = 0; i < modules.length; i++) {
                    var mod = modules[i];

                    if (mod.DL_Finished || mod.DL_Downloading || mod.DL_Error) {
                        res.DirtyExists = true;
                    }

                    if (mod.InstallRequested || mod.UninstallRequested)
                        res.InstallOrUninstallRequestedExists = true;
                    else if (!mod.Hidden && mod.UpdateLink &&
                        !(mod.UpdateVersionNotCompatible || mod.LicenseIsNotOK)) {
                        if (!mod.Installed) {//AutoInstall: No = 0, Optional = 1, Yes = 2
                            if (mod.AutoInstall == 2) {//Yes:Mandatory
                                res.MandatoryExists = true;
                                if (res.OptionalExists)
                                    break;
                            }
                            else if (mod.AutoInstall == 1) {//Optional
                                res.OptionalExists = true;
                                if (res.MandatoryExists)
                                    break;
                            }
                        }
                    }
                }
                if (callback)
                    callback(res, modules);
            }
        });
}



function CanRestartApp(callback) {
    if (window.hasOwnProperty('_CanRestartApp'))
        callback(window._CanRestartApp);
    else {
        $.post(HttpUtility.ResolveUrl('~/ModuleUpdateHandler?op=CanRestartApp'),
            function (ok) {
                window._CanRestartApp = ok;
                callback(ok);
            });
    }
}


function HumanFileSize(bytes) {
    if (!bytes)
        return "0 mb";
    if (bytes >= 1073741824)
        return _HumanFileSize(bytes);

    var mb = bytes / 1048576.0;
    var fx = 10;
    var mb1 = Math.round(mb * fx);
    while (!mb1) {
        fx *= 10;
        mb1 = Math.round(mb * fx);
    }
    mb1 /= fx;
    if (mb1 < 0.1)
        return _HumanFileSize(bytes);
    return mb1.toFixed(1) + ' mb';
}
function _HumanFileSize(bytes) {
    var thresh = 1024;
    //if (Math.abs(bytes) < thresh)
    //    return bytes.toFixed(2) + ' b';
    var units = ['kb', 'mb', 'gb', 'tb', 'pb', 'eb', 'zb', 'yb'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while (Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1) + ' ' + units[u];
}