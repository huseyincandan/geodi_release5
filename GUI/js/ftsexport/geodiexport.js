﻿//select and add

var db = openDatabase('GeodiDb2', '1.0', 'GeodiDb2', 2 * 1024 * 1024);

db.transaction(function (tx) {
    tx.executeSql('CREATE TABLE IF NOT EXISTS FTSNotes2 (dt, fl,sel,note,ws,recognizer,foundkw,status)');
});

function UrlDecode(val) {
    return window.decodeURIComponent ? window.decodeURIComponent(val) : val.replace(/%26/g, '&');
}

function exportNotes() {
    db.readTransaction(function (tx) {
        tx.executeSql('SELECT dt, fl,sel,note,ws,recognizer,foundkw,status FROM FTSNotes2', [], function (tx, results) {
            var lines = [];
            lines.push("<style>th, td { border: 1px solid #ccc; }</style>")
            lines.push("<style>th {background-color:#ddd; }</style>")
            lines.push("<table border=0 cellpadding=5 style='min-width:100%;'><tr><th>Date</th><th>File</th><th>Sel</th><th>Note</th><th>WS</th><th>Recognizer</th><th>KW</th><th>Status</th></tr>");
            for (var i = 0; i < results.rows.length; i++) {
                var row = results.rows.item(i);

                lines.push("<tr>");
                lines.push("<td>"); lines.push(row["dt"]); lines.push("</td>");
                lines.push("<td>"); lines.push(row["fl"]); lines.push("</td>");
                lines.push("<td>"); lines.push(row["sel"]); lines.push("</td>");
                lines.push("<td>"); lines.push(row["note"]); lines.push("</td>");
                lines.push("<td>"); lines.push(row["ws"]); lines.push("</td>");
				lines.push("<td>"); lines.push(row["recognizer"]); lines.push("</td>");
				lines.push("<td>"); lines.push(row["foundkw"]); lines.push("</td>");
				lines.push("<td>"); lines.push(row["status"]); lines.push("</td>");

                lines.push("</tr>");


            }

            lines.push("</table>");
            document.write(lines.join(''));

        });
    });
}

function deleteNotes() {
    if(window.confirm("Diğer dosyalar için alınmış notlarda silinecek. Tüm notları silmek istediğinize emin misiniz?"))
        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM FTSNotes2 ');
        });
}
function getSelectedText() {
    var sel, text = "";
    if (window.getSelection) {
        text = "" + window.getSelection();
    } else if ((sel = document.selection) && sel.type == "Text") {
        text = sel.createRange().text;
    }
    return text;
}
function addNote() {
    var t = getSelectedText();
    if (t && t.length && t.length > 0) {
        var t2 = window.prompt("'"+t + "' seçili alan için bilgi");
        if (t2 && t2.length && t2.length > 0) {

            db.transaction(function (tx) {

                tx.executeSql('INSERT INTO FTSNotes2 (dt, fl,sel,note,ws,recognizer,foundkw,status) VALUES (?, ?, ?, ?,?,?,?,?)', [new Date() + "", window.docName, t, t2,window.wsName,"","",""]);
            });





        }
    }
}
window.PosNexts={};
window.PosNextsLast={};
function SelRec(id) {
	
	 var all = $(".r"+id);
            var pos = window.PosNexts[id]== undefined?-1:window.PosNexts[id];
			if(all.length>0) {
pos++;
if(pos>=all.length) pos=0;
window.PosNexts[id]=pos;

            var el = $(all[pos]);
			var off=el.offset();
            if (off) {
				var st=$("html, body").scrollTop();
				var ste=st+$(window).height();
				
				if(off.top<st || off.top+20>ste)
                $("html, body").animate({
                    scrollTop: ((off.top ) )
                }, 500);
		if(window.PosNextsLast[id]) window.PosNextsLast[id].css("text-decoration","none");
		el.css("text-decoration","underline");
		window.PosNextsLast[id]=el;
            }
	}
		//$(".r"+id).fadeIn(500).fadeOut(500).fadeIn(750).fadeOut(750).fadeIn(1000);//yavaş.
}

$(document).ready(function () {
    $(document.body).append("<div class=rMenu><button class=rbutton onclick='addNote()'>Seçim için Not ekle</button><button class=rbutton onclick='exportNotes()'>Notları göster</button><button class='rbutton danger' onclick='deleteNotes()'>Tüm Notları Sil</button><div id=reclist></div></div>");
	
	var reclist=$("#reclist");
	reclist.append("<center><b><input type=checkbox checked id=chkHide> Tanıyıcılar</b></center>");
	$("#chkHide").change(function() {
		if($("#chkHide").prop('checked')	)
			$(".recognizer").fadeIn();
		else
			$(".recognizer").fadeOut();
	})
	
	$(".multikw").each(function() {
			var cnt=$(this).attr("exCount");
			$(this).append($("<span class=cnt>"+cnt+"</span>"));
		})
	if(window.Recs)
		$.each(window.Recs,function(i,j) {
			reclist.append("<a href=\"javascript:SelRec('"+i+"')\" class='r"+i+"'>"+j+" (<b>"+ $(".r"+i).length+")</b></a>");
		$(".r"+i).each(function() {
			var title=$(this).attr("title");
			if(title && title.indexOf("<br>")!=-1) $(this).attr("title",title.replace(/<br>/g,"\r\n"));
		})
			 $(".r"+i).click(function() {
				 if(!$(this).hasClass("recognizer")) return;
				 var title=$(this).attr("title");
				 if(title) {
					 title=title.replace("<br>","\r\n");
					 var f1=title.indexOf("\r");
					 if(f1)
						 title=title.substr(f1+2,title.length-(f1+2));
					 title=title.trim()
				 }
				 var t=$(this).text();
				 var isObj=$(this).hasClass("obj");
				 
					var t2 = window.prompt("'"+j + "' tanıyıcısı tarafından bulunan '"+t+"' için bilgi");
					if (t2 && t2.length && t2.length > 0) {

						db.transaction(function (tx) {

							tx.executeSql('INSERT INTO FTSNotes2 (dt, fl,sel,note,ws,recognizer,foundkw,status) VALUES (?, ?, ?, ?,?,?,?,?)', [new Date() + "", window.docName, t, t2,window.wsName,window.Recs[i],title,isObj?"OBJECT":""]);
						});


					}
				
				 
				 
			 });
		})
});
