﻿function ApplicationSettingsService(id) {
    this.ID = id;
    //GeodiClientState gibi timer olabilir.
    this.SetSetting = function (name,val,callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=SetSetting'), { 'setting': JSON.stringify({ 'Key': name, Value: val }) }, callBackFunction, 'json');
    }
    this.SetSettings = function (KeyValueArray, callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=SetSettings'), { 'settings': JSON.stringify(KeyValueArray) }, callBackFunction, 'json');
    }
    this.GetSetting = function (name,callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=GetSetting'), { 'key': name }, callBackFunction, 'json');
    }
    this.GetSettings = function (namesArray, callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=GetSettings'), { 'keys': JSON.stringify(namesArray) }, callBackFunction, 'json');
    }
    this.SetDefaultMailMessageManager = function (mailMessageManager, callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=SetDefaultMailMessageManager'), { 'mailMessageManager': JSON.stringify(mailMessageManager) }, callBackFunction, 'json');
    }
    this.GetDefaultMailMessageManager = function (callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=GetDefaultMailMessageManager'), {}, callBackFunction, 'json');
    }
    this.IsGuestLoginAllowed = function (callBackFunction) {
        return $.post(HttpUtility.ResolveUrl('~/ApplicationSettingsService?op=IsGuestLoginAllowed'), {}, callBackFunction, 'json');
    }
}

window.ApplicationSettingsService = new ApplicationSettingsService('');

function MailMessageManager()
{
    this.SmtpHost = '';
    this.SmtpPort = 0;
    this.SmtpUserName = '';
    this.SmtpPassword = '';
    this.SmtpEnableSsl = false;
}