﻿function GetAndShowLabelList(sender, e, advanceSupportPanel) {
    checkLabelPermissions(sender);

    if (advanceSupportPanel) {
        $(".ItemSelectedForAdvRight").removeClass("ItemSelectedForAdvRight");
        $(advanceSupportPanel).css("opacity", 0);
    }

    if (advanceSupportPanel && $(advanceSupportPanel).is(":visible")) {
        $(".openLink", $(sender).parents(".DocALL")).click();
        return;
    }
    e.stopPropagation();
    var contentID = $(sender).attr('data-item');
    window.activeLabelsModal = $(sender).parents(".DocALL");
    var refQuery = window.activeLabelsModal ? window.activeLabelsModal.data("query_data") : null;
    if (!window._hideAddLabel)
        window.CreateEmptyLabel = function () {
            var geodiLabel = new GeodiLabelReference();
            geodiLabel.ReferenceId = contentID;
            geodiLabel.LabelId = -1;
            geodiLabel.util = refQuery;
            return geodiLabel;
        }
    var buttons = [];
    if (!window._hideAddLabel)
        buttons.push({
            text: "[$default:add/js]", className: 'btn btn-primary', closeOnClick: false, onClickCallBack: function () {
                var geodiLabel = window.CreateEmptyLabel();
                startAddLabel(null, geodiLabel);
            }
        });
    buttons.push({
        text: "[$default:close/js]", className: 'btn btn-primary', closeOnClick: true, onClickCallBack: function () {
            $(sender).data('label', window.__CurrentLabels.length);
            $(sender).children('i').removeClass('fav-color').removeClass('fa-sticky-note').addClass('fa-sticky-note-o');
            Favorites.setFavorite($(sender).parent());
        }
    });
    var hw = Math.min($(window).height(), $(window).width());
    hw *= $(window).width() <= 450 ? .9 : .6;
    messageBoxEx('default', '[$docViewer:label/js]', '<div id="divModalDialogContent" style="min-height:200px;max-height:' + hw + 'px;overflow:auto"></div>', buttons, function () {
        //$('#labelDialog').remove();
    }, function () {
        var ws = refQuery && refQuery.wsName ? refQuery.wsName : StateForm.GetState('wsName');
        LabelManagerComm.GetLabelListByRef(contentID, '', 2, ws, function (data, textStatus) {
            window.__CurrentLabels = data;
            UpdateLabels($('#divModalDialogContent'));
        }, refQuery);
    }, 'labelDialog');
}
function checkLabelPermissions(sender) {
    var enmID = $(sender).parents(".DocArea").data("title_data").enm;
    window._hideAddLabel = false;
    window._hideEditLabel = false;
    window._currentEnumIdLabel = enmID;
    var ep = window.CurrentQueryContainer.CurrentWSInfo.EnumaratorPermissions;
    if (ep && ep.hasOwnProperty && ep.hasOwnProperty(enmID)) {
        var per = ep[enmID];
        window._hideAddLabel = (per & 1) == 0;
        window._hideEditLabel = (per & 2) == 0;
    }
}

function inUpdateLabelsIco(lbl, target) {
    if (target && target.attr("contentid") == lbl.ReferenceId) {
        $(".favLabel", target).data('label', __CurrentLabels.length);
        if (!window._hideAddLabel) {
            if (__CurrentLabels.length)
                $(".forAddLabelBtn").removeClass("forceHide");
            else
                $(".forAddLabelBtn").addClass("forceHide");
        }
        Favorites.setFavorite(target, true); //???
    }
}

function FireLabelAdd(lbl) {
    window.__CurrentLabels.unshift(lbl);
    UpdateLabels($('#divModalDialogContent'));
    UpdateLabels($('#inrightLabels'));
    inUpdateLabelsIco(lbl, window.activeLabelsModal);
    inUpdateLabelsIco(lbl, window.activeDocAllList);


}

function FireLabelUpdate(lbl) {
    for (var i = 0; i < window.__CurrentLabels.length; i++)
        if (__CurrentLabels[i].LabelId == lbl.LabelId) {
            __CurrentLabels[i] = lbl;
            UpdateLabels($('#divModalDialogContent'));
            UpdateLabels($('#inrightLabels'));
            return;
        }
    UpdateLabels($('#divModalDialogContent'));
    UpdateLabels($('#inrightLabels'));
}

function FireLabelRemove(lbl) {
    for (var i = 0; i < window.__CurrentLabels.length; i++)
        if (__CurrentLabels[i].LabelId == lbl.LabelId) {
            __CurrentLabels.splice(i, 1);
            UpdateLabels($('#divModalDialogContent'));
            UpdateLabels($('#inrightLabels'));
            break;
        }

    inUpdateLabelsIco(lbl, window.activeLabelsModal);
    inUpdateLabelsIco(lbl, window.activeDocAllList);
}
