﻿
window.DeceClientState = {
    __wTimer: {},
    IsNotDefinedOr: function (name, OrValue) {
        return (window.__userPreferences[name] == undefined || window.__userPreferences[name] == OrValue);
    },
    IsDefined: function (name, rememberLocal) {
        if (!rememberLocal)
            return window.__userPreferences[name] != undefined;
        if (window.localStorage)
            return window.localStorage[name] != undefined;
        return false;
    },
    //Remember functions
    GetState: function (name, defaultValue, rememberLocal) {
        var rtn = !rememberLocal ? window.__userPreferences[name] : (window.localStorage ? localStorage[name] : null);
        if (typeof (rtn) == "undefined") rtn = defaultValue;
        //return defaultValue;
        return rtn;
    },
    SetState: function (name, val, rememberLocal, sendNow) {
        var checkArray = !rememberLocal ? window.__userPreferences : (window.localStorage ? window.localStorage : null);
        if (checkArray && checkArray[name] != val) {
            if (!rememberLocal) {
                if (!window.__userPreferencesOriginal) window.__userPreferencesOriginal = [];
                if (!window.__userPreferencesOriginal[name])
                    window.__userPreferencesOriginal[name] = checkArray[name];
                checkArray[name] = val;
                if (this.__wTimer[name]) window.clearTimeout(this.__wTimer[name]);
                this.__wTimer[name] = window.setTimeout(function () {
                    if (window.__userPreferencesOriginal[name] != val)
                        $.post(HttpUtility.ResolveUrl('~/GUIJsonService?op=SetClientState'), { 'clientState': JSON.stringify({ 'Key': name, Value: val }) }, null, 'json');
                    delete window.__userPreferencesOriginal[name];
                }, sendNow ? 0 : 600);
            }
            else {
                try {
                    if (window.localStorage) window.localStorage[name] = val;
                }
                catch (e) {
                    //limit aşılabilir. istemek gerekmiyor.
                }
            }
        }
    },
    RemoveState: function (name) {
        $.post(HttpUtility.ResolveUrl('~/GUIJsonService?op=RemoveClientState'), { 'key': name }, null, 'json');
    },
    GetLanguage: function () { return window.__userLanguage; },
    GetOrjLanguage: function () { return window.__userOrjLanguage; },
    GetTheme: function () { return window.__userTheme; },
    SetLanguage: function (name, callBackFunction) {
        $.post(HttpUtility.ResolveUrl('~/GUIJsonService?op=SetUserLanguage'), { 'name': name }, callBackFunction, 'json');
    },
    SetTheme: function (name, callBackFunction) {
        $.post(HttpUtility.ResolveUrl('~/GUIJsonService?op=setTheme'), { 'themeName': name }, callBackFunction, 'json');
    },
    AddLanguageNameSpace: function (ns) {
        if (window.console)
            console.log("AddLanguageNameSpace-obsolete-unsupported call");
    }
}

if(window.$)
    $.t = function (key) {
        if (window.console)
            console.log("$.t()-obsolete-unsupported call");
        return key;
    }

window.runAfterLanguagesLoaded = function (func) {
    if (window.console)
        console.log("runAfterLanguagesLoaded-obsolete-unsupported call");
    $(window).ready(func);
}


//formun açık panellerini hatırlar ve onları yükler
function rememberAccordionState(id) {
    __rememberAccordionState(id, "", false);
}
function rememberAccordionStateLocal(id, keyid) {
    __rememberAccordionState(id, keyid, true);
}
function rememberAccordionStateSetSelectedStartup(id, keyid, rememberLocal, selectedId, IsClosed) {
    if (!keyid) keyid = "_DF";
    var pnlObj = $(id);
    keyid = id + "_state" + keyid;
    var lastActive = DeceClientState.GetState(keyid, null, rememberLocal);
    if (!lastActive)
        DeceClientState.SetState(keyid, IsClosed ? "cls_" : selectedId, rememberLocal);
}
function __rememberAccordionState(id, keyid, rememberLocal) {

    if (!keyid) keyid = "_DF";
    var pnlObj = $(id);
    keyid = id + "_state" + keyid;

    if (!pnlObj.attr("___rmbr")) {
        pnlObj.on('shown.bs.collapse', function () {
            var active = $(id + " .in").attr('id');
            DeceClientState.SetState(keyid, active, rememberLocal);
        });
        pnlObj.on('hidden.bs.collapse', function () {
            var lastActive = DeceClientState.GetState(keyid, null, rememberLocal);
            if (lastActive) {
                cssActive = $(id + " #" + lastActive).attr('class');
                if (cssActive && cssActive.indexOf(" in") == -1)
                    DeceClientState.SetState(keyid, "cls_" + lastActive, rememberLocal);//kapalı durumunu hatırla
            }
        });

        pnlObj.attr("___rmbr", "1");
    }
    var last = DeceClientState.GetState(keyid, null, rememberLocal);
    if (last != null) {
        $(id + " .panel-collapse").removeClass('in');
        $("#" + last).addClass("in");
    }
}