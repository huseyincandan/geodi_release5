﻿var defaultDataPlacement = 'bottom';

$.fn.embedHelp = function (force) {
    this.find('[data-help]').each(function () {
        var el = $(this);
        if (window.BrowserType.IsMobile() || window.BrowserType.IsGoogleEarth()) {
            el.hide();
            return;
        }
        var id = el.data('help'), helpns = el.data('ns'), target_place = el.data('placement'); //tooltip && popover
        var style = el.attr('style') ? el.attr('style') : '';
        var css = el.attr('class') ? el.attr('class') : '';
        var domId = el.attr('id') ? el.attr('id') : '';

        var tip = el.data('directtip');

        var tipVal = $(".directValue",el).html();
        if (tipVal) {
            if (tipVal == "-" || tipVal=="")
            {
                el.hide();
                return;
            }
            style = el.attr('style') ? el.attr('style') : '';
            tip = function (xType) {
                if (xType)
                    return $("." + xType, el).html();
                return tipVal;
            };
        }

        

        if (tip) //doğrudan tip geldi
            ___managePlacement(helpns, id, el, style, css, target_place, domId, tip);
        else if (window.console)
            console.log("GeodiHelpManager-obsolete-unsupported call");
    });
};

function ___managePlacement(helpns, id, el, style, css, target_place, domId, tip) {
    var getter = tip;
    tip = getter();
    var current = null;

    if (el.is('a') || el.is('button') || el.is('li') || el.is('.main-help-button'))
        current = el;
    while (tip.indexOf(" || ") != -1)
        tip = tip.replace(" || ", "||");
    var keys = tip.split("||");

    

    if (tip.indexOf("||") < 0) { //tek değer var bu durumda içinde http varsa url yoksa popover(basit)
        if (tip.toLowerCase().indexOf("http") == 0) {
            var rpl = ___urlButtonCreator(tip, el, style, css, current, domId);
            return ___elementReplacer(el, rpl, true);
        }
        else {
            if (!target_place)
                target_place = defaultDataPlacement;
            var rpl = ___popOverCreator(target_place, style, css, current, domId);
            var elm = ___elementReplacer(el, rpl, true);
            ___assignPopover(rpl, ___popOverContentTemplateChooser("simple", tip, getter));
            return elm;
        }
    }
    if (keys.length == 2) { // 2 değer var bu durumda placement yok default bottom veya help elemanında belirtilen konum
        if (keys[0].toLowerCase().indexOf('popover') > -1) {
            if (!target_place)
                target_place = defaultDataPlacement;
            var rpl = ___popOverCreator(target_place, style, css, current, domId);
            var elm = ___elementReplacer(el, rpl, true);
            ___assignPopover(rpl, ___popOverContentTemplateChooser(___templateSelector(keys[0]), keys[1], getter));
            return elm;
        }
        else if (keys[0].toLowerCase().indexOf('tooltip') > -1) {
            if (!target_place)
                target_place = defaultDataPlacement;
            var elm = ___elementReplacer(el, ___tooltipCreator(keys[1], target_place, style, css, current, domId), true);
            $('.helpButtonTool').tooltip();
            return elm;
        }
        else if (keys[0].toLowerCase().indexOf('url') > -1) {
            var rpl = ___urlButtonCreator(keys[1], el, style, css, current, domId);
            return ___elementReplacer(el, rpl, true);
        }
    }
    if (keys.length == 3) { // her üç değer de var doğru yerleşim yap
        if (keys[0].toLowerCase().indexOf('popover') > -1) {
            if (!target_place)
                target_place = keys[1].replace(/ /g,"");
            var rpl = ___popOverCreator(target_place, style, css, current, domId);
            var elm = ___elementReplacer(el, rpl, true);
            ___assignPopover(rpl, ___popOverContentTemplateChooser(___templateSelector(keys[0]), keys[2], getter));
            return elm;
        }
        else if (keys[0].toLowerCase().indexOf('tooltip') > -1) {
            if (!target_place)
                target_place = keys[1].replace(/ /g, "");;
            var elm = ___elementReplacer(el, ___tooltipCreator(keys[2], target_place, style, css, current, domId), true);
            $('.helpButtonTool').tooltip();
            return elm;
        }
    }
}

function ___assignPopover(rpl, template) {
    rpl.data("all", template);
    $('[data-rid="' + rpl.data('rid') + '"]').popover({ template: template.container, content: template.content, animation: template.animation });
    $('.helpButton').click(function (e) {
        $('.helpButton').not($(this)).popover('hide');
        $('.forPopover').not($(this)).popover('hide');
        publishHideMessage();
        e.preventDefault();
        e.stopPropagation();
    });
}

function ___templateSelector(key) {
    var tempIndex = key.indexOf('_');
    if (tempIndex > 0)
        return key.substring(tempIndex + 1, key.length);
    return "simple";
}

function ___urlButtonCreator(url, el, style, css, current, domId) {
    var rpl;
    if (!current) {
        rpl = $('<button type="button" id="' + domId + '" style="' + style + '" class="btn btn-md btn-transparent ' + css + '" onclick="_directForward(\'' + url + '\', event);"><span class="glyphicon glyphicon-question-sign" ></span></button>');
    }
    else {
        current.attr('id', domId);
        current.attr('onclick', '_directForward(\'' + url + '\', event);');
        current.attr('style', style);
        current.attr('class', css);
        current.removeAttr('data-ns');
        current.removeAttr('data-help');
        rpl = current;
    }
    return rpl;
}

var ___template = function () {
    this.container = null; //popup yapısı
    this.content = null; //popup içinin yapısı
    this.animation = null; //animasyon olacak mı
}

function ___popOverContentTemplateChooser(type, content, getter) {
    switch (type.replace(/ /g,"")) {
        case "simple":
            content = ___processInternalLinks(content);
            var simpleTemplate = new ___template();
            simpleTemplate.content = ___cleanBreaks(content);
            simpleTemplate.animation = true;

            var minWidth = 290;
            var maxWidth = $(window).width() - 50;
            if (maxWidth != 0 && minWidth > maxWidth)
                minWidth = maxWidth;
            simpleTemplate.container = '<div class="popover" style="min-width: ' + minWidth + 'px !important;"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title" style="display:none;"></h3><div class="popover-content"><p></p></div></div></div>';
            return simpleTemplate;
        case "noeffect":
            content = ___processInternalLinks(content);
            var noeffectTemplate = new ___template();
            noeffectTemplate.content = ___cleanBreaks(content);
            noeffectTemplate.animation = false;

            var minWidth = 290;
            var maxWidth = $(window).width() - 50;
            if (maxWidth != 0 && minWidth > maxWidth)
                minWidth = maxWidth;
            noeffectTemplate.container = '<div class="popover" style="min-width: ' + minWidth + 'px !important; -webkit-box-shadow: none; box-shadow: none; border-color:gray"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title" style="display:none;"></h3><div class="popover-content"><p></p></div></div></div>';
            return noeffectTemplate;
        case "adv":
            content = ___processInternalLinks(content);
            var header = ___getHeader(content);
            content = ___cleanContent(header, content);
            var linkSection = ___processLinks(getter("links"));

            var template = "<div class='container'><div class='row'><div class='pMainContent col-xs-7' style='border-right: 1px solid #ccc;'><h3 class='pHeader' style='margin-top:0px;'></h3></div><div class='col-xs-5' style='white-space: pre-wrap !important;'><pre class='pLinks pre-scrollable' style='white-space: pre-wrap !important; word-break: normal !important; margin: 0px; margin-left:-8px; margin-right:-8px;overflow:auto;'></pre></div></div></div>"

            var tDom = $(template);
            if (linkSection.has('a').length > 0)
                tDom.find('.pLinks').append(linkSection);
            else {
                tDom.find('.pMainContent').removeClass('col-xs-8').addClass('col-xs-12');
                tDom.find('.pLinks').remove();
            }
            tDom.find('.pHeader').html(header);
            tDom.find('.pMainContent').append(content);

            var advTemplate = new ___template();
            advTemplate.content = tDom.html();

            var minWidth = 620;
            var maxWidth = $(window).width();
            if (maxWidth != 0 && minWidth > maxWidth)
                minWidth = maxWidth;
            advTemplate.animation = true;
            advTemplate.container = '<div class="popover" style="min-width: ' + minWidth + 'px !important;"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div></div>';

            return advTemplate;
        case "adv_noeffect":
            content = ___processInternalLinks(content);
            var header = ___getHeader(content);
            content = ___cleanContent(header, content);
            var linkSection = ___processLinks(getter("links"));

            var template = "<div class='container'><div class='row'><div class='pMainContent col-xs-7' style='border-right: 1px solid #ccc;'><h3 class='pHeader' style='margin-top:0px;'></h3></div><div class='col-xs-5' style='white-space: pre-wrap !important;'><pre class='pLinks pre-scrollable' style='white-space: pre-wrap !important; word-break: normal !important; margin: 0px; margin-left:-8px; margin-right:-8px; overflow:auto;'></pre></div></div></div>"

            var tDom = $(template);
            if (linkSection.has('a').length > 0)
                tDom.find('.pLinks').append(linkSection);
            else {
                tDom.find('.pMainContent').removeClass('col-xs-9').addClass('col-xs-12');
                tDom.find('.pLinks').remove();
            }
            tDom.find('.pHeader').html(header);
            tDom.find('.pMainContent').append(content);

            var advTemplate = new ___template();
            advTemplate.content = tDom.html();

            var minWidth = 620;
            var maxWidth = $(window).width();
            if (maxWidth != 0 && minWidth > maxWidth)
                minWidth = maxWidth;
            advTemplate.animation = false;
            advTemplate.container = '<div class="popover" style="min-width: ' + minWidth + 'px !important; -webkit-box-shadow: none; box-shadow: none; border-color:gray"><div class="arrow"></div><div class="popover-inner"><div class="popover-content"><p></p></div></div></div>';

            return advTemplate;
    }
}

function ___popOverCreator(target_place, style, css, current, domId) {
    var rid = Math.floor((Math.random() * 100000000000) + 1);
    var rpl;
    if (!current) {
        rpl = $('<a data-container="body" id="' + domId + '" data-html="true" tabindex="1" data-rid="' + rid + '" data-toggle="popover" data-placement="' + target_place + '" title=""  type="button" style="' + style + '" class="helpButton btn btn-md btn-transparent ' + css + '"><span class="glyphicon glyphicon-question-sign" ></span></a>');
    }
    else {
        current.attr('id', domId);
        current.attr('data-html', 'true');
        current.attr('tabindex', '1');
        current.attr('data-toggle', 'popover');
        current.attr('data-container', 'body');
        current.attr('data-placement', target_place);
        current.attr('data-rid', rid);
        current.attr('style', style);
        current.attr('class', css + ' helpButton');
        current.removeAttr('data-ns');
        current.removeAttr('data-help');
        rpl = current;
    }
    return rpl;
}

function ___cleanBreaks(content) {
    var re = new RegExp('\n', 'g');
    return content.replace(re, '<br/>');
}

function ___cleanContent(header, content) {
    var index = content.indexOf(header);
    var res = content.substring(index + header.length + 1, content.length);
    return ___cleanBreaks(res);
}

function ___getHeader(content) {
    return content.split("\n")[0];
}

String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

function ___processInternalLinks(content) {
    var re = new RegExp('href=[\'"]?([^\'" >]+)', 'g');
    var links = content.match(re);
    if (links)
        for (var i = 0; i < links.length; i++) {
            var index = content.indexOf(links[i]);
            content = content.replace(links[i], 'href="javascript:void(0)');
            var link = links[i].replace('href="', '');
            content = content.splice(index, 0, 'onclick="_directForward(\'' + link + '\',event);" ');
        }
    return content;
}

function ___processLinks(links) {
    var domLink = $('<ul></ul>');
    var linkArray = links?links.split("\n"):[];
    for (var i = 0; i < linkArray.length; i++) {
        if (linkArray[i].indexOf('href') > -1) {
            var domI = $(linkArray[i]);
            var url = domI.attr('href');
            domI.attr('href', 'javascript:_directForward(\'' + url + '\', event);');
            // domI.attr('href', 'javascript:void(0);');
            // domI.attr('onclick', '_directForward(\'' + url + '\', event);');
            var coverLi = $("<li style='margin-left:-40px'></li>").append(domI);
            domLink.append(coverLi);
        }
    }
    return domLink;
}

function ___tooltipCreator(title, target_place, style, css, current, domId) {
    var rpl;
    if (!current) {
        rpl = $('<button data-container="body" id="' + domId + '" data-html="true" data-toggle="tooltip" data-placement="' + target_place + '" data-original-title="' + title + '" title="' + title + '" type="button" style="' + style + '" class="helpButtonTool btn btn-md btn-transparent ' + css + '"><span class="glyphicon glyphicon-question-sign" ></span></button>')
    }
    else {
        current.attr('id', domId);
        current.attr('data-html', 'true');
        current.attr('data-toggle', 'tooltip');
        current.attr('data-container', 'body');
        current.attr('data-placement', target_place);
        current.attr('data-original-title', title);
        current.attr('title', title);
        current.attr('style', style);
        current.attr('class', css);
        current.removeAttr('data-ns');
        current.removeAttr('data-help');
        rpl = current;
    }
    return rpl;
}

function ___elementReplacer(el, rpl, disableFade) {
    if (!disableFade) {
        rpl.hide();
        el.replaceWith(rpl);
        rpl.fadeIn();
        return rpl;
    }
    else {
        el.replaceWith(rpl);
        return rpl;
    }
}

function _getHelpUrl(id, event, helpns, url) {
    if (event) {
        event.preventDefault();
        event.stopPropagation();
    }
    var ns = helpns ? helpns : "help";
    var lng = DeceClientState.GetLanguage();
    var url = url ? url : HttpUtility.ResolveUrl('~/LanguageManager?op=goHelp&ns=' + HttpUtility.UrlEncode(ns) + '&lng=' + lng + '&id=' + HttpUtility.UrlEncode(id) + '&r=' + HttpUtility.GetVersionCode());
    return url;
}

function _directForward(url, event) {
    if (event) {
        event.preventDefault();
        event.stopPropagation();
    }
    _PSCC.Command({ Url: url, Name: 'Help', Command: 'OpenWindow', Width: 900, Height: 800, Dialog: true, Resizable: false, UseSystemBrowser: true });
}

function _forwardHelp(id, event, helpns, url) {
    url = _getHelpUrl(id, event, helpns, url);
    _PSCC.Command({ Url: url, Name: 'Help', Command: 'OpenWindow', Width: 900, Height: 800, Dialog: true, Resizable: false, UseSystemBrowser: true });
}


$(window).ready(function () {
    $('body').embedHelp();
});
$(document).on('click', function (e) {
    $('.helpButton').each(function () {
        $(this).popover('hide');
    });
    publishHideMessage(e);
});
$(document).on('touchend', function (e) {
    $('.helpButton').each(function () {
        $(this).popover('hide');
    });
    publishHideMessage(e);
});

function publishHideMessage(e) {
    if (window.disableCloseMessage)
        return;
    if (window.parent != window)
        window.parent.postMessage('hide_help', '*');
    else if (window.CloseNavMenu)
        window.CloseNavMenu(e);
    
    if (e && ($(e.target).hasClass("AutoFadeOuter") || $(e.target).parents(".AutoFadeOuter").length == 0)) $('.AutoFadeOut').fadeOut();
    if (e && ($(e.target).hasClass("AutoHider") || $(e.target).parents(".AutoHider").length == 0)) $('.AutoHide').hide();
    var frames = window.parent.frames;
    for (var i = 0; i < frames.length; i++) {
        if (frames[i] != window)
            frames[i].postMessage('hide_help', '*');
    }
}

$(function () {
    ListenWindowPostMessage(function (data, e) {
        if (!window.$) return;
        if (data == 'hide_help') {
            if (window.CloseNavMenu)
                window.CloseNavMenu();
            $('.AutoFadeOut').fadeOut();
            $('.AutoHide').hide();   
            $('.helpButton').popover('hide');
        }
    })
});