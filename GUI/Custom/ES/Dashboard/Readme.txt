﻿Buraya eklenecek jSettings dosyaları statik dashboard tanımlarıdır. Html rapor tipinde olmalıdır.
ConditionMacro ile  yetki grupları, kullanıcıya göre görüntülenmesi sağlanabilir. Macro içeriğinde kullanılabilen Data GeodiWorkspace nesnesidir.
ID değerleri benzersiz olmalıdır. 
ID m_ ile başlıyorsa hızlı erişim menülerine eklenir. başlamıyorsa sağ üst dropdown menüden erişilebilir.