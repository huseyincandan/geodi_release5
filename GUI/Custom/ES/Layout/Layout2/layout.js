﻿window.ClosedFacet=false;
function __Layout1_ResetBody(reset) {
	var lbody=$(document.body);	
	if(reset) {
		lbody.addClass("non-scrollable");
		lbody.removeClass("layout1-body")
		lbody.removeClass("ESScroll-down");
		$("#divMain").addClass("scrollable-content");
		$(".dc-search-group").css("top","unset");
		if(!window.IsLeftTopFacet) 
			$(".sidebar-wrapper").css("top","unset");
		$("#inRightPanelForDoc").css("top","unset");
		$(window).resize();
	}
	else {
		lbody.removeClass("non-scrollable");
		lbody.addClass("layout1-body")
		$("#divMain").removeClass("scrollable-content");
		$(window).resize();
	}
}
function __Layout1_ResetBodyTop() {
	var lbody=$(document.body);	
	var sTop=lbody.scrollTop();
	if(sTop>200) sTop=200;
	if(sTop>=140) 
		lbody.addClass("ESScroll-down");
	else
		lbody.removeClass("ESScroll-down");
	$(".dc-search-group").css("top",(160-sTop)+"px");
	if(!window.IsLeftTopFacet) {
		$(".sidebar-wrapper").css("top",(267-sTop)+"px");
		$(".sidebar-wrapper").css("height",($(window).height()-$(".navbarArea").height()-10)+"px");
	}
	$("#inRightPanelForDoc").css("top",(267-sTop)+"px");
}
function __Layout1_ResetBodyChangeView() {
		if(!window.CurrentQueryContainer) {
			window.setTimeout(__Layout1_ResetBodyChangeView,100);
			return;
		}
		var isDLV=CurrentQueryContainer.ActiveViews[0].Name=="DLV";
		__Layout1_ResetBody(!isDLV)
		if(isDLV)
			__Layout1_ResetBodyTop();
		$(window).resize();
}
window.DLVRightPanelMousewheel=function(e) { 
	 if (e.originalEvent && e.originalEvent.wheelDelta) {
                                    var t = $(e.target).parents(".rightDiv");
                                    if (t.length > 0) {
                                        t = $(t[0]);
                                        if (t[0].scrollHeight > t.height())
                                            return true;
                                    }
									$(document.body).animate({ scrollTop: (document.body.scrollTop+e.originalEvent.deltaY) }, 10)
                                }
								return true;
}
$(function() {
	var lbody=$(document.body);
	if(lbody.hasClass("ESBody")) {
			window._layout1_resizeNav=window.resizeNav;
			resizeNav=function() {
				window._layout1_resizeNav();
				if(!window.IsLeftTopFacet)
					$(".sidebar-wrapper").css({"height":"unset", "bottom":"10px"});
				$("#inRightPanelForDoc").css({"height":"unset", "bottom":"10px"})
				
			}
		lbody.on("GUI_ViewChanged",__Layout1_ResetBodyChangeView)
	}
})
$(window).ready(function() {
	var lbody=$(document.body);
	if(lbody.hasClass("ESBody")) {
		lbody.on('scroll', function () {
					if(CurrentQueryContainer.ActiveViews[0].Name!="DLV")
						return;
                    if ($('#divMainLoadingImage').is(':appeared') && !window.documentsLoading)
                        window.CurrentQueryContainer.OnScroll();
					__Layout1_ResetBodyTop();
                });
		if(!window.IsLeftTopFacet) {
			$(window).resize(__Layout1_ResetBodyTop);
			window.setTimeout(__Layout1_ResetBodyTop,500);
			window.setTimeout(__Layout1_ResetBodyTop,1000);
		}
		__Layout1_ResetBody(false);
	}
}) 