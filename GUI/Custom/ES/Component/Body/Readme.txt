﻿Bu klasöre eklenecek html içerikler tüm EnterpriseSearch arayüzlerinde body başlangıcına eklenir.
Script moduller için kullanılabilir.

Rapor mekanizmasında kullanılan   jsettings html yanına eklenerek ActionTargets içeriğinde proje adı kullanılabilir. 
ConditionMacro tanımı yapılabilir. Macro içerisindeki data nesnesi GeodiWorkspace'dir.