﻿%AppData%\App\GEODI\GUI\Custom\Projects altına proje isminde bir klasör oluşturup LeftBannerList adlı bir klasör oluşturup içerisine 0.jpg adında image atarak bannerı getirebiliyoruz.0.jpg ,1.jpg şeklinde n tane resim atarak her girildiğinde rastgele bir resim gelmesini sağlayabiliyoruz.

Bu klasör içerine logo.png atarak banner'ımıza logo ekleyebiliyoruz.

Bu klasörün altına LeftBannerCustomize adlı bir klasör oluşturularak buraya html dosyası atarak banner'ı özelleştirebiliriz.

Bir banner'ı tüm projelerde getirmek için aynı işlemleri _LeftGlobalBanner adlı klasör açarak tüm projelerde gelmesini sağlayabiliriz. Bu durumda projenin birinde değişiklik yapılmak istendiğinde proje isminde bir klasör oluşturduğumuzda o projede o banner'ı görecektir.