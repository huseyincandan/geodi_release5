﻿Bu klasöre eklenecek html içerikler tüm Viewer arayüzlerinde sağ tarafta açılır kapanır panele eklenecektir.
Html içerik eklenebilir. Collapsable olmasına ve id kullanımına dikkat edilmelidir. DefaultLayout.html sayfasındaki panelKeywords ve panelLabels div'lerinden faydalanılabilir.
Script moduller için kullanılabilir.

Rapor mekanizmasında kullanılan   jsettings html yanına eklenerek ActionTargets içeriğinde proje adı kullanılabilir. 
ConditionMacro tanımı yapılabilir. Macro içerisindeki data nesnesi GeodiWorkspace'dir.