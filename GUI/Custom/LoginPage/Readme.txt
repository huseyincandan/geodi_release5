﻿Eklenecek bir LoginPage.layout.html içeriği ile tüm giriş sayfası tararımı değiştirlebilir.

Bu alana eklenecek sabit isimli html içerikler Login sayfasında html içerik olarak gösterilirler.

Html içerikler responsive olmalıdır. Özel durumlar dışında Bootstrap sınıfları ve kuralları tercih edilmeli.

Top.html : Üst alan
Form_Top.html : Form iç üstü
Form_Center.html : Form ortası
Form_Bottom.html : Form iç altı
Bottom.html : Alt alan