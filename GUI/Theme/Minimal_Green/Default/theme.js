﻿window.IsLeftTopFacet=true;
window.DefaultChartTimelineStyle= {
	fill:true,
	backgroundColor:'rgba(192,66,64, 0.36)',
	borderWidth:1.2,
    borderColor:  'rgba(192,66,64,0.6)',
    labelTextColor: 'rgba(0,0,0,1)',
}
    
window.DefaultChartTimelineOptions = {
    scales: {
        yAxes: [{
            gridLines: {
                color: ['#ddd','#ddd','#ddd','#ddd','#ddd']
            },
        }],
          xAxes: [{
            gridLines: {
                display:false
            },
        }]
    }
}
window.CalendarItemBgColor=[65, 154, 114];
window.CalendarItemTextColor =[255,255,255];
window.CalendarItemBarColor = [65, 154, 114];
window.CalendarItemBarSize = 2;

function GetItemCountFor(tab){
    var area=$(window).width()*$(window).height();
    switch(tab) {
        case "KLV":  var rtn= (area/(390*55)); rtn=Math.floor(rtn); if(rtn<12) rtn=12; return rtn;
    }
    return 0;
}