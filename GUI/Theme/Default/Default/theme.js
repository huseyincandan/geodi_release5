﻿window.IsLeftTopFacet=true;
window.DefaultChartTimelineStyle= {
	fill:true,
	backgroundColor:'rgba(6,108,210, 0.36)',
	borderWidth:1.2,
    borderColor:  'rgba(6,108,210,0.6)',
    labelTextColor: 'rgba(0,0,0,1)',
}    
window.DefaultChartTimelineOptions = {
    scales: {
        yAxes: [{
            gridLines: {
                color: ['#ddd','#ddd','#ddd','#ddd','#ddd']
            },
        }],
          xAxes: [{
            gridLines: {
                display:false
            },
        }]
    }
}
window.CalendarItemBgColor=[37,143,251];
window.CalendarItemTextColor =[255,255,255];
window.CalendarItemBarColor = [236,239,243];
window.CalendarItemBarSize = 2;

function GetItemCountFor(tab){
    var area=$(window).width()*$(window).height();
    switch(tab) {
        case "KLV":  var rtn= (area/(390*55)); rtn=Math.floor(rtn); if(rtn<12) rtn=12; return rtn;
    }
    return 0;
}


$(
function() {

if(!window.BrowserType.IsMobile())
	$(document.body).on("LoadDateLimit",function()
		{
			var fUpdateSliderDom=function() {
				if(window.largeToolTip) {
					var dm1=$("#dateSlider_queryDate");
					if(dm1.attr("_moveOk")) return;
					dm1.attr("_moveOk",1);
					var orgn=$(".noUi-origin",dm1);
					window.largeToolTip.sliderTooltipLarge=$("#NN_R_H_0_NONENONE");
					$(orgn[0]).append(window.largeToolTip.left);
					$(orgn[1]).append(window.largeToolTip.right);
				}
			}
			window.setTimeout(fUpdateSliderDom,500);
		});
	
	
	if(window._RenderEnd)
	{
		window.__RenderEnd_001=window._RenderEnd;
		window._RenderEnd=function(e1) {
			window.__RenderEnd_001(e1);
			_RenderEnd_Theme();
		}
		
	}
}
)
function _RenderEnd_Theme() {
	$("#mycalendar tr").each( function() {
		var tr1=$(this);
		var last=null;
		$(".day",tr1).each(function() {
			var day=$(this);
			var dayStyle=day.attr("style");
			if(dayStyle) {
				var inColor=day.css("box-shadow").split(")")[0]+")";
				day.css("box-shadow","none");
				day.css("background-color",inColor);
				
				if(last)
					last.css("g-in-day","red"); 
				else
					day.addClass("g-first-day");
				last=day;
			}
			else {
				if(last)
					last.addClass("g-last-day"); 
				last=null;
			}
		})
		if(last)
				last.addClass("g-last-day"); 
		
	})
	
}